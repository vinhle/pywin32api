# -*- coding: utf-8 -*-
'''
@Author : LE VINH
@Project: BE-PUM, API Stub generation
@Time   : JAIST (2015-2016)
@Summary: Utilities for system functions
'''

import os
from os import path


# Get a list of files in a folder
def GetFiles(path):
    #path  = "../DBSource/JNA-HTML"; path: relative|directive path 
    files = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]
    return files


# Get a list of sub-folders in a folder
def GetSubFolder(path):
    subfloders = [name for name in os.listdir(path) if os.path.isdir(os.path.join(path, name))]
    return subfloders


# Count the number of python files
def CountFiles(fpath):
    files        = GetFiles(fpath)
    python_files = [f for f in files if f.endswith('.py')]
    folders      = GetSubFolder(fpath)
    print fpath
    for sub in folders:
        python_files += CountFiles(fpath+'\\'+sub)
    return python_files
#python_files = CountFiles(path.split(path.dirname(__file__))[0])
#print len(python_files), python_files


# Count the number of python programming lines
def CountLines(fpath):
    total_line   = 0
    files        = GetFiles(fpath)
    python_files = [f for f in files if f.endswith('.py')]
    for pyfile in python_files:
        total_line += sum(1 for _line in open(fpath+'\\'+pyfile, 'r'))
    
    folders      = GetSubFolder(fpath)
    for sub in folders:
        total_line += CountLines(fpath+'\\'+sub)
    return total_line
#print CountLines(path.split(path.dirname(__file__))[0])