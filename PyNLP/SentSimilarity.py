# -*- coding: utf-8 -*-
'''
@Author : LE VINH
@Project: BE-PUM, API Stub generation
@Time   : JAIST (2015-2016)
@Summary: Utilities for cosin sentence similarity
'''

import re, math

WORD = re.compile(r'\w+')

#---------------------- Task 1: Predict buffer pointer ---------------------------------
# Transform a sentence to its vector (task 1: classifing cell/buffer pointer)
def Text2Vector1(sent):
    #base_sent  = 'the parameter points to a buffer'
    sent_words  = WORD.findall(sent.lower())
    vector      = [0,0,0,0]
    keywords    = [['parameter','argument'],['point','points','pointer'],['to'],['buffer','memory','array']]
    keywords_w  = [1,3,2,3] # keyword weight
    
    # sentence -> vector by keywords
    for keyword in keywords:
        idx = keywords.index(keyword)
        for w in keyword:
            if w in sent_words:
                vector[idx] += keywords_w[idx]    
    return vector
# print Text2Vector1('Pointer to a buffer in which this function retrieves the formatted date string.')

# Compute cosin of two vectors
def GetCosine1(vec1, vec2):
    vec_product = sum([vec1[x] * vec2[x] for x in range(4)])
    sum_sqrt1   = sum([x**2 for x in vec1])
    sum_sqrt2   = sum([x**2 for x in vec2])
    denominator = math.sqrt(sum_sqrt1) * math.sqrt(sum_sqrt2)

    if not denominator: return 0.0
    else:
        return round(float(vec_product)/denominator, 3)


#---------------------- Task 2: Predict buffer length parameter ---------------------------------
# Transform a sentence to its vector (task 2: as a feature in predicting memory length parameter)
def Text2Vector2(sent):
    #base_sent  = 'the parameter describes the size of buffer' 
    sent_words  = WORD.findall(sent.lower())
    vector      = [0,0,0,0,0]
    keywords    = [['parameter','argument'],['describe','describes','express','expresses','present','presents'],
                   ['size','length','amount'],['of'],['buffer','memory','array','string']]
    keywords_w  = [1,1,3,2,3] # keyword weight
    
    # sentence -> vector by keywords
    for keyword in keywords:
        idx = keywords.index(keyword)
        for w in keyword:
            if w in sent_words:
                vector[idx] += keywords_w[idx]    
    return vector
        
# Compute cosin of two vectors
def GetCosine2(vec1, vec2):
    vec_product = sum([vec1[x] * vec2[x] for x in range(5)])
    sum_sqrt1   = sum([x**2 for x in vec1])
    sum_sqrt2   = sum([x**2 for x in vec2])
    denominator = math.sqrt(sum_sqrt1) * math.sqrt(sum_sqrt2)

    if not denominator: return 0.0
    else:
        return round(float(vec_product)/denominator, 3)

def test():
    # base_vect = ['size', 'of', 'buffer'], and size = length, buffer = memory = string
    base_vect = [1,1,3,2,3]
    text1 = 'The size of the buffer'
    text2 = 'The length of a volume name buffer, in TCHARs'
    text3 = 'The size of a volume name buffer, in TCHARs'
    text4 = 'A pointer to a buffer that receives the computer name or the cluster virtual server name. '
    
    #get_cosine(text_to_vector(text1),text_to_vector(text2))
    
    vector1 = Text2Vector2(text1)
    print vector1
    vector2 = Text2Vector2(text2)
    print vector2
    vector3 = Text2Vector2(text3)
    print vector3
    vector4 = Text2Vector2(text4)
    print vector4
    
    cosine = GetCosine2(base_vect, vector2)
    print 'Cosine:', cosine
    cosine = GetCosine2(base_vect, vector3)
    print 'Cosine:', cosine
    cosine = GetCosine2(base_vect, vector4)
    print 'Cosine:', cosine
#test()