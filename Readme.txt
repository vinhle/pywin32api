1.	Trong MSExtractor > WriteJsonList()
	Viết lại file ms_api.json

2.  Trong MSExtractor > APIsGroupByDLL()
	Sinh lại file nhóm các API theo Group DLL (cần cho việc sinh code bên Java)
	
3. Trong MS_JNAType > BuildTypeMapping(), ReviewTypeMapping()
	Xây dựng bảng Type Mapping lần 1

4. Trong Generator > TypeMap > TypeConversion
	ConstructTypeGraph(): contruct type_table.ls
	GetUnsupportType(): contruct unsup_type.ls

5. Trong ReviewTypeConversion > ReviewTypeMap()
	Điều chỉnh map kiểu theo cách bên Java

6. Trong ReviewAPISpec > UpdateEntireAPIs()
	Điều chỉnh Spec API trong json bằng cách add thêm các mối quan hệ buffer pointer từ model Bayes

Tóm lại những file cần sang bên Java
- ms_api_review.json
- type_table_review.json
- api_groupby_dll.ls