package v2.org.analysis.apihandle.winapi.comctl32;

import v2.org.analysis.apihandle.winapi.API;

public abstract class Comctl32API extends API {
	public Comctl32API () {
		this.libraryName = "comctl32.dll";
	}
}