package v2.org.analysis.apihandle.winapi.comctl32;

import com.sun.jna.Native;
import com.sun.jna.platform.win32.WinDef.CHARByReference;
import com.sun.jna.platform.win32.WinDef.HINSTANCE;
import com.sun.jna.platform.win32.WinDef.HWND;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.win32.StdCallLibrary;
import com.sun.jna.win32.W32APIOptions;

import v2.org.analysis.apihandle.structures.TRACKMOUSEEVENT;


public interface Comctl32DLL extends StdCallLibrary {
	Comctl32DLL INSTANCE = (Comctl32DLL) Native.loadLibrary("comctl32", Comctl32DLL.class, W32APIOptions.DEFAULT_OPTIONS);
	
	// API's Interfaces
	void InitCommonControls ( );

	int TaskDialog ( HWND hWndParent, HINSTANCE hInstance, CHARByReference pszWindowTitle, CHARByReference pszMainInstruction, CHARByReference pszContent, int dwCommonButtons, CHARByReference pszIcon, IntByReference pnButton );

	int _TrackMouseEvent ( TRACKMOUSEEVENT lpEventTrack );

}