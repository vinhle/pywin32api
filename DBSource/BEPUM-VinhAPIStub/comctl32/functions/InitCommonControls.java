/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.comctl32.functions
 * File name: InitCommonControls.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.comctl32.functions;

import v2.org.analysis.apihandle.winapi.comctl32.Comctl32API;
import v2.org.analysis.apihandle.winapi.comctl32.Comctl32DLL;

 
public class InitCommonControls extends Comctl32API {
	public InitCommonControls () {
		super();
		NUM_OF_PARMS = 0;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		
		// Step 2: type conversion from C++ to Java

		// Step 3: call API function
		Comctl32DLL.INSTANCE.InitCommonControls ();
		
		// Step 4: update environment (memory & eax register)

	}
}