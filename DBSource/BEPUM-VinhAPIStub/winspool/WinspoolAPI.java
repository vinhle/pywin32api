package v2.org.analysis.apihandle.winapi.winspool;

import v2.org.analysis.apihandle.winapi.API;

public abstract class WinspoolAPI extends API {
	public WinspoolAPI () {
		this.libraryName = "winspool.dll";
	}
}