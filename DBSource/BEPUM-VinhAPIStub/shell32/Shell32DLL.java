package v2.org.analysis.apihandle.winapi.shell32;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.ShellAPI.APPBARDATA;
import com.sun.jna.platform.win32.ShellAPI.SHFILEOPSTRUCT;
import com.sun.jna.platform.win32.WinBase.FILETIME;
import com.sun.jna.platform.win32.WinBase.SECURITY_ATTRIBUTES;
import com.sun.jna.platform.win32.WinDef.BOOL;
import com.sun.jna.platform.win32.WinDef.CHARByReference;
import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.platform.win32.WinDef.HICON;
import com.sun.jna.platform.win32.WinDef.HINSTANCE;
import com.sun.jna.platform.win32.WinDef.HWND;
import com.sun.jna.platform.win32.WinDef.LONG;
import com.sun.jna.platform.win32.WinDef.UINT;
import com.sun.jna.platform.win32.WinDef.ULONG;
import com.sun.jna.platform.win32.WinNT.HANDLE;
import com.sun.jna.platform.win32.WinNT.HANDLEByReference;
import com.sun.jna.platform.win32.WinReg.HKEY;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.ShortByReference;
import com.sun.jna.win32.StdCallLibrary;
import com.sun.jna.win32.W32APIOptions;

import v2.org.analysis.apihandle.structures.OPENASINFO;
import v2.org.analysis.apihandle.structures.SHELLEXECUTEINFO;
import v2.org.analysis.apihandle.structures.SHELLFLAGSTATE;
import v2.org.analysis.apihandle.structures.SHFILEINFO;
import v2.org.analysis.apihandle.structures.SHQUERYRBINFO;
import v2.org.analysis.apihandle.structures.ULARGE_INTEGER;


public interface Shell32DLL extends StdCallLibrary {
	Shell32DLL INSTANCE = (Shell32DLL) Native.loadLibrary("shell32", Shell32DLL.class, W32APIOptions.DEFAULT_OPTIONS);
	
	// API's Interfaces
	int CommandLineToArgvW ( CHARByReference lpCmdLine, IntByReference pNumArgs );

	int DuplicateIcon ( HINSTANCE hInst, HICON hIcon );

	int ExtractAssociatedIcon ( HINSTANCE hInst, String lpIconPath, ShortByReference lpiIcon );

	int ExtractIcon ( HINSTANCE hInst, String lpszExeFileName, UINT nIconIndex );

	int ExtractIconEx ( String lpszFile, int nIconIndex, HANDLEByReference phiconLarge, HANDLEByReference phiconSmall, UINT nIcons );

	void SHAddToRecentDocs ( UINT uFlags, Pointer pv );

	int SHAppBarMessage ( DWORD dwMessage, APPBARDATA pData );

	void SHChangeNotify ( LONG wEventId, UINT uFlags, Pointer dwItem1, Pointer dwItem2 );

	int SHCreateDirectory ( HWND hwnd, CHARByReference pszPath );

	int SHCreateDirectoryEx ( HWND hwnd, String pszPath, SECURITY_ATTRIBUTES psa );

	int ShellAbout ( HWND hWnd, String szApp, String szOtherStuff, HICON hIcon );

	int ShellExecute ( HWND hwnd, String lpOperation, String lpFile, String lpParameters, String lpDirectory, int nShowCmd );

	int ShellExecuteEx ( SHELLEXECUTEINFO pExecInfo );

	int SHEmptyRecycleBin ( HWND hwnd, String pszRootPath, DWORD dwFlags );

	int SHEnumerateUnreadMailAccounts ( HKEY hKeyUser, DWORD dwIndex, String pszMailAddress, int cchMailAddress );

	int SHFileOperation ( SHFILEOPSTRUCT lpFileOp );

	int SHFormatDrive ( HWND hwnd, UINT drive, UINT fmtID, UINT options );

	void SHFreeNameMappings ( HANDLE hNameMappings );

	int SHGetDiskFreeSpace ( String pszVolume, ULARGE_INTEGER pqwFreeCaller, ULARGE_INTEGER pqwTot, ULARGE_INTEGER pqwFree );

	int SHGetDiskFreeSpaceEx ( String pszDirectoryName, ULARGE_INTEGER pulFreeBytesAvailableToCaller, ULARGE_INTEGER pulTotalNumberOfBytes, ULARGE_INTEGER pulTotalNumberOfFreeBytes );

	int SHGetFileInfo ( String pszPath, DWORD dwFileAttributes, SHFILEINFO psfi, UINT cbFileInfo, UINT uFlags );

	int SHGetFolderPath ( HWND hwndOwner, int nFolder, HANDLE hToken, DWORD dwFlags, String pszPath );

	int SHGetFolderPathAndSubDir ( HWND hwnd, int csidl, HANDLE hToken, DWORD dwFlags, String pszSubDir, String pszPath );

	int SHGetIconOverlayIndex ( String pszIconPath, int iIconIndex );

	int SHGetNewLinkInfo ( String pszLinkTo, String pszDir, String pszName, IntByReference pfMustCopy, UINT uFlags );

	void SHGetSettings ( SHELLFLAGSTATE lpsfs, DWORD dwMask );

	int SHGetSpecialFolderPath ( HWND hwndOwner, String lpszPath, int csidl, BOOL fCreate );

	int SHGetUnreadMailCount ( HKEY hKeyUser, String pszMailAddress, IntByReference pdwCount, FILETIME pFileTime, String pszShellExecuteCommand, int cchShellExecuteCommand );

	int SHInvokePrinterCommand ( HWND hwnd, UINT uAction, String lpBuf1, String lpBuf2, BOOL fModal );

	int SHIsFileAvailableOffline ( CHARByReference pszPath, IntByReference pdwStatus );

	int SHLoadNonloadedIconOverlayIdentifiers ( );

	int SHOpenWithDialog ( HWND hwndParent, OPENASINFO poainfo );

	int SHQueryRecycleBin ( String pszRootPath, SHQUERYRBINFO pSHQueryRBInfo );

	int SHSetFolderPath ( int csidl, HANDLE hToken, DWORD dwFlags, String pszPath );

	int SHSetUnreadMailCount ( String pszMailAddress, DWORD dwCount, String pszShellExecuteCommand );

	int SHTestTokenMembership ( HANDLE hToken, ULONG ulRID );

	void SHUpdateImage ( String pszHashItem, int iIndex, UINT uFlags, int iImageIndex );

}