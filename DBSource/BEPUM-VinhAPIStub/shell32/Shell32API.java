package v2.org.analysis.apihandle.winapi.shell32;

import v2.org.analysis.apihandle.winapi.API;

public abstract class Shell32API extends API {
	public Shell32API () {
		this.libraryName = "shell32.dll";
	}
}