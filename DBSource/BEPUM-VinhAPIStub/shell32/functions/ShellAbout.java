/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.shell32.functions
 * File name: ShellAbout.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.shell32.functions;

import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.WinDef.HICON;
import com.sun.jna.platform.win32.WinDef.HWND;

import v2.org.analysis.apihandle.winapi.shell32.Shell32API;
import v2.org.analysis.apihandle.winapi.shell32.Shell32DLL;
import v2.org.analysis.value.LongValue;
 
public class ShellAbout extends Shell32API {
	public ShellAbout () {
		super();
		NUM_OF_PARMS = 4;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		long t2 = this.params.get(2);
		long t3 = this.params.get(3);
		
		// Step 2: type conversion from C++ to Java
		HWND hWnd = null;
		if ( t0 != 0L ) {
			hWnd = new HWND ();
			hWnd.setPointer(new Pointer(t0));
		}
		String szApp = null;
		if ( t1 != 0L ) szApp = memory.getText(this, t1);
		String szOtherStuff = null;
		if ( t2 != 0L ) szOtherStuff = memory.getText(this, t2);
		HICON hIcon = null;
		if ( t3 != 0L ) {
			hIcon = new HICON ();
			hIcon.setPointer(new Pointer(t3));
		}

		// Step 3: call API function
		int ret = Shell32DLL.INSTANCE.ShellAbout (hWnd, szApp, szOtherStuff, hIcon);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));

	}
}