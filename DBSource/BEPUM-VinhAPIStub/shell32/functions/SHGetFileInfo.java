/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.shell32.functions
 * File name: SHGetFileInfo.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.shell32.functions;

import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.platform.win32.WinDef.HICON;
import com.sun.jna.platform.win32.WinDef.UINT;

import v2.org.analysis.apihandle.structures.SHFILEINFO;
import v2.org.analysis.apihandle.winapi.shell32.Shell32API;
import v2.org.analysis.apihandle.winapi.shell32.Shell32DLL;
import v2.org.analysis.value.LongValue;
 
public class SHGetFileInfo extends Shell32API {
	public SHGetFileInfo () {
		super();
		NUM_OF_PARMS = 5;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		long t2 = this.params.get(2);
		long t3 = this.params.get(3);
		long t4 = this.params.get(4);
		
		// Step 2: type conversion from C++ to Java
		String pszPath = null;
		if ( t0 != 0L ) pszPath = memory.getText(this, t0);
		DWORD dwFileAttributes = new DWORD (t1);
		SHFILEINFO psfi = null;
		if ( t2 != 0L) {
			psfi = new SHFILEINFO ();
			psfi.hIcon = new HICON (new Pointer(((LongValue)memory.getDoubleWordMemoryValue (t2)).getValue()));
			t2 += 4;
			psfi.iIcon = (int) ((LongValue)memory.getDoubleWordMemoryValue (t2)).getValue();
			t2 += 4;
			psfi.dwAttributes = new DWORD (((LongValue)memory.getDoubleWordMemoryValue (t2)).getValue());
			t2 += 4;
			for (int i = 0; i < psfi.szDisplayName.length; i++) {
				psfi.szDisplayName [i] = (char) ((LongValue) memory.getWordMemoryValue (t2)).getValue();
				t2 += 2;
			}
			for (int i = 0; i < psfi.szTypeName.length; i++) {
				psfi.szTypeName [i] = (char) ((LongValue) memory.getWordMemoryValue (t2)).getValue();
				t2 += 2;
			}
		}
		UINT cbFileInfo = new UINT (t3);
		UINT uFlags = new UINT (t4);

		// Step 3: call API function
		int ret = Shell32DLL.INSTANCE.SHGetFileInfo (pszPath, dwFileAttributes, psfi, cbFileInfo, uFlags);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));

	}
}