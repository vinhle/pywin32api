/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.shell32.functions
 * File name: SHGetSpecialFolderPath.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.shell32.functions;

import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.WinDef.BOOL;
import com.sun.jna.platform.win32.WinDef.HWND;

import v2.org.analysis.apihandle.winapi.shell32.Shell32API;
import v2.org.analysis.apihandle.winapi.shell32.Shell32DLL;
import v2.org.analysis.value.LongValue;
 
public class SHGetSpecialFolderPath extends Shell32API {
	public SHGetSpecialFolderPath () {
		super();
		NUM_OF_PARMS = 4;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		long t2 = this.params.get(2);
		long t3 = this.params.get(3);
		
		// Step 2: type conversion from C++ to Java
		HWND hwndOwner = null;
		if ( t0 != 0L ) {
			hwndOwner = new HWND ();
			hwndOwner.setPointer(new Pointer(t0));
		}
		String lpszPath = null;
		if ( t1 != 0L ) lpszPath = memory.getText(this, t1);
		int csidl = (int) t2;
		BOOL fCreate = new BOOL (t3);

		// Step 3: call API function
		int ret = Shell32DLL.INSTANCE.SHGetSpecialFolderPath (hwndOwner, lpszPath, csidl, fCreate);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));
		memory.setText(this, t1, new String(lpszPath));
	}
}