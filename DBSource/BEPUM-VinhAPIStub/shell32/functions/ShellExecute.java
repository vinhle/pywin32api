/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.shell32.functions
 * File name: ShellExecute.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.shell32.functions;

import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.WinDef.HWND;

import v2.org.analysis.apihandle.winapi.shell32.Shell32API;
import v2.org.analysis.apihandle.winapi.shell32.Shell32DLL;
import v2.org.analysis.value.LongValue;
 
public class ShellExecute extends Shell32API {
	public ShellExecute () {
		super();
		NUM_OF_PARMS = 6;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		long t2 = this.params.get(2);
		long t3 = this.params.get(3);
		long t4 = this.params.get(4);
		long t5 = this.params.get(5);
		
		// Step 2: type conversion from C++ to Java
		HWND hwnd = null;
		if ( t0 != 0L ) {
			hwnd = new HWND ();
			hwnd.setPointer(new Pointer(t0));
		}
		String lpOperation = null;
		if ( t1 != 0L ) lpOperation = memory.getText(this, t1);
		String lpFile = null;
		if ( t2 != 0L ) lpFile = memory.getText(this, t2);
		String lpParameters = null;
		if ( t3 != 0L ) lpParameters = memory.getText(this, t3);
		String lpDirectory = null;
		if ( t4 != 0L ) lpDirectory = memory.getText(this, t4);
		int nShowCmd = (int) t5;

		// Step 3: call API function
		int ret = Shell32DLL.INSTANCE.ShellExecute (hwnd, lpOperation, lpFile, lpParameters, lpDirectory, nShowCmd);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));

	}
}