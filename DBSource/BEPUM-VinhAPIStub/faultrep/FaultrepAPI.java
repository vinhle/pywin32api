package v2.org.analysis.apihandle.winapi.faultrep;

import v2.org.analysis.apihandle.winapi.API;

public abstract class FaultrepAPI extends API {
	public FaultrepAPI () {
		this.libraryName = "faultrep.dll";
	}
}