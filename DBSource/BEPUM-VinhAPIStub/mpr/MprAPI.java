package v2.org.analysis.apihandle.winapi.mpr;

import v2.org.analysis.apihandle.winapi.API;

public abstract class MprAPI extends API {
	public MprAPI () {
		this.libraryName = "mpr.dll";
	}
}