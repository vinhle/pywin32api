package v2.org.analysis.apihandle.winapi.wininet;

import com.sun.jna.Native;
import com.sun.jna.platform.win32.BaseTSD.DWORD_PTR;
import com.sun.jna.platform.win32.WinDef.BOOL;
import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.platform.win32.WinNT.HANDLE;
import com.sun.jna.win32.StdCallLibrary;
import com.sun.jna.win32.W32APIOptions;


public interface WininetDLL extends StdCallLibrary {
	WininetDLL INSTANCE = (WininetDLL) Native.loadLibrary("wininet", WininetDLL.class, W32APIOptions.DEFAULT_OPTIONS);
	
	// API's Interfaces
	int FtpGetFile ( HANDLE hConnect, String lpszRemoteFile, String lpszNewFile, BOOL fFailIfExists, DWORD dwFlagsAndAttributes, DWORD dwFlags, DWORD_PTR dwContext );

	int HttpAddRequestHeaders ( HANDLE hRequest, String lpszHeaders, DWORD dwHeadersLength, DWORD dwModifiers );

	int HttpOpenRequest ( HANDLE hConnect, String lpszVerb, String lpszObjectName, String lpszVersion, String lpszReferer, String[] lplpszAcceptTypes, DWORD dwFlags, DWORD_PTR dwContext );

	int InternetCloseHandle ( HANDLE hInternet );

	int InternetOpen ( String lpszAgent, DWORD dwAccessType, String lpszProxyName, String lpszProxyBypass, DWORD dwFlags );

	int InternetOpenUrl ( HANDLE hInternet, String lpszUrl, String lpszHeaders, DWORD dwHeadersLength, DWORD dwFlags, DWORD_PTR dwContext );

}