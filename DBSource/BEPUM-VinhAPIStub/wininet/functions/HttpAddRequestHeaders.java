/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.wininet.functions
 * File name: HttpAddRequestHeaders.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.wininet.functions;

import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.platform.win32.WinNT.HANDLE;

import v2.org.analysis.apihandle.winapi.wininet.WininetAPI;
import v2.org.analysis.apihandle.winapi.wininet.WininetDLL;
import v2.org.analysis.value.LongValue;
 
public class HttpAddRequestHeaders extends WininetAPI {
	public HttpAddRequestHeaders () {
		super();
		NUM_OF_PARMS = 4;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		long t2 = this.params.get(2);
		long t3 = this.params.get(3);
		
		// Step 2: type conversion from C++ to Java
		HANDLE hRequest = null;
		if ( t0 != 0L ) {
			hRequest = new HANDLE ();
			hRequest.setPointer(new Pointer(t0));
		}
		String lpszHeaders = null;
		if ( t1 != 0L ) lpszHeaders = memory.getText(this, t1);
		DWORD dwHeadersLength = new DWORD (t2);
		DWORD dwModifiers = new DWORD (t3);

		// Step 3: call API function
		int ret = WininetDLL.INSTANCE.HttpAddRequestHeaders (hRequest, lpszHeaders, dwHeadersLength, dwModifiers);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));

	}
}