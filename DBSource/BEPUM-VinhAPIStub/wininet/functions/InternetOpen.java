/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.wininet.functions
 * File name: InternetOpen.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.wininet.functions;

import com.sun.jna.platform.win32.WinDef.DWORD;

import v2.org.analysis.apihandle.winapi.wininet.WininetAPI;
import v2.org.analysis.apihandle.winapi.wininet.WininetDLL;
import v2.org.analysis.value.LongValue;
 
public class InternetOpen extends WininetAPI {
	public InternetOpen () {
		super();
		NUM_OF_PARMS = 5;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		long t2 = this.params.get(2);
		long t3 = this.params.get(3);
		long t4 = this.params.get(4);
		
		// Step 2: type conversion from C++ to Java
		String lpszAgent = null;
		if ( t0 != 0L ) lpszAgent = memory.getText(this, t0);
		DWORD dwAccessType = new DWORD (t1);
		String lpszProxyName = null;
		if ( t2 != 0L ) lpszProxyName = memory.getText(this, t2);
		String lpszProxyBypass = null;
		if ( t3 != 0L ) lpszProxyBypass = memory.getText(this, t3);
		DWORD dwFlags = new DWORD (t4);

		// Step 3: call API function
		int ret = WininetDLL.INSTANCE.InternetOpen (lpszAgent, dwAccessType, lpszProxyName, lpszProxyBypass, dwFlags);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));

	}
}