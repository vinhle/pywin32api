/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.wininet.functions
 * File name: InternetOpenUrl.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.wininet.functions;

import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.BaseTSD.DWORD_PTR;
import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.platform.win32.WinNT.HANDLE;

import v2.org.analysis.apihandle.winapi.wininet.WininetAPI;
import v2.org.analysis.apihandle.winapi.wininet.WininetDLL;
import v2.org.analysis.value.LongValue;
 
public class InternetOpenUrl extends WininetAPI {
	public InternetOpenUrl () {
		super();
		NUM_OF_PARMS = 6;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		long t2 = this.params.get(2);
		long t3 = this.params.get(3);
		long t4 = this.params.get(4);
		long t5 = this.params.get(5);
		
		// Step 2: type conversion from C++ to Java
		HANDLE hInternet = null;
		if ( t0 != 0L ) {
			hInternet = new HANDLE ();
			hInternet.setPointer(new Pointer(t0));
		}
		String lpszUrl = null;
		if ( t1 != 0L ) lpszUrl = memory.getText(this, t1);
		String lpszHeaders = null;
		if ( t2 != 0L ) lpszHeaders = memory.getText(this, t2);
		DWORD dwHeadersLength = new DWORD (t3);
		DWORD dwFlags = new DWORD (t4);
		DWORD_PTR dwContext = new DWORD_PTR (t5);

		// Step 3: call API function
		int ret = WininetDLL.INSTANCE.InternetOpenUrl (hInternet, lpszUrl, lpszHeaders, dwHeadersLength, dwFlags, dwContext);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));

	}
}