/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.wininet.functions
 * File name: InternetCloseHandle.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.wininet.functions;

import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.WinNT.HANDLE;

import v2.org.analysis.apihandle.winapi.wininet.WininetAPI;
import v2.org.analysis.apihandle.winapi.wininet.WininetDLL;
import v2.org.analysis.value.LongValue;
 
public class InternetCloseHandle extends WininetAPI {
	public InternetCloseHandle () {
		super();
		NUM_OF_PARMS = 1;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		
		// Step 2: type conversion from C++ to Java
		HANDLE hInternet = null;
		if ( t0 != 0L ) {
			hInternet = new HANDLE ();
			hInternet.setPointer(new Pointer(t0));
		}

		// Step 3: call API function
		int ret = WininetDLL.INSTANCE.InternetCloseHandle (hInternet);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));

	}
}