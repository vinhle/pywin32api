/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.wininet.functions
 * File name: FtpGetFile.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.wininet.functions;

import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.BaseTSD.DWORD_PTR;
import com.sun.jna.platform.win32.WinDef.BOOL;
import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.platform.win32.WinNT.HANDLE;

import v2.org.analysis.apihandle.winapi.wininet.WininetAPI;
import v2.org.analysis.apihandle.winapi.wininet.WininetDLL;
import v2.org.analysis.value.LongValue;
 
public class FtpGetFile extends WininetAPI {
	public FtpGetFile () {
		super();
		NUM_OF_PARMS = 7;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		long t2 = this.params.get(2);
		long t3 = this.params.get(3);
		long t4 = this.params.get(4);
		long t5 = this.params.get(5);
		long t6 = this.params.get(6);
		
		// Step 2: type conversion from C++ to Java
		HANDLE hConnect = null;
		if ( t0 != 0L ) {
			hConnect = new HANDLE ();
			hConnect.setPointer(new Pointer(t0));
		}
		String lpszRemoteFile = null;
		if ( t1 != 0L ) lpszRemoteFile = memory.getText(this, t1);
		String lpszNewFile = null;
		if ( t2 != 0L ) lpszNewFile = memory.getText(this, t2);
		BOOL fFailIfExists = new BOOL (t3);
		DWORD dwFlagsAndAttributes = new DWORD (t4);
		DWORD dwFlags = new DWORD (t5);
		DWORD_PTR dwContext = new DWORD_PTR (t6);

		// Step 3: call API function
		int ret = WininetDLL.INSTANCE.FtpGetFile (hConnect, lpszRemoteFile, lpszNewFile, fFailIfExists, dwFlagsAndAttributes, dwFlags, dwContext);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));

	}
}