package v2.org.analysis.apihandle.winapi.wininet;

import v2.org.analysis.apihandle.winapi.API;

public abstract class WininetAPI extends API {
	public WininetAPI () {
		this.libraryName = "wininet.dll";
	}
}