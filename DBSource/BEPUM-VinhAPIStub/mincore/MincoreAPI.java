package v2.org.analysis.apihandle.winapi.mincore;

import v2.org.analysis.apihandle.winapi.API;

public abstract class MincoreAPI extends API {
	public MincoreAPI () {
		this.libraryName = "mincore.dll";
	}
}