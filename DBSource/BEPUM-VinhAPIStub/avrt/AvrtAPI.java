package v2.org.analysis.apihandle.winapi.avrt;

import v2.org.analysis.apihandle.winapi.API;

public abstract class AvrtAPI extends API {
	public AvrtAPI () {
		this.libraryName = "avrt.dll";
	}
}