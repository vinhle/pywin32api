package v2.org.analysis.apihandle.winapi.ntmsapi;

import v2.org.analysis.apihandle.winapi.API;

public abstract class NtmsapiAPI extends API {
	public NtmsapiAPI () {
		this.libraryName = "ntmsapi.dll";
	}
}