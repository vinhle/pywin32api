/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.ole32.functions
 * File name: CoUninitialize.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.ole32.functions;

import v2.org.analysis.apihandle.winapi.ole32.Ole32API;
import v2.org.analysis.apihandle.winapi.ole32.Ole32DLL;

 
public class CoUninitialize extends Ole32API {
	public CoUninitialize () {
		super();
		NUM_OF_PARMS = 0;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		
		// Step 2: type conversion from C++ to Java

		// Step 3: call API function
		Ole32DLL.INSTANCE.CoUninitialize ();
		
		// Step 4: update environment (memory & eax register)

	}
}