/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.ole32.functions
 * File name: CoFileTimeNow.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.ole32.functions;

import com.sun.jna.platform.win32.WinBase.FILETIME;

import v2.org.analysis.apihandle.winapi.ole32.Ole32API;
import v2.org.analysis.apihandle.winapi.ole32.Ole32DLL;
import v2.org.analysis.value.LongValue;
 
public class CoFileTimeNow extends Ole32API {
	public CoFileTimeNow () {
		super();
		NUM_OF_PARMS = 1;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		
		// Step 2: type conversion from C++ to Java
		FILETIME lpFileTime = new FILETIME ();

		// Step 3: call API function
		int ret = Ole32DLL.INSTANCE.CoFileTimeNow (lpFileTime);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));
		t0 = this.params.get(0);
		memory.setDoubleWordMemoryValue (t0, new LongValue(lpFileTime.dwLowDateTime));
		t0 += 4;
		memory.setDoubleWordMemoryValue (t0, new LongValue(lpFileTime.dwHighDateTime));
		t0 += 4;

	}
}