package v2.org.analysis.apihandle.winapi.ole32;

import com.sun.jna.Native;
import com.sun.jna.platform.win32.WinBase.FILETIME;
import com.sun.jna.win32.StdCallLibrary;
import com.sun.jna.win32.W32APIOptions;


public interface Ole32DLL extends StdCallLibrary {
	Ole32DLL INSTANCE = (Ole32DLL) Native.loadLibrary("ole32", Ole32DLL.class, W32APIOptions.DEFAULT_OPTIONS);
	
	// API's Interfaces
	int CoFileTimeNow ( FILETIME lpFileTime );

	void CoUninitialize ( );

	void OleUninitialize ( );

}