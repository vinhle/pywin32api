package v2.org.analysis.apihandle.winapi.ole32;

import v2.org.analysis.apihandle.winapi.API;

public abstract class Ole32API extends API {
	public Ole32API () {
		this.libraryName = "ole32.dll";
	}
}