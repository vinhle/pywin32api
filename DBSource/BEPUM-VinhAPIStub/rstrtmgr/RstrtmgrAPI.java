package v2.org.analysis.apihandle.winapi.rstrtmgr;

import v2.org.analysis.apihandle.winapi.API;

public abstract class RstrtmgrAPI extends API {
	public RstrtmgrAPI () {
		this.libraryName = "rstrtmgr.dll";
	}
}