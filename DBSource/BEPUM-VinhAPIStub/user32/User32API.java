package v2.org.analysis.apihandle.winapi.user32;

import v2.org.analysis.apihandle.winapi.API;

public abstract class User32API extends API {
	public User32API () {
		this.libraryName = "user32.dll";
	}
}