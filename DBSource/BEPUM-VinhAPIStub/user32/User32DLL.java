package v2.org.analysis.apihandle.winapi.user32;

import com.sun.jna.Native;
import com.sun.jna.platform.win32.BaseTSD.DWORD_PTR;
import com.sun.jna.platform.win32.BaseTSD.LONG_PTR;
import com.sun.jna.platform.win32.BaseTSD.ULONG_PTR;
import com.sun.jna.platform.win32.WinBase.SECURITY_ATTRIBUTES;
import com.sun.jna.platform.win32.WinDef.BOOL;
import com.sun.jna.platform.win32.WinDef.BYTE;
import com.sun.jna.platform.win32.WinDef.CHARByReference;
import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.platform.win32.WinDef.HBITMAP;
import com.sun.jna.platform.win32.WinDef.HBRUSH;
import com.sun.jna.platform.win32.WinDef.HCURSOR;
import com.sun.jna.platform.win32.WinDef.HDC;
import com.sun.jna.platform.win32.WinDef.HICON;
import com.sun.jna.platform.win32.WinDef.HINSTANCE;
import com.sun.jna.platform.win32.WinDef.HMENU;
import com.sun.jna.platform.win32.WinDef.HRGN;
import com.sun.jna.platform.win32.WinDef.HWND;
import com.sun.jna.platform.win32.WinDef.LONG;
import com.sun.jna.platform.win32.WinDef.LPARAM;
import com.sun.jna.platform.win32.WinDef.LRESULT;
import com.sun.jna.platform.win32.WinDef.POINT;
import com.sun.jna.platform.win32.WinDef.RECT;
import com.sun.jna.platform.win32.WinDef.UINT;
import com.sun.jna.platform.win32.WinDef.UINT_PTR;
import com.sun.jna.platform.win32.WinDef.WORD;
import com.sun.jna.platform.win32.WinDef.WPARAM;
import com.sun.jna.platform.win32.WinGDI.ICONINFO;
import com.sun.jna.platform.win32.WinNT.HANDLE;
import com.sun.jna.platform.win32.WinNT.HANDLEByReference;
import com.sun.jna.platform.win32.WinUser.BLENDFUNCTION;
import com.sun.jna.platform.win32.WinUser.FLASHWINFO;
import com.sun.jna.platform.win32.WinUser.GUITHREADINFO;
import com.sun.jna.platform.win32.WinUser.HDEVNOTIFY;
import com.sun.jna.platform.win32.WinUser.HHOOK;
import com.sun.jna.platform.win32.WinUser.HMONITOR;
import com.sun.jna.platform.win32.WinUser.HOOKPROC;
import com.sun.jna.platform.win32.WinUser.INPUT;
import com.sun.jna.platform.win32.WinUser.LASTINPUTINFO;
import com.sun.jna.platform.win32.WinUser.MONITORENUMPROC;
import com.sun.jna.platform.win32.WinUser.MONITORINFO;
import com.sun.jna.platform.win32.WinUser.MSG;
import com.sun.jna.platform.win32.WinUser.RAWINPUTDEVICELIST;
import com.sun.jna.platform.win32.WinUser.SIZE;
import com.sun.jna.platform.win32.WinUser.WINDOWINFO;
import com.sun.jna.platform.win32.WinUser.WINDOWPLACEMENT;
import com.sun.jna.platform.win32.WinUser.WNDCLASSEX;
import com.sun.jna.platform.win32.WinUser.WNDENUMPROC;
import com.sun.jna.ptr.ByteByReference;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.PointerByReference;
import com.sun.jna.ptr.ShortByReference;
import com.sun.jna.win32.StdCallLibrary;
import com.sun.jna.win32.W32APIOptions;

import v2.org.analysis.apihandle.structures.ACCEL;
import v2.org.analysis.apihandle.structures.ALTTABINFO;
import v2.org.analysis.apihandle.structures.BSMINFO;
import v2.org.analysis.apihandle.structures.COMBOBOXINFO;
import v2.org.analysis.apihandle.structures.CONVCONTEXT;
import v2.org.analysis.apihandle.structures.CONVINFO;
import v2.org.analysis.apihandle.structures.CURSORINFO;
import v2.org.analysis.apihandle.structures.DEVMODE;
import v2.org.analysis.apihandle.structures.DISPLAY_DEVICE;
import v2.org.analysis.apihandle.structures.DRAWTEXTPARAMS;
import v2.org.analysis.apihandle.structures.MENUBARINFO;
import v2.org.analysis.apihandle.structures.MENUINFO;
import v2.org.analysis.apihandle.structures.MENUITEMINFO;
import v2.org.analysis.apihandle.structures.MOUSEMOVEPOINT;
import v2.org.analysis.apihandle.structures.PAINTSTRUCT;
import v2.org.analysis.apihandle.structures.RAWINPUT;
import v2.org.analysis.apihandle.structures.RAWINPUTDEVICE;
import v2.org.analysis.apihandle.structures.SCROLLBARINFO;
import v2.org.analysis.apihandle.structures.SCROLLINFO;
import v2.org.analysis.apihandle.structures.SECURITY_QUALITY_OF_SERVICE;
import v2.org.analysis.apihandle.structures.TITLEBARINFO;
import v2.org.analysis.apihandle.structures.TPMPARAMS;
import v2.org.analysis.apihandle.structures.TRACKMOUSEEVENT;


public interface User32DLL extends StdCallLibrary {
	User32DLL INSTANCE = (User32DLL) Native.loadLibrary("user32", User32DLL.class, W32APIOptions.DEFAULT_OPTIONS);
	
	// API's Interfaces
	int ActivateKeyboardLayout ( HANDLE hkl, UINT Flags );

	int AddClipboardFormatListener ( HWND hwnd );

	int AdjustWindowRect ( RECT lpRect, DWORD dwStyle, BOOL bMenu );

	int AdjustWindowRectEx ( RECT lpRect, DWORD dwStyle, BOOL bMenu, DWORD dwExStyle );

	int AllowSetForegroundWindow ( DWORD dwProcessId );

	int AnimateWindow ( HWND hwnd, DWORD dwTime, DWORD dwFlags );

	int AnyPopup ( );

	int AppendMenu ( HMENU hMenu, UINT uFlags, UINT_PTR uIDNewItem, String lpNewItem );

	int ArrangeIconicWindows ( HWND hWnd );

	int AttachThreadInput ( DWORD idAttach, DWORD idAttachTo, BOOL fAttach );

	int BeginDeferWindowPos ( int nNumWindows );

	int BeginPaint ( HWND hwnd, PAINTSTRUCT lpPaint );

	int BlockInput ( BOOL fBlockIt );

	int BringWindowToTop ( HWND hWnd );

	int BroadcastSystemMessage ( DWORD dwFlags, IntByReference lpdwRecipients, UINT uiMessage, WPARAM wParam, LPARAM lParam );

	int BroadcastSystemMessageEx ( DWORD dwFlags, IntByReference lpdwRecipients, UINT uiMessage, WPARAM wParam, LPARAM lParam, BSMINFO pBSMInfo );

	int CallMsgFilter ( MSG lpMsg, int nCode );

	int CallNextHookEx ( HHOOK hhk, int nCode, WPARAM wParam, LPARAM lParam );

	int CascadeWindows ( HWND hwndParent, UINT wHow, RECT lpRect, UINT cKids, HANDLEByReference lpKids );

	int ChangeClipboardChain ( HWND hWndRemove, HWND hWndNewNext );

	int ChangeDisplaySettings ( DEVMODE lpDevMode, DWORD dwflags );

	int ChangeWindowMessageFilter ( UINT message, DWORD dwFlag );

	int CharLower ( String lpsz );

	int CharLowerBuff ( String lpsz, DWORD cchLength );

	int CharNext ( String lpsz );

	int CharNextExA ( WORD CodePage, ByteByReference lpCurrentChar, DWORD dwFlags );

	int CharPrev ( String lpszStart, String lpszCurrent );

	int CharPrevExA ( WORD CodePage, ByteByReference lpStart, ByteByReference lpCurrentChar, DWORD dwFlags );

	int CharToOem ( String lpszSrc, ByteByReference lpszDst );

	int CharToOemBuff ( String lpszSrc, ByteByReference lpszDst, DWORD cchDstLength );

	int CharUpper ( String lpsz );

	int CharUpperBuff ( String lpsz, DWORD cchLength );

	int CheckDlgButton ( HWND hDlg, int nIDButton, UINT uCheck );

	int CheckMenuItem ( HMENU hmenu, UINT uIDCheckItem, UINT uCheck );

	int CheckMenuRadioItem ( HMENU hmenu, UINT idFirst, UINT idLast, UINT idCheck, UINT uFlags );

	int CheckRadioButton ( HWND hDlg, int nIDFirstButton, int nIDLastButton, int nIDCheckButton );

	int ChildWindowFromPoint ( HWND hWndParent, POINT Point );

	int ChildWindowFromPointEx ( HWND hwndParent, POINT pt, UINT uFlags );

	int ClientToScreen ( HWND hWnd, POINT lpPoint );

	int ClipCursor ( RECT lpRect );

	int CloseClipboard ( );

	int CloseDesktop ( HANDLE hDesktop );

	int CloseWindow ( HWND hWnd );

	int CloseWindowStation ( HANDLE hWinSta );

	int CopyAcceleratorTable ( HANDLE hAccelSrc, ACCEL lpAccelDst, int cAccelEntries );

	int CopyIcon ( HICON hIcon );

	int CopyImage ( HANDLE hImage, UINT uType, int cxDesired, int cyDesired, UINT fuFlags );

	int CopyRect ( RECT lprcDst, RECT lprcSrc );

	int CountClipboardFormats ( );

	int CreateAcceleratorTable ( ACCEL lpaccl, int cEntries );

	int CreateCaret ( HWND hWnd, HBITMAP hBitmap, int nWidth, int nHeight );

	int CreateDesktop ( String lpszDesktop, String lpszDevice, DEVMODE pDevmode, DWORD dwFlags, int dwDesiredAccess, SECURITY_ATTRIBUTES lpsa );

	int CreateIcon ( HINSTANCE hInstance, int nWidth, int nHeight, BYTE cPlanes, BYTE cBitsPixel, byte lpbANDbits, byte lpbXORbits );

	int CreateIconFromResource ( byte presbits, DWORD dwResSize, BOOL fIcon, DWORD dwVer );

	int CreateIconFromResourceEx ( byte pbIconBits, DWORD cbIconBits, BOOL fIcon, DWORD dwVersion, int cxDesired, int cyDesired, UINT uFlags );

	int CreateIconIndirect ( ICONINFO piconinfo );

	int CreateMDIWindow ( String lpClassName, String lpWindowName, DWORD dwStyle, int X, int Y, int nWidth, int nHeight, HWND hWndParent, HINSTANCE hInstance, LPARAM lParam );

	int CreateMenu ( );

	int CreatePopupMenu ( );

	int CreateWindowStation ( String lpwinsta, DWORD dwFlags, int dwDesiredAccess, SECURITY_ATTRIBUTES lpsa );

	int DdeAbandonTransaction ( DWORD idInst, HANDLE hConv, DWORD idTransaction );

	int DdeAccessData ( HANDLE hData, IntByReference pcbDataSize );

	int DdeAddData ( HANDLE hData, byte pSrc, DWORD cb, DWORD cbOff );

	int DdeClientTransaction ( byte pData, DWORD cbData, HANDLE hConv, HANDLE hszItem, UINT wFmt, UINT wType, DWORD dwTimeout, IntByReference pdwResult );

	int DdeCmpStringHandles ( HANDLE hsz1, HANDLE hsz2 );

	int DdeConnect ( DWORD idInst, HANDLE hszService, HANDLE hszTopic, CONVCONTEXT pCC );

	int DdeConnectList ( DWORD idInst, HANDLE hszService, HANDLE hszTopic, HANDLE hConvList, CONVCONTEXT pCC );

	int DdeCreateDataHandle ( DWORD idInst, byte pSrc, DWORD cb, DWORD cbOff, HANDLE hszItem, UINT wFmt, UINT afCmd );

	int DdeCreateStringHandle ( DWORD idInst, String psz, int iCodePage );

	int DdeDisconnect ( HANDLE hConv );

	int DdeDisconnectList ( HANDLE hConvList );

	int DdeEnableCallback ( DWORD idInst, HANDLE hConv, UINT wCmd );

	int DdeFreeDataHandle ( HANDLE hData );

	int DdeFreeStringHandle ( DWORD idInst, HANDLE hsz );

	int DdeGetData ( HANDLE hData, byte[] pDst, DWORD cbMax, DWORD cbOff );

	int DdeGetLastError ( DWORD idInst );

	int DdeImpersonateClient ( HANDLE hConv );

	int DdeKeepStringHandle ( DWORD idInst, HANDLE hsz );

	int DdeNameService ( DWORD idInst, HANDLE hsz1, HANDLE hsz2, UINT afCmd );

	int DdePostAdvise ( DWORD idInst, HANDLE hszTopic, HANDLE hszItem );

	int DdeQueryConvInfo ( HANDLE hConv, DWORD idTransaction, CONVINFO pConvInfo );

	int DdeQueryNextServer ( HANDLE hConvList, HANDLE hConvPrev );

	int DdeQueryString ( DWORD idInst, HANDLE hsz, char[] psz, DWORD cchMax, int iCodePage );

	int DdeReconnect ( HANDLE hConv );

	int DdeSetQualityOfService ( HWND hwndClient, SECURITY_QUALITY_OF_SERVICE pqosNew, SECURITY_QUALITY_OF_SERVICE pqosPrev );

	int DdeSetUserHandle ( HANDLE hConv, DWORD id, DWORD_PTR hUser );

	int DdeUnaccessData ( HANDLE hData );

	int DdeUninitialize ( DWORD idInst );

	int DeferWindowPos ( HANDLE hWinPosInfo, HWND hWnd, HWND hWndInsertAfter, int x, int y, int cx, int cy, UINT uFlags );

	int DefFrameProc ( HWND hWnd, HWND hWndMDIClient, UINT uMsg, WPARAM wParam, LPARAM lParam );

	int DefMDIChildProc ( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam );

	int DefRawInputProc ( PointerByReference paRawInput, int nInput, UINT cbSizeHeader );

	int DefWindowProc ( HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam );

	int DeleteMenu ( HMENU hMenu, UINT uPosition, UINT uFlags );

	int DeregisterShellHookWindow ( HWND hWnd );

	int DestroyAcceleratorTable ( HANDLE hAccel );

	int DestroyCaret ( );

	int DestroyCursor ( HCURSOR hCursor );

	int DestroyIcon ( HICON hIcon );

	int DestroyMenu ( HMENU hMenu );

	int DestroyWindow ( HWND hWnd );

	void DisableProcessWindowsGhosting ( );

	int DispatchMessage ( MSG lpmsg );

	int DlgDirList ( HWND hDlg, char[] lpPathSpec, int nIDListBox, int nIDStaticPath, UINT uFileType );

	int DlgDirListComboBox ( HWND hDlg, char[] lpPathSpec, int nIDComboBox, int nIDStaticPath, UINT uFiletype );

	int DlgDirSelectComboBoxEx ( HWND hDlg, char[] lpString, int nCount, int nIDComboBox );

	int DlgDirSelectEx ( HWND hDlg, char[] lpString, int nCount, int nIDListBox );

	int DragDetect ( HWND hwnd, POINT pt );

	int DrawAnimatedRects ( HWND hwnd, int idAni, RECT lprcFrom, RECT lprcTo );

	int DrawCaption ( HWND hwnd, HDC hdc, RECT lprc, UINT uFlags );

	int DrawEdge ( HDC hdc, RECT qrc, UINT edge, UINT grfFlags );

	int DrawFocusRect ( HDC hDC, RECT lprc );

	int DrawFrameControl ( HDC hdc, RECT lprc, UINT uType, UINT uState );

	int DrawIcon ( HDC hDC, int X, int Y, HICON hIcon );

	int DrawIconEx ( HDC hdc, int xLeft, int yTop, HICON hIcon, int cxWidth, int cyWidth, UINT istepIfAniCur, HBRUSH hbrFlickerFreeDraw, UINT diFlags );

	int DrawMenuBar ( HWND hWnd );

	int DrawText ( HDC hDC, String lpchText, int nCount, RECT lpRect, UINT uFormat );

	int DrawTextEx ( HDC hdc, String lpchText, int cchText, RECT lprc, UINT dwDTFormat, DRAWTEXTPARAMS lpDTParams );

	int EmptyClipboard ( );

	int EnableMenuItem ( HMENU hMenu, UINT uIDEnableItem, UINT uEnable );

	int EnableScrollBar ( HWND hWnd, UINT wSBflags, UINT wArrows );

	int EnableWindow ( HWND hWnd, BOOL bEnable );

	int EndDeferWindowPos ( HANDLE hWinPosInfo );

	int EndMenu ( );

	int EndPaint ( HWND hWnd, PAINTSTRUCT lpPaint );

	int EndTask ( HWND hWnd, BOOL fShutDown, BOOL fForce );

	int EnumChildWindows ( HWND hWndParent, WNDENUMPROC lpEnumFunc, LPARAM lParam );

	int EnumClipboardFormats ( UINT format );

	int EnumDesktopWindows ( HANDLE hDesktop, WNDENUMPROC lpfn, LPARAM lParam );

	int EnumDisplayDevices ( String lpDevice, DWORD iDevNum, DISPLAY_DEVICE lpDisplayDevice, DWORD dwFlags );

	int EnumDisplayMonitors ( HDC hdc, RECT lprcClip, MONITORENUMPROC lpfnEnum, LPARAM dwData );

	int EnumDisplaySettings ( String lpszDeviceName, DWORD iModeNum, DEVMODE lpDevMode );

	int EnumDisplaySettingsEx ( String lpszDeviceName, DWORD iModeNum, DEVMODE lpDevMode, DWORD dwFlags );

	int EnumThreadWindows ( DWORD dwThreadId, WNDENUMPROC lpfn, LPARAM lParam );

	int EnumWindows ( WNDENUMPROC lpEnumFunc, LPARAM lParam );

	int EqualRect ( RECT lprc1, RECT lprc2 );

	int ExcludeUpdateRgn ( HDC hDC, HWND hWnd );

	int ExitWindowsEx ( UINT uFlags, DWORD dwReason );

	int FillRect ( HDC hDC, RECT lprc, HBRUSH hbr );

	int FindWindow ( String lpClassName, String lpWindowName );

	int FindWindowEx ( HWND hwndParent, HWND hwndChildAfter, String lpszClass, String lpszWindow );

	int FlashWindow ( HWND hWnd, BOOL bInvert );

	int FlashWindowEx ( FLASHWINFO pfwi );

	int FrameRect ( HDC hDC, RECT lprc, HBRUSH hbr );

	int FreeDDElParam ( UINT msg, LPARAM lParam );

	int GetActiveWindow ( );

	int GetAltTabInfo ( HWND hwnd, int iItem, ALTTABINFO pati, String pszItemText, UINT cchItemText );

	int GetAncestor ( HWND hwnd, UINT gaFlags );

	int GetAsyncKeyState ( int vKey );

	int GetCapture ( );

	int GetCaretBlinkTime ( );

	int GetCaretPos ( POINT lpPoint );

	int GetClassInfoEx ( HINSTANCE hinst, String lpszClass, WNDCLASSEX lpwcx );

	int GetClassLong ( HWND hWnd, int nIndex );

	int GetClassLongPtr ( HWND hWnd, int nIndex );

	int GetClassName ( HWND hWnd, String lpClassName, int nMaxCount );

	int GetClassWord ( HWND hWnd, int nIndex );

	int GetClientRect ( HWND hWnd, RECT lpRect );

	int GetClipboardData ( UINT uFormat );

	int GetClipboardFormatName ( UINT format, String lpszFormatName, int cchMaxCount );

	int GetClipboardOwner ( );

	int GetClipboardSequenceNumber ( );

	int GetClipboardViewer ( );

	int GetClipCursor ( RECT lpRect );

	int GetComboBoxInfo ( HWND hwndCombo, COMBOBOXINFO pcbi );

	int GetCursor ( );

	int GetCursorInfo ( CURSORINFO pci );

	int GetCursorPos ( POINT lpPoint );

	int GetDC ( HWND hWnd );

	int GetDCEx ( HWND hWnd, HRGN hrgnClip, DWORD flags );

	int GetDesktopWindow ( );

	int GetDoubleClickTime ( );

	int GetFocus ( );

	int GetForegroundWindow ( );

	int GetGuiResources ( HANDLE hProcess, DWORD uiFlags );

	int GetGUIThreadInfo ( DWORD idThread, GUITHREADINFO lpgui );

	int GetIconInfo ( HICON hIcon, ICONINFO piconinfo );

	int GetInputState ( );

	int GetKBCodePage ( );

	int GetKeyboardLayout ( DWORD idThread );

	int GetKeyboardLayoutList ( int nBuff, HANDLEByReference lpList );

	int GetKeyboardLayoutName ( String pwszKLID );

	int GetKeyboardState ( byte lpKeyState );

	int GetKeyboardType ( int nTypeFlag );

	int GetKeyNameText ( LONG lParam, String lpString, int cchSize );

	int GetKeyState ( int nVirtKey );

	int GetLastActivePopup ( HWND hWnd );

	int GetLastInputInfo ( LASTINPUTINFO plii );

	int GetLayeredWindowAttributes ( HWND hwnd, IntByReference pcrKey, byte pbAlpha, IntByReference pdwFlags );

	int GetListBoxInfo ( HWND hwnd );

	int GetMenu ( HWND hWnd );

	int GetMenuBarInfo ( HWND hwnd, LONG idObject, LONG idItem, MENUBARINFO pmbi );

	int GetMenuCheckMarkDimensions ( );

	int GetMenuDefaultItem ( HMENU hMenu, UINT fByPos, UINT gmdiFlags );

	int GetMenuInfo ( HMENU hmenu, MENUINFO lpcmi );

	int GetMenuItemCount ( HMENU hMenu );

	int GetMenuItemID ( HMENU hMenu, int nPos );

	int GetMenuItemInfo ( HMENU hMenu, UINT uItem, BOOL fByPosition, MENUITEMINFO lpmii );

	int GetMenuItemRect ( HWND hWnd, HMENU hMenu, UINT uItem, RECT lprcItem );

	int GetMenuState ( HMENU hMenu, UINT uId, UINT uFlags );

	int GetMenuString ( HMENU hMenu, UINT uIDItem, String lpString, int nMaxCount, UINT uFlag );

	int GetMessage ( MSG lpMsg, HWND hWnd, UINT wMsgFilterMin, UINT wMsgFilterMax );

	int GetMessageExtraInfo ( );

	int GetMessagePos ( );

	int GetMessageTime ( );

	int GetMonitorInfo ( HMONITOR hMonitor, MONITORINFO lpmi );

	int GetMouseMovePointsEx ( UINT cbSize, MOUSEMOVEPOINT lppt, MOUSEMOVEPOINT lpptBuf, int nBufPoints, DWORD resolution );

	int GetOpenClipboardWindow ( );

	int GetParent ( HWND hWnd );

	int GetPriorityClipboardFormat ( IntByReference paFormatPriorityList, int cFormats );

	int GetProcessDefaultLayout ( IntByReference pdwDefaultLayout );

	int GetProcessWindowStation ( );

	int GetProp ( HWND hWnd, String lpString );

	int GetQueueStatus ( UINT flags );

	int GetRawInputBuffer ( RAWINPUT pData, IntByReference pcbSize, UINT cbSizeHeader );

	int GetRawInputDeviceList ( RAWINPUTDEVICELIST pRawInputDeviceList, int[] puiNumDevices, UINT cbSize );

	int GetRegisteredRawInputDevices ( RAWINPUTDEVICE pRawInputDevices, IntByReference puiNumDevices, UINT cbSize );

	int GetScrollBarInfo ( HWND hwnd, LONG idObject, SCROLLBARINFO psbi );

	int GetScrollInfo ( HWND hwnd, int fnBar, SCROLLINFO lpsi );

	int GetScrollPos ( HWND hWnd, int nBar );

	int GetScrollRange ( HWND hWnd, int nBar, IntByReference lpMinPos, IntByReference lpMaxPos );

	int GetShellWindow ( );

	int GetSubMenu ( HMENU hMenu, int nPos );

	int GetSysColor ( int nIndex );

	int GetSysColorBrush ( int nIndex );

	int GetSystemMenu ( HWND hWnd, BOOL bRevert );

	int GetSystemMetrics ( int nIndex );

	int GetTabbedTextExtent ( HDC hDC, String lpString, int nCount, int nTabPositions, IntByReference lpnTabStopPositions );

	int GetThreadDesktop ( DWORD dwThreadId );

	int GetTitleBarInfo ( HWND hwnd, TITLEBARINFO pti );

	int GetTopWindow ( HWND hWnd );

	int GetUpdateRect ( HWND hWnd, RECT lpRect, BOOL bErase );

	int GetUpdateRgn ( HWND hWnd, HRGN hRgn, BOOL bErase );

	int GetWindow ( HWND hWnd, UINT uCmd );

	int GetWindowDC ( HWND hWnd );

	int GetWindowInfo ( HWND hwnd, WINDOWINFO pwi );

	int GetWindowLong ( HWND hWnd, int nIndex );

	int GetWindowLongPtr ( HWND hWnd, int nIndex );

	int GetWindowModuleFileName ( HWND hwnd, String lpszFileName, UINT cchFileNameMax );

	int GetWindowPlacement ( HWND hWnd, WINDOWPLACEMENT lpwndpl );

	int GetWindowRect ( HWND hWnd, RECT lpRect );

	int GetWindowRgn ( HWND hWnd, HRGN hRgn );

	int GetWindowRgnBox ( HWND hWnd, RECT lprc );

	int GetWindowText ( HWND hWnd, String lpString, int nMaxCount );

	int GetWindowTextLength ( HWND hWnd );

	int GetWindowThreadProcessId ( HWND hWnd, IntByReference lpdwProcessId );

	int HideCaret ( HWND hWnd );

	int HiliteMenuItem ( HWND hwnd, HMENU hmenu, UINT uItemHilite, UINT uHilite );

	int ImpersonateDdeClientWindow ( HWND hWndClient, HWND hWndServer );

	int InflateRect ( RECT lprc, int dx, int dy );

	int InSendMessage ( );

	int InsertMenu ( HMENU hMenu, UINT uPosition, UINT uFlags, UINT_PTR uIDNewItem, String lpNewItem );

	int InsertMenuItem ( HMENU hMenu, UINT uItem, BOOL fByPosition, MENUITEMINFO lpmii );

	int InternalGetWindowText ( HWND hWnd, CHARByReference lpString, int nMaxCount );

	int IntersectRect ( RECT lprcDst, RECT lprcSrc1, RECT lprcSrc2 );

	int InvalidateRect ( HWND hWnd, RECT lpRect, BOOL bErase );

	int InvalidateRgn ( HWND hWnd, HRGN hRgn, BOOL bErase );

	int InvertRect ( HDC hDC, RECT lprc );

	int IsCharAlpha ( char ch );

	int IsCharAlphaNumeric ( char ch );

	int IsCharLower ( char ch );

	int IsCharUpper ( char ch );

	int IsChild ( HWND hWndParent, HWND hWnd );

	int IsClipboardFormatAvailable ( UINT format );

	int IsDlgButtonChecked ( HWND hDlg, int nIDButton );

	int IsGUIThread ( BOOL bConvert );

	int IsHungAppWindow ( HWND hWnd );

	int IsIconic ( HWND hWnd );

	int IsMenu ( HMENU hMenu );

	int IsRectEmpty ( RECT lprc );

	int IsWindow ( HWND hWnd );

	int IsWindowEnabled ( HWND hWnd );

	int IsWindowUnicode ( HWND hWnd );

	int IsWindowVisible ( HWND hWnd );

	int IsWow64Message ( );

	int IsZoomed ( HWND hWnd );

	void keybd_event ( BYTE bVk, BYTE bScan, DWORD dwFlags, ULONG_PTR dwExtraInfo );

	int KillTimer ( HWND hWnd, UINT_PTR uIDEvent );

	int LoadAccelerators ( HINSTANCE hInstance, String lpTableName );

	int LoadBitmap ( HINSTANCE hInstance, String lpBitmapName );

	int LoadCursor ( HINSTANCE hInstance, String lpCursorName );

	int LoadCursorFromFile ( String lpFileName );

	int LoadIcon ( HINSTANCE hInstance, String lpIconName );

	int LoadImage ( HINSTANCE hinst, String lpszName, UINT uType, int cxDesired, int cyDesired, UINT fuLoad );

	int LoadKeyboardLayout ( String pwszKLID, UINT Flags );

	int LoadMenu ( HINSTANCE hInstance, String lpMenuName );

	int LoadString ( HINSTANCE hInstance, UINT uID, String lpBuffer, int nBufferMax );

	int LockSetForegroundWindow ( UINT uLockCode );

	int LockWindowUpdate ( HWND hWndLock );

	int LockWorkStation ( );

	int LogicalToPhysicalPoint ( HWND hWnd, POINT lpPoint );

	int LookupIconIdFromDirectory ( byte presbits, BOOL fIcon );

	int LookupIconIdFromDirectoryEx ( byte presbits, BOOL fIcon, int cxDesired, int cyDesired, UINT Flags );

	int MapDialogRect ( HWND hDlg, RECT lpRect );

	int MapVirtualKey ( UINT uCode, UINT uMapType );

	int MapVirtualKeyEx ( UINT uCode, UINT uMapType, HANDLE dwhkl );

	int MapWindowPoints ( HWND hWndFrom, HWND hWndTo, POINT lpPoints, UINT cPoints );

	int MenuItemFromPoint ( HWND hWnd, HMENU hMenu, POINT ptScreen );

	int MessageBeep ( UINT uType );

	int MessageBox ( HWND hWnd, String lpText, String lpCaption, UINT uType );

	int MessageBoxEx ( HWND hWnd, String lpText, String lpCaption, UINT uType, WORD wLanguageId );

	int ModifyMenu ( HMENU hMnu, UINT uPosition, UINT uFlags, UINT_PTR uIDNewItem, String lpNewItem );

	int MonitorFromPoint ( POINT pt, DWORD dwFlags );

	int MonitorFromRect ( RECT lprc, DWORD dwFlags );

	int MonitorFromWindow ( HWND hwnd, DWORD dwFlags );

	void mouse_event ( DWORD dwFlags, DWORD dx, DWORD dy, DWORD dwData, ULONG_PTR dwExtraInfo );

	int MoveWindow ( HWND hWnd, int X, int Y, int nWidth, int nHeight, BOOL bRepaint );

	int MsgWaitForMultipleObjects ( DWORD nCount, HANDLEByReference pHandles, BOOL bWaitAll, DWORD dwMilliseconds, DWORD dwWakeMask );

	int MsgWaitForMultipleObjectsEx ( DWORD nCount, HANDLEByReference pHandles, DWORD dwMilliseconds, DWORD dwWakeMask, DWORD dwFlags );

	int OemKeyScan ( WORD wOemChar );

	int OemToChar ( ByteByReference lpszSrc, String lpszDst );

	int OemToCharBuff ( ByteByReference lpszSrc, String lpszDst, DWORD cchDstLength );

	int OffsetRect ( RECT lprc, int dx, int dy );

	int OpenClipboard ( HWND hWndNewOwner );

	int OpenDesktop ( String lpszDesktop, DWORD dwFlags, BOOL fInherit, int dwDesiredAccess );

	int OpenIcon ( HWND hWnd );

	int OpenInputDesktop ( DWORD dwFlags, BOOL fInherit, int dwDesiredAccess );

	int OpenWindowStation ( String lpszWinSta, BOOL fInherit, int dwDesiredAccess );

	int PackDDElParam ( UINT msg, UINT_PTR uiLo, UINT_PTR uiHi );

	int PaintDesktop ( HDC hdc );

	int PeekMessage ( MSG lpMsg, HWND hWnd, UINT wMsgFilterMin, UINT wMsgFilterMax, UINT wRemoveMsg );

	int PhysicalToLogicalPoint ( HWND hWnd, POINT lpPoint );

	int PostMessage ( HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam );

	void PostQuitMessage ( int nExitCode );

	int PostThreadMessage ( DWORD idThread, UINT Msg, WPARAM wParam, LPARAM lParam );

	int PrintWindow ( HWND hwnd, HDC hdcBlt, UINT nFlags );

	int PrivateExtractIcons ( String lpszFile, int nIconIndex, int cxIcon, int cyIcon, HANDLEByReference phicon, IntByReference piconid, UINT nIcons, UINT flags );

	int PtInRect ( RECT lprc, POINT pt );

	int RealChildWindowFromPoint ( HWND hwndParent, POINT ptParentClientCoords );

	int RealGetWindowClass ( HWND hwnd, String pszType, UINT cchType );

	int RedrawWindow ( HWND hWnd, RECT lprcUpdate, HRGN hrgnUpdate, UINT flags );

	//int RegisterClass ( WNDCLASS lpWndClass );

	int RegisterClassEx ( WNDCLASSEX lpwcx );

	int RegisterClipboardFormat ( String lpszFormat );

	int RegisterHotKey ( HWND hWnd, int id, UINT fsModifiers, UINT vk );

	int RegisterShellHookWindow ( HWND hWnd );

	int RegisterWindowMessage ( String lpString );

	int ReleaseCapture ( );

	int ReleaseDC ( HWND hWnd, HDC hDC );

	int RemoveMenu ( HMENU hMenu, UINT uPosition, UINT uFlags );

	int RemoveProp ( HWND hWnd, String lpString );

	int ReplyMessage ( LRESULT lResult );

	int ReuseDDElParam ( LPARAM lParam, UINT msgIn, UINT msgOut, UINT_PTR uiLo, UINT_PTR uiHi );

	int ScreenToClient ( HWND hWnd, POINT lpPoint );

	int ScrollDC ( HDC hDC, int dx, int dy, RECT lprcScroll, RECT lprcClip, HRGN hrgnUpdate, RECT lprcUpdate );

	int ScrollWindow ( HWND hWnd, int XAmount, int YAmount, RECT lpRect, RECT lpClipRect );

	int ScrollWindowEx ( HWND hWnd, int dx, int dy, RECT prcScroll, RECT prcClip, HRGN hrgnUpdate, RECT prcUpdate, UINT flags );

	int SendDlgItemMessage ( HWND hDlg, int nIDDlgItem, UINT Msg, WPARAM wParam, LPARAM lParam );

	int SendInput ( UINT nInputs, INPUT pInputs, int cbSize );

	int SendMessage ( HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam );

	int SendMessageTimeout ( HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam, UINT fuFlags, UINT uTimeout, IntByReference lpdwResult );

	int SendNotifyMessage ( HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam );

	int SetActiveWindow ( HWND hWnd );

	int SetCapture ( HWND hWnd );

	int SetCaretBlinkTime ( UINT uMSeconds );

	int SetCaretPos ( int X, int Y );

	int SetClassLong ( HWND hWnd, int nIndex, LONG dwNewLong );

	int SetClassLongPtr ( HWND hWnd, int nIndex, LONG_PTR dwNewLong );

	int SetClassWord ( HWND hWnd, int nIndex, WORD wNewWord );

	int SetClipboardData ( UINT uFormat, HANDLE hMem );

	int SetClipboardViewer ( HWND hWndNewViewer );

	int SetCursor ( HCURSOR hCursor );

	int SetCursorPos ( int X, int Y );

	int SetDlgItemInt ( HWND hDlg, int nIDDlgItem, UINT uValue, BOOL bSigned );

	int SetDlgItemText ( HWND hDlg, int nIDDlgItem, String lpString );

	int SetDoubleClickTime ( UINT uInterval );

	int SetFocus ( HWND hWnd );

	int SetForegroundWindow ( HWND hWnd );

	int SetKeyboardState ( byte lpKeyState );

	void SetLastErrorEx ( DWORD dwErrCode, DWORD dwType );

	int SetLayeredWindowAttributes ( HWND hwnd, int crKey, BYTE bAlpha, DWORD dwFlags );

	int SetMenu ( HWND hWnd, HMENU hMenu );

	int SetMenuDefaultItem ( HMENU hMenu, UINT uItem, UINT fByPos );

	int SetMenuInfo ( HMENU hmenu, MENUINFO lpcmi );

	int SetMenuItemBitmaps ( HMENU hMenu, UINT uPosition, UINT uFlags, HBITMAP hBitmapUnchecked, HBITMAP hBitmapChecked );

	int SetMenuItemInfo ( HMENU hMenu, UINT uItem, BOOL fByPosition, MENUITEMINFO lpmii );

	int SetMessageExtraInfo ( LPARAM lParam );

	int SetParent ( HWND hWndChild, HWND hWndNewParent );

	int SetPhysicalCursorPos ( int X, int Y );

	int SetProcessDefaultLayout ( DWORD dwDefaultLayout );

	int SetProcessWindowStation ( HANDLE hWinSta );

	int SetProp ( HWND hWnd, String lpString, HANDLE hData );

	int SetRect ( RECT lprc, int xLeft, int yTop, int xRight, int yBottom );

	int SetRectEmpty ( RECT lprc );

	int SetScrollInfo ( HWND hwnd, int fnBar, SCROLLINFO lpsi, BOOL fRedraw );

	int SetScrollPos ( HWND hWnd, int nBar, int nPos, BOOL bRedraw );

	int SetScrollRange ( HWND hWnd, int nBar, int nMinPos, int nMaxPos, BOOL bRedraw );

	int SetSysColors ( int cElements, IntByReference lpaElements, IntByReference lpaRgbValues );

	int SetSystemCursor ( HCURSOR hcur, DWORD id );

	int SetThreadDesktop ( HANDLE hDesktop );

	int SetWindowLong ( HWND hWnd, int nIndex, LONG dwNewLong );

	int SetWindowLongPtr ( HWND hWnd, int nIndex, LONG_PTR dwNewLong );

	int SetWindowPlacement ( HWND hWnd, WINDOWPLACEMENT lpwndpl );

	int SetWindowPos ( HWND hWnd, HWND hWndInsertAfter, int X, int Y, int cx, int cy, UINT uFlags );

	int SetWindowRgn ( HWND hWnd, HRGN hRgn, BOOL bRedraw );

	int SetWindowsHookEx ( int idHook, HOOKPROC lpfn, HINSTANCE hMod, DWORD dwThreadId );

	int SetWindowText ( HWND hWnd, String lpString );

	int ShowCaret ( HWND hWnd );

	int ShowCursor ( BOOL bShow );

	int ShowOwnedPopups ( HWND hWnd, BOOL fShow );

	int ShowScrollBar ( HWND hWnd, int wBar, BOOL bShow );

	int ShowWindow ( HWND hWnd, int nCmdShow );

	int ShowWindowAsync ( HWND hWnd, int nCmdShow );

	int ShutdownBlockReasonCreate ( HWND hWnd, CHARByReference pwszReason );

	int ShutdownBlockReasonDestroy ( HWND hWnd );

	int ShutdownBlockReasonQuery ( HWND hWnd, char[] pwszBuff, IntByReference pcchBuff );

	int SubtractRect ( RECT lprcDst, RECT lprcSrc1, RECT lprcSrc2 );

	int SwapMouseButton ( BOOL fSwap );

	int SwitchDesktop ( HANDLE hDesktop );

	void SwitchToThisWindow ( HWND hWnd, BOOL fAltTab );

	int TabbedTextOut ( HDC hDC, int X, int Y, String lpString, int nCount, int nTabPositions, int[] lpnTabStopPositions, int nTabOrigin );

	int TileWindows ( HWND hwndParent, UINT wHow, RECT lpRect, UINT cKids, HANDLEByReference lpKids );

	int ToAscii ( UINT uVirtKey, UINT uScanCode, byte lpKeyState, ShortByReference lpChar, UINT uFlags );

	int ToAsciiEx ( UINT uVirtKey, UINT uScanCode, byte lpKeyState, short[] lpChar, UINT uFlags, HANDLE dwhkl );

	int ToUnicode ( UINT wVirtKey, UINT wScanCode, byte lpKeyState, CHARByReference pwszBuff, int cchBuff, UINT wFlags );

	int ToUnicodeEx ( UINT wVirtKey, UINT wScanCode, byte lpKeyState, CHARByReference pwszBuff, int cchBuff, UINT wFlags, HANDLE dwhkl );

	int TrackMouseEvent ( TRACKMOUSEEVENT lpEventTrack );

	int TrackPopupMenu ( HMENU hMenu, UINT uFlags, int x, int y, int nReserved, HWND hWnd, RECT prcRect );

	int TrackPopupMenuEx ( HMENU hmenu, UINT fuFlags, int x, int y, HWND hwnd, TPMPARAMS lptpm );

	int TranslateAccelerator ( HWND hWnd, HANDLE hAccTable, MSG lpMsg );

	int TranslateMDISysAccel ( HWND hWndClient, MSG lpMsg );

	int TranslateMessage ( MSG lpMsg );

	int UnhookWindowsHookEx ( HHOOK hhk );

	int UnionRect ( RECT lprcDst, RECT lprcSrc1, RECT lprcSrc2 );

	int UnloadKeyboardLayout ( HANDLE hkl );

	int UnpackDDElParam ( UINT msg, LPARAM lParam, IntByReference puiLo, IntByReference puiHi );

	int UnregisterClass ( String lpClassName, HINSTANCE hInstance );

	int UnregisterDeviceNotification ( HDEVNOTIFY Handle );

	int UnregisterHotKey ( HWND hWnd, int id );

	int UpdateLayeredWindow ( HWND hwnd, HDC hdcDst, POINT pptDst, SIZE psize, HDC hdcSrc, POINT pptSrc, int crKey, BLENDFUNCTION pblend, DWORD dwFlags );

	int UpdateWindow ( HWND hWnd );

	int UserHandleGrantAccess ( HANDLE hUserHandle, HANDLE hJob, BOOL bGrant );

	int ValidateRect ( HWND hWnd, RECT lpRect );

	int ValidateRgn ( HWND hWnd, HRGN hRgn );

	int VkKeyScan ( char ch );

	int VkKeyScanEx ( char ch, HANDLE dwhkl );

	int WaitForInputIdle ( HANDLE hProcess, DWORD dwMilliseconds );

	int WaitMessage ( );

	int WindowFromDC ( HDC hDC );

	int WindowFromPoint ( POINT Point );

}