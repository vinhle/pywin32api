/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.user32.functions
 * File name: GetTitleBarInfo.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.user32.functions;

import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.platform.win32.WinDef.HWND;
import com.sun.jna.platform.win32.WinDef.RECT;

import v2.org.analysis.apihandle.structures.TITLEBARINFO;
import v2.org.analysis.apihandle.winapi.user32.User32API;
import v2.org.analysis.apihandle.winapi.user32.User32DLL;
import v2.org.analysis.value.LongValue;
 
public class GetTitleBarInfo extends User32API {
	public GetTitleBarInfo () {
		super();
		NUM_OF_PARMS = 2;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		
		// Step 2: type conversion from C++ to Java
		HWND hwnd = null;
		if ( t0 != 0L ) {
			hwnd = new HWND ();
			hwnd.setPointer(new Pointer(t0));
		}
		TITLEBARINFO pti = null;
		if ( t1 != 0L) {
			pti = new TITLEBARINFO ();
			pti.cbSize = new DWORD (((LongValue)memory.getDoubleWordMemoryValue (t1)).getValue());
			t1 += 4;
			pti.rcTitleBar = new RECT ();
			// Nested Structure
			pti.rcTitleBar.left = (int) ((LongValue)memory.getDoubleWordMemoryValue (t1 += 0)).getValue();
			pti.rcTitleBar.top = (int) ((LongValue)memory.getDoubleWordMemoryValue (t1 += 4)).getValue();
			pti.rcTitleBar.right = (int) ((LongValue)memory.getDoubleWordMemoryValue (t1 += 4)).getValue();
			pti.rcTitleBar.bottom = (int) ((LongValue)memory.getDoubleWordMemoryValue (t1 += 4)).getValue();
			for (int i = 0; i < pti.rgstate.length; i++) {
				pti.rgstate [i] = new DWORD (((LongValue) memory.getDoubleWordMemoryValue (t1)).getValue());
				t1 += 4;
			}
		}

		// Step 3: call API function
		int ret = User32DLL.INSTANCE.GetTitleBarInfo (hwnd, pti);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));

	}
}