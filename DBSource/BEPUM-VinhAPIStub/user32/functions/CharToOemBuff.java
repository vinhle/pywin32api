/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.user32.functions
 * File name: CharToOemBuff.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.user32.functions;

import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.ptr.ByteByReference;

import v2.org.analysis.apihandle.winapi.user32.User32API;
import v2.org.analysis.apihandle.winapi.user32.User32DLL;
import v2.org.analysis.value.LongValue;
 
public class CharToOemBuff extends User32API {
	public CharToOemBuff () {
		super();
		NUM_OF_PARMS = 3;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		long t2 = this.params.get(2);
		
		// Step 2: type conversion from C++ to Java
		String lpszSrc = null;
		if ( t0 != 0L ) lpszSrc = memory.getText(this, t0);
		ByteByReference lpszDst = new ByteByReference ((byte) t1);
		DWORD cchDstLength = new DWORD (t2);

		// Step 3: call API function
		int ret = User32DLL.INSTANCE.CharToOemBuff (lpszSrc, lpszDst, cchDstLength);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));
		memory.setDoubleWordMemoryValue(t1, new LongValue(lpszDst.getValue()));

		

	}
}