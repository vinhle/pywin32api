/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.user32.functions
 * File name: CharPrev.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.user32.functions;

import v2.org.analysis.apihandle.winapi.user32.User32API;
import v2.org.analysis.apihandle.winapi.user32.User32DLL;
import v2.org.analysis.value.LongValue;
 
public class CharPrev extends User32API {
	public CharPrev () {
		super();
		NUM_OF_PARMS = 2;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		
		// Step 2: type conversion from C++ to Java
		String lpszStart = null;
		if ( t0 != 0L ) lpszStart = memory.getText(this, t0);
		String lpszCurrent = null;
		if ( t1 != 0L ) lpszCurrent = memory.getText(this, t1);

		// Step 3: call API function
		int ret = User32DLL.INSTANCE.CharPrev (lpszStart, lpszCurrent);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));

	}
}