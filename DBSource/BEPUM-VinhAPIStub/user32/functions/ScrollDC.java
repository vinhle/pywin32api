/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.user32.functions
 * File name: ScrollDC.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.user32.functions;

import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.WinDef.HDC;
import com.sun.jna.platform.win32.WinDef.HRGN;
import com.sun.jna.platform.win32.WinDef.RECT;

import v2.org.analysis.apihandle.winapi.user32.User32API;
import v2.org.analysis.apihandle.winapi.user32.User32DLL;
import v2.org.analysis.value.LongValue;
 
public class ScrollDC extends User32API {
	public ScrollDC () {
		super();
		NUM_OF_PARMS = 7;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		long t2 = this.params.get(2);
		long t3 = this.params.get(3);
		long t4 = this.params.get(4);
		long t5 = this.params.get(5);
		long t6 = this.params.get(6);
		
		// Step 2: type conversion from C++ to Java
		HDC hDC = null;
		if ( t0 != 0L ) {
			hDC = new HDC ();
			hDC.setPointer(new Pointer(t0));
		}
		int dx = (int) t1;
		int dy = (int) t2;
		RECT lprcScroll = null;
		if ( t3 != 0L) {
			lprcScroll = new RECT ();
			lprcScroll.left = (int) ((LongValue)memory.getDoubleWordMemoryValue (t3)).getValue();
			t3 += 4;
			lprcScroll.top = (int) ((LongValue)memory.getDoubleWordMemoryValue (t3)).getValue();
			t3 += 4;
			lprcScroll.right = (int) ((LongValue)memory.getDoubleWordMemoryValue (t3)).getValue();
			t3 += 4;
			lprcScroll.bottom = (int) ((LongValue)memory.getDoubleWordMemoryValue (t3)).getValue();
			t3 += 4;
		}
		RECT lprcClip = null;
		if ( t4 != 0L) {
			lprcClip = new RECT ();
			lprcClip.left = (int) ((LongValue)memory.getDoubleWordMemoryValue (t4)).getValue();
			t4 += 4;
			lprcClip.top = (int) ((LongValue)memory.getDoubleWordMemoryValue (t4)).getValue();
			t4 += 4;
			lprcClip.right = (int) ((LongValue)memory.getDoubleWordMemoryValue (t4)).getValue();
			t4 += 4;
			lprcClip.bottom = (int) ((LongValue)memory.getDoubleWordMemoryValue (t4)).getValue();
			t4 += 4;
		}
		HRGN hrgnUpdate = null;
		if ( t5 != 0L ) {
			hrgnUpdate = new HRGN ();
			hrgnUpdate.setPointer(new Pointer(t5));
		}
		RECT lprcUpdate = new RECT ();

		// Step 3: call API function
		int ret = User32DLL.INSTANCE.ScrollDC (hDC, dx, dy, lprcScroll, lprcClip, hrgnUpdate, lprcUpdate);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));
		t6 = this.params.get(6);
		memory.setDoubleWordMemoryValue (t6, new LongValue(lprcUpdate.left));
		t6 += 4;
		memory.setDoubleWordMemoryValue (t6, new LongValue(lprcUpdate.top));
		t6 += 4;
		memory.setDoubleWordMemoryValue (t6, new LongValue(lprcUpdate.right));
		t6 += 4;
		memory.setDoubleWordMemoryValue (t6, new LongValue(lprcUpdate.bottom));
		t6 += 4;

	}
}