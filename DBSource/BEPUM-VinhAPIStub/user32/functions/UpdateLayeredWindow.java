/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.user32.functions
 * File name: UpdateLayeredWindow.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.user32.functions;

import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.platform.win32.WinDef.HDC;
import com.sun.jna.platform.win32.WinDef.HWND;
import com.sun.jna.platform.win32.WinDef.POINT;
import com.sun.jna.platform.win32.WinUser.BLENDFUNCTION;
import com.sun.jna.platform.win32.WinUser.SIZE;

import v2.org.analysis.apihandle.winapi.user32.User32API;
import v2.org.analysis.apihandle.winapi.user32.User32DLL;
import v2.org.analysis.value.LongValue;
 
public class UpdateLayeredWindow extends User32API {
	public UpdateLayeredWindow () {
		super();
		NUM_OF_PARMS = 9;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		long t2 = this.params.get(2);
		long t3 = this.params.get(3);
		long t4 = this.params.get(4);
		long t5 = this.params.get(5);
		long t6 = this.params.get(6);
		long t7 = this.params.get(7);
		long t8 = this.params.get(8);
		
		// Step 2: type conversion from C++ to Java
		HWND hwnd = null;
		if ( t0 != 0L ) {
			hwnd = new HWND ();
			hwnd.setPointer(new Pointer(t0));
		}
		HDC hdcDst = null;
		if ( t1 != 0L ) {
			hdcDst = new HDC ();
			hdcDst.setPointer(new Pointer(t1));
		}
		POINT pptDst = null;
		if ( t2 != 0L) {
			pptDst = new POINT ();
			pptDst.x = (int) ((LongValue)memory.getDoubleWordMemoryValue (t2)).getValue();
			t2 += 4;
			pptDst.y = (int) ((LongValue)memory.getDoubleWordMemoryValue (t2)).getValue();
			t2 += 4;
		}
		SIZE psize = null;
		if ( t3 != 0L) {
			psize = new SIZE ();
			psize.cx = (int) ((LongValue)memory.getDoubleWordMemoryValue (t3)).getValue();
			t3 += 4;
			psize.cy = (int) ((LongValue)memory.getDoubleWordMemoryValue (t3)).getValue();
			t3 += 4;
		}
		HDC hdcSrc = null;
		if ( t4 != 0L ) {
			hdcSrc = new HDC ();
			hdcSrc.setPointer(new Pointer(t4));
		}
		POINT pptSrc = null;
		if ( t5 != 0L) {
			pptSrc = new POINT ();
			pptSrc.x = (int) ((LongValue)memory.getDoubleWordMemoryValue (t5)).getValue();
			t5 += 4;
			pptSrc.y = (int) ((LongValue)memory.getDoubleWordMemoryValue (t5)).getValue();
			t5 += 4;
		}
		int crKey = (int) t6;
		BLENDFUNCTION pblend = null;
		if ( t7 != 0L) {
			pblend = new BLENDFUNCTION ();
			pblend.BlendOp = (byte) ((LongValue)memory.getByteMemoryValue (t7)).getValue();
			t7 += 1;
			pblend.BlendFlags = (byte) ((LongValue)memory.getByteMemoryValue (t7)).getValue();
			t7 += 1;
			pblend.SourceConstantAlpha = (byte) ((LongValue)memory.getByteMemoryValue (t7)).getValue();
			t7 += 1;
			pblend.AlphaFormat = (byte) ((LongValue)memory.getByteMemoryValue (t7)).getValue();
			t7 += 1;
		}
		DWORD dwFlags = new DWORD (t8);

		// Step 3: call API function
		int ret = User32DLL.INSTANCE.UpdateLayeredWindow (hwnd, hdcDst, pptDst, psize, hdcSrc, pptSrc, crKey, pblend, dwFlags);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));

	}
}