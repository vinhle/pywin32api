/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.user32.functions
 * File name: LoadString.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.user32.functions;

import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.WinDef.HINSTANCE;
import com.sun.jna.platform.win32.WinDef.UINT;

import v2.org.analysis.apihandle.winapi.user32.User32API;
import v2.org.analysis.apihandle.winapi.user32.User32DLL;
import v2.org.analysis.value.LongValue;
 
public class LoadString extends User32API {
	public LoadString () {
		super();
		NUM_OF_PARMS = 4;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		long t2 = this.params.get(2);
		long t3 = this.params.get(3);
		
		// Step 2: type conversion from C++ to Java
		HINSTANCE hInstance = null;
		if ( t0 != 0L ) {
			hInstance = new HINSTANCE ();
			hInstance.setPointer(new Pointer(t0));
		}
		UINT uID = new UINT (t1);
		String lpBuffer = null;
		if ( t2 != 0L ) lpBuffer = memory.getText(this, t2);
		int nBufferMax = (int) t3;

		// Step 3: call API function
		int ret = User32DLL.INSTANCE.LoadString (hInstance, uID, lpBuffer, nBufferMax);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));
		memory.setText(this, t2, new String(lpBuffer));
	}
}