/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.user32.functions
 * File name: SetLastErrorEx.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.user32.functions;

import com.sun.jna.platform.win32.WinDef.DWORD;

import v2.org.analysis.apihandle.winapi.user32.User32API;
import v2.org.analysis.apihandle.winapi.user32.User32DLL;
 
public class SetLastErrorEx extends User32API {
	public SetLastErrorEx () {
		super();
		NUM_OF_PARMS = 2;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		
		// Step 2: type conversion from C++ to Java
		DWORD dwErrCode = new DWORD (t0);
		DWORD dwType = new DWORD (t1);

		// Step 3: call API function
		User32DLL.INSTANCE.SetLastErrorEx (dwErrCode, dwType);
		
		// Step 4: update environment (memory & eax register)

	}
}