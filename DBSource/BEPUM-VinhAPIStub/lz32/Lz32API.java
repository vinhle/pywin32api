package v2.org.analysis.apihandle.winapi.lz32;

import v2.org.analysis.apihandle.winapi.API;

public abstract class Lz32API extends API {
	public Lz32API () {
		this.libraryName = "lz32.dll";
	}
}