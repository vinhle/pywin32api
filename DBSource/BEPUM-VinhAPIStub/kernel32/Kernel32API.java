package v2.org.analysis.apihandle.winapi.kernel32;

import v2.org.analysis.apihandle.winapi.API;

public abstract class Kernel32API extends API {
	public Kernel32API () {
		this.libraryName = "kernel32.dll";
	}
}