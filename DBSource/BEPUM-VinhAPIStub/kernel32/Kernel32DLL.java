package v2.org.analysis.apihandle.winapi.kernel32;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.BaseTSD.DWORD_PTR;
import com.sun.jna.platform.win32.BaseTSD.SIZE_T;
import com.sun.jna.platform.win32.BaseTSD.ULONG_PTR;
import com.sun.jna.platform.win32.Tlhelp32.PROCESSENTRY32;
import com.sun.jna.platform.win32.WinBase.COMMTIMEOUTS;
import com.sun.jna.platform.win32.WinBase.COMPUTER_NAME_FORMAT;
import com.sun.jna.platform.win32.WinBase.DCB;
import com.sun.jna.platform.win32.WinBase.FILETIME;
import com.sun.jna.platform.win32.WinBase.MEMORYSTATUSEX;
import com.sun.jna.platform.win32.WinBase.OVERLAPPED;
import com.sun.jna.platform.win32.WinBase.SECURITY_ATTRIBUTES;
import com.sun.jna.platform.win32.WinBase.STARTUPINFO;
import com.sun.jna.platform.win32.WinBase.SYSTEMTIME;
import com.sun.jna.platform.win32.WinBase.SYSTEM_INFO;
import com.sun.jna.platform.win32.WinBase.TIME_ZONE_INFORMATION;
import com.sun.jna.platform.win32.WinDef.ATOM;
import com.sun.jna.platform.win32.WinDef.BOOL;
import com.sun.jna.platform.win32.WinDef.BYTE;
import com.sun.jna.platform.win32.WinDef.CHARByReference;
import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.platform.win32.WinDef.DWORDLONG;
import com.sun.jna.platform.win32.WinDef.HMODULE;
import com.sun.jna.platform.win32.WinDef.HRSRC;
import com.sun.jna.platform.win32.WinDef.HWND;
import com.sun.jna.platform.win32.WinDef.LCID;
import com.sun.jna.platform.win32.WinDef.LONG;
import com.sun.jna.platform.win32.WinDef.LONGLONG;
import com.sun.jna.platform.win32.WinDef.UCHAR;
import com.sun.jna.platform.win32.WinDef.UINT;
import com.sun.jna.platform.win32.WinDef.UINT_PTR;
import com.sun.jna.platform.win32.WinDef.ULONG;
import com.sun.jna.platform.win32.WinDef.ULONGLONG;
import com.sun.jna.platform.win32.WinDef.WORD;
import com.sun.jna.platform.win32.WinNT;
import com.sun.jna.platform.win32.WinNT.HANDLE;
import com.sun.jna.platform.win32.WinNT.HANDLEByReference;
import com.sun.jna.platform.win32.WinNT.LARGE_INTEGER;
import com.sun.jna.platform.win32.WinNT.MEMORY_BASIC_INFORMATION;
import com.sun.jna.platform.win32.WinNT.OSVERSIONINFOEX;
import com.sun.jna.platform.win32.WinNT.PSID;
import com.sun.jna.platform.win32.WinNT.SYSTEM_LOGICAL_PROCESSOR_INFORMATION;
import com.sun.jna.ptr.ByteByReference;
import com.sun.jna.ptr.DoubleByReference;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.LongByReference;
import com.sun.jna.ptr.PointerByReference;
import com.sun.jna.ptr.ShortByReference;
import com.sun.jna.win32.StdCallLibrary;
import com.sun.jna.win32.W32APIOptions;

import v2.org.analysis.apihandle.structures.BY_HANDLE_FILE_INFORMATION;
import v2.org.analysis.apihandle.structures.CHAR_INFO;
import v2.org.analysis.apihandle.structures.COMMCONFIG;
import v2.org.analysis.apihandle.structures.COMMPROP;
import v2.org.analysis.apihandle.structures.COMSTAT;
import v2.org.analysis.apihandle.structures.CONSOLE_CURSOR_INFO;
import v2.org.analysis.apihandle.structures.CONSOLE_FONT_INFO;
import v2.org.analysis.apihandle.structures.CONSOLE_FONT_INFOEX;
import v2.org.analysis.apihandle.structures.CONSOLE_HISTORY_INFO;
import v2.org.analysis.apihandle.structures.CONSOLE_SCREEN_BUFFER_INFO;
import v2.org.analysis.apihandle.structures.CONSOLE_SCREEN_BUFFER_INFOEX;
import v2.org.analysis.apihandle.structures.CONSOLE_SELECTION_INFO;
import v2.org.analysis.apihandle.structures.COORD;
import v2.org.analysis.apihandle.structures.CPINFO;
import v2.org.analysis.apihandle.structures.CPINFOEX;
import v2.org.analysis.apihandle.structures.CURRENCYFMT;
import v2.org.analysis.apihandle.structures.DYNAMIC_TIME_ZONE_INFORMATION;
import v2.org.analysis.apihandle.structures.FILE_SEGMENT_ELEMENT;
import v2.org.analysis.apihandle.structures.HEAPENTRY32;
import v2.org.analysis.apihandle.structures.HEAPLIST32;
import v2.org.analysis.apihandle.structures.INPUT_RECORD;
import v2.org.analysis.apihandle.structures.IO_COUNTERS;
import v2.org.analysis.apihandle.structures.LDT_ENTRY;
import v2.org.analysis.apihandle.structures.MEMORYSTATUS;
import v2.org.analysis.apihandle.structures.MODULEENTRY32;
import v2.org.analysis.apihandle.structures.MODULEINFO;
import v2.org.analysis.apihandle.structures.NUMBERFMT;
import v2.org.analysis.apihandle.structures.OFSTRUCT;
import v2.org.analysis.apihandle.structures.OVERLAPPED_ENTRY;
import v2.org.analysis.apihandle.structures.PERFORMANCE_INFORMATION;
import v2.org.analysis.apihandle.structures.PROCESS_HEAP_ENTRY;
import v2.org.analysis.apihandle.structures.PROCESS_MEMORY_COUNTERS;
import v2.org.analysis.apihandle.structures.PSAPI_WS_WATCH_INFORMATION;
import v2.org.analysis.apihandle.structures.SMALL_RECT;
import v2.org.analysis.apihandle.structures.SYSTEM_POWER_STATUS;
import v2.org.analysis.apihandle.structures.THREADENTRY32;
import v2.org.analysis.apihandle.structures.ULARGE_INTEGER;
import v2.org.analysis.apihandle.structures.WIN32_FIND_DATA;


public interface Kernel32DLL extends StdCallLibrary {
	Kernel32DLL INSTANCE = (Kernel32DLL) Native.loadLibrary("kernel32", Kernel32DLL.class, W32APIOptions.DEFAULT_OPTIONS);
	
	// API's Interfaces
	int AddAtom ( String lpString );

	int AddSIDToBoundaryDescriptor ( HANDLEByReference BoundaryDescriptor, PSID RequiredSid );

	int AllocateUserPhysicalPages ( HANDLE hProcess, IntByReference NumberOfPages, int[] UserPfnArray );

	int AllocateUserPhysicalPagesNuma ( HANDLE hProcess, IntByReference NumberOfPages, int[] PageArray, DWORD nndPreferred );

	int AllocConsole ( );

	void ApplicationRecoveryFinished ( BOOL bSuccess );

	int ApplicationRecoveryInProgress ( IntByReference pbCanceled );

	int AreFileApisANSI ( );

	int AssignProcessToJobObject ( HANDLE hJob, HANDLE hProcess );

	int AttachConsole ( DWORD dwProcessId );

	int Beep ( DWORD dwFreq, DWORD dwDuration );

	int BeginUpdateResource ( String pFileName, BOOL bDeleteExistingResources );

	int BindIoCompletionCallback ( HANDLE FileHandle, WinNT.OVERLAPPED_COMPLETION_ROUTINE Function, ULONG Flags );

	int BuildCommDCB ( String lpDef, DCB lpDCB );

	int BuildCommDCBAndTimeouts ( String lpDef, DCB lpDCB, COMMTIMEOUTS lpCommTimeouts );

	int CancelIo ( HANDLE hFile );

	int CancelIoEx ( HANDLE hFile, OVERLAPPED lpOverlapped );

	int CancelSynchronousIo ( HANDLE hThread );

	int CancelWaitableTimer ( HANDLE hTimer );

	int ChangeTimerQueueTimer ( HANDLE TimerQueue, HANDLE Timer, ULONG DueTime, ULONG Period );

	int CheckNameLegalDOS8Dot3 ( String lpName, byte[] lpOemName, DWORD OemNameSize, IntByReference pbNameContainsSpaces, IntByReference pbNameLegal );

	int CheckRemoteDebuggerPresent ( HANDLE hProcess, IntByReference pbDebuggerPresent );

	int ClearCommBreak ( HANDLE hFile );

	int ClearCommError ( HANDLE hFile, IntByReference lpErrors, COMSTAT lpStat );

	int CloseHandle ( HANDLE hObject );

	int ClosePrivateNamespace ( HANDLE Handle, ULONG Flags );

	int CommConfigDialog ( String lpszName, HWND hWnd, COMMCONFIG lpCC );

	int CompareFileTime ( FILETIME lpFileTime1, FILETIME lpFileTime2 );

	int CompareString ( LCID Locale, DWORD dwCmpFlags, String lpString1, int cchCount1, String lpString2, int cchCount2 );

	int ConnectNamedPipe ( HANDLE hNamedPipe, OVERLAPPED lpOverlapped );

	int ContinueDebugEvent ( DWORD dwProcessId, DWORD dwThreadId, DWORD dwContinueStatus );

	int ConvertDefaultLocale ( LCID Locale );

	int ConvertFiberToThread ( );

	int CopyFile ( String lpExistingFileName, String lpNewFileName, BOOL bFailIfExists );

	int CreateBoundaryDescriptor ( String Name, ULONG Flags );

	int CreateDirectory ( String lpPathName, SECURITY_ATTRIBUTES lpSecurityAttributes );

	int CreateDirectoryEx ( String lpTemplateDirectory, String lpNewDirectory, SECURITY_ATTRIBUTES lpSecurityAttributes );

	int CreateEvent ( SECURITY_ATTRIBUTES lpEventAttributes, BOOL bManualReset, BOOL bInitialState, String lpName );

	int CreateEventEx ( SECURITY_ATTRIBUTES lpEventAttributes, String lpName, DWORD dwFlags, DWORD dwDesiredAccess );

	int CreateFile ( String lpFileName, DWORD dwDesiredAccess, DWORD dwShareMode, SECURITY_ATTRIBUTES lpSecurityAttributes, DWORD dwCreationDisposition, DWORD dwFlagsAndAttributes, HANDLE hTemplateFile );

	int CreateFileMapping ( HANDLE hFile, SECURITY_ATTRIBUTES lpAttributes, DWORD flProtect, DWORD dwMaximumSizeHigh, DWORD dwMaximumSizeLow, String lpName );

	int CreateFileMappingNuma ( HANDLE hFile, SECURITY_ATTRIBUTES lpFileMappingAttributes, DWORD flProtect, DWORD dwMaximumSizeHigh, DWORD dwMaximumSizeLow, String lpName, DWORD nndPreferred );

	int CreateHardLink ( String lpFileName, String lpExistingFileName, SECURITY_ATTRIBUTES lpSecurityAttributes );

	int CreateIoCompletionPort ( HANDLE FileHandle, HANDLE ExistingCompletionPort, ULONG_PTR CompletionKey, DWORD NumberOfConcurrentThreads );

	int CreateJobObject ( SECURITY_ATTRIBUTES lpJobAttributes, String lpName );

	int CreateMailslot ( String lpName, DWORD nMaxMessageSize, DWORD lReadTimeout, SECURITY_ATTRIBUTES lpSecurityAttributes );

	int CreateMutex ( SECURITY_ATTRIBUTES lpMutexAttributes, BOOL bInitialOwner, String lpName );

	int CreateMutexEx ( SECURITY_ATTRIBUTES lpMutexAttributes, String lpName, DWORD dwFlags, DWORD dwDesiredAccess );

	int CreateNamedPipe ( String lpName, DWORD dwOpenMode, DWORD dwPipeMode, DWORD nMaxInstances, DWORD nOutBufferSize, DWORD nInBufferSize, DWORD nDefaultTimeOut, SECURITY_ATTRIBUTES lpSecurityAttributes );

	int CreatePipe ( HANDLEByReference hReadPipe, HANDLEByReference hWritePipe, SECURITY_ATTRIBUTES lpPipeAttributes, DWORD nSize );

	int CreateSemaphore ( SECURITY_ATTRIBUTES lpSemaphoreAttributes, LONG lInitialCount, LONG lMaximumCount, String lpName );

	int CreateSemaphoreEx ( SECURITY_ATTRIBUTES lpSemaphoreAttributes, LONG lInitialCount, LONG lMaximumCount, String lpName, DWORD dwFlags, DWORD dwDesiredAccess );

	int CreateSymbolicLink ( String lpSymlinkFileName, String lpTargetFileName, DWORD dwFlags );

	int CreateTapePartition ( HANDLE hDevice, DWORD dwPartitionMethod, DWORD dwCount, DWORD dwSize );

	int CreateTimerQueue ( );

	int CreateToolhelp32Snapshot ( DWORD dwFlags, DWORD th32ProcessID );

	int CreateWaitableTimer ( SECURITY_ATTRIBUTES lpTimerAttributes, BOOL bManualReset, String lpTimerName );

	int CreateWaitableTimerEx ( SECURITY_ATTRIBUTES lpTimerAttributes, String lpTimerName, DWORD dwFlags, DWORD dwDesiredAccess );

	int DebugActiveProcess ( DWORD dwProcessId );

	int DebugActiveProcessStop ( DWORD dwProcessId );

	void DebugBreak ( );

	int DebugBreakProcess ( HANDLE Process );

	int DebugSetProcessKillOnExit ( BOOL KillOnExit );

	int DefineDosDevice ( DWORD dwFlags, String lpDeviceName, String lpTargetPath );

	int DeleteAtom ( ATOM nAtom );

	void DeleteBoundaryDescriptor ( HANDLE BoundaryDescriptor );

	int DeleteFile ( String lpFileName );

	int DeleteTimerQueue ( HANDLE TimerQueue );

	int DeleteTimerQueueEx ( HANDLE TimerQueue, HANDLE CompletionEvent );

	int DeleteTimerQueueTimer ( HANDLE TimerQueue, HANDLE Timer, HANDLE CompletionEvent );

	int DeleteVolumeMountPoint ( String lpszVolumeMountPoint );

	int DisableThreadLibraryCalls ( HMODULE hModule );

	int DisconnectNamedPipe ( HANDLE hNamedPipe );

	int DnsHostnameToComputerName ( String Hostname, char[] ComputerName, IntByReference nSize );

	int DosDateTimeToFileTime ( WORD wFatDate, WORD wFatTime, FILETIME lpFileTime );

	int DuplicateHandle ( HANDLE hSourceProcessHandle, HANDLE hSourceHandle, HANDLE hTargetProcessHandle, HANDLE lpTargetHandle, DWORD dwDesiredAccess, BOOL bInheritHandle, DWORD dwOptions );

	int EmptyWorkingSet ( HANDLE hProcess );

	int EndUpdateResource ( HANDLE hUpdate, BOOL fDiscard );

	int EnumProcesses ( int[] pProcessIds, DWORD cb, IntByReference pBytesReturned );

	int EnumProcessModules ( HANDLE hProcess, HANDLEByReference lphModule, DWORD cb, IntByReference lpcbNeeded );

	int EnumProcessModulesEx ( HANDLE hProcess, HANDLEByReference lphModule, DWORD cb, IntByReference lpcbNeeded, DWORD dwFilterFlag );

	int EraseTape ( HANDLE hDevice, DWORD dwEraseType, BOOL bImmediate );

	int EscapeCommFunction ( HANDLE hFile, DWORD dwFunc );

	void ExitProcess ( UINT uExitCode );

	void ExitThread ( DWORD dwExitCode );

	int ExpandEnvironmentStrings ( String lpSrc, char[] lpDst, DWORD nSize );

	void FatalAppExit ( UINT uAction, String lpMessageText );

	void FatalExit ( int ExitCode );

	int FileTimeToDosDateTime ( FILETIME lpFileTime, ShortByReference lpFatDate, ShortByReference lpFatTime );

	int FileTimeToLocalFileTime ( FILETIME lpFileTime, FILETIME lpLocalFileTime );

	int FileTimeToSystemTime ( FILETIME lpFileTime, SYSTEMTIME lpSystemTime );

	int FillConsoleOutputAttribute ( HANDLE hConsoleOutput, WORD wAttribute, DWORD nLength, COORD dwWriteCoord, IntByReference lpNumberOfAttrsWritten );

	int FillConsoleOutputCharacter ( HANDLE hConsoleOutput, char cCharacter, DWORD nLength, COORD dwWriteCoord, int[] lpNumberOfCharsWritten );

	int FindAtom ( String lpString );

	int FindClose ( HANDLE hFindFile );

	int FindCloseChangeNotification ( HANDLE hChangeHandle );

	int FindFirstChangeNotification ( String lpPathName, BOOL bWatchSubtree, DWORD dwNotifyFilter );

	int FindFirstFile ( String lpFileName, WIN32_FIND_DATA lpFindFileData );

	int FindFirstFileNameW ( CHARByReference lpFileName, DWORD dwFlags, IntByReference StringLength, char[] LinkName );

	int FindFirstVolume ( char[] lpszVolumeName, DWORD cchBufferLength );

	int FindFirstVolumeMountPoint ( String lpszRootPathName, char[] lpszVolumeMountPoint, DWORD cchBufferLength );

	int FindNextChangeNotification ( HANDLE hChangeHandle );

	int FindNextFile ( HANDLE hFindFile, WIN32_FIND_DATA lpFindFileData );

	int FindNextFileNameW ( HANDLE hFindStream, IntByReference StringLength, char LinkName );

	int FindNextVolume ( HANDLE hFindVolume, String lpszVolumeName, DWORD cchBufferLength );

	int FindNextVolumeMountPoint ( HANDLE hFindVolumeMountPoint, char[] lpszVolumeMountPoint, DWORD cchBufferLength );

	int FindResource ( HMODULE hModule, String lpName, String lpType );

	int FindResourceEx ( HMODULE hModule, String lpType, String lpName, WORD wLanguage );

	int FindVolumeClose ( HANDLE hFindVolume );

	int FindVolumeMountPointClose ( HANDLE hFindVolumeMountPoint );

	int FlsFree ( DWORD dwFlsIndex );

	int FlushConsoleInputBuffer ( HANDLE hConsoleInput );

	int FlushFileBuffers ( HANDLE hFile );

	int FlushInstructionCache ( HANDLE hProcess, Pointer lpBaseAddress, SIZE_T dwSize );

	int FlushViewOfFile ( Pointer lpBaseAddress, SIZE_T dwNumberOfBytesToFlush );

	int FoldString ( DWORD dwMapFlags, String lpSrcStr, int cchSrc, char[] lpDestStr, int cchDest );

	int FormatMessage ( DWORD dwFlags, Pointer lpSource, DWORD dwMessageId, DWORD dwLanguageId, char[] lpBuffer, DWORD nSize, Pointer Arguments );

	int FreeConsole ( );

	int FreeEnvironmentStrings ( Pointer lpszEnvironmentBlock );

	int FreeLibrary ( HMODULE hModule );

	void FreeLibraryAndExitThread ( HMODULE hModule, DWORD dwExitCode );

	int FreeUserPhysicalPages ( HANDLE hProcess, IntByReference NumberOfPages, int[] UserPfnArray );

	int GenerateConsoleCtrlEvent ( DWORD dwCtrlEvent, DWORD dwProcessGroupId );

	int GetACP ( );

	int GetApplicationRestartSettings ( HANDLE hProcess, char[] pwzCommandline, IntByReference pcchSize, IntByReference pdwFlags );

	int GetAtomName ( ATOM nAtom, String lpBuffer, int nSize );

	int GetBinaryType ( String lpApplicationName, IntByReference lpBinaryType );

	int GetCalendarInfo ( LCID Locale, int Calendar, int CalType, char[] lpCalData, int cchData, IntByReference lpValue );

	int GetCommandLine ( );

	int GetCommConfig ( HANDLE hCommDev, COMMCONFIG lpCC, IntByReference lpdwSize );

	int GetCommMask ( HANDLE hFile, IntByReference lpEvtMask );

	int GetCommModemStatus ( HANDLE hFile, IntByReference lpModemStat );

	int GetCommProperties ( HANDLE hFile, COMMPROP lpCommProp );

	int GetCommState ( HANDLE hFile, DCB lpDCB );

	int GetCommTimeouts ( HANDLE hFile, COMMTIMEOUTS lpCommTimeouts );

	int GetCompressedFileSize ( String lpFileName, IntByReference lpFileSizeHigh );

	int GetComputerName ( char[] lpBuffer, IntByReference lpnSize );

	int GetComputerNameEx ( COMPUTER_NAME_FORMAT NameType, char[] lpBuffer, IntByReference lpnSize );

	int GetConsoleCP ( );

	int GetConsoleCursorInfo ( HANDLE hConsoleOutput, CONSOLE_CURSOR_INFO lpConsoleCursorInfo );

	int GetConsoleDisplayMode ( IntByReference lpModeFlags );

	int GetConsoleFontSize ( HANDLE hConsoleOutput, DWORD nFont );

	int GetConsoleHistoryInfo ( CONSOLE_HISTORY_INFO lpConsoleHistoryInfo );

	int GetConsoleMode ( HANDLE hConsoleHandle, IntByReference lpMode );

	int GetConsoleOriginalTitle ( char[] lpConsoleTitle, DWORD nSize );

	int GetConsoleOutputCP ( );

	int GetConsoleProcessList ( int[] lpdwProcessList, DWORD dwProcessCount );

	int GetConsoleScreenBufferInfo ( HANDLE hConsoleOutput, CONSOLE_SCREEN_BUFFER_INFO lpConsoleScreenBufferInfo );

	int GetConsoleScreenBufferInfoEx ( HANDLE hConsoleOutput, CONSOLE_SCREEN_BUFFER_INFOEX lpConsoleScreenBufferInfoEx );

	int GetConsoleSelectionInfo ( CONSOLE_SELECTION_INFO lpConsoleSelectionInfo );

	int GetConsoleTitle ( char[] lpConsoleTitle, DWORD nSize );

	int GetConsoleWindow ( );

	int GetCPInfo ( UINT CodePage, CPINFO lpCPInfo );

	int GetCPInfoEx ( UINT CodePage, DWORD dwFlags, CPINFOEX lpCPInfoEx );

	int GetCurrencyFormat ( LCID Locale, DWORD dwFlags, String lpValue, CURRENCYFMT lpFormat, char[] lpCurrencyStr, int cchCurrency );

	int GetCurrentConsoleFont ( HANDLE hConsoleOutput, BOOL bMaximumWindow, CONSOLE_FONT_INFO lpConsoleCurrentFont );

	int GetCurrentConsoleFontEx ( HANDLE hConsoleOutput, BOOL bMaximumWindow, CONSOLE_FONT_INFOEX lpConsoleCurrentFontEx );

	int GetCurrentDirectory ( DWORD nBufferLength, char[] lpBuffer );

	int GetCurrentProcess ( );

	int GetCurrentProcessId ( );

	int GetCurrentProcessorNumber ( );

	int GetCurrentThread ( );

	int GetCurrentThreadId ( );

	int GetDateFormat ( LCID Locale, DWORD dwFlags, SYSTEMTIME lpDate, String lpFormat, char[] lpDateStr, int cchDate );

	int GetDefaultCommConfig ( String lpszName, COMMCONFIG lpCC, IntByReference lpdwSize );

	int GetDevicePowerState ( HANDLE hDevice, IntByReference pfOn );

	int GetDiskFreeSpace ( String lpRootPathName, IntByReference lpSectorsPerCluster, IntByReference lpBytesPerSector, IntByReference lpNumberOfFreeClusters, IntByReference lpTotalNumberOfClusters );

	int GetDiskFreeSpaceEx ( String lpDirectoryName, ULARGE_INTEGER lpFreeBytesAvailable, ULARGE_INTEGER lpTotalNumberOfBytes, ULARGE_INTEGER lpTotalNumberOfFreeBytes );

	int GetDllDirectory ( DWORD nBufferLength, char[] lpBuffer );

	int GetDriveType ( String lpRootPathName );

	int GetDynamicTimeZoneInformation ( DYNAMIC_TIME_ZONE_INFORMATION pTimeZoneInformation );

	int GetEnvironmentStrings ( );

	int GetEnvironmentVariable ( String lpName, char[] lpBuffer, DWORD nSize );

	int GetErrorMode ( );

	int GetExitCodeProcess ( HANDLE hProcess, IntByReference lpExitCode );

	int GetExitCodeThread ( HANDLE hThread, IntByReference lpExitCode );

	int GetFileAttributes ( String lpFileName );

	int GetFileBandwidthReservation ( HANDLE hFile, IntByReference lpPeriodMilliseconds, IntByReference lpBytesPerPeriod, IntByReference pDiscardable, IntByReference lpTransferSize, IntByReference lpNumOutstandingRequests );

	int GetFileInformationByHandle ( HANDLE hFile, BY_HANDLE_FILE_INFORMATION lpFileInformation );

	int GetFileSize ( HANDLE hFile, IntByReference lpFileSizeHigh );

	int GetFileSizeEx ( HANDLE hFile, LARGE_INTEGER lpFileSize );

	int GetFileTime ( HANDLE hFile, FILETIME lpCreationTime, FILETIME lpLastAccessTime, FILETIME lpLastWriteTime );

	int GetFileType ( HANDLE hFile );

	int GetFinalPathNameByHandle ( HANDLE hFile, char[] lpszFilePath, DWORD cchFilePath, DWORD dwFlags );

	int GetFullPathName ( String lpFileName, DWORD nBufferLength, char[] lpBuffer, PointerByReference lpFilePart );

	int GetGeoInfo ( int Location, int GeoType, char[] lpGeoData, int cchData, short LangId );

	int GetLargePageMinimum ( );

	int GetLargestConsoleWindowSize ( HANDLE hConsoleOutput );

	int GetLastError ( );

	int GetLocaleInfo ( LCID Locale, int LCType, char[] lpLCData, int cchData );

	int GetLocaleInfoEx ( CHARByReference lpLocaleName, int LCType, char[] lpLCData, int cchData );

	void GetLocalTime ( SYSTEMTIME lpSystemTime );

	int GetLogicalDrives ( );

	int GetLogicalDriveStrings ( DWORD nBufferLength, char[] lpBuffer );

	int GetLogicalProcessorInformation ( SYSTEM_LOGICAL_PROCESSOR_INFORMATION Buffer, IntByReference ReturnLength );

	int GetLongPathName ( String lpszShortPath, char[] lpszLongPath, DWORD cchBuffer );

	int GetMailslotInfo ( HANDLE hMailslot, IntByReference lpMaxMessageSize, IntByReference lpNextSize, IntByReference lpMessageCount, IntByReference lpReadTimeout );

	int GetModuleBaseName ( HANDLE hProcess, HMODULE hModule, char[] lpBaseName, DWORD nSize );

	int GetModuleFileName ( HMODULE hModule, char[] lpFilename, DWORD nSize );

	int GetModuleFileNameEx ( HANDLE hProcess, HMODULE hModule, char[] lpFilename, DWORD nSize );

	int GetModuleHandle ( String lpModuleName );

	int GetModuleHandleEx ( DWORD dwFlags, String lpModuleName, HANDLEByReference phModule );

	int GetModuleInformation ( HANDLE hProcess, HMODULE hModule, MODULEINFO lpmodinfo, DWORD cb );

	int GetNamedPipeClientComputerName ( HANDLE Pipe, String ClientComputerName, ULONG ClientComputerNameLength );

	int GetNamedPipeClientProcessId ( HANDLE Pipe, IntByReference ClientProcessId );

	int GetNamedPipeClientSessionId ( HANDLE Pipe, IntByReference ClientSessionId );

	int GetNamedPipeHandleState ( HANDLE hNamedPipe, IntByReference lpState, IntByReference lpCurInstances, IntByReference lpMaxCollectionCount, IntByReference lpCollectDataTimeout, char[] lpUserName, DWORD nMaxUserNameSize );

	int GetNamedPipeInfo ( HANDLE hNamedPipe, IntByReference lpFlags, int[] lpOutBufferSize, int[] lpInBufferSize, IntByReference lpMaxInstances );

	int GetNamedPipeServerProcessId ( HANDLE Pipe, IntByReference ServerProcessId );

	int GetNamedPipeServerSessionId ( HANDLE Pipe, IntByReference ServerSessionId );

	void GetNativeSystemInfo ( SYSTEM_INFO lpSystemInfo );

	int GetNumaAvailableMemoryNode ( UCHAR Node, DoubleByReference AvailableBytes );

	int GetNumaHighestNodeNumber ( IntByReference HighestNodeNumber );

	int GetNumaNodeProcessorMask ( UCHAR Node, DoubleByReference ProcessorMask );

	int GetNumaProcessorNode ( UCHAR Processor, byte NodeNumber );

	int GetNumaProximityNode ( ULONG ProximityId, byte NodeNumber );

	int GetNumberFormat ( LCID Locale, DWORD dwFlags, String lpValue, NUMBERFMT lpFormat, char[] lpNumberStr, int cchNumber );

	int GetNumberFormatEx ( CHARByReference lpLocaleName, DWORD dwFlags, CHARByReference lpValue, NUMBERFMT lpFormat, char[] lpNumberStr, int cchNumber );

	int GetNumberOfConsoleInputEvents ( HANDLE hConsoleInput, IntByReference lpcNumberOfEvents );

	int GetNumberOfConsoleMouseButtons ( IntByReference lpNumberOfMouseButtons );

	int GetOEMCP ( );

	int GetOverlappedResult ( HANDLE hFile, OVERLAPPED lpOverlapped, IntByReference lpNumberOfBytesTransferred, BOOL bWait );

	int GetPerformanceInfo ( PERFORMANCE_INFORMATION pPerformanceInformation, DWORD cb );

	int GetPriorityClass ( HANDLE hProcess );

	int GetPrivateProfileInt ( String lpAppName, String lpKeyName, int nDefault, String lpFileName );

	int GetPrivateProfileSection ( String lpAppName, char[] lpReturnedString, DWORD nSize, String lpFileName );

	int GetPrivateProfileSectionNames ( char[] lpszReturnBuffer, DWORD nSize, String lpFileName );

	int GetPrivateProfileString ( String lpAppName, String lpKeyName, String lpDefault, char[] lpReturnedString, DWORD nSize, String lpFileName );

	int GetProcessAffinityMask ( HANDLE hProcess, IntByReference lpProcessAffinityMask, IntByReference lpSystemAffinityMask );

	int GetProcessHandleCount ( HANDLE hProcess, IntByReference pdwHandleCount );

	int GetProcessHeap ( );

	int GetProcessHeaps ( DWORD NumberOfHeaps, HANDLEByReference ProcessHeaps );

	int GetProcessId ( HANDLE Process );

	int GetProcessIdOfThread ( HANDLE Thread );

	int GetProcessIoCounters ( HANDLE hProcess, IO_COUNTERS lpIoCounters );

	int GetProcessMemoryInfo ( HANDLE Process, PROCESS_MEMORY_COUNTERS ppsmemCounters, DWORD cb );

	int GetProcessPriorityBoost ( HANDLE hProcess, IntByReference pDisablePriorityBoost );

	int GetProcessShutdownParameters ( IntByReference lpdwLevel, IntByReference lpdwFlags );

	int GetProcessTimes ( HANDLE hProcess, FILETIME lpCreationTime, FILETIME lpExitTime, FILETIME lpKernelTime, FILETIME lpUserTime );

	int GetProcessVersion ( DWORD ProcessId );

	int GetProcessWorkingSetSize ( HANDLE hProcess, IntByReference lpMinimumWorkingSetSize, IntByReference lpMaximumWorkingSetSize );

	int GetProcessWorkingSetSizeEx ( HANDLE hProcess, IntByReference lpMinimumWorkingSetSize, IntByReference lpMaximumWorkingSetSize, IntByReference Flags );

	int GetProductInfo ( DWORD dwOSMajorVersion, DWORD dwOSMinorVersion, DWORD dwSpMajorVersion, DWORD dwSpMinorVersion, IntByReference pdwReturnedProductType );

	int GetProfileInt ( String lpAppName, String lpKeyName, int nDefault );

	int GetProfileSection ( String lpAppName, char[] lpReturnedString, DWORD nSize );

	int GetProfileString ( String lpAppName, String lpKeyName, String lpDefault, char[] lpReturnedString, DWORD nSize );

	int GetQueuedCompletionStatus ( HANDLE CompletionPort, IntByReference lpNumberOfBytes, IntByReference lpCompletionKey, PointerByReference lpOverlapped, DWORD dwMilliseconds );

	int GetQueuedCompletionStatusEx ( HANDLE CompletionPort, OVERLAPPED_ENTRY lpCompletionPortEntries, ULONG ulCount, IntByReference ulNumEntriesRemoved, DWORD dwMilliseconds, BOOL fAlertable );

	int GetShortPathName ( String lpszLongPath, char[] lpszShortPath, DWORD cchBuffer );

	void GetStartupInfo ( STARTUPINFO lpStartupInfo );

	int GetStdHandle ( DWORD nStdHandle );

	int GetStringTypeA ( LCID Locale, DWORD dwInfoType, ByteByReference lpSrcStr, int cchSrc, ShortByReference lpCharType );

	int GetStringTypeEx ( LCID Locale, DWORD dwInfoType, String lpSrcStr, int cchSrc, ShortByReference lpCharType );

	int GetStringTypeW ( DWORD dwInfoType, CHARByReference lpSrcStr, int cchSrc, ShortByReference lpCharType );

	int GetSystemDefaultLangID ( );

	int GetSystemDefaultLCID ( );

	int GetSystemDefaultUILanguage ( );

	int GetSystemDirectory ( char[] lpBuffer, UINT uSize );

	int GetSystemFileCacheSize ( IntByReference lpMinimumFileCacheSize, IntByReference lpMaximumFileCacheSize, IntByReference lpFlags );

	void GetSystemInfo ( SYSTEM_INFO lpSystemInfo );

	int GetSystemPowerStatus ( SYSTEM_POWER_STATUS lpSystemPowerStatus );

	int GetSystemRegistryQuota ( IntByReference pdwQuotaAllowed, IntByReference pdwQuotaUsed );

	void GetSystemTime ( SYSTEMTIME lpSystemTime );

	int GetSystemTimeAdjustment ( IntByReference lpTimeAdjustment, IntByReference lpTimeIncrement, IntByReference lpTimeAdjustmentDisabled );

	void GetSystemTimeAsFileTime ( FILETIME lpSystemTimeAsFileTime );

	int GetSystemTimes ( FILETIME lpIdleTime, FILETIME lpKernelTime, FILETIME lpUserTime );

	int GetSystemWindowsDirectory ( char[] lpBuffer, UINT uSize );

	int GetSystemWow64Directory ( char[] lpBuffer, UINT uSize );

	int GetTapePosition ( HANDLE hDevice, DWORD dwPositionType, IntByReference lpdwPartition, IntByReference lpdwOffsetLow, IntByReference lpdwOffsetHigh );

	int GetTapeStatus ( HANDLE hDevice );

	int GetTempFileName ( String lpPathName, String lpPrefixString, UINT uUnique, String lpTempFileName );

	int GetTempPath ( DWORD nBufferLength, char[] lpBuffer );

	int GetThreadId ( HANDLE Thread );

	int GetThreadIOPendingFlag ( HANDLE hThread, IntByReference lpIOIsPending );

	int GetThreadLocale ( );

	int GetThreadPreferredUILanguages ( DWORD dwFlags, IntByReference pulNumLanguages, CHARByReference pwszLanguagesBuffer, IntByReference pcchLanguagesBuffer );

	int GetThreadPriority ( HANDLE hThread );

	int GetThreadPriorityBoost ( HANDLE hThread, IntByReference pDisablePriorityBoost );

	int GetThreadSelectorEntry ( HANDLE hThread, DWORD dwSelector, LDT_ENTRY lpSelectorEntry );

	int GetThreadTimes ( HANDLE hThread, FILETIME lpCreationTime, FILETIME lpExitTime, FILETIME lpKernelTime, FILETIME lpUserTime );

	int GetThreadUILanguage ( );

	int GetTickCount ( );

	int GetTickCount64 ( );

	int GetTimeFormat ( LCID Locale, DWORD dwFlags, SYSTEMTIME lpTime, String lpFormat, char[] lpTimeStr, int cchTime );

	int GetTimeFormatEx ( CHARByReference lpLocaleName, DWORD dwFlags, SYSTEMTIME lpTime, CHARByReference lpFormat, char[] lpTimeStr, int cchTime );

	int GetTimeZoneInformation ( TIME_ZONE_INFORMATION lpTimeZoneInformation );

	int GetUserDefaultLangID ( );

	int GetUserDefaultLCID ( );

	int GetUserDefaultUILanguage ( );

	int GetUserGeoID ( int GeoClass );

	int GetVersion ( );

	int GetVersionEx ( WinNT.OSVERSIONINFOEX lpVersionInfo );

	int GetVolumeInformation ( String lpRootPathName, char[] lpVolumeNameBuffer, DWORD nVolumeNameSize, IntByReference lpVolumeSerialNumber, IntByReference lpMaximumComponentLength, IntByReference lpFileSystemFlags, char[] lpFileSystemNameBuffer, DWORD nFileSystemNameSize );

	int GetVolumeNameForVolumeMountPoint ( String lpszVolumeMountPoint, String lpszVolumeName, DWORD cchBufferLength );

	int GetVolumePathName ( String lpszFileName, String lpszVolumePathName, DWORD cchBufferLength );

	int GetVolumePathNamesForVolumeName ( String lpszVolumeName, char[] lpszVolumePathNames, DWORD cchBufferLength, IntByReference lpcchReturnLength );

	int GetWindowsDirectory ( char[] lpBuffer, UINT uSize );

	int GetWsChanges ( HANDLE hProcess, PSAPI_WS_WATCH_INFORMATION lpWatchInfo, DWORD cb );

	int GlobalAddAtom ( String lpString );

	int GlobalAlloc ( UINT uFlags, SIZE_T dwBytes );

	int GlobalDeleteAtom ( ATOM nAtom );

	int GlobalFindAtom ( String lpString );

	int GlobalFlags ( HANDLE hMem );

	int GlobalFree ( HANDLE hMem );

	int GlobalGetAtomName ( ATOM nAtom, String lpBuffer, int nSize );

	int GlobalHandle ( Pointer pMem );

	void GlobalMemoryStatus ( MEMORYSTATUS lpBuffer );

	int GlobalMemoryStatusEx ( MEMORYSTATUSEX lpBuffer );

	int GlobalReAlloc ( HANDLE hMem, SIZE_T dwBytes, UINT uFlags );

	int GlobalSize ( HANDLE hMem );

	int GlobalUnlock ( HANDLE hMem );

	int Heap32First ( HEAPENTRY32 lphe, DWORD th32ProcessID, ULONG_PTR th32HeapID );

	int Heap32ListFirst ( HANDLE hSnapshot, HEAPLIST32 lphl );

	int Heap32ListNext ( HANDLE hSnapshot, HEAPLIST32 lphl );

	int Heap32Next ( HEAPENTRY32 lphe );

	int HeapCompact ( HANDLE hHeap, DWORD dwFlags );

	int HeapCreate ( DWORD flOptions, SIZE_T dwInitialSize, SIZE_T dwMaximumSize );

	int HeapDestroy ( HANDLE hHeap );

	int HeapLock ( HANDLE hHeap );

	int HeapSize ( HANDLE hHeap, DWORD dwFlags, Pointer lpMem );

	int HeapUnlock ( HANDLE hHeap );

	int HeapValidate ( HANDLE hHeap, DWORD dwFlags, Pointer lpMem );

	int HeapWalk ( HANDLE hHeap, PROCESS_HEAP_ENTRY lpEntry );

	int InitAtomTable ( DWORD nSize );

	int InitializeProcessForWsWatch ( HANDLE hProcess );

	int InterlockedCompareExchange ( IntByReference Destination, LONG Exchange, LONG Comparand );

	int InterlockedDecrement ( IntByReference Addend );

	int InterlockedExchange ( IntByReference Target, LONG Value );

	int InterlockedExchangeAdd ( IntByReference Addend, LONG Value );

	int InterlockedIncrement ( IntByReference Addend );

	int IsBadStringPtr ( String lpsz, UINT_PTR ucchMax );

	int IsDBCSLeadByte ( BYTE TestChar );

	int IsDebuggerPresent ( );

	int IsProcessInJob ( HANDLE ProcessHandle, HANDLE JobHandle, IntByReference Result );

	int IsProcessorFeaturePresent ( DWORD ProcessorFeature );

	int IsSystemResumeAutomatic ( );

	int IsThreadAFiber ( );

	int IsValidCodePage ( UINT CodePage );

	int IsValidLanguageGroup ( int LanguageGroup, DWORD dwFlags );

	int IsValidLocale ( LCID Locale, DWORD dwFlags );

	int IsValidLocaleName ( CHARByReference lpLocaleName );

	int IsWow64Process ( HANDLE hProcess, IntByReference Wow64Process );

	int LCMapString ( LCID Locale, DWORD dwMapFlags, String lpSrcStr, int cchSrc, char[] lpDestStr, int cchDest );

	int LoadLibrary ( String lpFileName );

	int LoadLibraryEx ( String lpFileName, HANDLE hFile, DWORD dwFlags );

	int LoadResource ( HMODULE hModule, HRSRC hResInfo );

	int LocalAlloc ( UINT uFlags, SIZE_T uBytes );

	int LocalFileTimeToFileTime ( FILETIME lpLocalFileTime, FILETIME lpFileTime );

	int LocalFlags ( HANDLE hMem );

	int LocalFree ( HANDLE hMem );

	int LocalHandle ( Pointer pMem );

	int LocalReAlloc ( HANDLE hMem, SIZE_T uBytes, UINT uFlags );

	int LocalSize ( HANDLE hMem );

	int LocalUnlock ( HANDLE hMem );

	int LockFile ( HANDLE hFile, DWORD dwFileOffsetLow, DWORD dwFileOffsetHigh, DWORD nNumberOfBytesToLockLow, DWORD nNumberOfBytesToLockHigh );

	int LockFileEx ( HANDLE hFile, DWORD dwFlags, DWORD dwReserved, DWORD nNumberOfBytesToLockLow, DWORD nNumberOfBytesToLockHigh, OVERLAPPED lpOverlapped );

	int lstrcat ( String lpString1, String lpString2 );

	int lstrcmp ( String lpString1, String lpString2 );

	int lstrcmpi ( String lpString1, String lpString2 );

	int lstrcpy ( String lpString1, String lpString2 );

	int lstrcpyn ( String lpString1, String lpString2, int iMaxLength );

	int lstrlen ( String lpString );

	int MapUserPhysicalPagesScatter ( PointerByReference VirtualAddresses, ULONG_PTR NumberOfPages, IntByReference PageArray );

	int Module32First ( HANDLE hSnapshot, MODULEENTRY32 lpme );

	int Module32Next ( HANDLE hSnapshot, MODULEENTRY32 lpme );

	int MoveFile ( String lpExistingFileName, String lpNewFileName );

	int MoveFileEx ( String lpExistingFileName, String lpNewFileName, DWORD dwFlags );

	int MulDiv ( int nNumber, int nNumerator, int nDenominator );

	int MultiByteToWideChar ( UINT CodePage, DWORD dwFlags, ByteByReference lpMultiByteStr, int cbMultiByte, char[] lpWideCharStr, int cchWideChar );

	int OpenEvent ( DWORD dwDesiredAccess, BOOL bInheritHandle, String lpName );

	int OpenFile ( ByteByReference lpFileName, OFSTRUCT lpReOpenBuff, UINT uStyle );

	int OpenFileMapping ( DWORD dwDesiredAccess, BOOL bInheritHandle, String lpName );

	int OpenJobObject ( DWORD dwDesiredAccess, BOOL bInheritHandles, String lpName );

	int OpenMutex ( DWORD dwDesiredAccess, BOOL bInheritHandle, String lpName );

	int OpenProcess ( DWORD dwDesiredAccess, BOOL bInheritHandle, DWORD dwProcessId );

	int OpenSemaphore ( DWORD dwDesiredAccess, BOOL bInheritHandle, String lpName );

	int OpenThread ( DWORD dwDesiredAccess, BOOL bInheritHandle, DWORD dwThreadId );

	int OpenWaitableTimer ( DWORD dwDesiredAccess, BOOL bInheritHandle, String lpTimerName );

	void OutputDebugString ( String lpOutputString );

	int PeekConsoleInput ( HANDLE hConsoleInput, INPUT_RECORD lpBuffer, DWORD nLength, IntByReference lpNumberOfEventsRead );

	int PostQueuedCompletionStatus ( HANDLE CompletionPort, DWORD dwNumberOfBytesTransferred, ULONG_PTR dwCompletionKey, OVERLAPPED lpOverlapped );

	int PrepareTape ( HANDLE hDevice, DWORD dwOperation, BOOL bImmediate );

	int Process32First ( HANDLE hSnapshot, PROCESSENTRY32 lppe );

	int Process32Next ( HANDLE hSnapshot, PROCESSENTRY32 lppe );

	int PulseEvent ( HANDLE hEvent );

	int PurgeComm ( HANDLE hFile, DWORD dwFlags );

	int QueryDosDevice ( String lpDeviceName, char[] lpTargetPath, DWORD ucchMax );

	int QueryFullProcessImageName ( HANDLE hProcess, DWORD dwFlags, String lpExeName, IntByReference lpdwSize );

	int QueryIdleProcessorCycleTime ( IntByReference BufferLength, LongByReference ProcessorIdleCycleTime );

	int QueryPerformanceCounter ( LARGE_INTEGER lpPerformanceCount );

	int QueryPerformanceFrequency ( LARGE_INTEGER lpFrequency );

	int QueryProcessCycleTime ( HANDLE ProcessHandle, LongByReference CycleTime );

	int QueryThreadCycleTime ( HANDLE ThreadHandle, LongByReference CycleTime );

	void RaiseException ( DWORD dwExceptionCode, DWORD dwExceptionFlags, DWORD nNumberOfArguments, IntByReference lpArguments );

	int ReadConsoleInput ( HANDLE hConsoleInput, INPUT_RECORD lpBuffer, DWORD nLength, IntByReference lpNumberOfEventsRead );

	int ReadConsoleOutput ( HANDLE hConsoleOutput, CHAR_INFO lpBuffer, COORD dwBufferSize, COORD dwBufferCoord, SMALL_RECT lpReadRegion );

	int ReadConsoleOutputAttribute ( HANDLE hConsoleOutput, short[] lpAttribute, DWORD nLength, COORD dwReadCoord, IntByReference lpNumberOfAttrsRead );

	int ReadConsoleOutputCharacter ( HANDLE hConsoleOutput, char[] lpCharacter, DWORD nLength, COORD dwReadCoord, IntByReference lpNumberOfCharsRead );

	int ReadFileScatter ( HANDLE hFile, FILE_SEGMENT_ELEMENT aSegmentArray[], DWORD nNumberOfBytesToRead, IntByReference lpReserved, OVERLAPPED lpOverlapped );

	int RegisterApplicationRestart ( CHARByReference pwzCommandline, DWORD dwFlags );

	int ReleaseMutex ( HANDLE hMutex );

	int ReleaseSemaphore ( HANDLE hSemaphore, LONG lReleaseCount, IntByReference lpPreviousCount );

	int RemoveDirectory ( String lpPathName );

	int ReOpenFile ( HANDLE hOriginalFile, DWORD dwDesiredAccess, DWORD dwShareMode, DWORD dwFlags );

	int RequestWakeupLatency ( int latency );

	int ResetEvent ( HANDLE hEvent );

	int ResumeThread ( HANDLE hThread );

	int ScrollConsoleScreenBuffer ( HANDLE hConsoleOutput, SMALL_RECT lpScrollRectangle, SMALL_RECT lpClipRectangle, COORD dwDestinationOrigin, CHAR_INFO lpFill );

	int SearchPath ( String lpPath, String lpFileName, String lpExtension, DWORD nBufferLength, char[] lpBuffer, PointerByReference lpFilePart );

	int SetCalendarInfo ( LCID Locale, int Calendar, int CalType, String lpCalData );

	int SetCommBreak ( HANDLE hFile );

	int SetCommConfig ( HANDLE hCommDev, COMMCONFIG lpCC, DWORD dwSize );

	int SetCommMask ( HANDLE hFile, DWORD dwEvtMask );

	int SetCommState ( HANDLE hFile, DCB lpDCB );

	int SetCommTimeouts ( HANDLE hFile, COMMTIMEOUTS lpCommTimeouts );

	int SetComputerName ( String lpComputerName );

	int SetComputerNameEx ( COMPUTER_NAME_FORMAT NameType, String lpBuffer );

	int SetConsoleActiveScreenBuffer ( HANDLE hConsoleOutput );

	int SetConsoleCP ( UINT wCodePageID );

	int SetConsoleCursorInfo ( HANDLE hConsoleOutput, CONSOLE_CURSOR_INFO lpConsoleCursorInfo );

	int SetConsoleCursorPosition ( HANDLE hConsoleOutput, COORD dwCursorPosition );

	int SetConsoleHistoryInfo ( CONSOLE_HISTORY_INFO lpConsoleHistoryInfo );

	int SetConsoleMode ( HANDLE hConsoleHandle, DWORD dwMode );

	int SetConsoleOutputCP ( UINT wCodePageID );

	int SetConsoleScreenBufferInfoEx ( HANDLE hConsoleOutput, CONSOLE_SCREEN_BUFFER_INFOEX lpConsoleScreenBufferInfoEx );

	int SetConsoleScreenBufferSize ( HANDLE hConsoleOutput, COORD dwSize );

	int SetConsoleTextAttribute ( HANDLE hConsoleOutput, WORD wAttributes );

	int SetConsoleTitle ( String lpConsoleTitle );

	int SetConsoleWindowInfo ( HANDLE hConsoleOutput, BOOL bAbsolute, SMALL_RECT lpConsoleWindow );

	int SetCurrentConsoleFontEx ( HANDLE hConsoleOutput, BOOL bMaximumWindow, CONSOLE_FONT_INFOEX lpConsoleCurrentFontEx );

	int SetCurrentDirectory ( String lpPathName );

	int SetDefaultCommConfig ( String lpszName, COMMCONFIG lpCC, DWORD dwSize );

	int SetDllDirectory ( String lpPathName );

	int SetDynamicTimeZoneInformation ( DYNAMIC_TIME_ZONE_INFORMATION lpTimeZoneInformation );

	int SetEndOfFile ( HANDLE hFile );

	int SetEnvironmentVariable ( String lpName, String lpValue );

	int SetErrorMode ( UINT uMode );

	int SetEvent ( HANDLE hEvent );

	void SetFileApisToANSI ( );

	void SetFileApisToOEM ( );

	int SetFileAttributes ( String lpFileName, DWORD dwFileAttributes );

	int SetFileBandwidthReservation ( HANDLE hFile, DWORD nPeriodMilliseconds, DWORD nBytesPerPeriod, BOOL bDiscardable, IntByReference lpTransferSize, IntByReference lpNumOutstandingRequests );

	int SetFileCompletionNotificationModes ( HANDLE FileHandle, UCHAR Flags );

	int SetFileIoOverlappedRange ( HANDLE FileHandle, byte OverlappedRangeStart, ULONG Length );

	int SetFilePointer ( HANDLE hFile, LONG lDistanceToMove, IntByReference lpDistanceToMoveHigh, DWORD dwMoveMethod );

	int SetFilePointerEx ( HANDLE hFile, LARGE_INTEGER liDistanceToMove, LARGE_INTEGER lpNewFilePointer, DWORD dwMoveMethod );

	int SetFileShortName ( HANDLE hFile, String lpShortName );

	int SetFileTime ( HANDLE hFile, FILETIME lpCreationTime, FILETIME lpLastAccessTime, FILETIME lpLastWriteTime );

	int SetFileValidData ( HANDLE hFile, LONGLONG ValidDataLength );

	int SetHandleInformation ( HANDLE hObject, DWORD dwMask, DWORD dwFlags );

	void SetLastError ( DWORD dwErrCode );

	int SetLocaleInfo ( LCID Locale, int LCType, String lpLCData );

	int SetLocalTime ( SYSTEMTIME lpSystemTime );

	int SetMailslotInfo ( HANDLE hMailslot, DWORD lReadTimeout );

	int SetNamedPipeHandleState ( HANDLE hNamedPipe, IntByReference lpMode, IntByReference lpMaxCollectionCount, IntByReference lpCollectDataTimeout );

	int SetPriorityClass ( HANDLE hProcess, DWORD dwPriorityClass );

	int SetProcessAffinityMask ( HANDLE hProcess, DWORD_PTR dwProcessAffinityMask );

	int SetProcessPriorityBoost ( HANDLE hProcess, BOOL DisablePriorityBoost );

	int SetProcessShutdownParameters ( DWORD dwLevel, DWORD dwFlags );

	int SetProcessWorkingSetSize ( HANDLE hProcess, SIZE_T dwMinimumWorkingSetSize, SIZE_T dwMaximumWorkingSetSize );

	int SetProcessWorkingSetSizeEx ( HANDLE hProcess, SIZE_T dwMinimumWorkingSetSize, SIZE_T dwMaximumWorkingSetSize, DWORD Flags );

	int SetStdHandle ( DWORD nStdHandle, HANDLE hHandle );

	int SetSystemFileCacheSize ( SIZE_T MinimumFileCacheSize, SIZE_T MaximumFileCacheSize, DWORD Flags );

	int SetSystemPowerState ( BOOL fSuspend, BOOL fForce );

	int SetSystemTime ( SYSTEMTIME lpSystemTime );

	int SetSystemTimeAdjustment ( DWORD dwTimeAdjustment, BOOL bTimeAdjustmentDisabled );

	int SetTapePosition ( HANDLE hDevice, DWORD dwPositionMethod, DWORD dwPartition, DWORD dwOffsetLow, DWORD dwOffsetHigh, BOOL bImmediate );

	int SetThreadAffinityMask ( HANDLE hThread, DWORD_PTR dwThreadAffinityMask );

	int SetThreadExecutionState ( int esFlags );

	int SetThreadIdealProcessor ( HANDLE hThread, DWORD dwIdealProcessor );

	int SetThreadLocale ( LCID Locale );

	int SetThreadPriority ( HANDLE hThread, int nPriority );

	int SetThreadPriorityBoost ( HANDLE hThread, BOOL DisablePriorityBoost );

	int SetThreadStackGuarantee ( IntByReference StackSizeInBytes );

	int SetTimeZoneInformation ( TIME_ZONE_INFORMATION lpTimeZoneInformation );

	int SetupComm ( HANDLE hFile, DWORD dwInQueue, DWORD dwOutQueue );

	int SetUserGeoID ( int GeoId );

	int SetVolumeLabel ( String lpRootPathName, String lpVolumeName );

	int SetVolumeMountPoint ( String lpszVolumeMountPoint, String lpszVolumeName );

	int SignalObjectAndWait ( HANDLE hObjectToSignal, HANDLE hObjectToWaitOn, DWORD dwMilliseconds, BOOL bAlertable );

	int SizeofResource ( HMODULE hModule, HRSRC hResInfo );

	void Sleep ( DWORD dwMilliseconds );

	int SleepEx ( DWORD dwMilliseconds, BOOL bAlertable );

	int SuspendThread ( HANDLE hThread );

	int SwitchToThread ( );

	int SystemTimeToFileTime ( SYSTEMTIME lpSystemTime, FILETIME lpFileTime );

	int SystemTimeToTzSpecificLocalTime ( TIME_ZONE_INFORMATION lpTimeZone, SYSTEMTIME lpUniversalTime, SYSTEMTIME lpLocalTime );

	int TerminateJobObject ( HANDLE hJob, UINT uExitCode );

	int TerminateProcess ( HANDLE hProcess, UINT uExitCode );

	int TerminateThread ( HANDLE hThread, DWORD dwExitCode );

	int Thread32First ( HANDLE hSnapshot, THREADENTRY32 lpte );

	int Thread32Next ( HANDLE hSnapshot, THREADENTRY32 lpte );

	int TlsAlloc ( );

	int TlsFree ( DWORD dwTlsIndex );

	int TransmitCommChar ( HANDLE hFile, char cChar );

	int TzSpecificLocalTimeToSystemTime ( TIME_ZONE_INFORMATION lpTimeZoneInformation, SYSTEMTIME lpLocalTime, SYSTEMTIME lpUniversalTime );

	int UnhandledExceptionFilter ( Pointer ExceptionInfo );

	int UnlockFile ( HANDLE hFile, DWORD dwFileOffsetLow, DWORD dwFileOffsetHigh, DWORD nNumberOfBytesToUnlockLow, DWORD nNumberOfBytesToUnlockHigh );

	int UnlockFileEx ( HANDLE hFile, DWORD dwReserved, DWORD nNumberOfBytesToUnlockLow, DWORD nNumberOfBytesToUnlockHigh, OVERLAPPED lpOverlapped );

	int UnmapViewOfFile ( Pointer lpBaseAddress );

	int UnregisterApplicationRecoveryCallback ( );

	int UnregisterApplicationRestart ( );

	int UnregisterWait ( HANDLE WaitHandle );

	int UnregisterWaitEx ( HANDLE WaitHandle, HANDLE CompletionEvent );

	int VerifyVersionInfo ( OSVERSIONINFOEX lpVersionInfo, DWORD dwTypeMask, DWORDLONG dwlConditionMask );

	int VerSetConditionMask ( ULONGLONG dwlConditionMask, DWORD dwTypeBitMask, BYTE dwConditionMask );

	int VirtualQuery ( Pointer lpAddress, MEMORY_BASIC_INFORMATION lpBuffer, SIZE_T dwLength );

	int VirtualQueryEx ( HANDLE hProcess, Pointer lpAddress, MEMORY_BASIC_INFORMATION lpBuffer, SIZE_T dwLength );

	int WaitCommEvent ( HANDLE hFile, IntByReference lpEvtMask, OVERLAPPED lpOverlapped );

	int WaitForMultipleObjects ( DWORD nCount, HANDLEByReference lpHandles, BOOL bWaitAll, DWORD dwMilliseconds );

	int WaitForMultipleObjectsEx ( DWORD nCount, HANDLEByReference lpHandles, BOOL bWaitAll, DWORD dwMilliseconds, BOOL bAlertable );

	int WaitForSingleObject ( HANDLE hHandle, DWORD dwMilliseconds );

	int WaitForSingleObjectEx ( HANDLE hHandle, DWORD dwMilliseconds, BOOL bAlertable );

	int WaitNamedPipe ( String lpNamedPipeName, DWORD nTimeOut );

	int WerGetFlags ( HANDLE hProcess, IntByReference pdwFlags );

	int WerRegisterFile ( CHARByReference pwzFile, int regFileType, DWORD dwFlags );

	int WerSetFlags ( DWORD dwFlags );

	int WerUnregisterFile ( CHARByReference pwzFilePath );

	int WideCharToMultiByte ( UINT CodePage, DWORD dwFlags, CHARByReference lpWideCharStr, int cchWideChar, byte[] lpMultiByteStr, int cbMultiByte, ByteByReference lpDefaultChar, IntByReference lpUsedDefaultChar );

	int WinExec ( ByteByReference lpCmdLine, UINT uCmdShow );

	int Wow64DisableWow64FsRedirection ( PointerByReference OldValue );

	int Wow64EnableWow64FsRedirection ( byte Wow64FsEnableRedirection );

	int Wow64SuspendThread ( HANDLE hThread );

	int WriteConsoleInput ( HANDLE hConsoleInput, INPUT_RECORD lpBuffer, DWORD nLength, IntByReference lpNumberOfEventsWritten );

	int WriteConsoleOutput ( HANDLE hConsoleOutput, CHAR_INFO lpBuffer, COORD dwBufferSize, COORD dwBufferCoord, SMALL_RECT lpWriteRegion );

	int WriteConsoleOutputAttribute ( HANDLE hConsoleOutput, ShortByReference lpAttribute, DWORD nLength, COORD dwWriteCoord, int[] lpNumberOfAttrsWritten );

	int WriteConsoleOutputCharacter ( HANDLE hConsoleOutput, String lpCharacter, DWORD nLength, COORD dwWriteCoord, IntByReference lpNumberOfCharsWritten );

	int WriteFile ( HANDLE hFile, Pointer lpBuffer, DWORD nNumberOfBytesToWrite, IntByReference lpNumberOfBytesWritten, OVERLAPPED lpOverlapped );

	int WriteFileEx ( HANDLE hFile, Pointer lpBuffer, DWORD nNumberOfBytesToWrite, OVERLAPPED lpOverlapped, WinNT.OVERLAPPED_COMPLETION_ROUTINE lpCompletionRoutine );

	int WriteFileGather ( HANDLE hFile, FILE_SEGMENT_ELEMENT aSegmentArray[], DWORD nNumberOfBytesToWrite, IntByReference lpReserved, OVERLAPPED lpOverlapped );

	int WritePrivateProfileSection ( String lpAppName, String lpString, String lpFileName );

	int WritePrivateProfileString ( String lpAppName, String lpKeyName, String lpString, String lpFileName );

	int WriteProfileSection ( String lpAppName, String lpString );

	int WriteProfileString ( String lpAppName, String lpKeyName, String lpString );

	int WriteTapemark ( HANDLE hDevice, DWORD dwTapemarkType, DWORD dwTapemarkCount, BOOL bImmediate );

}