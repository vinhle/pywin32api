/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.kernel32.functions
 * File name: SearchPath.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.kernel32.functions;

import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.ptr.PointerByReference;

import v2.org.analysis.apihandle.winapi.kernel32.Kernel32API;
import v2.org.analysis.apihandle.winapi.kernel32.Kernel32DLL;
import v2.org.analysis.value.LongValue;
 
public class SearchPath extends Kernel32API {
	public SearchPath () {
		super();
		NUM_OF_PARMS = 6;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		long t2 = this.params.get(2);
		long t3 = this.params.get(3);
		long t4 = this.params.get(4);
		long t5 = this.params.get(5);
		
		// Step 2: type conversion from C++ to Java
		String lpPath = null;
		if ( t0 != 0L ) lpPath = memory.getText(this, t0);
		String lpFileName = null;
		if ( t1 != 0L ) lpFileName = memory.getText(this, t1);
		String lpExtension = null;
		if ( t2 != 0L ) lpExtension = memory.getText(this, t2);
		DWORD nBufferLength = new DWORD (t3);
		char[] lpBuffer = null;
		if ( t4 != 0L ) lpBuffer = new char[(int) t3];
		for (int i = 0; i < lpBuffer.length; i++) {
			lpBuffer [i] = (char) ((LongValue) memory.getByteMemoryValue (t4)).getValue();
			t4 += 1;
		}
		PointerByReference lpFilePart = new PointerByReference (new Pointer(t5));

		// Step 3: call API function
		int ret = Kernel32DLL.INSTANCE.SearchPath (lpPath, lpFileName, lpExtension, nBufferLength, lpBuffer, lpFilePart);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));
		t4 = this.params.get(4);
		for (int i = 0; i < lpBuffer.length; i++) {
			memory.setByteMemoryValue (t4, new LongValue(lpBuffer [i]));
			t4 += 1;
		}		memory.setDoubleWordMemoryValue(t5, new LongValue(Pointer.nativeValue(lpFilePart.getValue())));

		

	}
}