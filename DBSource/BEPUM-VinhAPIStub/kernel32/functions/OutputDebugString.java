/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.kernel32.functions
 * File name: OutputDebugString.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.kernel32.functions;

import v2.org.analysis.apihandle.winapi.kernel32.Kernel32API;
import v2.org.analysis.apihandle.winapi.kernel32.Kernel32DLL;
 
public class OutputDebugString extends Kernel32API {
	public OutputDebugString () {
		super();
		NUM_OF_PARMS = 1;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		
		// Step 2: type conversion from C++ to Java
		String lpOutputString = null;
		if ( t0 != 0L ) lpOutputString = memory.getText(this, t0);

		// Step 3: call API function
		Kernel32DLL.INSTANCE.OutputDebugString (lpOutputString);
		
		// Step 4: update environment (memory & eax register)

	}
}