/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.kernel32.functions
 * File name: CreateFileMapping.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.kernel32.functions;

import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.WinBase.SECURITY_ATTRIBUTES;
import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.platform.win32.WinNT.HANDLE;

import v2.org.analysis.apihandle.winapi.kernel32.Kernel32API;
import v2.org.analysis.apihandle.winapi.kernel32.Kernel32DLL;
import v2.org.analysis.value.LongValue;
 
public class CreateFileMapping extends Kernel32API {
	public CreateFileMapping () {
		super();
		NUM_OF_PARMS = 6;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		long t2 = this.params.get(2);
		long t3 = this.params.get(3);
		long t4 = this.params.get(4);
		long t5 = this.params.get(5);
		
		// Step 2: type conversion from C++ to Java
		HANDLE hFile = null;
		if ( t0 != 0L ) {
			hFile = new HANDLE ();
			hFile.setPointer(new Pointer(t0));
		}
		SECURITY_ATTRIBUTES lpAttributes = null;
		if ( t1 != 0L) {
			lpAttributes = new SECURITY_ATTRIBUTES ();
			lpAttributes.dwLength = new DWORD (((LongValue)memory.getDoubleWordMemoryValue (t1)).getValue());
			t1 += 4;
			lpAttributes.lpSecurityDescriptor = new Pointer (((LongValue)memory.getDoubleWordMemoryValue (t1)).getValue());
			t1 += 4;
			lpAttributes.bInheritHandle = (((LongValue)memory.getDoubleWordMemoryValue (t1)).getValue() != 0L ) ? true : false;
			t1 += 4;
		}
		DWORD flProtect = new DWORD (t2);
		DWORD dwMaximumSizeHigh = new DWORD (t3);
		DWORD dwMaximumSizeLow = new DWORD (t4);
		String lpName = null;
		if ( t5 != 0L ) lpName = memory.getText(this, t5);

		// Step 3: call API function
		int ret = Kernel32DLL.INSTANCE.CreateFileMapping (hFile, lpAttributes, flProtect, dwMaximumSizeHigh, dwMaximumSizeLow, lpName);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));

	}
}