/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.kernel32.functions
 * File name: LCMapString.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.kernel32.functions;

import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.platform.win32.WinDef.LCID;

import v2.org.analysis.apihandle.winapi.kernel32.Kernel32API;
import v2.org.analysis.apihandle.winapi.kernel32.Kernel32DLL;
import v2.org.analysis.value.LongValue;
 
public class LCMapString extends Kernel32API {
	public LCMapString () {
		super();
		NUM_OF_PARMS = 6;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		long t2 = this.params.get(2);
		long t3 = this.params.get(3);
		long t4 = this.params.get(4);
		long t5 = this.params.get(5);
		
		// Step 2: type conversion from C++ to Java
		LCID Locale = new LCID (t0);
		DWORD dwMapFlags = new DWORD (t1);
		String lpSrcStr = null;
		if ( t2 != 0L ) lpSrcStr = memory.getText(this, t2);
		int cchSrc = (int) t3;
		char[] lpDestStr = null;
		if ( t4 != 0L ) lpDestStr = new char[(int) t5];
		for (int i = 0; i < lpDestStr.length; i++) {
			lpDestStr [i] = (char) ((LongValue) memory.getByteMemoryValue (t4)).getValue();
			t4 += 1;
		}
		int cchDest = (int) t5;

		// Step 3: call API function
		int ret = Kernel32DLL.INSTANCE.LCMapString (Locale, dwMapFlags, lpSrcStr, cchSrc, lpDestStr, cchDest);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));
		t4 = this.params.get(4);
		for (int i = 0; i < lpDestStr.length; i++) {
			memory.setByteMemoryValue (t4, new LongValue(lpDestStr [i]));
			t4 += 1;
		}
	}
}