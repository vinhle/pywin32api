/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.kernel32.functions
 * File name: GetSystemTime.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.kernel32.functions;

import com.sun.jna.platform.win32.WinBase.SYSTEMTIME;

import v2.org.analysis.apihandle.winapi.kernel32.Kernel32API;
import v2.org.analysis.apihandle.winapi.kernel32.Kernel32DLL;
import v2.org.analysis.value.LongValue;
 
public class GetSystemTime extends Kernel32API {
	public GetSystemTime () {
		super();
		NUM_OF_PARMS = 1;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		
		// Step 2: type conversion from C++ to Java
		SYSTEMTIME lpSystemTime = new SYSTEMTIME ();

		// Step 3: call API function
		Kernel32DLL.INSTANCE.GetSystemTime (lpSystemTime);
		
		// Step 4: update environment (memory & eax register)
		t0 = this.params.get(0);
		memory.setWordMemoryValue (t0, new LongValue(lpSystemTime.wYear));
		t0 += 2;
		memory.setWordMemoryValue (t0, new LongValue(lpSystemTime.wMonth));
		t0 += 2;
		memory.setWordMemoryValue (t0, new LongValue(lpSystemTime.wDayOfWeek));
		t0 += 2;
		memory.setWordMemoryValue (t0, new LongValue(lpSystemTime.wDay));
		t0 += 2;
		memory.setWordMemoryValue (t0, new LongValue(lpSystemTime.wHour));
		t0 += 2;
		memory.setWordMemoryValue (t0, new LongValue(lpSystemTime.wMinute));
		t0 += 2;
		memory.setWordMemoryValue (t0, new LongValue(lpSystemTime.wSecond));
		t0 += 2;
		memory.setWordMemoryValue (t0, new LongValue(lpSystemTime.wMilliseconds));
		t0 += 2;

	}
}