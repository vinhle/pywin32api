/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.kernel32.functions
 * File name: FormatMessage.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.kernel32.functions;

import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.WinDef.DWORD;

import v2.org.analysis.apihandle.winapi.kernel32.Kernel32API;
import v2.org.analysis.apihandle.winapi.kernel32.Kernel32DLL;
import v2.org.analysis.value.LongValue;
 
public class FormatMessage extends Kernel32API {
	public FormatMessage () {
		super();
		NUM_OF_PARMS = 7;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		long t2 = this.params.get(2);
		long t3 = this.params.get(3);
		long t4 = this.params.get(4);
		long t5 = this.params.get(5);
		long t6 = this.params.get(6);
		
		// Step 2: type conversion from C++ to Java
		DWORD dwFlags = new DWORD (t0);
		Pointer lpSource = new Pointer (t1);
		DWORD dwMessageId = new DWORD (t2);
		DWORD dwLanguageId = new DWORD (t3);
		char[] lpBuffer = null;
		if ( t4 != 0L ) lpBuffer = new char[(int) t5];
		for (int i = 0; i < lpBuffer.length; i++) {
			lpBuffer [i] = (char) ((LongValue) memory.getByteMemoryValue (t4)).getValue();
			t4 += 1;
		}
		DWORD nSize = new DWORD (t5);
		Pointer Arguments = new Pointer (t6);

		// Step 3: call API function
		int ret = Kernel32DLL.INSTANCE.FormatMessage (dwFlags, lpSource, dwMessageId, dwLanguageId, lpBuffer, nSize, Arguments);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));
		t4 = this.params.get(4);
		for (int i = 0; i < lpBuffer.length; i++) {
			memory.setByteMemoryValue (t4, new LongValue(lpBuffer [i]));
			t4 += 1;
		}
	}
}