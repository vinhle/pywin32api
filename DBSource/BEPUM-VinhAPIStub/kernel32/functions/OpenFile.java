/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.kernel32.functions
 * File name: OpenFile.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.kernel32.functions;

import com.sun.jna.platform.win32.WinDef.UINT;
import com.sun.jna.ptr.ByteByReference;

import v2.org.analysis.apihandle.structures.OFSTRUCT;
import v2.org.analysis.apihandle.winapi.kernel32.Kernel32API;
import v2.org.analysis.apihandle.winapi.kernel32.Kernel32DLL;
import v2.org.analysis.value.LongValue;
 
public class OpenFile extends Kernel32API {
	public OpenFile () {
		super();
		NUM_OF_PARMS = 3;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		long t2 = this.params.get(2);
		
		// Step 2: type conversion from C++ to Java
		ByteByReference lpFileName = new ByteByReference ((byte) t0);
		OFSTRUCT lpReOpenBuff = new OFSTRUCT ();
		UINT uStyle = new UINT (t2);

		// Step 3: call API function
		int ret = Kernel32DLL.INSTANCE.OpenFile (lpFileName, lpReOpenBuff, uStyle);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));
		t1 = this.params.get(1);
		memory.setByteMemoryValue (t1, new LongValue(lpReOpenBuff.cBytes.longValue()));
		t1 += 1;
		memory.setByteMemoryValue (t1, new LongValue(lpReOpenBuff.fFixedDisk.longValue()));
		t1 += 1;
		memory.setWordMemoryValue (t1, new LongValue(lpReOpenBuff.nErrCode.longValue()));
		t1 += 2;
		memory.setWordMemoryValue (t1, new LongValue(lpReOpenBuff.Reserved1.longValue()));
		t1 += 2;
		memory.setWordMemoryValue (t1, new LongValue(lpReOpenBuff.Reserved2.longValue()));
		t1 += 2;
		for (int i = 0; i < lpReOpenBuff.szPathName.length; i++) {
			memory.setByteMemoryValue (t1, new LongValue(lpReOpenBuff.szPathName [i].longValue()));
			t1 += 1;
		}

	}
}