/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.kernel32.functions
 * File name: Process32First.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.kernel32.functions;

import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.BaseTSD.ULONG_PTR;
import com.sun.jna.platform.win32.Tlhelp32.PROCESSENTRY32;
import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.platform.win32.WinDef.LONG;
import com.sun.jna.platform.win32.WinNT.HANDLE;

import v2.org.analysis.apihandle.winapi.kernel32.Kernel32API;
import v2.org.analysis.apihandle.winapi.kernel32.Kernel32DLL;
import v2.org.analysis.value.LongValue;
 
public class Process32First extends Kernel32API {
	public Process32First () {
		super();
		NUM_OF_PARMS = 2;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		
		// Step 2: type conversion from C++ to Java
		HANDLE hSnapshot = null;
		if ( t0 != 0L ) {
			hSnapshot = new HANDLE ();
			hSnapshot.setPointer(new Pointer(t0));
		}
		PROCESSENTRY32 lppe = null;
		if ( t1 != 0L) {
			lppe = new PROCESSENTRY32 ();
			lppe.dwSize = new DWORD (((LongValue)memory.getDoubleWordMemoryValue (t1)).getValue());
			t1 += 4;
			lppe.cntUsage = new DWORD (((LongValue)memory.getDoubleWordMemoryValue (t1)).getValue());
			t1 += 4;
			lppe.th32ProcessID = new DWORD (((LongValue)memory.getDoubleWordMemoryValue (t1)).getValue());
			t1 += 4;
			lppe.th32DefaultHeapID = new ULONG_PTR (((LongValue)memory.getDoubleWordMemoryValue (t1)).getValue());
			t1 += 4;
			lppe.th32ModuleID = new DWORD (((LongValue)memory.getDoubleWordMemoryValue (t1)).getValue());
			t1 += 4;
			lppe.cntThreads = new DWORD (((LongValue)memory.getDoubleWordMemoryValue (t1)).getValue());
			t1 += 4;
			lppe.th32ParentProcessID = new DWORD (((LongValue)memory.getDoubleWordMemoryValue (t1)).getValue());
			t1 += 4;
			lppe.pcPriClassBase = new LONG (((LongValue)memory.getDoubleWordMemoryValue (t1)).getValue());
			t1 += 4;
			lppe.dwFlags = new DWORD (((LongValue)memory.getDoubleWordMemoryValue (t1)).getValue());
			t1 += 4;
			for (int i = 0; i < lppe.szExeFile.length; i++) {
				lppe.szExeFile [i] = (char) ((LongValue) memory.getWordMemoryValue (t1)).getValue();
				t1 += 2;
			}
		}

		// Step 3: call API function
		int ret = Kernel32DLL.INSTANCE.Process32First (hSnapshot, lppe);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));

	}
}