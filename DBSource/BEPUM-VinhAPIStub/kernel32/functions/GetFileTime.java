/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.kernel32.functions
 * File name: GetFileTime.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.kernel32.functions;

import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.WinBase.FILETIME;
import com.sun.jna.platform.win32.WinNT.HANDLE;

import v2.org.analysis.apihandle.winapi.kernel32.Kernel32API;
import v2.org.analysis.apihandle.winapi.kernel32.Kernel32DLL;
import v2.org.analysis.value.LongValue;
 
public class GetFileTime extends Kernel32API {
	public GetFileTime () {
		super();
		NUM_OF_PARMS = 4;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		long t2 = this.params.get(2);
		long t3 = this.params.get(3);
		
		// Step 2: type conversion from C++ to Java
		HANDLE hFile = null;
		if ( t0 != 0L ) {
			hFile = new HANDLE ();
			hFile.setPointer(new Pointer(t0));
		}
		FILETIME lpCreationTime = new FILETIME ();
		FILETIME lpLastAccessTime = new FILETIME ();
		FILETIME lpLastWriteTime = new FILETIME ();

		// Step 3: call API function
		int ret = Kernel32DLL.INSTANCE.GetFileTime (hFile, lpCreationTime, lpLastAccessTime, lpLastWriteTime);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));
		t1 = this.params.get(1);
		memory.setDoubleWordMemoryValue (t1, new LongValue(lpCreationTime.dwLowDateTime));
		t1 += 4;
		memory.setDoubleWordMemoryValue (t1, new LongValue(lpCreationTime.dwHighDateTime));
		t1 += 4;
		t2 = this.params.get(2);
		memory.setDoubleWordMemoryValue (t2, new LongValue(lpLastAccessTime.dwLowDateTime));
		t2 += 4;
		memory.setDoubleWordMemoryValue (t2, new LongValue(lpLastAccessTime.dwHighDateTime));
		t2 += 4;
		t3 = this.params.get(3);
		memory.setDoubleWordMemoryValue (t3, new LongValue(lpLastWriteTime.dwLowDateTime));
		t3 += 4;
		memory.setDoubleWordMemoryValue (t3, new LongValue(lpLastWriteTime.dwHighDateTime));
		t3 += 4;

	}
}