/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.kernel32.functions
 * File name: GetSystemTimeAsFileTime.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.kernel32.functions;

import com.sun.jna.platform.win32.WinBase.FILETIME;

import v2.org.analysis.apihandle.winapi.kernel32.Kernel32API;
import v2.org.analysis.apihandle.winapi.kernel32.Kernel32DLL;
import v2.org.analysis.value.LongValue;
 
public class GetSystemTimeAsFileTime extends Kernel32API {
	public GetSystemTimeAsFileTime () {
		super();
		NUM_OF_PARMS = 1;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		
		// Step 2: type conversion from C++ to Java
		FILETIME lpSystemTimeAsFileTime = new FILETIME ();

		// Step 3: call API function
		Kernel32DLL.INSTANCE.GetSystemTimeAsFileTime (lpSystemTimeAsFileTime);
		
		// Step 4: update environment (memory & eax register)
		t0 = this.params.get(0);
		memory.setDoubleWordMemoryValue (t0, new LongValue(lpSystemTimeAsFileTime.dwLowDateTime));
		t0 += 4;
		memory.setDoubleWordMemoryValue (t0, new LongValue(lpSystemTimeAsFileTime.dwHighDateTime));
		t0 += 4;

	}
}