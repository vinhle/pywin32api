/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.kernel32.functions
 * File name: GetCPInfo.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.kernel32.functions;

import com.sun.jna.platform.win32.WinDef.UINT;

import v2.org.analysis.apihandle.structures.CPINFO;
import v2.org.analysis.apihandle.winapi.kernel32.Kernel32API;
import v2.org.analysis.apihandle.winapi.kernel32.Kernel32DLL;
import v2.org.analysis.value.LongValue;
 
public class GetCPInfo extends Kernel32API {
	public GetCPInfo () {
		super();
		NUM_OF_PARMS = 2;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		
		// Step 2: type conversion from C++ to Java
		UINT CodePage = new UINT (t0);
		CPINFO lpCPInfo = new CPINFO ();

		// Step 3: call API function
		int ret = Kernel32DLL.INSTANCE.GetCPInfo (CodePage, lpCPInfo);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));
		t1 = this.params.get(1);
		memory.setDoubleWordMemoryValue (t1, new LongValue(lpCPInfo.MaxCharSize.longValue()));
		t1 += 4;
		for (int i = 0; i < lpCPInfo.DefaultChar.length; i++) {
			memory.setByteMemoryValue (t1, new LongValue(lpCPInfo.DefaultChar [i].longValue()));
			t1 += 1;
		}
		for (int i = 0; i < lpCPInfo.LeadByte.length; i++) {
			memory.setByteMemoryValue (t1, new LongValue(lpCPInfo.LeadByte [i].longValue()));
			t1 += 1;
		}

	}
}