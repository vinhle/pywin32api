/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.kernel32.functions
 * File name: MultiByteToWideChar.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.kernel32.functions;

import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.platform.win32.WinDef.UINT;
import com.sun.jna.ptr.ByteByReference;

import v2.org.analysis.apihandle.winapi.kernel32.Kernel32API;
import v2.org.analysis.apihandle.winapi.kernel32.Kernel32DLL;
import v2.org.analysis.value.LongValue;
 
public class MultiByteToWideChar extends Kernel32API {
	public MultiByteToWideChar () {
		super();
		NUM_OF_PARMS = 6;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		long t2 = this.params.get(2);
		long t3 = this.params.get(3);
		long t4 = this.params.get(4);
		long t5 = this.params.get(5);
		
		// Step 2: type conversion from C++ to Java
		UINT CodePage = new UINT (t0);
		DWORD dwFlags = new DWORD (t1);
		ByteByReference lpMultiByteStr = new ByteByReference ((byte) t2);
		int cbMultiByte = (int) t3;
		char[] lpWideCharStr = null;
		if ( t4 != 0L ) lpWideCharStr = new char[(int) t5];
		for (int i = 0; i < lpWideCharStr.length; i++) {
			lpWideCharStr [i] = (char) ((LongValue) memory.getWordMemoryValue (t4)).getValue();
			t4 += 2;
		}
		int cchWideChar = (int) t5;

		// Step 3: call API function
		int ret = Kernel32DLL.INSTANCE.MultiByteToWideChar (CodePage, dwFlags, lpMultiByteStr, cbMultiByte, lpWideCharStr, cchWideChar);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));
		t4 = this.params.get(4);
		for (int i = 0; i < lpWideCharStr.length; i++) {
			memory.setWordMemoryValue (t4, new LongValue(lpWideCharStr [i]));
			t4 += 2;
		}
	}
}