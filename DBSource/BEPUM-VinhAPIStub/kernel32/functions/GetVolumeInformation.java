/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.kernel32.functions
 * File name: GetVolumeInformation.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.kernel32.functions;

import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.ptr.IntByReference;

import v2.org.analysis.apihandle.winapi.kernel32.Kernel32API;
import v2.org.analysis.apihandle.winapi.kernel32.Kernel32DLL;
import v2.org.analysis.value.LongValue;
 
public class GetVolumeInformation extends Kernel32API {
	public GetVolumeInformation () {
		super();
		NUM_OF_PARMS = 8;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		long t2 = this.params.get(2);
		long t3 = this.params.get(3);
		long t4 = this.params.get(4);
		long t5 = this.params.get(5);
		long t6 = this.params.get(6);
		long t7 = this.params.get(7);
		
		// Step 2: type conversion from C++ to Java
		String lpRootPathName = null;
		if ( t0 != 0L ) lpRootPathName = memory.getText(this, t0);
		char[] lpVolumeNameBuffer = null;
		if ( t1 != 0L ) lpVolumeNameBuffer = new char[(int) t2];
		for (int i = 0; i < lpVolumeNameBuffer.length; i++) {
			lpVolumeNameBuffer [i] = (char) ((LongValue) memory.getByteMemoryValue (t1)).getValue();
			t1 += 1;
		}
		DWORD nVolumeNameSize = new DWORD (t2);
		IntByReference lpVolumeSerialNumber = new IntByReference ((int) t3);
		IntByReference lpMaximumComponentLength = new IntByReference ((int) t4);
		IntByReference lpFileSystemFlags = new IntByReference ((int) t5);
		char[] lpFileSystemNameBuffer = null;
		if ( t6 != 0L ) lpFileSystemNameBuffer = new char[(int) t7];
		for (int i = 0; i < lpFileSystemNameBuffer.length; i++) {
			lpFileSystemNameBuffer [i] = (char) ((LongValue) memory.getByteMemoryValue (t6)).getValue();
			t6 += 1;
		}
		DWORD nFileSystemNameSize = new DWORD (t7);

		// Step 3: call API function
		int ret = Kernel32DLL.INSTANCE.GetVolumeInformation (lpRootPathName, lpVolumeNameBuffer, nVolumeNameSize, lpVolumeSerialNumber, lpMaximumComponentLength, lpFileSystemFlags, lpFileSystemNameBuffer, nFileSystemNameSize);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));
		t1 = this.params.get(1);
		for (int i = 0; i < lpVolumeNameBuffer.length; i++) {
			memory.setByteMemoryValue (t1, new LongValue(lpVolumeNameBuffer [i]));
			t1 += 1;
		}		memory.setDoubleWordMemoryValue(t3, new LongValue(lpVolumeSerialNumber.getValue()));

		
		memory.setDoubleWordMemoryValue(t4, new LongValue(lpMaximumComponentLength.getValue()));

		
		memory.setDoubleWordMemoryValue(t5, new LongValue(lpFileSystemFlags.getValue()));

		
		t6 = this.params.get(6);
		for (int i = 0; i < lpFileSystemNameBuffer.length; i++) {
			memory.setByteMemoryValue (t6, new LongValue(lpFileSystemNameBuffer [i]));
			t6 += 1;
		}
	}
}