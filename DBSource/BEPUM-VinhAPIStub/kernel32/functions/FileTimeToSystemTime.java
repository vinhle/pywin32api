/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.kernel32.functions
 * File name: FileTimeToSystemTime.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.kernel32.functions;

import com.sun.jna.platform.win32.WinBase.FILETIME;
import com.sun.jna.platform.win32.WinBase.SYSTEMTIME;

import v2.org.analysis.apihandle.winapi.kernel32.Kernel32API;
import v2.org.analysis.apihandle.winapi.kernel32.Kernel32DLL;
import v2.org.analysis.value.LongValue;
 
public class FileTimeToSystemTime extends Kernel32API {
	public FileTimeToSystemTime () {
		super();
		NUM_OF_PARMS = 2;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		
		// Step 2: type conversion from C++ to Java
		FILETIME lpFileTime = null;
		if ( t0 != 0L) {
			lpFileTime = new FILETIME ();
			lpFileTime.dwLowDateTime = (int) ((LongValue)memory.getDoubleWordMemoryValue (t0)).getValue();
			t0 += 4;
			lpFileTime.dwHighDateTime = (int) ((LongValue)memory.getDoubleWordMemoryValue (t0)).getValue();
			t0 += 4;
		}
		SYSTEMTIME lpSystemTime = new SYSTEMTIME ();

		// Step 3: call API function
		int ret = Kernel32DLL.INSTANCE.FileTimeToSystemTime (lpFileTime, lpSystemTime);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));
		t1 = this.params.get(1);
		memory.setWordMemoryValue (t1, new LongValue(lpSystemTime.wYear));
		t1 += 2;
		memory.setWordMemoryValue (t1, new LongValue(lpSystemTime.wMonth));
		t1 += 2;
		memory.setWordMemoryValue (t1, new LongValue(lpSystemTime.wDayOfWeek));
		t1 += 2;
		memory.setWordMemoryValue (t1, new LongValue(lpSystemTime.wDay));
		t1 += 2;
		memory.setWordMemoryValue (t1, new LongValue(lpSystemTime.wHour));
		t1 += 2;
		memory.setWordMemoryValue (t1, new LongValue(lpSystemTime.wMinute));
		t1 += 2;
		memory.setWordMemoryValue (t1, new LongValue(lpSystemTime.wSecond));
		t1 += 2;
		memory.setWordMemoryValue (t1, new LongValue(lpSystemTime.wMilliseconds));
		t1 += 2;

	}
}