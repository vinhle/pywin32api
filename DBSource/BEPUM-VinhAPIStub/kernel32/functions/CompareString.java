/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.kernel32.functions
 * File name: CompareString.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.kernel32.functions;

import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.platform.win32.WinDef.LCID;

import v2.org.analysis.apihandle.winapi.kernel32.Kernel32API;
import v2.org.analysis.apihandle.winapi.kernel32.Kernel32DLL;
import v2.org.analysis.value.LongValue;
 
public class CompareString extends Kernel32API {
	public CompareString () {
		super();
		NUM_OF_PARMS = 6;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		long t2 = this.params.get(2);
		long t3 = this.params.get(3);
		long t4 = this.params.get(4);
		long t5 = this.params.get(5);
		
		// Step 2: type conversion from C++ to Java
		LCID Locale = new LCID (t0);
		DWORD dwCmpFlags = new DWORD (t1);
		String lpString1 = null;
		if ( t2 != 0L ) lpString1 = memory.getText(this, t2);
		int cchCount1 = (int) t3;
		String lpString2 = null;
		if ( t4 != 0L ) lpString2 = memory.getText(this, t4);
		int cchCount2 = (int) t5;

		// Step 3: call API function
		int ret = Kernel32DLL.INSTANCE.CompareString (Locale, dwCmpFlags, lpString1, cchCount1, lpString2, cchCount2);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));

	}
}