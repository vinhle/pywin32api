/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.kernel32.functions
 * File name: GetFullPathName.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.kernel32.functions;

import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.ptr.PointerByReference;

import v2.org.analysis.apihandle.winapi.kernel32.Kernel32API;
import v2.org.analysis.apihandle.winapi.kernel32.Kernel32DLL;
import v2.org.analysis.value.LongValue;
 
public class GetFullPathName extends Kernel32API {
	public GetFullPathName () {
		super();
		NUM_OF_PARMS = 4;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		long t2 = this.params.get(2);
		long t3 = this.params.get(3);
		
		// Step 2: type conversion from C++ to Java
		String lpFileName = null;
		if ( t0 != 0L ) lpFileName = memory.getText(this, t0);
		DWORD nBufferLength = new DWORD (t1);
		char[] lpBuffer = null;
		if ( t2 != 0L ) lpBuffer = new char[(int) t1];
		for (int i = 0; i < lpBuffer.length; i++) {
			lpBuffer [i] = (char) ((LongValue) memory.getByteMemoryValue (t2)).getValue();
			t2 += 1;
		}
		PointerByReference lpFilePart = new PointerByReference (new Pointer(t3));

		// Step 3: call API function
		int ret = Kernel32DLL.INSTANCE.GetFullPathName (lpFileName, nBufferLength, lpBuffer, lpFilePart);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));
		t2 = this.params.get(2);
		for (int i = 0; i < lpBuffer.length; i++) {
			memory.setByteMemoryValue (t2, new LongValue(lpBuffer [i]));
			t2 += 1;
		}		memory.setDoubleWordMemoryValue(t3, new LongValue(Pointer.nativeValue(lpFilePart.getValue())));

		

	}
}