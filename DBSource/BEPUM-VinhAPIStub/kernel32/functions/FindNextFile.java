/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.kernel32.functions
 * File name: FindNextFile.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.kernel32.functions;

import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.WinNT.HANDLE;

import v2.org.analysis.apihandle.structures.WIN32_FIND_DATA;
import v2.org.analysis.apihandle.winapi.kernel32.Kernel32API;
import v2.org.analysis.apihandle.winapi.kernel32.Kernel32DLL;
import v2.org.analysis.value.LongValue;
 
public class FindNextFile extends Kernel32API {
	public FindNextFile () {
		super();
		NUM_OF_PARMS = 2;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		
		// Step 2: type conversion from C++ to Java
		HANDLE hFindFile = null;
		if ( t0 != 0L ) {
			hFindFile = new HANDLE ();
			hFindFile.setPointer(new Pointer(t0));
		}
		WIN32_FIND_DATA lpFindFileData = new WIN32_FIND_DATA ();

		// Step 3: call API function
		int ret = Kernel32DLL.INSTANCE.FindNextFile (hFindFile, lpFindFileData);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));
		t1 = this.params.get(1);
		memory.setDoubleWordMemoryValue (t1, new LongValue(lpFindFileData.dwFileAttributes.longValue()));
		t1 += 4;
		// Nested Structure
			lpFindFileData.ftCreationTime.dwLowDateTime = (int) ((LongValue)memory.getDoubleWordMemoryValue (t1 += 0)).getValue();
			lpFindFileData.ftCreationTime.dwHighDateTime = (int) ((LongValue)memory.getDoubleWordMemoryValue (t1 += 4)).getValue();
		// Nested Structure
			lpFindFileData.ftLastAccessTime.dwLowDateTime = (int) ((LongValue)memory.getDoubleWordMemoryValue (t1 += 0)).getValue();
			lpFindFileData.ftLastAccessTime.dwHighDateTime = (int) ((LongValue)memory.getDoubleWordMemoryValue (t1 += 4)).getValue();
		// Nested Structure
			lpFindFileData.ftLastWriteTime.dwLowDateTime = (int) ((LongValue)memory.getDoubleWordMemoryValue (t1 += 0)).getValue();
			lpFindFileData.ftLastWriteTime.dwHighDateTime = (int) ((LongValue)memory.getDoubleWordMemoryValue (t1 += 4)).getValue();
		memory.setDoubleWordMemoryValue (t1, new LongValue(lpFindFileData.nFileSizeHigh.longValue()));
		t1 += 4;
		memory.setDoubleWordMemoryValue (t1, new LongValue(lpFindFileData.nFileSizeLow.longValue()));
		t1 += 4;
		memory.setDoubleWordMemoryValue (t1, new LongValue(lpFindFileData.dwReserved0.longValue()));
		t1 += 4;
		memory.setDoubleWordMemoryValue (t1, new LongValue(lpFindFileData.dwReserved1.longValue()));
		t1 += 4;
		for (int i = 0; i < lpFindFileData.cFileName.length; i++) {
			memory.setWordMemoryValue (t1, new LongValue(lpFindFileData.cFileName [i]));
			t1 += 2;
		}
		for (int i = 0; i < lpFindFileData.cAlternateFileName.length; i++) {
			memory.setWordMemoryValue (t1, new LongValue(lpFindFileData.cAlternateFileName [i]));
			t1 += 2;
		}

	}
}