/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.kernel32.functions
 * File name: WideCharToMultiByte.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.kernel32.functions;

import com.sun.jna.platform.win32.WinDef.CHAR;
import com.sun.jna.platform.win32.WinDef.CHARByReference;
import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.platform.win32.WinDef.UINT;
import com.sun.jna.ptr.ByteByReference;
import com.sun.jna.ptr.IntByReference;

import v2.org.analysis.apihandle.winapi.kernel32.Kernel32API;
import v2.org.analysis.apihandle.winapi.kernel32.Kernel32DLL;
import v2.org.analysis.value.LongValue;
 
public class WideCharToMultiByte extends Kernel32API {
	public WideCharToMultiByte () {
		super();
		NUM_OF_PARMS = 8;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		long t2 = this.params.get(2);
		long t3 = this.params.get(3);
		long t4 = this.params.get(4);
		long t5 = this.params.get(5);
		long t6 = this.params.get(6);
		long t7 = this.params.get(7);
		
		// Step 2: type conversion from C++ to Java
		UINT CodePage = new UINT (t0);
		DWORD dwFlags = new DWORD (t1);
		CHARByReference lpWideCharStr = new CHARByReference (new CHAR(t2));
		int cchWideChar = (int) t3;
		byte[] lpMultiByteStr = null;
		if ( t4 != 0L ) lpMultiByteStr = new byte[(int) t5];
		for (int i = 0; i < lpMultiByteStr.length; i++) {
			lpMultiByteStr [i] = (byte) ((LongValue) memory.getByteMemoryValue (t4)).getValue();
			t4 += 1;
		}
		int cbMultiByte = (int) t5;
		ByteByReference lpDefaultChar = new ByteByReference ((byte) t6);
		IntByReference lpUsedDefaultChar = new IntByReference ((int) t7);

		// Step 3: call API function
		int ret = Kernel32DLL.INSTANCE.WideCharToMultiByte (CodePage, dwFlags, lpWideCharStr, cchWideChar, lpMultiByteStr, cbMultiByte, lpDefaultChar, lpUsedDefaultChar);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));
		t4 = this.params.get(4);
		for (int i = 0; i < lpMultiByteStr.length; i++) {
			memory.setByteMemoryValue (t4, new LongValue(lpMultiByteStr [i]));
			t4 += 1;
		}		memory.setDoubleWordMemoryValue(t7, new LongValue(lpUsedDefaultChar.getValue()));

		

	}
}