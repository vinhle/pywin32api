/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.kernel32.functions
 * File name: lstrcat.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.kernel32.functions;

import v2.org.analysis.apihandle.winapi.kernel32.Kernel32API;
import v2.org.analysis.apihandle.winapi.kernel32.Kernel32DLL;
import v2.org.analysis.value.LongValue;
 
public class lstrcat extends Kernel32API {
	public lstrcat () {
		super();
		NUM_OF_PARMS = 2;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		
		// Step 2: type conversion from C++ to Java
		String lpString1 = null;
		if ( t0 != 0L ) lpString1 = memory.getText(this, t0);
		String lpString2 = null;
		if ( t1 != 0L ) lpString2 = memory.getText(this, t1);

		// Step 3: call API function
		int ret = Kernel32DLL.INSTANCE.lstrcat (lpString1, lpString2);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));

	}
}