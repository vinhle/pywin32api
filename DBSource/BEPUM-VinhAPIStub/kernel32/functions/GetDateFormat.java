/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.kernel32.functions
 * File name: GetDateFormat.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.kernel32.functions;

import com.sun.jna.platform.win32.WinBase.SYSTEMTIME;
import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.platform.win32.WinDef.LCID;

import v2.org.analysis.apihandle.winapi.kernel32.Kernel32API;
import v2.org.analysis.apihandle.winapi.kernel32.Kernel32DLL;
import v2.org.analysis.value.LongValue;
 
public class GetDateFormat extends Kernel32API {
	public GetDateFormat () {
		super();
		NUM_OF_PARMS = 6;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		long t2 = this.params.get(2);
		long t3 = this.params.get(3);
		long t4 = this.params.get(4);
		long t5 = this.params.get(5);
		
		// Step 2: type conversion from C++ to Java
		LCID Locale = new LCID (t0);
		DWORD dwFlags = new DWORD (t1);
		SYSTEMTIME lpDate = null;
		if ( t2 != 0L) {
			lpDate = new SYSTEMTIME ();
			lpDate.wYear = (short) ((LongValue)memory.getWordMemoryValue (t2)).getValue();
			t2 += 2;
			lpDate.wMonth = (short) ((LongValue)memory.getWordMemoryValue (t2)).getValue();
			t2 += 2;
			lpDate.wDayOfWeek = (short) ((LongValue)memory.getWordMemoryValue (t2)).getValue();
			t2 += 2;
			lpDate.wDay = (short) ((LongValue)memory.getWordMemoryValue (t2)).getValue();
			t2 += 2;
			lpDate.wHour = (short) ((LongValue)memory.getWordMemoryValue (t2)).getValue();
			t2 += 2;
			lpDate.wMinute = (short) ((LongValue)memory.getWordMemoryValue (t2)).getValue();
			t2 += 2;
			lpDate.wSecond = (short) ((LongValue)memory.getWordMemoryValue (t2)).getValue();
			t2 += 2;
			lpDate.wMilliseconds = (short) ((LongValue)memory.getWordMemoryValue (t2)).getValue();
			t2 += 2;
		}
		String lpFormat = null;
		if ( t3 != 0L ) lpFormat = memory.getText(this, t3);
		char[] lpDateStr = null;
		if ( t4 != 0L ) lpDateStr = new char[(int) t5];
		for (int i = 0; i < lpDateStr.length; i++) {
			lpDateStr [i] = (char) ((LongValue) memory.getByteMemoryValue (t4)).getValue();
			t4 += 1;
		}
		int cchDate = (int) t5;

		// Step 3: call API function
		int ret = Kernel32DLL.INSTANCE.GetDateFormat (Locale, dwFlags, lpDate, lpFormat, lpDateStr, cchDate);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));
		t4 = this.params.get(4);
		for (int i = 0; i < lpDateStr.length; i++) {
			memory.setByteMemoryValue (t4, new LongValue(lpDateStr [i]));
			t4 += 1;
		}
	}
}