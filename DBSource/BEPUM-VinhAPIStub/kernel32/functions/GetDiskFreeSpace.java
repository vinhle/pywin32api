/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.kernel32.functions
 * File name: GetDiskFreeSpace.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.kernel32.functions;

import com.sun.jna.ptr.IntByReference;

import v2.org.analysis.apihandle.winapi.kernel32.Kernel32API;
import v2.org.analysis.apihandle.winapi.kernel32.Kernel32DLL;
import v2.org.analysis.value.LongValue;
 
public class GetDiskFreeSpace extends Kernel32API {
	public GetDiskFreeSpace () {
		super();
		NUM_OF_PARMS = 5;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		long t2 = this.params.get(2);
		long t3 = this.params.get(3);
		long t4 = this.params.get(4);
		
		// Step 2: type conversion from C++ to Java
		String lpRootPathName = null;
		if ( t0 != 0L ) lpRootPathName = memory.getText(this, t0);
		IntByReference lpSectorsPerCluster = new IntByReference ((int) t1);
		IntByReference lpBytesPerSector = new IntByReference ((int) t2);
		IntByReference lpNumberOfFreeClusters = new IntByReference ((int) t3);
		IntByReference lpTotalNumberOfClusters = new IntByReference ((int) t4);

		// Step 3: call API function
		int ret = Kernel32DLL.INSTANCE.GetDiskFreeSpace (lpRootPathName, lpSectorsPerCluster, lpBytesPerSector, lpNumberOfFreeClusters, lpTotalNumberOfClusters);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));
		memory.setDoubleWordMemoryValue(t1, new LongValue(lpSectorsPerCluster.getValue()));

		
		memory.setDoubleWordMemoryValue(t2, new LongValue(lpBytesPerSector.getValue()));

		
		memory.setDoubleWordMemoryValue(t3, new LongValue(lpNumberOfFreeClusters.getValue()));

		
		memory.setDoubleWordMemoryValue(t4, new LongValue(lpTotalNumberOfClusters.getValue()));

		

	}
}