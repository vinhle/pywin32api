/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.kernel32.functions
 * File name: GetModuleBaseName.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.kernel32.functions;

import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.platform.win32.WinDef.HMODULE;
import com.sun.jna.platform.win32.WinNT.HANDLE;

import v2.org.analysis.apihandle.winapi.kernel32.Kernel32API;
import v2.org.analysis.apihandle.winapi.kernel32.Kernel32DLL;
import v2.org.analysis.value.LongValue;
 
public class GetModuleBaseName extends Kernel32API {
	public GetModuleBaseName () {
		super();
		NUM_OF_PARMS = 4;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		long t2 = this.params.get(2);
		long t3 = this.params.get(3);
		
		// Step 2: type conversion from C++ to Java
		HANDLE hProcess = null;
		if ( t0 != 0L ) {
			hProcess = new HANDLE ();
			hProcess.setPointer(new Pointer(t0));
		}
		HMODULE hModule = null;
		if ( t1 != 0L ) {
			hModule = new HMODULE ();
			hModule.setPointer(new Pointer(t1));
		}
		char[] lpBaseName = null;
		if ( t2 != 0L ) lpBaseName = new char[(int) t3];
		for (int i = 0; i < lpBaseName.length; i++) {
			lpBaseName [i] = (char) ((LongValue) memory.getByteMemoryValue (t2)).getValue();
			t2 += 1;
		}
		DWORD nSize = new DWORD (t3);

		// Step 3: call API function
		int ret = Kernel32DLL.INSTANCE.GetModuleBaseName (hProcess, hModule, lpBaseName, nSize);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));
		t2 = this.params.get(2);
		for (int i = 0; i < lpBaseName.length; i++) {
			memory.setByteMemoryValue (t2, new LongValue(lpBaseName [i]));
			t2 += 1;
		}
	}
}