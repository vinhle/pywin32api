/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.kernel32.functions
 * File name: GetCPInfoEx.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.kernel32.functions;

import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.platform.win32.WinDef.UINT;

import v2.org.analysis.apihandle.structures.CPINFOEX;
import v2.org.analysis.apihandle.winapi.kernel32.Kernel32API;
import v2.org.analysis.apihandle.winapi.kernel32.Kernel32DLL;
import v2.org.analysis.value.LongValue;
 
public class GetCPInfoEx extends Kernel32API {
	public GetCPInfoEx () {
		super();
		NUM_OF_PARMS = 3;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		long t2 = this.params.get(2);
		
		// Step 2: type conversion from C++ to Java
		UINT CodePage = new UINT (t0);
		DWORD dwFlags = new DWORD (t1);
		CPINFOEX lpCPInfoEx = new CPINFOEX ();

		// Step 3: call API function
		int ret = Kernel32DLL.INSTANCE.GetCPInfoEx (CodePage, dwFlags, lpCPInfoEx);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));
		t2 = this.params.get(2);
		memory.setDoubleWordMemoryValue (t2, new LongValue(lpCPInfoEx.MaxCharSize.longValue()));
		t2 += 4;
		for (int i = 0; i < lpCPInfoEx.DefaultChar.length; i++) {
			memory.setByteMemoryValue (t2, new LongValue(lpCPInfoEx.DefaultChar [i].longValue()));
			t2 += 1;
		}
		for (int i = 0; i < lpCPInfoEx.LeadByte.length; i++) {
			memory.setByteMemoryValue (t2, new LongValue(lpCPInfoEx.LeadByte [i].longValue()));
			t2 += 1;
		}
		memory.setByteMemoryValue (t2, new LongValue(lpCPInfoEx.UnicodeDefaultChar));
		t2 += 1;
		memory.setDoubleWordMemoryValue (t2, new LongValue(lpCPInfoEx.CodePage.longValue()));
		t2 += 4;
		for (int i = 0; i < lpCPInfoEx.CodePageName.length; i++) {
			memory.setWordMemoryValue (t2, new LongValue(lpCPInfoEx.CodePageName [i]));
			t2 += 2;
		}

	}
}