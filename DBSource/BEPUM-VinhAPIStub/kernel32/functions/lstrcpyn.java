/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.kernel32.functions
 * File name: lstrcpyn.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.kernel32.functions;

import v2.org.analysis.apihandle.winapi.kernel32.Kernel32API;
import v2.org.analysis.apihandle.winapi.kernel32.Kernel32DLL;
import v2.org.analysis.value.LongValue;
 
public class lstrcpyn extends Kernel32API {
	public lstrcpyn () {
		super();
		NUM_OF_PARMS = 3;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		long t2 = this.params.get(2);
		
		// Step 2: type conversion from C++ to Java
		String lpString1 = null;
		if ( t0 != 0L ) lpString1 = memory.getText(this, t0);
		String lpString2 = null;
		if ( t1 != 0L ) lpString2 = memory.getText(this, t1);
		int iMaxLength = (int) t2;

		// Step 3: call API function
		int ret = Kernel32DLL.INSTANCE.lstrcpyn (lpString1, lpString2, iMaxLength);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));
		memory.setText(this, t0, new String(lpString1));
	}
}