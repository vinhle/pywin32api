/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.kernel32.functions
 * File name: GetTempFileName.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.kernel32.functions;

import com.sun.jna.platform.win32.WinDef.UINT;

import v2.org.analysis.apihandle.winapi.kernel32.Kernel32API;
import v2.org.analysis.apihandle.winapi.kernel32.Kernel32DLL;
import v2.org.analysis.value.LongValue;
 
public class GetTempFileName extends Kernel32API {
	public GetTempFileName () {
		super();
		NUM_OF_PARMS = 4;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		long t2 = this.params.get(2);
		long t3 = this.params.get(3);
		
		// Step 2: type conversion from C++ to Java
		String lpPathName = null;
		if ( t0 != 0L ) lpPathName = memory.getText(this, t0);
		String lpPrefixString = null;
		if ( t1 != 0L ) lpPrefixString = memory.getText(this, t1);
		UINT uUnique = new UINT (t2);
		String lpTempFileName = null;
		if ( t3 != 0L ) lpTempFileName = memory.getText(this, t3);

		// Step 3: call API function
		int ret = Kernel32DLL.INSTANCE.GetTempFileName (lpPathName, lpPrefixString, uUnique, lpTempFileName);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));
		memory.setText(this, t3, new String(lpTempFileName));
	}
}