/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.kernel32.functions
 * File name: Process32Next.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.kernel32.functions;

import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.Tlhelp32.PROCESSENTRY32;
import com.sun.jna.platform.win32.WinNT.HANDLE;

import v2.org.analysis.apihandle.winapi.kernel32.Kernel32API;
import v2.org.analysis.apihandle.winapi.kernel32.Kernel32DLL;
import v2.org.analysis.value.LongValue;
 
public class Process32Next extends Kernel32API {
	public Process32Next () {
		super();
		NUM_OF_PARMS = 2;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		
		// Step 2: type conversion from C++ to Java
		HANDLE hSnapshot = null;
		if ( t0 != 0L ) {
			hSnapshot = new HANDLE ();
			hSnapshot.setPointer(new Pointer(t0));
		}
		PROCESSENTRY32 lppe = new PROCESSENTRY32 ();

		// Step 3: call API function
		int ret = Kernel32DLL.INSTANCE.Process32Next (hSnapshot, lppe);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));
		t1 = this.params.get(1);
		memory.setDoubleWordMemoryValue (t1, new LongValue(lppe.dwSize.longValue()));
		t1 += 4;
		memory.setDoubleWordMemoryValue (t1, new LongValue(lppe.cntUsage.longValue()));
		t1 += 4;
		memory.setDoubleWordMemoryValue (t1, new LongValue(lppe.th32ProcessID.longValue()));
		t1 += 4;
		memory.setDoubleWordMemoryValue (t1, new LongValue(lppe.th32DefaultHeapID.longValue()));
		t1 += 4;
		memory.setDoubleWordMemoryValue (t1, new LongValue(lppe.th32ModuleID.longValue()));
		t1 += 4;
		memory.setDoubleWordMemoryValue (t1, new LongValue(lppe.cntThreads.longValue()));
		t1 += 4;
		memory.setDoubleWordMemoryValue (t1, new LongValue(lppe.th32ParentProcessID.longValue()));
		t1 += 4;
		memory.setDoubleWordMemoryValue (t1, new LongValue(lppe.pcPriClassBase.longValue()));
		t1 += 4;
		memory.setDoubleWordMemoryValue (t1, new LongValue(lppe.dwFlags.longValue()));
		t1 += 4;
		for (int i = 0; i < lppe.szExeFile.length; i++) {
			memory.setWordMemoryValue (t1, new LongValue(lppe.szExeFile [i]));
			t1 += 2;
		}

	}
}