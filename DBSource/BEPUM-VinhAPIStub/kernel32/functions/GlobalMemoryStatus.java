/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.kernel32.functions
 * File name: GlobalMemoryStatus.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.kernel32.functions;

import v2.org.analysis.apihandle.structures.MEMORYSTATUS;
import v2.org.analysis.apihandle.winapi.kernel32.Kernel32API;
import v2.org.analysis.apihandle.winapi.kernel32.Kernel32DLL;
import v2.org.analysis.value.LongValue;
 
public class GlobalMemoryStatus extends Kernel32API {
	public GlobalMemoryStatus () {
		super();
		NUM_OF_PARMS = 1;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		
		// Step 2: type conversion from C++ to Java
		MEMORYSTATUS lpBuffer = new MEMORYSTATUS ();

		// Step 3: call API function
		Kernel32DLL.INSTANCE.GlobalMemoryStatus (lpBuffer);
		
		// Step 4: update environment (memory & eax register)
		t0 = this.params.get(0);
		memory.setDoubleWordMemoryValue (t0, new LongValue(lpBuffer.dwLength.longValue()));
		t0 += 4;
		memory.setDoubleWordMemoryValue (t0, new LongValue(lpBuffer.dwMemoryLoad.longValue()));
		t0 += 4;
		memory.setDoubleWordMemoryValue (t0, new LongValue(lpBuffer.dwTotalPhys.longValue()));
		t0 += 4;
		memory.setDoubleWordMemoryValue (t0, new LongValue(lpBuffer.dwAvailPhys.longValue()));
		t0 += 4;
		memory.setDoubleWordMemoryValue (t0, new LongValue(lpBuffer.dwTotalPageFile.longValue()));
		t0 += 4;
		memory.setDoubleWordMemoryValue (t0, new LongValue(lpBuffer.dwAvailPageFile.longValue()));
		t0 += 4;
		memory.setDoubleWordMemoryValue (t0, new LongValue(lpBuffer.dwTotalVirtual.longValue()));
		t0 += 4;
		memory.setDoubleWordMemoryValue (t0, new LongValue(lpBuffer.dwAvailVirtual.longValue()));
		t0 += 4;

	}
}