package v2.org.analysis.apihandle.winapi.nddeapi;

import v2.org.analysis.apihandle.winapi.API;

public abstract class NddeapiAPI extends API {
	public NddeapiAPI () {
		this.libraryName = "nddeapi.dll";
	}
}