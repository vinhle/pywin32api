package v2.org.analysis.apihandle.winapi.advapi32;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.BaseTSD.ULONG_PTR;
import com.sun.jna.platform.win32.BaseTSD.ULONG_PTRByReference;
import com.sun.jna.platform.win32.WinBase.FILETIME;
import com.sun.jna.platform.win32.WinBase.SECURITY_ATTRIBUTES;
import com.sun.jna.platform.win32.WinDef.BOOL;
import com.sun.jna.platform.win32.WinDef.CHARByReference;
import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.platform.win32.WinDef.UINT;
import com.sun.jna.platform.win32.WinDef.ULONG;
import com.sun.jna.platform.win32.WinNT;
import com.sun.jna.platform.win32.WinNT.ACL;
import com.sun.jna.platform.win32.WinNT.GENERIC_MAPPING;
import com.sun.jna.platform.win32.WinNT.HANDLE;
import com.sun.jna.platform.win32.WinNT.HANDLEByReference;
import com.sun.jna.platform.win32.WinNT.PRIVILEGE_SET;
import com.sun.jna.platform.win32.WinNT.PSID;
import com.sun.jna.platform.win32.WinNT.SECURITY_IMPERSONATION_LEVEL;
import com.sun.jna.platform.win32.WinNT.TOKEN_PRIVILEGES;
import com.sun.jna.platform.win32.WinNT.TOKEN_TYPE;
import com.sun.jna.platform.win32.WinNT.WELL_KNOWN_SID_TYPE;
import com.sun.jna.platform.win32.WinReg.HKEY;
import com.sun.jna.platform.win32.Winsvc.SC_HANDLE;
import com.sun.jna.platform.win32.Winsvc.SC_STATUS_TYPE;
import com.sun.jna.platform.win32.Winsvc.SERVICE_STATUS;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.PointerByReference;
import com.sun.jna.win32.StdCallLibrary;
import com.sun.jna.win32.W32APIOptions;

import v2.org.analysis.apihandle.structures.ENUM_SERVICE_STATUS;
import v2.org.analysis.apihandle.structures.EVENT_INSTANCE_INFO;
import v2.org.analysis.apihandle.structures.HW_PROFILE_INFO;
import v2.org.analysis.apihandle.structures.QUERY_SERVICE_CONFIG;
import v2.org.analysis.apihandle.structures.QUERY_SERVICE_LOCK_STATUS;
import v2.org.analysis.apihandle.structures.VALENT;


public interface Advapi32DLL extends StdCallLibrary {
	Advapi32DLL INSTANCE = (Advapi32DLL) Native.loadLibrary("advapi32", Advapi32DLL.class, W32APIOptions.DEFAULT_OPTIONS);
	
	// API's Interfaces
	int AbortSystemShutdown ( String lpMachineName );

	int AccessCheck ( Pointer pSecurityDescriptor, HANDLE ClientToken, DWORD DesiredAccess, GENERIC_MAPPING GenericMapping, PRIVILEGE_SET PrivilegeSet, IntByReference PrivilegeSetLength, IntByReference GrantedAccess, IntByReference AccessStatus );

	int AdjustTokenPrivileges ( HANDLE TokenHandle, BOOL DisableAllPrivileges, TOKEN_PRIVILEGES NewState, DWORD BufferLength, TOKEN_PRIVILEGES PreviousState, int[] ReturnLength );

	int BackupEventLog ( HANDLE hEventLog, String lpBackupFileName );

	int ChangeServiceConfig ( SC_HANDLE hService, DWORD dwServiceType, DWORD dwStartType, DWORD dwErrorControl, String lpBinaryPathName, String lpLoadOrderGroup, IntByReference lpdwTagId, char[] lpDependencies, String lpServiceStartName, String lpPassword, String lpDisplayName );

	int ClearEventLog ( HANDLE hEventLog, String lpBackupFileName );

	int OpenEncryptedFileRaw ( String lpFileName, ULONG ulFlags, PointerByReference pvContext );

	int CloseEventLog ( HANDLE hEventLog );

	int CloseServiceHandle ( SC_HANDLE hSCObject );

	int ControlService ( SC_HANDLE hService, DWORD dwControl, SERVICE_STATUS lpServiceStatus );

	int ConvertSidToStringSid ( PSID Sid, PointerByReference StringSid );

	int ConvertStringSidToSid ( String StringSid, WinNT.PSIDByReference Sid );

	int CreateService ( SC_HANDLE hSCManager, String lpServiceName, String lpDisplayName, DWORD dwDesiredAccess, DWORD dwServiceType, DWORD dwStartType, DWORD dwErrorControl, String lpBinaryPathName, String lpLoadOrderGroup, IntByReference lpdwTagId, char[] lpDependencies, String lpServiceStartName, String lpPassword );

	int CreateTraceInstanceId ( HANDLE RegHandle, EVENT_INSTANCE_INFO pInstInfo );

	int CreateWellKnownSid ( WELL_KNOWN_SID_TYPE WellKnownSidType, PSID DomainSid, PSID pSid, IntByReference cbSid );

	int CryptAcquireContext ( ULONG_PTRByReference phProv, String pszContainer, String pszProvider, DWORD dwProvType, DWORD dwFlags );

	int CryptCreateHash ( ULONG_PTR hProv, UINT Algid, ULONG_PTR hKey, DWORD dwFlags, ULONG_PTRByReference phHash );

	int CryptDecrypt ( ULONG_PTR hKey, ULONG_PTR hHash, BOOL Final, DWORD dwFlags, byte[] pbData, IntByReference pdwDataLen );

	int CryptDeriveKey ( ULONG_PTR hProv, UINT Algid, ULONG_PTR hBaseData, DWORD dwFlags, ULONG_PTRByReference phKey );

	int CryptDestroyHash ( ULONG_PTR hHash );

	int CryptDestroyKey ( ULONG_PTR hKey );

	int CryptHashData ( ULONG_PTR hHash, byte[] pbData, DWORD dwDataLen, DWORD dwFlags );

	int CryptReleaseContext ( ULONG_PTR hProv, DWORD dwFlags );

	int DecryptFile ( String lpFileName, DWORD dwReserved );

	int DeleteService ( SC_HANDLE hService );

	int DeregisterEventSource ( HANDLE hEventLog );

	int DuplicateEncryptionInfoFile ( String SrcFileName, String DstFileName, DWORD dwCreationDistribution, DWORD dwAttributes, SECURITY_ATTRIBUTES lpSecurityAttributes );

	int DuplicateToken ( HANDLE ExistingTokenHandle, SECURITY_IMPERSONATION_LEVEL ImpersonationLevel, HANDLEByReference DuplicateTokenHandle );

	int DuplicateTokenEx ( HANDLE hExistingToken, DWORD dwDesiredAccess, SECURITY_ATTRIBUTES lpTokenAttributes, SECURITY_IMPERSONATION_LEVEL ImpersonationLevel, TOKEN_TYPE TokenType, HANDLEByReference phNewToken );

	int EncryptFile ( String lpFileName );

	int EncryptionDisable ( CHARByReference DirPath, BOOL Disable );

	int EnumDependentServices ( SC_HANDLE hService, DWORD dwServiceState, ENUM_SERVICE_STATUS lpServices, DWORD cbBufSize, int[] pcbBytesNeeded, IntByReference lpServicesReturned );

	int EnumerateTraceGuids ( PointerByReference GuidPropertiesArray, ULONG PropertyArrayCount, IntByReference GuidCount );

	int EnumServicesStatus ( SC_HANDLE hSCManager, DWORD dwServiceType, DWORD dwServiceState, ENUM_SERVICE_STATUS lpServices, DWORD cbBufSize, int[] pcbBytesNeeded, IntByReference lpServicesReturned, IntByReference lpResumeHandle );

	int FileEncryptionStatus ( String lpFileName, IntByReference lpStatus );

	int GetCurrentHwProfile ( HW_PROFILE_INFO lpHwProfileInfo );

	int GetFileSecurity ( String lpFileName, int RequestedInformation, Pointer pSecurityDescriptor, DWORD nLength, IntByReference lpnLengthNeeded );

	int GetLengthSid ( PSID pSid );

	int GetNamedSecurityInfo ( String pObjectName, int ObjectType, int SecurityInfo, WinNT.PSIDByReference ppsidOwner, WinNT.PSIDByReference ppsidGroup, PointerByReference ppDacl, PointerByReference ppSacl, PointerByReference ppSecurityDescriptor );

	int GetNumberOfEventLogRecords ( HANDLE hEventLog, IntByReference NumberOfRecords );

	int GetOldestEventLogRecord ( HANDLE hEventLog, IntByReference OldestRecord );

	int GetSecurityDescriptorLength ( Pointer pSecurityDescriptor );

	int GetServiceDisplayName ( SC_HANDLE hSCManager, String lpServiceName, char[] lpDisplayName, IntByReference lpcchBuffer );

	int GetServiceKeyName ( SC_HANDLE hSCManager, String lpDisplayName, char[] lpServiceName, IntByReference lpcchBuffer );

	int GetUserName ( char[] lpBuffer, IntByReference lpnSize );

	int ImpersonateLoggedOnUser ( HANDLE hToken );

	int ImpersonateSelf ( SECURITY_IMPERSONATION_LEVEL ImpersonationLevel );

	int InitiateShutdown ( String lpMachineName, String lpMessage, DWORD dwGracePeriod, DWORD dwShutdownFlags, DWORD dwReason );

	int InitiateSystemShutdown ( String lpMachineName, String lpMessage, DWORD dwTimeout, BOOL bForceAppsClosed, BOOL bRebootAfterShutdown );

	int InitiateSystemShutdownEx ( String lpMachineName, String lpMessage, DWORD dwTimeout, BOOL bForceAppsClosed, BOOL bRebootAfterShutdown, DWORD dwReason );

	int IsValidAcl ( ACL pAcl );

	int IsValidSecurityDescriptor ( Pointer pSecurityDescriptor );

	int IsValidSid ( PSID pSid );

	int IsWellKnownSid ( PSID pSid, WELL_KNOWN_SID_TYPE WellKnownSidType );

	int LockServiceDatabase ( SC_HANDLE hSCManager );

	int LogonUser ( String lpszUsername, String lpszDomain, String lpszPassword, DWORD dwLogonType, DWORD dwLogonProvider, HANDLEByReference phToken );

	int LookupAccountName ( String lpSystemName, String lpAccountName, PSID Sid, IntByReference cbSid, String ReferencedDomainName, IntByReference cchReferencedDomainName, PointerByReference peUse );

	int LookupAccountSid ( String lpSystemName, PSID lpSid, char[] lpName, IntByReference cchName, char[] lpReferencedDomainName, IntByReference cchReferencedDomainName, PointerByReference peUse );

	int LookupPrivilegeName ( String lpSystemName, WinNT.LUID lpLuid, char[] lpName, IntByReference cchName );

	int LookupPrivilegeValue ( String lpSystemName, String lpName, WinNT.LUID lpLuid );

	void MapGenericMask ( IntByReference AccessMask, GENERIC_MAPPING GenericMapping );

	int NotifyBootConfigStatus ( BOOL BootAcceptable );

	int NotifyChangeEventLog ( HANDLE hEventLog, HANDLE hEvent );

	int OpenBackupEventLog ( String lpUNCServerName, String lpFileName );

	int OpenEventLog ( String lpUNCServerName, String lpSourceName );

	int OpenProcessToken ( HANDLE ProcessHandle, DWORD DesiredAccess, HANDLEByReference TokenHandle );

	int OpenSCManager ( String lpMachineName, String lpDatabaseName, DWORD dwDesiredAccess );

	int OpenService ( SC_HANDLE hSCManager, String lpServiceName, DWORD dwDesiredAccess );

	int OpenThreadToken ( HANDLE ThreadHandle, DWORD DesiredAccess, BOOL OpenAsSelf, HANDLEByReference TokenHandle );

	int QueryServiceConfig ( SC_HANDLE hService, QUERY_SERVICE_CONFIG lpServiceConfig, DWORD cbBufSize, IntByReference pcbBytesNeeded );

	int QueryServiceConfig2 ( SC_HANDLE hService, DWORD dwInfoLevel, byte[] lpBuffer, DWORD cbBufSize, IntByReference pcbBytesNeeded );

	int QueryServiceLockStatus ( SC_HANDLE hSCManager, QUERY_SERVICE_LOCK_STATUS lpLockStatus, DWORD cbBufSize, IntByReference pcbBytesNeeded );

	int QueryServiceStatus ( SC_HANDLE hService, SERVICE_STATUS lpServiceStatus );

	int QueryServiceStatusEx ( SC_HANDLE hService, SC_STATUS_TYPE InfoLevel, byte[] lpBuffer, DWORD cbBufSize, IntByReference pcbBytesNeeded );

	int RegCloseKey ( HKEY hKey );

	int RegConnectRegistry ( String lpMachineName, HKEY hKey, HANDLEByReference phkResult );

	int RegCopyTree ( HKEY hKeySrc, String lpSubKey, HKEY hKeyDest );

	int RegCreateKey ( HKEY hKey, String lpSubKey, HANDLEByReference phkResult );

	int RegCreateKeyEx ( HKEY hKey, String lpSubKey, DWORD Reserved, String lpClass, DWORD dwOptions, int samDesired, SECURITY_ATTRIBUTES lpSecurityAttributes, HANDLEByReference phkResult, IntByReference lpdwDisposition );

	int RegDeleteKey ( HKEY hKey, String lpSubKey );

	int RegDeleteKeyEx ( HKEY hKey, String lpSubKey, int samDesired, DWORD Reserved );

	int RegDeleteKeyValue ( HKEY hKey, String lpSubKey, String lpValueName );

	int RegDeleteTree ( HKEY hKey, String lpSubKey );

	int RegDeleteValue ( HKEY hKey, String lpValueName );

	int RegDisablePredefinedCache ( );

	int RegDisablePredefinedCacheEx ( );

	int RegDisableReflectionKey ( HKEY hBase );

	int RegEnableReflectionKey ( HKEY hBase );

	int RegEnumKey ( HKEY hKey, DWORD dwIndex, String lpName, DWORD cchName );

	int RegEnumKeyEx ( HKEY hKey, DWORD dwIndex, String lpName, IntByReference lpcName, IntByReference lpReserved, String lpClass, IntByReference lpcClass, FILETIME lpftLastWriteTime );

	int RegEnumValue ( HKEY hKey, DWORD dwIndex, String lpValueName, IntByReference lpcchValueName, IntByReference lpReserved, IntByReference lpType, byte lpData, IntByReference lpcbData );

	int RegFlushKey ( HKEY hKey );

	int RegisterEventSource ( String lpUNCServerName, String lpSourceName );

	int RegLoadAppKey ( String lpFile, HANDLEByReference phkResult, int samDesired, DWORD dwOptions, DWORD Reserved );

	int RegLoadKey ( HKEY hKey, String lpSubKey, String lpFile );

	int RegLoadMUIString ( HKEY hKey, String pszValue, String pszOutBuf, DWORD cbOutBuf, IntByReference pcbData, DWORD Flags, String pszDirectory );

	int RegNotifyChangeKeyValue ( HKEY hKey, BOOL bWatchSubtree, DWORD dwNotifyFilter, HANDLE hEvent, BOOL fAsynchronous );

	int RegOpenCurrentUser ( int samDesired, HANDLEByReference phkResult );

	int RegOpenKey ( HKEY hKey, String lpSubKey, HANDLEByReference phkResult );

	int RegOpenKeyEx ( HKEY hKey, String lpSubKey, DWORD ulOptions, int samDesired, HANDLEByReference phkResult );

	int RegOpenUserClassesRoot ( HANDLE hToken, DWORD dwOptions, int samDesired, HANDLEByReference phkResult );

	int RegOverridePredefKey ( HKEY hKey, HKEY hNewHKey );

	int RegQueryInfoKey ( HKEY hKey, char[] lpClass, IntByReference lpcClass, IntByReference lpReserved, IntByReference lpcSubKeys, IntByReference lpcMaxSubKeyLen, IntByReference lpcMaxClassLen, IntByReference lpcValues, IntByReference lpcMaxValueNameLen, IntByReference lpcMaxValueLen, IntByReference lpcbSecurityDescriptor, FILETIME lpftLastWriteTime );

	int RegQueryMultipleValues ( HKEY hKey, VALENT val_list, DWORD num_vals, String lpValueBuf, IntByReference ldwTotsize );

	int RegQueryReflectionKey ( HKEY hBase, IntByReference bIsReflectionDisabled );

	int RegQueryValue ( HKEY hKey, String lpSubKey, String lpValue, IntByReference lpcbValue );

	int RegQueryValueEx ( HKEY hKey, String lpValueName, IntByReference lpReserved, IntByReference lpType, byte lpData, IntByReference lpcbData );

	int RegReplaceKey ( HKEY hKey, String lpSubKey, String lpNewFile, String lpOldFile );

	int RegRestoreKey ( HKEY hKey, String lpFile, DWORD dwFlags );

	int RegSaveKey ( HKEY hKey, String lpFile, SECURITY_ATTRIBUTES lpSecurityAttributes );

	int RegSaveKeyEx ( HKEY hKey, String lpFile, SECURITY_ATTRIBUTES lpSecurityAttributes, DWORD Flags );

	int RegSetKeyValue ( HKEY hKey, String lpSubKey, String lpValueName, DWORD dwType, Pointer lpData, DWORD cbData );

	int RegSetValue ( HKEY hKey, String lpSubKey, DWORD dwType, String lpData, DWORD cbData );

	int RegSetValueEx ( HKEY hKey, String lpValueName, DWORD Reserved, DWORD dwType, byte lpData, DWORD cbData );

	int RegUnLoadKey ( HKEY hKey, String lpSubKey );

	int RevertToSelf ( );

	int SetNamedSecurityInfo ( String pObjectName, int ObjectType, int SecurityInfo, PSID psidOwner, PSID psidGroup, ACL pDacl, ACL pSacl );

	int SetServiceBits ( HANDLE hServiceStatus, DWORD dwServiceBits, BOOL bSetBitsOn, BOOL bUpdateImmediately );

	int SetServiceStatus ( HANDLE hServiceStatus, SERVICE_STATUS lpServiceStatus );

	int SetThreadToken ( HANDLEByReference Thread, HANDLE Token );

	int StartService ( SC_HANDLE hService, DWORD dwNumServiceArgs, String[] lpServiceArgVectors );

	int UnlockServiceDatabase ( Pointer ScLock );

}