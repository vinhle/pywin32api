/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.advapi32.functions
 * File name: CryptAcquireContext.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.advapi32.functions;

import com.sun.jna.platform.win32.BaseTSD.ULONG_PTR;
import com.sun.jna.platform.win32.BaseTSD.ULONG_PTRByReference;
import com.sun.jna.platform.win32.WinDef.DWORD;

import v2.org.analysis.apihandle.winapi.advapi32.Advapi32API;
import v2.org.analysis.apihandle.winapi.advapi32.Advapi32DLL;
import v2.org.analysis.value.LongValue;
 
public class CryptAcquireContext extends Advapi32API {
	public CryptAcquireContext () {
		super();
		NUM_OF_PARMS = 5;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		long t2 = this.params.get(2);
		long t3 = this.params.get(3);
		long t4 = this.params.get(4);
		
		// Step 2: type conversion from C++ to Java
		ULONG_PTRByReference phProv = new ULONG_PTRByReference (new ULONG_PTR(t0));
		String pszContainer = null;
		if ( t1 != 0L ) pszContainer = memory.getText(this, t1);
		String pszProvider = null;
		if ( t2 != 0L ) pszProvider = memory.getText(this, t2);
		DWORD dwProvType = new DWORD (t3);
		DWORD dwFlags = new DWORD (t4);

		// Step 3: call API function
		int ret = Advapi32DLL.INSTANCE.CryptAcquireContext (phProv, pszContainer, pszProvider, dwProvType, dwFlags);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));
		memory.setDoubleWordMemoryValue(t0, new LongValue(phProv.getValue().longValue()));

		

	}
}