/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.advapi32.functions
 * File name: OpenProcessToken.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.advapi32.functions;

import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.platform.win32.WinNT.HANDLE;
import com.sun.jna.platform.win32.WinNT.HANDLEByReference;

import v2.org.analysis.apihandle.winapi.advapi32.Advapi32API;
import v2.org.analysis.apihandle.winapi.advapi32.Advapi32DLL;
import v2.org.analysis.value.LongValue;
 
public class OpenProcessToken extends Advapi32API {
	public OpenProcessToken () {
		super();
		NUM_OF_PARMS = 3;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		long t2 = this.params.get(2);
		
		// Step 2: type conversion from C++ to Java
		HANDLE ProcessHandle = null;
		if ( t0 != 0L ) {
			ProcessHandle = new HANDLE ();
			ProcessHandle.setPointer(new Pointer(t0));
		}
		DWORD DesiredAccess = new DWORD (t1);
		HANDLEByReference TokenHandle = new HANDLEByReference (new HANDLE(new Pointer(t2)));

		// Step 3: call API function
		int ret = Advapi32DLL.INSTANCE.OpenProcessToken (ProcessHandle, DesiredAccess, TokenHandle);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));
		memory.setDoubleWordMemoryValue(t2, new LongValue(Pointer.nativeValue(TokenHandle.getValue().getPointer())));

		

	}
}