/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.advapi32.functions
 * File name: CryptDeriveKey.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.advapi32.functions;

import com.sun.jna.platform.win32.BaseTSD.ULONG_PTR;
import com.sun.jna.platform.win32.BaseTSD.ULONG_PTRByReference;
import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.platform.win32.WinDef.UINT;

import v2.org.analysis.apihandle.winapi.advapi32.Advapi32API;
import v2.org.analysis.apihandle.winapi.advapi32.Advapi32DLL;
import v2.org.analysis.value.LongValue;
 
public class CryptDeriveKey extends Advapi32API {
	public CryptDeriveKey () {
		super();
		NUM_OF_PARMS = 5;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		long t2 = this.params.get(2);
		long t3 = this.params.get(3);
		long t4 = this.params.get(4);
		
		// Step 2: type conversion from C++ to Java
		ULONG_PTR hProv = new ULONG_PTR (t0);
		UINT Algid = new UINT (t1);
		ULONG_PTR hBaseData = new ULONG_PTR (t2);
		DWORD dwFlags = new DWORD (t3);
		ULONG_PTRByReference phKey = new ULONG_PTRByReference (new ULONG_PTR(t4));

		// Step 3: call API function
		int ret = Advapi32DLL.INSTANCE.CryptDeriveKey (hProv, Algid, hBaseData, dwFlags, phKey);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));

	}
}