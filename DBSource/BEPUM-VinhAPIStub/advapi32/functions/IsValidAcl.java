/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.advapi32.functions
 * File name: IsValidAcl.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.advapi32.functions;

import com.sun.jna.platform.win32.WinNT.ACL;

import v2.org.analysis.apihandle.winapi.advapi32.Advapi32API;
import v2.org.analysis.apihandle.winapi.advapi32.Advapi32DLL;
import v2.org.analysis.value.LongValue;
 
public class IsValidAcl extends Advapi32API {
	public IsValidAcl () {
		super();
		NUM_OF_PARMS = 1;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		
		// Step 2: type conversion from C++ to Java
		ACL pAcl = null;
		if ( t0 != 0L) {
			pAcl = new ACL ();
			pAcl.AclRevision = (byte) ((LongValue)memory.getByteMemoryValue (t0)).getValue();
			t0 += 1;
			pAcl.Sbz1 = (byte) ((LongValue)memory.getByteMemoryValue (t0)).getValue();
			t0 += 1;
			pAcl.AclSize = (short) ((LongValue)memory.getWordMemoryValue (t0)).getValue();
			t0 += 2;
			pAcl.AceCount = (short) ((LongValue)memory.getWordMemoryValue (t0)).getValue();
			t0 += 2;
			pAcl.Sbz2 = (short) ((LongValue)memory.getWordMemoryValue (t0)).getValue();
			t0 += 2;
		}

		// Step 3: call API function
		int ret = Advapi32DLL.INSTANCE.IsValidAcl (pAcl);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));

	}
}