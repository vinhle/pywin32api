/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.advapi32.functions
 * File name: CryptDecrypt.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.advapi32.functions;

import com.sun.jna.platform.win32.BaseTSD.ULONG_PTR;
import com.sun.jna.platform.win32.WinDef.BOOL;
import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.ptr.IntByReference;

import v2.org.analysis.apihandle.winapi.advapi32.Advapi32API;
import v2.org.analysis.apihandle.winapi.advapi32.Advapi32DLL;
import v2.org.analysis.value.LongValue;
 
public class CryptDecrypt extends Advapi32API {
	public CryptDecrypt () {
		super();
		NUM_OF_PARMS = 6;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		long t2 = this.params.get(2);
		long t3 = this.params.get(3);
		long t4 = this.params.get(4);
		long t5 = this.params.get(5);
		
		// Step 2: type conversion from C++ to Java
		ULONG_PTR hKey = new ULONG_PTR (t0);
		ULONG_PTR hHash = new ULONG_PTR (t1);
		BOOL Final = new BOOL (t2);
		DWORD dwFlags = new DWORD (t3);
		byte[] pbData = null;
		if ( t4 != 0L ) pbData = new byte[(int) t5];
		for (int i = 0; i < pbData.length; i++) {
			pbData [i] = (byte) ((LongValue) memory.getByteMemoryValue (t4)).getValue();
			t4 += 1;
		}
		IntByReference pdwDataLen = new IntByReference ((int) t5);

		// Step 3: call API function
		int ret = Advapi32DLL.INSTANCE.CryptDecrypt (hKey, hHash, Final, dwFlags, pbData, pdwDataLen);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));

	}
}