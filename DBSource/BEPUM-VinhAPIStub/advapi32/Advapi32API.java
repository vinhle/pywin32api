package v2.org.analysis.apihandle.winapi.advapi32;

import v2.org.analysis.apihandle.winapi.API;

public abstract class Advapi32API extends API {
	public Advapi32API () {
		this.libraryName = "advapi32.dll";
	}
}