package v2.org.analysis.apihandle.winapi.msimg32;

import v2.org.analysis.apihandle.winapi.API;

public abstract class Msimg32API extends API {
	public Msimg32API () {
		this.libraryName = "msimg32.dll";
	}
}