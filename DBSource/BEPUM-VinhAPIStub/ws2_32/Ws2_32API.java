package v2.org.analysis.apihandle.winapi.ws2_32;

import v2.org.analysis.apihandle.winapi.API;

public abstract class Ws2_32API extends API {
	public Ws2_32API () {
		this.libraryName = "ws2_32.dll";
	}
}