package v2.org.analysis.apihandle.winapi.ws2_32;

import com.sun.jna.Native;
import com.sun.jna.win32.StdCallLibrary;
import com.sun.jna.win32.W32APIOptions;


public interface Ws2_32DLL extends StdCallLibrary {
	Ws2_32DLL INSTANCE = (Ws2_32DLL) Native.loadLibrary("ws2_32", Ws2_32DLL.class, W32APIOptions.DEFAULT_OPTIONS);
	
	// API's Interfaces
	int gethostname ( byte[] name, int namelen );

	int WSACleanup ( );

	//int WSAStartup ( WORD wVersionRequested, WSADATA lpWSAData );

}