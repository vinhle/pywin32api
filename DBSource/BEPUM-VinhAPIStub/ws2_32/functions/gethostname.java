/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.ws2_32.functions
 * File name: gethostname.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.ws2_32.functions;

import v2.org.analysis.apihandle.winapi.ws2_32.Ws2_32API;
import v2.org.analysis.apihandle.winapi.ws2_32.Ws2_32DLL;
import v2.org.analysis.value.LongValue;

 
public class gethostname extends Ws2_32API {
	public gethostname () {
		super();
		NUM_OF_PARMS = 2;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		long t1 = this.params.get(1);
		
		// Step 2: type conversion from C++ to Java
		byte[] name = null;
		if ( t0 != 0L ) name = new byte[(int) t1];
		for (int i = 0; i < name.length; i++) {
			name [i] = (byte) ((LongValue) memory.getByteMemoryValue (t0)).getValue();
			t0 += 1;
		}
		int namelen = (int) t1;

		// Step 3: call API function
		int ret = Ws2_32DLL.INSTANCE.gethostname (name, namelen);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));
		t0 = this.params.get(0);
		for (int i = 0; i < name.length; i++) {
			memory.setByteMemoryValue (t0, new LongValue(name [i]));
			t0 += 1;
		}
	}
}