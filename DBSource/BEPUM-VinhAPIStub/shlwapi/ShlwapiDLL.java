package v2.org.analysis.apihandle.winapi.shlwapi;

import com.sun.jna.Native;
import com.sun.jna.platform.win32.WinDef.CHARByReference;
import com.sun.jna.win32.StdCallLibrary;
import com.sun.jna.win32.W32APIOptions;


public interface ShlwapiDLL extends StdCallLibrary {
	ShlwapiDLL INSTANCE = (ShlwapiDLL) Native.loadLibrary("shlwapi", ShlwapiDLL.class, W32APIOptions.DEFAULT_OPTIONS);
	
	// API's Interfaces
	int PathFindFileName ( CHARByReference pPath );

	void PathUndecorate ( String pszPath );

	int StrChrI ( CHARByReference pszStart, char wMatch );

}