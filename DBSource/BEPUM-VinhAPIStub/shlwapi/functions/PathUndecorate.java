/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.shlwapi.functions
 * File name: PathUndecorate.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.shlwapi.functions;

import v2.org.analysis.apihandle.winapi.shlwapi.ShlwapiAPI;
import v2.org.analysis.apihandle.winapi.shlwapi.ShlwapiDLL;
 
public class PathUndecorate extends ShlwapiAPI {
	public PathUndecorate () {
		super();
		NUM_OF_PARMS = 1;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		
		// Step 2: type conversion from C++ to Java
		String pszPath = null;
		if ( t0 != 0L ) pszPath = memory.getText(this, t0);

		// Step 3: call API function
		ShlwapiDLL.INSTANCE.PathUndecorate (pszPath);
		
		// Step 4: update environment (memory & eax register)

	}
}