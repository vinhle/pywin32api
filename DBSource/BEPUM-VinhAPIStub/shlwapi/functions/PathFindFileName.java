/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.shlwapi.functions
 * File name: PathFindFileName.java
 * Author: Vinh Le
 */

package v2.org.analysis.apihandle.winapi.shlwapi.functions;

import com.sun.jna.platform.win32.WinDef.CHAR;
import com.sun.jna.platform.win32.WinDef.CHARByReference;

import v2.org.analysis.apihandle.winapi.shlwapi.ShlwapiAPI;
import v2.org.analysis.apihandle.winapi.shlwapi.ShlwapiDLL;
import v2.org.analysis.value.LongValue;
 
public class PathFindFileName extends ShlwapiAPI {
	public PathFindFileName () {
		super();
		NUM_OF_PARMS = 1;
	}

	@Override
	public void execute() {
		// Step 1: get original parameter values from stack
		long t0 = this.params.get(0);
		
		// Step 2: type conversion from C++ to Java
		CHARByReference pPath = new CHARByReference (new CHAR(t0));

		// Step 3: call API function
		int ret = ShlwapiDLL.INSTANCE.PathFindFileName (pPath);
		
		// Step 4: update environment (memory & eax register)
		long value = ret;
		register.mov("eax", new LongValue(value));

	}
}