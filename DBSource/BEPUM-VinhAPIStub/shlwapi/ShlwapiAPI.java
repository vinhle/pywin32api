package v2.org.analysis.apihandle.winapi.shlwapi;

import v2.org.analysis.apihandle.winapi.API;

public abstract class ShlwapiAPI extends API {
	public ShlwapiAPI () {
		this.libraryName = "shlwapi.dll";
	}
}