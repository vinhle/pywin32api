{
  "PMEMORY_BASIC_INFORMATION": [
    "WinNT.MEMORY_BASIC_INFORMATION"
  ], 
  "PVOID*": [
    "PointerByReference"
  ], 
  "LPPROCESSENTRY32": [
    "Tlhelp32.PROCESSENTRY32"
  ], 
  "PDWORD": [
    "WinDef.DWORDByReference"
  ], 
  "HMONITOR": [
    "WinUser.HMONITOR"
  ], 
  "_EXCEPTION_POINTERS*": [
    "Pointer"
  ], 
  "LPCRITICAL_SECTION": [
    "PRTL_CRITICAL_SECTION"
  ], 
  "HCRYPTPROV*": [
    "ULONG_PTRByReference"
  ], 
  "LPOVERLAPPED*": [
    "PointerByReference"
  ], 
  "HHOOK": [
    "WinUser.HHOOK"
  ], 
  "LPMEMORYSTATUSEX": [
    "WinBase.MEMORYSTATUSEX"
  ], 
  "COLORREF*": [
    "IntByReference"
  ], 
  "ULONG_PTR": [
    "Pointer"
  ], 
  "PAPPBARDATA": [
    "ShellAPI.APPBARDATA"
  ], 
  "LPOVERLAPPED": [
    "WinBase.OVERLAPPED"
  ], 
  "HGLOBAL": [
    "HANDLE"
  ], 
  "PIXELFORMATDESCRIPTOR*": [
    "WinGDI.PIXELFORMATDESCRIPTOR.ByReference"
  ], 
  "TIMERPROC": [
    "Callback"
  ], 
  "REFKNOWNFOLDERID": [
    "Guid.GUID"
  ], 
  "LPTIME_ZONE_INFORMATION": [
    "WinBase.TIME_ZONE_INFORMATION"
  ], 
  "LPOSVERSIONINFO": [
    "WinNT.OSVERSIONINFOEX"
  ], 
  "WNDENUMPROC": [
    "WinUser.WNDENUMPROC"
  ], 
  "COMPUTER_NAME_FORMAT": [
    "int"
  ], 
  "LCTYPE": [
    "DWORD"
  ], 
  "PFILETIME": [
    "WinBase.FILETIME"
  ], 
  "LPTOP_LEVEL_EXCEPTION_FILTER": [
    "Callback"
  ], 
  "PSID_NAME_USE": [
    "PointerByReference"
  ], 
  "LPCWSTR": [
    "WString"
  ], 
  "LPPOINT": [
    "POINT"
  ], 
  "SIZE_T*": [
    "ULONG_PTRByReference"
  ], 
  "PHKEY": [
    "WinReg.HKEYByReference"
  ], 
  "PACL*": [
    "PointerByReference"
  ], 
  "LPCVOID": [
    "Pointer", 
    "byte[]", 
    "LPVOID"
  ], 
  "LPMSG": [
    "WinUser.MSG"
  ], 
  "SC_STATUS_TYPE": [
    "int"
  ], 
  "HMODULE": [
    "HMODULE"
  ], 
  "HWND": [
    "WinDef.HWND"
  ], 
  "HANDLE*": [
    "WinNT.HANDLE[]"
  ], 
  "LPVOID": [
    "String", 
    "Structure", 
    "Pointer", 
    "byte[]", 
    "WinNT.FILE_NOTIFY_INFORMATION", 
    "WinDef.LPVOID", 
    "ByteBuffer"
  ], 
  "SC_HANDLE": [
    "Winsvc.SC_HANDLE"
  ], 
  "RGNDATA*": [
    "WinGDI.RGNDATA"
  ], 
  "POINT*": [
    "WinDef.POINT[]", 
    "WinDef.POINT"
  ], 
  "MONITORENUMPROC": [
    "WinUser.MONITORENUMPROC"
  ], 
  "LPCMENUITEMINFO": [
    "MENUITEMINFO"
  ], 
  "PACL": [
    "ACL"
  ], 
  "HDEVNOTIFY": [
    "WinUser.HDEVNOTIFY"
  ], 
  "LONG_PTR": [
    "Pointer"
  ], 
  "LARGE_INTEGER*": [
    "LARGE_INTEGER"
  ], 
  "PLONG": [
    "LONGByReference"
  ], 
  "PTSTR": [
    "String"
  ], 
  "va_list*": [
    "Pointer", 
    "String[]"
  ], 
  "LPMEMORYSTATUS": [
    "MEMORYSTATUS"
  ], 
  "LPHANDLE": [
    "WinNT.HANDLEByReference"
  ], 
  "HDC": [
    "WinDef.HDC"
  ], 
  "PRAWINPUTDEVICELIST": [
    "WinUser.RAWINPUTDEVICELIST[]"
  ], 
  "LPPROCESS_INFORMATION": [
    "WinBase.PROCESS_INFORMATION"
  ], 
  "PUINT": [
    "IntByReference"
  ], 
  "ULONG": [
    "WinDef.ULONG"
  ], 
  "LPSHFILEOPSTRUCT": [
    "ShellAPI.SHFILEOPSTRUCT"
  ], 
  "LPCOMMTIMEOUTS": [
    "WinBase.COMMTIMEOUTS"
  ], 
  "LPSECURITY_ATTRIBUTES": [
    "WinBase.SECURITY_ATTRIBUTES"
  ], 
  "PULARGE_INTEGER": [
    "WinNT.LARGE_INTEGER"
  ], 
  "PSECURITY_DESCRIPTOR": [
    "Pointer"
  ], 
  "HINTERNET": [
    "HANDLE"
  ], 
  "PLUID": [
    "WinNT.LUID"
  ], 
  "PBOOL": [
    "IntByReference"
  ], 
  "SECURITY_INFORMATION": [
    "int"
  ], 
  "LPCRECT": [
    "WinDef.RECT"
  ], 
  "WNDCLASSEX*": [
    "WinUser.WNDCLASSEX"
  ], 
  "HCRYPTKEY": [
    "ULONG_PTR"
  ], 
  "LONG*": [
    "LONGByReference"
  ], 
  "LPSTARTUPINFO": [
    "WinBase.STARTUPINFO"
  ], 
  "LPSERVICE_STATUS": [
    "Winsvc.SERVICE_STATUS"
  ], 
  "LPCPINFO": [
    "CPINFO"
  ], 
  "ACL_INFORMATION_CLASS": [
    "int"
  ], 
  "LPTCH": [
    "Pointer"
  ], 
  "PSECURITY_DESCRIPTOR*": [
    "PointerByReference"
  ], 
  "PSID": [
    "WinNT.PSID"
  ], 
  "LPARAM": [
    "WinDef.LPARAM"
  ], 
  "SECURITY_IMPERSONATION_LEVEL": [
    "int"
  ], 
  "LPDCB": [
    "WinBase.DCB"
  ], 
  "HLOCAL": [
    "HANDLE"
  ], 
  "INT": [
    "int"
  ], 
  "TOKEN_INFORMATION_CLASS": [
    "int"
  ], 
  "HCRYPTKEY*": [
    "ULONG_PTRByReference"
  ], 
  "FILETIME*": [
    "WinBase.FILETIME"
  ], 
  "LPWSTR": [
    "char[]"
  ], 
  "HMENU": [
    "WinDef.HMENU"
  ], 
  "KEY_INFORMATION_CLASS": [
    "int"
  ], 
  "PULONG": [
    "WinDef.ULONGByReference"
  ], 
  "SIZE_T": [
    "BaseTSD.SIZE_T"
  ], 
  "PHANDLER_ROUTINE": [
    "Callback"
  ], 
  "LPWSADATA": [
    "WSADATA"
  ], 
  "LPCTSTR*": [
    "String[]"
  ], 
  "HRGN": [
    "WinDef.HRGN"
  ], 
  "HMODULE*": [
    "HANDLEByReference"
  ], 
  "REGSAM": [
    "int"
  ], 
  "HGDIOBJ": [
    "WinNT.HANDLE"
  ], 
  "PFE_IMPORT_FUNC": [
    "WinBase.FE_IMPORT_FUNC"
  ], 
  "LPWORD": [
    "short[]"
  ], 
  "PWINDOWINFO": [
    "WinUser.WINDOWINFO"
  ], 
  "PTITLEBARINFO": [
    "TITLEBARINFO"
  ], 
  "BITMAPINFO*": [
    "WinGDI.BITMAPINFO"
  ], 
  "PFLS_CALLBACK_FUNCTION": [
    "Callback"
  ], 
  "SE_OBJECT_TYPE": [
    "int"
  ], 
  "HINSTANCE": [
    "WinDef.HINSTANCE"
  ], 
  "LPOVERLAPPED_COMPLETION_ROUTINE": [
    "WinNT.OVERLAPPED_COMPLETION_ROUTINE"
  ], 
  "LPBITMAPINFO": [
    "WinGDI.BITMAPINFO"
  ], 
  "LPCPINFOEX": [
    "CPINFOEX"
  ], 
  "WORD": [
    "int"
  ], 
  "HPALETTE": [
    "HANDLE"
  ], 
  "PFE_EXPORT_FUNC": [
    "WinBase.FE_EXPORT_FUNC"
  ], 
  "POINT": [
    "WinDef.POINT"
  ], 
  "LPTHREAD_START_ROUTINE": [
    "WinBase.FOREIGN_THREAD_START_ROUTINE"
  ], 
  "PULONG_PTR": [
    "BaseTSD.ULONG_PTRByReference"
  ], 
  "LPCTSTR": [
    "String", 
    "WString", 
    "char[]"
  ], 
  "WNDCLASS*": [
    "WNDCLASS"
  ], 
  "PTOKEN_PRIVILEGES": [
    "WinNT.TOKEN_PRIVILEGES"
  ], 
  "LPSYSTEM_INFO": [
    "WinBase.SYSTEM_INFO"
  ], 
  "SYSTEMTIME*": [
    "WinBase.SYSTEMTIME"
  ], 
  "HCRYPTHASH": [
    "ULONG_PTR"
  ], 
  "PPRIVILEGE_SET": [
    "WinNT.PRIVILEGE_SET"
  ], 
  "HANDLE": [
    "WinNT.HANDLE"
  ], 
  "LOGPALETTE*": [
    "LOGPALETTE"
  ], 
  "HCRYPTHASH*": [
    "ULONG_PTRByReference"
  ], 
  "PALETTEENTRY*": [
    "PALETTEENTRY"
  ], 
  "WINDOWPLACEMENT*": [
    "WinUser.WINDOWPLACEMENT"
  ], 
  "UINT_PTR": [
    "UINT_PTR"
  ], 
  "HBITMAP": [
    "WinDef.HBITMAP"
  ], 
  "BYTE": [
    "byte", 
    "BYTE"
  ], 
  "HCRYPTPROV": [
    "ULONG_PTR"
  ], 
  "PSYSTEM_LOGICAL_PROCESSOR_INFORMATION": [
    "Pointer"
  ], 
  "COLORREF": [
    "DWORD"
  ], 
  "RECT*": [
    "WinDef.RECT"
  ], 
  "ALG_ID": [
    "UINT"
  ], 
  "LPWNDCLASSEX": [
    "WinUser.WNDCLASSEX"
  ], 
  "LPDWORD": [
    "WinDef.DWORDByReference"
  ], 
  "LPFILETIME": [
    "WinBase.FILETIME"
  ], 
  "BOOL": [
    "WinDef.BOOL"
  ], 
  "HKEY": [
    "WinReg.HKEY"
  ], 
  "LPSTR": [
    "char[]"
  ], 
  "PDWORD_PTR": [
    "WinDef.DWORDByReference"
  ], 
  "SIZE*": [
    "WinUser.SIZE"
  ], 
  "PSID*": [
    "WinNT.PSIDByReference"
  ], 
  "VOID*": [
    "Pointer", 
    "PointerByReference", 
    "LPVOID"
  ], 
  "int": [
    "int"
  ], 
  "LPTSTR": [
    "String", 
    "char[]", 
    "PointerByReference", 
    "WString"
  ], 
  "PWSTR*": [
    "PointerByReference"
  ], 
  "BITMAPINFOHEADER*": [
    "WinGDI.BITMAPINFOHEADER"
  ], 
  "char*": [
    "byte[]"
  ], 
  "PHANDLE": [
    "WinNT.HANDLEByReference"
  ], 
  "HICON": [
    "WinDef.HICON"
  ], 
  "DWORD": [
    "WinDef.DWORD"
  ], 
  "LPBOOL": [
    "WinDef.BOOLByReference"
  ], 
  "PFLASHWINFO": [
    "WinUser.FLASHWINFO"
  ], 
  "PICONINFO": [
    "WinGDI.ICONINFO"
  ], 
  "LPMONITORINFO": [
    "WinUser.MONITORINFOEX"
  ], 
  "DWORD*": [
    "IntByReference", 
    "DWORDByReference"
  ], 
  "PGENERIC_MAPPING": [
    "WinNT.GENERIC_MAPPING"
  ], 
  "LPINPUT": [
    "WinUser.INPUT[]"
  ], 
  "PLASTINPUTINFO": [
    "WinUser.LASTINPUTINFO"
  ], 
  "LPCSTR": [
    "String"
  ], 
  "INT*": [
    "int[]"
  ], 
  "LPSYSTEMTIME": [
    "WinBase.SYSTEMTIME"
  ], 
  "LCID": [
    "LCID"
  ], 
  "BYTE*": [
    "char[]", 
    "ByteByReference"
  ], 
  "LPTHREADENTRY32": [
    "THREADENTRY32"
  ], 
  "HOOKPROC": [
    "WinUser.HOOKPROC"
  ], 
  "MSG*": [
    "WinUser.MSG"
  ], 
  "XFORM*": [
    "Pointer"
  ], 
  "LPTSTR*": [
    "PointerByReference"
  ], 
  "LPWIN32_FIND_DATA": [
    "WIN32_FIND_DATA"
  ], 
  "UINT": [
    "WinDef.UINT"
  ], 
  "LPBYTE": [
    "byte[]", 
    "Pointer", 
    "Buffer"
  ], 
  "ATOM": [
    "ATOM"
  ], 
  "HRSRC": [
    "HRSRC"
  ], 
  "IShellFolder*": [
    "PointerByReference"
  ], 
  "BLENDFUNCTION*": [
    "WinUser.BLENDFUNCTION"
  ], 
  "WPARAM": [
    "WinDef.WPARAM"
  ], 
  "PVOID": [
    "Pointer", 
    "byte[]", 
    "Structure", 
    "PVOID", 
    "Buffer"
  ], 
  "WELL_KNOWN_SID_TYPE": [
    "int"
  ], 
  "HCURSOR": [
    "HCURSOR"
  ], 
  "PBYTE": [
    "byte[]"
  ], 
  "TOKEN_TYPE": [
    "int"
  ], 
  "LPOFSTRUCT": [
    "OFSTRUCT"
  ], 
  "LPGUITHREADINFO": [
    "WinUser.GUITHREADINFO"
  ], 
  "LPRECT": [
    "WinDef.RECT"
  ], 
  "LONG": [
    "LONG"
  ]
}