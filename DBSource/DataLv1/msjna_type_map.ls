{
  "PMEMORY_BASIC_INFORMATION": [
    "WinNT.MEMORY_BASIC_INFORMATION"
  ], 
  "HBITMAP": [
    "WinDef.HBITMAP"
  ], 
  "PIXELFORMATDESCRIPTOR*": [
    "WinGDI.PIXELFORMATDESCRIPTOR.ByReference"
  ], 
  "PDWORD": [
    "IntByReference", 
    "WinDef.DWORDByReference"
  ], 
  "HMODULE*": [
    "HANDLEByReference"
  ], 
  "_EXCEPTION_POINTERS*": [
    "Pointer"
  ], 
  "LPCRITICAL_SECTION": [
    "PRTL_CRITICAL_SECTION"
  ], 
  "HCRYPTPROV*": [
    "ULONG_PTRByReference"
  ], 
  "LPOVERLAPPED*": [
    "PointerByReference"
  ], 
  "LPTOP_LEVEL_EXCEPTION_FILTER": [
    "Callback"
  ], 
  "HHOOK": [
    "WinUser.HHOOK"
  ], 
  "COLORREF*": [
    "IntByReference"
  ], 
  "ULONG_PTR": [
    "Pointer"
  ], 
  "PAPPBARDATA": [
    "ShellAPI.APPBARDATA"
  ], 
  "LPSYSTEMTIME": [
    "WinBase.SYSTEMTIME"
  ], 
  "HGLOBAL": [
    "Pointer", 
    "HANDLE"
  ], 
  "LPPROCESSENTRY32": [
    "Tlhelp32.PROCESSENTRY32"
  ], 
  "BITMAPINFO*": [
    "WinGDI.BITMAPINFO"
  ], 
  "REFKNOWNFOLDERID": [
    "Guid.GUID"
  ], 
  "LPTIME_ZONE_INFORMATION": [
    "WinBase.TIME_ZONE_INFORMATION"
  ], 
  "LPOSVERSIONINFO": [
    "WinNT.OSVERSIONINFOEX"
  ], 
  "WNDENUMPROC": [
    "WinUser.WNDENUMPROC"
  ], 
  "COMPUTER_NAME_FORMAT": [
    "int"
  ], 
  "LCTYPE": [
    "DWORD"
  ], 
  "PTITLEBARINFO": [
    "TITLEBARINFO"
  ], 
  "PFILETIME": [
    "WinBase.FILETIME"
  ], 
  "PICONINFO": [
    "WinGDI.ICONINFO"
  ], 
  "PSECURITY_DESCRIPTOR": [
    "Pointer"
  ], 
  "PSID_NAME_USE": [
    "PointerByReference"
  ], 
  "LPCWSTR": [
    "WString"
  ], 
  "LPPOINT": [
    "POINT"
  ], 
  "SIZE_T*": [
    "IntByReference", 
    "ULONG_PTRByReference"
  ], 
  "PHKEY": [
    "WinReg.HKEYByReference"
  ], 
  "PACL*": [
    "PointerByReference"
  ], 
  "LPMSG": [
    "WinUser.MSG"
  ], 
  "SC_STATUS_TYPE": [
    "int"
  ], 
  "HMODULE": [
    "HMODULE"
  ], 
  "HWND": [
    "WinDef.HWND", 
    "Pointer"
  ], 
  "HANDLE*": [
    "WinNT.HANDLE[]"
  ], 
  "LPVOID": [
    "String", 
    "Structure", 
    "Pointer", 
    "byte[]", 
    "WinNT.FILE_NOTIFY_INFORMATION", 
    "WinDef.LPVOID", 
    "ByteBuffer"
  ], 
  "SC_HANDLE": [
    "Winsvc.SC_HANDLE"
  ], 
  "HOOKPROC": [
    "WinUser.HOOKPROC"
  ], 
  "POINT*": [
    "WinDef.POINT[]", 
    "WinDef.POINT"
  ], 
  "LPCOMMTIMEOUTS": [
    "WinBase.COMMTIMEOUTS"
  ], 
  "LPCMENUITEMINFO": [
    "MENUITEMINFO"
  ], 
  "PACL": [
    "Pointer", 
    "ACL"
  ], 
  "HDEVNOTIFY": [
    "WinUser.HDEVNOTIFY"
  ], 
  "LONG_PTR": [
    "Pointer"
  ], 
  "LARGE_INTEGER*": [
    "LONGLONGByReference", 
    "LARGE_INTEGER"
  ], 
  "PLONG": [
    "LONGByReference"
  ], 
  "LPWNDCLASSEX": [
    "WinUser.WNDCLASSEX"
  ], 
  "va_list*": [
    "Pointer", 
    "String[]"
  ], 
  "LPMEMORYSTATUS": [
    "MEMORYSTATUS"
  ], 
  "LPHANDLE": [
    "WinNT.HANDLEByReference"
  ], 
  "HDC": [
    "WinDef.HDC"
  ], 
  "PRAWINPUTDEVICELIST": [
    "WinUser.RAWINPUTDEVICELIST[]"
  ], 
  "LPPROCESS_INFORMATION": [
    "WinBase.PROCESS_INFORMATION"
  ], 
  "LPWIN32_FIND_DATA": [
    "WIN32_FIND_DATA"
  ], 
  "PUINT": [
    "IntByReference"
  ], 
  "ULONG": [
    "WinDef.ULONG", 
    "int"
  ], 
  "LPSHFILEOPSTRUCT": [
    "ShellAPI.SHFILEOPSTRUCT"
  ], 
  "MONITORENUMPROC": [
    "WinUser.MONITORENUMPROC"
  ], 
  "LPSECURITY_ATTRIBUTES": [
    "WinBase.SECURITY_ATTRIBUTES"
  ], 
  "PTSTR": [
    "String"
  ], 
  "PULARGE_INTEGER": [
    "WinNT.LARGE_INTEGER"
  ], 
  "LPTSTR*": [
    "PointerByReference"
  ], 
  "HINTERNET": [
    "HANDLE"
  ], 
  "PLUID": [
    "WinNT.LUID"
  ], 
  "PBOOL": [
    "IntByReference"
  ], 
  "LPCRECT": [
    "WinDef.RECT"
  ], 
  "WNDCLASSEX*": [
    "WinUser.WNDCLASSEX"
  ], 
  "HCRYPTKEY": [
    "ULONG_PTR"
  ], 
  "LONG*": [
    "LONGByReference"
  ], 
  "LPSTARTUPINFO": [
    "WinBase.STARTUPINFO"
  ], 
  "LPSERVICE_STATUS": [
    "Winsvc.SERVICE_STATUS"
  ], 
  "LPCPINFO": [
    "CPINFO"
  ], 
  "ACL_INFORMATION_CLASS": [
    "int"
  ], 
  "LPTCH": [
    "Pointer"
  ], 
  "HPALETTE": [
    "HANDLE"
  ], 
  "PSID": [
    "WinNT.PSID", 
    "Pointer"
  ], 
  "LPARAM": [
    "WinDef.LPARAM", 
    "Pointer", 
    "long"
  ], 
  "SECURITY_IMPERSONATION_LEVEL": [
    "int"
  ], 
  "LPDCB": [
    "WinBase.DCB"
  ], 
  "HLOCAL": [
    "Pointer", 
    "HANDLE"
  ], 
  "INT": [
    "int"
  ], 
  "TIMERPROC": [
    "Callback"
  ], 
  "TOKEN_INFORMATION_CLASS": [
    "int"
  ], 
  "HCRYPTKEY*": [
    "ULONG_PTRByReference"
  ], 
  "FILETIME*": [
    "WinBase.FILETIME"
  ], 
  "LPWSTR": [
    "char[]"
  ], 
  "HMENU": [
    "WinDef.HMENU"
  ], 
  "KEY_INFORMATION_CLASS": [
    "int"
  ], 
  "PULONG": [
    "WinDef.ULONGByReference", 
    "IntByReference"
  ], 
  "LPCPINFOEX": [
    "CPINFOEX"
  ], 
  "SIZE_T": [
    "int", 
    "BaseTSD.SIZE_T"
  ], 
  "PHANDLER_ROUTINE": [
    "Callback"
  ], 
  "LPWSADATA": [
    "WSADATA"
  ], 
  "LPCTSTR*": [
    "String[]"
  ], 
  "HRGN": [
    "WinDef.HRGN"
  ], 
  "HMONITOR": [
    "WinUser.HMONITOR"
  ], 
  "REGSAM": [
    "int", 
    "WORD"
  ], 
  "HGDIOBJ": [
    "WinNT.HANDLE"
  ], 
  "PFE_IMPORT_FUNC": [
    "WinBase.FE_IMPORT_FUNC"
  ], 
  "LPWORD": [
    "short[]"
  ], 
  "PWINDOWINFO": [
    "WinUser.WINDOWINFO"
  ], 
  "LPBYTE": [
    "Winsvc.SERVICE_STATUS_PROCESS", 
    "byte[]", 
    "Pointer", 
    "Buffer"
  ], 
  "LONG": [
    "int", 
    "LONG"
  ], 
  "PFLS_CALLBACK_FUNCTION": [
    "Callback"
  ], 
  "SE_OBJECT_TYPE": [
    "int"
  ], 
  "HINSTANCE": [
    "WinDef.HINSTANCE"
  ], 
  "LPOVERLAPPED_COMPLETION_ROUTINE": [
    "WinNT.OVERLAPPED_COMPLETION_ROUTINE"
  ], 
  "LPBITMAPINFO": [
    "WinGDI.BITMAPINFO"
  ], 
  "SECURITY_INFORMATION": [
    "int"
  ], 
  "WORD": [
    "int"
  ], 
  "PSECURITY_DESCRIPTOR*": [
    "PointerByReference"
  ], 
  "PFE_EXPORT_FUNC": [
    "WinBase.FE_EXPORT_FUNC"
  ], 
  "POINT": [
    "WinDef.POINT"
  ], 
  "LPTHREAD_START_ROUTINE": [
    "WinBase.FOREIGN_THREAD_START_ROUTINE"
  ], 
  "PULONG_PTR": [
    "BaseTSD.ULONG_PTRByReference"
  ], 
  "LPCTSTR": [
    "String", 
    "WString", 
    "char[]"
  ], 
  "WNDCLASS*": [
    "WNDCLASS"
  ], 
  "PTOKEN_PRIVILEGES": [
    "WinNT.TOKEN_PRIVILEGES"
  ], 
  "LPSYSTEM_INFO": [
    "WinBase.SYSTEM_INFO"
  ], 
  "SYSTEMTIME*": [
    "WinBase.SYSTEMTIME"
  ], 
  "HCRYPTHASH": [
    "ULONG_PTR"
  ], 
  "PPRIVILEGE_SET": [
    "WinNT.PRIVILEGE_SET"
  ], 
  "HANDLE": [
    "WinNT.HANDLE", 
    "Pointer"
  ], 
  "LOGPALETTE*": [
    "LOGPALETTE"
  ], 
  "HCRYPTHASH*": [
    "ULONG_PTRByReference"
  ], 
  "PALETTEENTRY*": [
    "PALETTEENTRY"
  ], 
  "WINDOWPLACEMENT*": [
    "WinUser.WINDOWPLACEMENT"
  ], 
  "UINT_PTR": [
    "UINT_PTR"
  ], 
  "PVOID*": [
    "PointerByReference"
  ], 
  "BYTE": [
    "byte", 
    "BYTE"
  ], 
  "HCRYPTPROV": [
    "ULONG_PTR"
  ], 
  "PSYSTEM_LOGICAL_PROCESSOR_INFORMATION": [
    "Pointer"
  ], 
  "COLORREF": [
    "int", 
    "DWORD"
  ], 
  "RECT*": [
    "WinDef.RECT"
  ], 
  "ALG_ID": [
    "UINT"
  ], 
  "LPMEMORYSTATUSEX": [
    "WinBase.MEMORYSTATUSEX"
  ], 
  "LPDWORD": [
    "WinDef.DWORDByReference", 
    "IntByReference", 
    "int", 
    "Pointer"
  ], 
  "LPFILETIME": [
    "WinBase.FILETIME"
  ], 
  "BOOL": [
    "boolean", 
    "WinDef.BOOL"
  ], 
  "HCURSOR": [
    "HCURSOR"
  ], 
  "LPSTR": [
    "char[]"
  ], 
  "PDWORD_PTR": [
    "WinDef.DWORDByReference"
  ], 
  "SIZE*": [
    "WinUser.SIZE"
  ], 
  "PSID*": [
    "WinNT.PSIDByReference", 
    "PointerByReference"
  ], 
  "VOID*": [
    "Pointer", 
    "PointerByReference", 
    "LPVOID"
  ], 
  "int": [
    "int"
  ], 
  "LPTSTR": [
    "String", 
    "char[]", 
    "PointerByReference", 
    "WString"
  ], 
  "PWSTR*": [
    "PointerByReference"
  ], 
  "BITMAPINFOHEADER*": [
    "WinGDI.BITMAPINFOHEADER"
  ], 
  "char*": [
    "byte[]"
  ], 
  "PHANDLE": [
    "WinNT.HANDLEByReference"
  ], 
  "HICON": [
    "WinDef.HICON"
  ], 
  "DWORD": [
    "WinDef.DWORD", 
    "int"
  ], 
  "LPBOOL": [
    "WinDef.BOOLByReference"
  ], 
  "PFLASHWINFO": [
    "WinUser.FLASHWINFO"
  ], 
  "LPMONITORINFO": [
    "WinUser.MONITORINFOEX"
  ], 
  "PGENERIC_MAPPING": [
    "WinNT.GENERIC_MAPPING"
  ], 
  "LPINPUT": [
    "WinUser.INPUT[]"
  ], 
  "PLASTINPUTINFO": [
    "WinUser.LASTINPUTINFO"
  ], 
  "LPCSTR": [
    "String"
  ], 
  "INT*": [
    "int[]"
  ], 
  "LPOVERLAPPED": [
    "WinBase.OVERLAPPED", 
    "Pointer"
  ], 
  "LCID": [
    "LCID"
  ], 
  "BYTE*": [
    "char[]", 
    "ByteByReference"
  ], 
  "LPTHREADENTRY32": [
    "THREADENTRY32"
  ], 
  "RGNDATA*": [
    "WinGDI.RGNDATA"
  ], 
  "MSG*": [
    "WinUser.MSG"
  ], 
  "XFORM*": [
    "Pointer"
  ], 
  "LPCVOID": [
    "Pointer", 
    "byte[]", 
    "LPVOID"
  ], 
  "LPRECT": [
    "WinDef.RECT"
  ], 
  "UINT": [
    "int", 
    "WinDef.UINT", 
    "WinDef.DWORD"
  ], 
  "ATOM": [
    "ATOM"
  ], 
  "HRSRC": [
    "HRSRC"
  ], 
  "IShellFolder*": [
    "PointerByReference"
  ], 
  "BLENDFUNCTION*": [
    "WinUser.BLENDFUNCTION"
  ], 
  "WPARAM": [
    "WinDef.WPARAM", 
    "long"
  ], 
  "WELL_KNOWN_SID_TYPE": [
    "int"
  ], 
  "HKEY": [
    "WinReg.HKEY"
  ], 
  "PBYTE": [
    "byte[]"
  ], 
  "TOKEN_TYPE": [
    "int"
  ], 
  "LPOFSTRUCT": [
    "OFSTRUCT"
  ], 
  "LPGUITHREADINFO": [
    "WinUser.GUITHREADINFO"
  ], 
  "DWORD*": [
    "IntByReference", 
    "DWORDByReference"
  ], 
  "PVOID": [
    "Pointer", 
    "byte[]", 
    "Structure", 
    "PVOID", 
    "Buffer"
  ]
}