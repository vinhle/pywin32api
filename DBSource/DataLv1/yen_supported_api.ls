BOOL CryptAcquireContext(ULONG_PTRByReference phProv, WString pszContainer, WString pszProvider, DWORD dwProvType, DWORD dwFlags); Advapi32DLL.java
BOOL CryptHashData(ULONG_PTR hHash, ByteByReference pbData, DWORD dwDataLen, DWORD dwFlags); Advapi32DLL.java
BOOL CryptDeriveKey(ULONG_PTR hProv, UINT Algid, ULONG_PTR hBaseData, DWORD dwFlags, ULONG_PTRByReference phKey); Advapi32DLL.java
BOOL CryptDestroyHash(ULONG_PTR hHash); Advapi32DLL.java
BOOL CryptCreateHash(ULONG_PTR hProv, UINT Algid, ULONG_PTR hKey, DWORD dwFlags, ULONG_PTRByReference phHash); Advapi32DLL.java
BOOL CryptDecrypt(ULONG_PTR hKey, ULONG_PTR hHash, BOOL Final, DWORD dwFlags, ByteByReference pbData, DWORDByReference pdwDataLen); Advapi32DLL.java
BOOL CryptDestroyKey(ULONG_PTR hKey); Advapi32DLL.java
BOOL CryptReleaseContext(ULONG_PTR hProv, DWORD dwFlags); Advapi32DLL.java
LONG RegOpenKey(HKEY hKey, String lpSubKey, HKEYByReference phkResult); Advapi32DLL.java
LONG RegQueryValueEx(HKEY hKey, String lpValueName, DWORDByReference lpReserved, DWORDByReference lpType, Buffer lpData, DWORDByReference lpcbData); Advapi32DLL.java
LONG RegCreateKeyEx(HKEY hKey, String lpSubKey, DWORD Reserved, String lpClass, DWORD dwOptions, WORD samDesired, SECURITY_ATTRIBUTES lpSecurityAttributes, HKEY phkResult, DWORD lpdwDisposition); Advapi32DLL.java
BOOL IsValidAcl(ACL pAcl); Advapi32DLL.java
BOOL OpenProcessToken(HANDLE ProcessHandle, DWORD DesiredAccess, HANDLEByReference TokenHandle); Advapi32DLL.java
void InitCommonControls(); Comctl32DLL.java
int AnimatePalette(HANDLE hpal, UINT iStartIndex, UINT cEntries, PALETTEENTRY pe); Gdi32DLL.java
int AddFontResource(WString lpszFilename); Gdi32DLL.java
HANDLE GetStockObject(int fnObject); Gdi32DLL.java
DWORD GdiGetBatchLimit(); Gdi32DLL.java
DWORD GetBkColor(HDC hdc); Gdi32DLL.java
DWORD SetBkColor(HDC hdc, DWORD crColor); Gdi32DLL.java
HDC CreateCompatibleDC(HDC hdc); Gdi32DLL.java
HANDLE CreatePalette(LOGPALETTE lplgpl); Gdi32DLL.java
BOOL DeleteObject(HANDLE hObject); Gdi32DLL.java
UINT SetTextAlign(HDC hdc, UINT fMode); Gdi32DLL.java
BOOL StrokePath( HDC hdc); Gdi32DLL.java
BOOL StrokeAndFillPath(HDC hdc); Gdi32DLL.java
BOOL DPtoLP(HDC hdc, POINT lpPoints, int nCount); Gdi32DLL.java
int WinExec(String lpCmdLine, int uCmdShow); Kernel32DLL.java
long GetProcAddress(HMODULE hModule, String lpProcName); Kernel32DLL.java
HMODULE LoadLibrary(String lpFileName); Kernel32DLL.java
HMODULE LoadLibraryEx(String lpFileName, HANDLE hFile, DWORD dwFlags); Kernel32DLL.java
DWORD SetFilePointer(HANDLE hFile, LONG lDistanceToMove, LONGByReference lpDistanceToMoveHigh, DWORD dwMoveMethod); Kernel32DLL.java
BOOL FindClose(HANDLE hFindFile); Kernel32DLL.java
BOOL SetEndOfFile(HANDLE hFile); Kernel32DLL.java
String lstrcat(String lpString1, String lpString2); Kernel32DLL.java
int lstrcmp(String lpString1, String lpString2); Kernel32DLL.java
int lstrlen(String lpString); Kernel32DLL.java
WString lstrcpy(WString lpString1, WString lpString2); Kernel32DLL.java
WString lstrcpy(char[] lpString1, WString lpString2); Kernel32DLL.java
HANDLE CreateThread(SECURITY_ATTRIBUTES lpThreadAttributes, SIZE_T dwStackSize, THREAD_START_ROUTINE lpStartAddress, LPVOID lpParameter, DWORD dwCreationFlags, DWORDByReference lpThreadId); Kernel32DLL.java
void ExitProcess(int uExitCode); Kernel32DLL.java
Pointer GetEnvironmentStrings(); Kernel32DLL.java
BOOL FreeEnvironmentStrings(Pointer lpszEnvironmentBlock); Kernel32DLL.java
Pointer GetCommandLine(); Kernel32DLL.java
boolean IsDebuggerPresent(); Kernel32DLL.java
void GetStartupInfo(STARTUPINFO lpStartupInfo); Kernel32DLL.java
HANDLE GetStdHandle(DWORD nStdHandle); Kernel32DLL.java
LPVOID HeapAlloc(HANDLE hHeap, DWORD dwFlags, SIZE_T dwBytes); Kernel32DLL.java
HANDLE HeapCreate(DWORD flOptions, SIZE_T dwInitialSize, SIZE_T dwMaximumSize); Kernel32DLL.java
boolean HeapDestroy(HANDLE hHeap); Kernel32DLL.java
boolean HeapFree(HANDLE hHeap, DWORD dwFlags, LPVOID lpMem); Kernel32DLL.java
LPVOID HeapReAlloc(HANDLE hHeap, DWORD dwFlags, LPVOID lpMem, SIZE_T dwBytes); Kernel32DLL.java
UINT SetHandleCount(UINT uNumber); Kernel32DLL.java
BOOL VirtualFree(LPVOID lpAddress, SIZE_T dwSize, DWORD dwFreeType); Kernel32DLL.java
HANDLE FindFirstFile(WString lpFileName, WIN32_FIND_DATA lpFindFileData); Kernel32DLL.java
BOOL FindNextFile(HANDLE hFindFile, WIN32_FIND_DATA lpFindFileData); Kernel32DLL.java
BOOL SetCurrentDirectory(WString lpPathName); Kernel32DLL.java
DWORD GetCurrentDirectory(DWORD nBufferLength, char[] lpBuffer); Kernel32DLL.java
UINT GetWindowsDirectory(char[] lpBuffer, UINT uSize); Kernel32DLL.java
DWORD GetModuleFileName(HMODULE hModule, char[] lpFilename, DWORD nSize); Kernel32DLL.java
int _lclose(int hFile); Kernel32DLL.java
int _lcreat(String lpPathName, int iAttribute); Kernel32DLL.java
int _lopen(String lpPathName, int iReadWrite); Kernel32DLL.java
LONG _llseek(int hFile, LONG lOffset, int iOrigin); Kernel32DLL.java
LONG CompareFileTime(FILETIME lpFileTime1, FILETIME lpFileTime2); Kernel32DLL.java
int CompareString(LCID Locale, DWORD dwCmpFlags, WString lpString1, int cchCount1, WString lpString2, int cchCount2); Kernel32DLL.java
BOOL CreateDirectory(String lpPathName, SECURITY_ATTRIBUTES lpSecurityAttributes); Kernel32DLL.java
HANDLE CreateEvent(SECURITY_ATTRIBUTES lpEventAttributes, BOOL bManualReset, BOOL bInitialState, String lpName); Kernel32DLL.java
HANDLE CreateMutex(SECURITY_ATTRIBUTES lpMutexAttributes, BOOL bInitialOwner, String lpName); Kernel32DLL.java
HANDLE CreateToolhelp32Snapshot(DWORD dwFlags, DWORD th32ProcessID); Kernel32DLL.java
void DeleteCriticalSection(PRTL_CRITICAL_SECTION lpCriticalSection); Kernel32DLL.java
BOOL DeviceIoControl(HANDLE hDevice, DWORD dwIoControlCode, ByteBuffer lpInBuffer, int nInBufferSize, ByteBuffer lpOutBuffer, int nOutBufferSize, DWORDByReference lpBytesReturned, OVERLAPPED lpOverlapped); Kernel32DLL.java
BOOL DisableThreadLibraryCalls(HMODULE hModule); Kernel32DLL.java
void EnterCriticalSection(RTL_CRITICAL_SECTION lpCriticalSection); Kernel32DLL.java
BOOL FileTimeToSystemTime(FILETIME lpFileTime, SYSTEMTIME lpSystemTime); Kernel32DLL.java
HRSRC FindResource(HMODULE hModule, String lpName, String lpType); Kernel32DLL.java
BOOL FlushInstructionCache(HANDLE hProcess, LPVOID lpBaseAddress, SIZE_T dwSize); Kernel32DLL.java
BOOL FreeLibrary(HMODULE hModule); Kernel32DLL.java
UINT GetACP(); Kernel32DLL.java
BOOL GetCPInfo(UINT CodePage, CPINFO lpCPInfo); Kernel32DLL.java
int GetDateFormatW(LCID Locale, DWORD dwFlags, SYSTEMTIME lpDate, WString lpFormat, char[] lpDateStr, int cchDate); Kernel32DLL.java
BOOL GetDiskFreeSpace(WString lpRootPathName, DWORDByReference lpSectorsPerCluster, DWORDByReference lpBytesPerSector, DWORDByReference lpNumberOfFreeClusters, DWORDByReference lpTotalNumberOfClusters); Kernel32DLL.java
DWORD GetFileSize(HANDLE hFile, DWORDByReference lpFileSizeHigh); Kernel32DLL.java
void GetSystemTimeAsFileTime(FILETIME lpSystemTimeAsFileTime); Kernel32DLL.java
int GetLocaleInfo(LCID Locale, DWORD LCType, char[] lpLCData, int cchData); Kernel32DLL.java
DWORD GetLogicalDrives(); Kernel32DLL.java
HANDLE GetProcessHeap(); Kernel32DLL.java
BOOL GetStringTypeW(DWORD dwInfoType, WString lpSrcStr, int cchSrc, short[] lpCharType); Kernel32DLL.java
BOOL GetStringTypeA(LCID Locale, DWORD dwInfoType, String lpSrcStr, int cchSrc, short[] lpCharType); Kernel32DLL.java
UINT GetTempFileName(String lpPathName, String lpPrefixString, UINT uUnique, char[] lpTempFileName); Kernel32DLL.java
HANDLE GlobalAlloc(UINT uFlags, SIZE_T dwBytes); Kernel32DLL.java
HANDLE GlobalFree(HANDLE hMem); Kernel32DLL.java
LPVOID GlobalLock(HANDLE hMem); Kernel32DLL.java
void GlobalMemoryStatus(MEMORYSTATUS lpBuffer); Kernel32DLL.java
void InitializeCriticalSection(RTL_CRITICAL_SECTION lpCriticalSection); Kernel32DLL.java
LONG InterlockedDecrement(LONGByReference Addend); Kernel32DLL.java
void RegisterServiceProcess(long dwProcessId, long dwType); Kernel32DLL.java
BOOL IsBadWritePtr(LPVOID lp, UINT_PTR ucb); Kernel32DLL.java
int LCMapString(LCID Locale, DWORD dwMapFlags, WString lpSrcStr, int cchSrc, char[] lpDestStr, int cchDest); Kernel32DLL.java
HANDLE LoadResource(HMODULE hModule, HRSRC hResInfo); Kernel32DLL.java
HANDLE LocalAlloc(UINT uFlags, SIZE_T uBytes); Kernel32DLL.java
HANDLE LocalFree(HANDLE hMem); Kernel32DLL.java
LPVOID LocalLock(HANDLE hMem); Kernel32DLL.java
HANDLE LocalReAlloc(HANDLE hMem, SIZE_T uBytes, UINT uFlags); Kernel32DLL.java
LPVOID LockResource(HANDLE hResData); Kernel32DLL.java
int lstrcmpi(WString lpString1, WString lpString2); Kernel32DLL.java
WString lstrcpyn(WString lpString1, WString lpString2, int iMaxLength); Kernel32DLL.java
int MultiByteToWideChar(UINT CodePage, DWORD dwFlags, String lpMultiByteStr, int cbMultiByte, char[] lpWideCharStr, int cchWideChar); Kernel32DLL.java
HANDLE OpenEvent(DWORD dwDesiredAccess, BOOL bInheritHandle, WString lpName); Kernel32DLL.java
int OpenFile(String lpFileName, OFSTRUCT lpReOpenBuff, UINT uStyle); Kernel32DLL.java
HANDLE OpenMutex(DWORD dwDesiredAccess, BOOL bInheritHandle, WString lpName); Kernel32DLL.java
HANDLE OpenProcess(DWORD dwDesiredAccess, BOOL bInheritHandle, DWORD dwProcessId); Kernel32DLL.java
void OutputDebugString(WString lpOutputString); Kernel32DLL.java
BOOL Process32First(HANDLE hSnapshot, PROCESSENTRY32 lppe); Kernel32DLL.java
BOOL Process32Next(HANDLE hSnapshot, PROCESSENTRY32 lppe); Kernel32DLL.java
BOOL ReadProcessMemory(HANDLE hProcess, LPVOID lpBaseAddress, LPVOID lpBuffer, SIZE_T nSize, ULONG_PTRByReference lpNumberOfBytesRead); Kernel32DLL.java
BOOL ReleaseMutex(HANDLE hMutex); Kernel32DLL.java
void RtlZeroMemory(Pointer Destination, SIZE_T Length); Kernel32DLL.java
DWORD SearchPath(String lpPath, String lpFileName, String lpExtension, DWORD nBufferLength, char[] lpBuffer, Pointer lpFilePart); Kernel32DLL.java
BOOL SetConsoleCtrlHandler(Callback HandlerRoutine, BOOL Add); Kernel32DLL.java
UINT SetErrorMode(UINT uMode); Kernel32DLL.java
BOOL SetEvent(HANDLE hEvent); Kernel32DLL.java
BOOL SetFileTime(HANDLE hFile, FILETIME lpCreationTime, FILETIME lpLastAccessTime, FILETIME lpLastWriteTime); Kernel32DLL.java
BOOL SetThreadPriority(HANDLE hThread, int nPriority); Kernel32DLL.java
UINT_PTR SetTimer(HWND hWnd, UINT_PTR nIDEvent, UINT uElapse, Callback lpTimerFunc); Kernel32DLL.java
Callback SetUnhandledExceptionFilter(Callback lpTopLevelExceptionFilter); Kernel32DLL.java
DWORD SizeofResource(HMODULE hModule, HRSRC hResInfo); Kernel32DLL.java
void Sleep(DWORD dwMilliseconds); Kernel32DLL.java
BOOL SystemTimeToFileTime(SYSTEMTIME lpSystemTime, FILETIME lpFileTime); Kernel32DLL.java
BOOL TerminateProcess(HANDLE hProcess, UINT uExitCode); Kernel32DLL.java
BOOL TerminateThread(HANDLE hThread, DWORD dwExitCode); Kernel32DLL.java
DWORD TlsAlloc(); Kernel32DLL.java
BOOL TlsFree(DWORD dwTlsIndex); Kernel32DLL.java
LPVOID TlsGetValue(DWORD dwTlsIndex); Kernel32DLL.java
BOOL TlsSetValue(DWORD dwTlsIndex, LPVOID lpTlsValue); Kernel32DLL.java
LPVOID VirtualAlloc(LPVOID lpAddress, SIZE_T dwSize, DWORD flAllocationType, DWORD flProtect); Kernel32DLL.java
LPVOID VirtualAllocEx(HANDLE hProcess, LPVOID lpAddress, SIZE_T dwSize, DWORD flAllocationType, DWORD flProtect); Kernel32DLL.java
BOOL VirtualProtect(LPVOID lpAddress, SIZE_T dwSize, DWORD flNewProtect, DWORDByReference lpflOldProtect); Kernel32DLL.java
SIZE_T VirtualQuery(LPVOID lpAddress, MEMORY_BASIC_INFORMATION lpBuffer, SIZE_T dwLength); Kernel32DLL.java
int WideCharToMultiByte(UINT CodePage, DWORD dwFlags, WString lpWideCharStr, int cchWideChar, char[] lpMultiByteStr, int cbMultiByte, String lpDefaultChar, BOOLByReference lpUsedDefaultChar); Kernel32DLL.java
BOOL WritePrivateProfileString(WString lpAppName, WString lpKeyName, WString lpString, WString lpFileName); Kernel32DLL.java
BOOL WriteProcessMemory(HANDLE hProcess, LPVOID lpBaseAddress, LPVOID lpBuffer, SIZE_T nSize, ULONG_PTRByReference lpNumberOfBytesWritten); Kernel32DLL.java
int wsprintf(char[] lpOut, String lpFmt, long[] args); Kernel32DLL.java
DWORD FormatMessageW(DWORD dwFlags, String pMessage, DWORD dwMessageId, DWORD dwLanguageId, char[] lpBuffer, DWORD nSize, String[] pArgs); Kernel32DLL.java
DWORD FormatMessage(DWORD dwFlags, LPVOID lpSource, DWORD dwMessageId, DWORD dwLanguageId, char[] lpBuffer, DWORD nSize, String[] Arguments); Kernel32DLL.java
DWORD GetPriorityClass(HANDLE hProcess); Kernel32DLL.java
BOOL SetPriorityClass(HANDLE hProcess, DWORD dwPriorityClass); Kernel32DLL.java
BOOL Thread32First(HANDLE hSnapshot, THREADENTRY32 lpte); Kernel32DLL.java
BOOL Thread32Next(HANDLE hSnapshot, THREADENTRY32 lpte); Kernel32DLL.java
HANDLE OpenThread(DWORD dwDesiredAccess, BOOL bInheritHandle, DWORD dwThreadId); Kernel32DLL.java
DWORD SuspendThread(HANDLE hThread); Kernel32DLL.java
DWORD ResumeThread(HANDLE hThread); Kernel32DLL.java
void ExitThread(DWORD dwExitCode); Kernel32DLL.java
BOOL Beep(DWORD dwFreq, DWORD dwDuration); Kernel32DLL.java
BOOL VirtualProtectEx(HANDLE hProcess, LPVOID lpAddress, SIZE_T dwSize, DWORD flNewProtect, DWORDByReference lpflOldProtect); Kernel32DLL.java
LONG UnhandledExceptionFilter(Pointer ExceptionInfo); Kernel32DLL.java
BOOL GetVolumeInformation(WString lpRootPathName, char[] lpVolumeNameBuffer, DWORD nVolumeNameSize, DWORDByReference lpVolumeSerialNumber, DWORDByReference lpMaximumComponentLength, DWORDByReference lpFileSystemFlags, char[] lpFileSystemNameBuffer, DWORD nFileSystemNameSize); Kernel32DLL.java
BOOL AreFileApisANSI(); Kernel32DLL.java
int MulDiv(int nNumber, int nNumerator, int nDenominator); Kernel32DLL.java
LCID GetThreadLocale(); Kernel32DLL.java
void LeaveCriticalSection(RTL_CRITICAL_SECTION lpCriticalSection); Kernel32DLL.java
UINT GlobalGetAtomName(ATOM nAtom, char[] lpBuffer, int nSize); Kernel32DLL.java
BOOL WaitNamedPipe(String lpNamedPipeName, DWORD nTimeOut); Kernel32DLL.java
void RtlUnwind(PVOID TargetFrame, PVOID TargetIp, EXCEPTION_RECORD ExceptionRecord, PVOID ReturnValue); Kernel32DLL.java
BOOL InitializeCriticalSectionAndSpinCount(RTL_CRITICAL_SECTION lpCriticalSection, DWORD dwSpinCount); Kernel32DLL.java
BOOL FlsSetValue(DWORD dwFlsIndex, PVOID lpFlsData); Kernel32DLL.java
PVOID FlsGetValue(DWORD dwFlsIndex); Kernel32DLL.java
DWORD FlsAlloc(Callback lpCallback); Kernel32DLL.java
SIZE_T HeapSize(HANDLE hHeap, DWORD dwFlags, Pointer lpMem); Kernel32DLL.java
BOOL QueryPerformanceCounter(LONGLONGByReference lpPerformanceCount); Kernel32DLL.java
BOOL IsDBCSLeadByte(BYTE TestChar); Kernel32DLL.java
PVOID EncodePointer(PVOID Ptr); Kernel32DLL.java
PVOID DecodePointer(PVOID Ptr); Kernel32DLL.java
ATOM FindAtom(String lpString); Kernel32DLL.java
LONG InterlockedExchange(LONGByReference Target, LONG Value); Kernel32DLL.java
UINT GetConsoleCP(); Kernel32DLL.java
WORD GetUserDefaultLangID(); Kernel32DLL.java
int GetThreadPriority(HANDLE hThread); Kernel32DLL.java
HANDLE OpenFileMapping(DWORD dwDesiredAccess, BOOL bInheritHandle, String lpName); Kernel32DLL.java
UINT GetOEMCP(); Kernel32DLL.java
BOOL GetAclInformation(ACL pAcl, Structure pAclInformation, DWORD nAclInformationLength, int dwAclInformationClass); Kernel32DLL.java
int GetLargestConsoleWindowSize(HANDLE hConsoleOutput); Kernel32DLL.java
ATOM GlobalFindAtom(String lpString); Kernel32DLL.java
BOOL RemoveDirectory(String lpPathName); Kernel32DLL.java
LONG InterlockedIncrement(LONG Addend); Kernel32DLL.java
BOOL MoveFileEx(String lpExistingFileName, String lpNewFileName, DWORD dwFlags); Kernel32DLL.java
UINT GetAtomName(ATOM nAtom, char[] lpBuffer, int nSize); Kernel32DLL.java
BOOL GetCPInfoEx(UINT CodePage, DWORD dwFlags, CPINFOEX lpCPInfoEx); Kernel32DLL.java
BOOL RegisterWowExec(DWORD Unknown); Kernel32DLL.java
WORD GetSystemDefaultUILanguage(); Kernel32DLL.java
BOOL GetConsoleMode(HANDLE hConsoleHandle, DWORDByReference lpMode); Kernel32DLL.java
BOOL IsBadReadPtr(LPVOID lp, UINT_PTR ucb); Kernel32DLL.java
LCID GetUserDefaultLCID(); Kernel32DLL.java
BOOL QueryPerformanceFrequency(LARGE_INTEGER lpFrequency); Kernel32DLL.java
BOOL SetThreadLocale(LCID Locale); Kernel32DLL.java
int GetFullPathName(String lpFileName, int nBufferLength, char[] lpBuffer, Pointer lpFilePart); Kernel32DLL.java
BOOL IsProcessorFeaturePresent(int ProcessorFeature); Kernel32DLL.java
BOOL IsValidCodePage(UINT CodePage); Kernel32DLL.java
BOOL GetModuleHandleEx(int dwFlags, String lpModuleName, HANDLEByReference phModule); Kernel32DLL.java
int _CorExeMain(); MsCorEEDLL.java
void _fpreset(); MSVCRTDLL.java
int __getmainargs(IntByReference _Argc, Pointer _Argv, Memory _Env, int _DoWildCard, _startupinfo _StartInfo); MSVCRTDLL.java
int __p__environ(); MSVCRTDLL.java
Pointer __p___initenv(); MSVCRTDLL.java
int atexit(Pointer func); MSVCRTDLL.java
Pointer memset(byte[] ptr, byte value, SIZE_T num); MSVCRTDLL.java
Pointer strstr(char[] str1, char[] str2); MSVCRTDLL.java
long time(LongByReference timer); MSVCRTDLL.java
void srand(int seed); MSVCRTDLL.java
void __set_app_type(int at); MSVCRTDLL.java
IntByReference __p__fmode(); MSVCRTDLL.java
int _setmode(int fd, int mode); MSVCRTDLL.java
Pointer malloc(SIZE_T size); MSVCRTDLL.java
void _cexit(); MSVCRTDLL.java
IntByReference __p__commode(); MSVCRTDLL.java
UINT _controlfp(UINT nnew, UINT mask); MSVCRTDLL.java
WString _itow(int value, WString str, int radix); MSVCRTDLL.java
String _itoa(int value, String str, int radix); MSVCRTDLL.java
int _except_handler3(EXCEPTION_RECORD exception_record, Pointer registration, CONTEXT context, Pointer dispatcher); MSVCRTDLL.java
Pointer memcpy(Pointer destination, Pointer source, SIZE_T num); MSVCRTDLL.java
Pointer _ltoa(long value, char[] str, int radix); MSVCRTDLL.java
int _strcmpi(String _Str1, String _Str2); MSVCRTDLL.java
int isalpha(int c); MSVCRTDLL.java
int is_wctype(USHORT _C, USHORT _Type); MSVCRTDLL.java
FILE fopen(String filename, String mode); MSVCRTDLL.java
int fopen_s(Pointer pFile, String filename, String mode); MSVCRTDLL.java
UINT fread(Buffer ptr, UINT size, UINT count, FILE2 stream); MSVCRTDLL.java
SIZE_T fwrite(byte[] ptr, SIZE_T size, SIZE_T count, FILE2 stream); MSVCRTDLL.java
void system(String command); MSVCRTDLL.java
int fputs(String content, FILE fp); MSVCRTDLL.java
String fgets(Memory memory, int size, FILE fp); MSVCRTDLL.java
void free(Pointer ptr); MSVCRTDLL.java
int remove(String filename); MSVCRTDLL.java
int strncmp(String str1, String str2, int num); MSVCRTDLL.java
int fclose(FILE stream); MSVCRTDLL.java
int fprintf(FILE stream, String... format); MSVCRTDLL.java
Pointer calloc(int num, int size); MSVCRTDLL.java
int sprintf(char[] str, String... format); MSVCRTDLL.java
int NtQueryInformationProcess(HANDLE ProcessHandle, int ProcessInformationClass, Structure.ByReference ProcessInformation, ULONG ProcessInformationLength, ULONGByReference ReturnLength); NtdllDLL.java
int NtQueryInformationProcess(HANDLE ProcessHandle, int ProcessInformationClass, com.sun.jna.ptr.ByReference ProcessInformation, ULONG ProcessInformationLength, ULONGByReference ReturnLength); NtdllDLL.java
int NtQueryInformation(); NtdllDLL.java
LONG NtProtectVirtualMemory(HANDLE ProcessHandle, PointerByReference BaseAddress, ULONGByReference NumberOfBytesToProtect, ULONG NewAccessProtection, ULONGByReference OldAccessProtection); NtdllDLL.java
LONG NtWriteVirtualMemory(HANDLE ProcessHandle, PVOID BaseAddress, byte[] Buffer, ULONG NumberOfBytesToWrite, ULONGByReference NumberOfBytesWritten ); NtdllDLL.java
LONG NtCreateSection(HANDLEByReference SectionHandle, DWORD DesiredAccess, OBJECT_ATTRIBUTES ObjectAttributes, LARGE_INTEGER MaximumSize, ULONG SectionPageProtection, ULONG AllocationAttributes, HANDLE FileHandle); NtdllDLL.java
HRESULT OleInitialize(LPVOID pvReserved); Ole32DLL.java
void OleUninitialize(); Ole32DLL.java
HRESULT CoCreateGuid(GUID pguid); Ole32DLL.java
HRESULT CoFileTimeNow(FILETIME lpFileTime); Ole32DLL.java
int ShellAbout(HWND hWnd, String szApp, String szOtherStuff, HICON hIcon); Shell32DLL.java
String PathFindFileName(String pPath); ShlwapiDLL.java
void PathUndecorate(char[] pszPath); ShlwapiDLL.java
BOOL PostMessage(HWND hWnd, int Msg, WPARAM wParam, LPARAM lParam); User32DLL.java
LRESULT SendMessage(HWND hWnd, int Msg, WPARAM wParam, LPARAM lParam); User32DLL.java
int MessageBox(HWND hWnd, String lpText, String lpCaption, UINT uType); User32DLL.java
Pointer CharLower(char[] lpsz); User32DLL.java
long CharNext(char[] lpsz); User32DLL.java
WString CharPrev(WString lpszStart, WString lpszCurrent); User32DLL.java
DWORD CharUpperBuff(char[] lpsz, DWORD cchLength); User32DLL.java
HWND CreateDialogParamW(HINSTANCE hInstance, String lpTemplateName, HWND hWndParent, int lpDialogFunc, LPARAM dwInitParam); User32DLL.java
BOOL DestroyMenu(HMENU hMenu); User32DLL.java
BOOL GetClassInfoEx(HINSTANCE hinst, WString lpszClass, WNDCLASSEX lpwcx); User32DLL.java
HWND GetFocus(); User32DLL.java
int GetKeyboardType(int nTypeFlag); User32DLL.java
BOOL GetMessage(MSG lpMsg, HWND hWnd, UINT wMsgFilterMin, UINT wMsgFilterMax); User32DLL.java
DWORD GetSysColor(int nIndex); User32DLL.java
HBRUSH GetSysColorBrush(int nIndex); User32DLL.java
HMENU GetSystemMenu(HWND hWnd, BOOL bRevert); User32DLL.java
DWORD GetWindowThreadProcessId(HWND hWnd, DWORDByReference lpdwProcessId); User32DLL.java
BOOL InsertMenuItem(HMENU hMenu, UINT uItem, BOOL fByPosition, MENUITEMINFO lpmii); User32DLL.java
BOOL IsDialogMessage(HWND hDlg, MSG lpMsg); User32DLL.java
HBITMAP LoadBitmap(HINSTANCE hInstance, WString lpBitmapName); User32DLL.java
HCURSOR LoadCursor(HINSTANCE hInstance, WString lpCursorName); User32DLL.java
HICON LoadIcon(HINSTANCE hInstance, WString lpIconName); User32DLL.java
HMENU LoadMenu(HINSTANCE hInstance, WString lpMenuName); User32DLL.java
int LoadString(HINSTANCE hInstance, UINT uID, char[] lpBuffer, int nBufferMax); User32DLL.java
BOOL OpenClipboard(HWND hWndNewOwner); User32DLL.java
ATOM RegisterClass(WNDCLASS lpWndClass); User32DLL.java
HCURSOR SetCursor(HCURSOR hCursor); User32DLL.java
BOOL ShowWindow(HWND hWnd, int nCmdShow); User32DLL.java
BOOL TranslateMessage(MSG lpMsg); User32DLL.java
BOOL UnregisterClass(String lpClassName, HINSTANCE hInstance); User32DLL.java
HWND GetTopWindow(HWND hWnd); User32DLL.java
BOOL BlockInput(BOOL fBlockIt); User32DLL.java
LONG GetWindowLong(HWND hWnd, int nIndex); User32DLL.java
LONG SetWindowLong(HWND hWnd, int nIndex, LONG dwNewLong); User32DLL.java
HWND GetShellWindow(); User32DLL.java
INT_PTR DialogBoxParam(HINSTANCE hInstance, String lpTemplateName, HWND hWndParent, DLGPROC lpDialogFunc, LPARAM dwInitParam); User32DLL.java
BOOL OffsetRect(RECT lprc, int dx, int dy); User32DLL.java
LRESULT DefFrameProc(HWND hWnd, HWND hWndMDIClient, UINT uMsg, WPARAM wParam, LPARAM lParam); User32DLL.java
int GetWindowText(HWND hWnd, char[] lpString, int nMaxCount); User32DLL.java
BOOL SetCaretBlinkTime(UINT uMSeconds); User32DLL.java
INT_PTR DialogBoxIndirectParam(HINSTANCE hInstance, DLGTEMPLATE hDialogTemplate, HWND hWndParent, DLGPROC lpDialogFunc, LPARAM dwInitParam); User32DLL.java
BOOL InflateRect(RECT lprc, int dx, int dy); User32DLL.java
BOOL IsWindowVisible(HWND hWnd); User32DLL.java
BOOL GetWindowRect(HWND hWnd, RECT lpRect); User32DLL.java
BOOL EqualRect(RECT lprc1, RECT lprc2); User32DLL.java
BOOL GetCursorPos(POINT lpPoint); User32DLL.java
BOOL ClientToScreen(HWND hWnd, POINT lpPoint); User32DLL.java
HWND GetActiveWindow(); User32DLL.java
HANDLE GetProcessWindowStation(); User32DLL.java
BOOL GetUserObjectInformation(HANDLE hObj, int nIndex, Buffer pvInfo, DWORD nLength, DWORDByReference lpnLengthNeeded); User32DLL.java
BOOL GetCaretPos(POINT lpPoint); User32DLL.java
BOOL CreateCaret(HWND hWnd, HBITMAP hBitmap, int nWidth, int nHeight); User32DLL.java
BOOL IsZoomed(HWND hWnd); User32DLL.java
BOOL GetTitleBarInfo(HWND hwnd, TITLEBARINFO pti); User32DLL.java
BOOL UpdateLayeredWindow(HWND hwnd, HDC hdcDst, POINT pptDst, SIZE psize, HDC hdcSrc, POINT pptSrc, DWORD crKey, BLENDFUNCTION pblend, DWORD dwFlags); User32DLL.java
BOOL SendNotifyMessage(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam); User32DLL.java
BOOL AnyPopup(); User32DLL.java
BOOL IsChild(HWND hWndParent, HWND hWnd); User32DLL.java
HWND GetLastActivePopup(HWND hWnd); User32DLL.java
void SetLastErrorEx(DWORD dwErrCode, DWORD dwType); User32DLL.java
int GetSystemMetrics(int nIndex); User32DLL.java
DWORD SetClassLong(HWND hWnd, int nIndex, LONG dwNewLong); User32DLL.java
HANDLE RemoveProp(HWND hWnd, String lpString); User32DLL.java
BOOL ScrollDC(HDC hDC, int dx, int dy, RECT lprcScroll, RECT lprcClip, HRGN hrgnUpdate, RECT lprcUpdate); User32DLL.java
BOOL SetCaretPos(int X, int Y); User32DLL.java
BOOL ShowCaret(HWND hWnd); User32DLL.java
BOOL DestroyCaret(); User32DLL.java
HWND GetParent(HWND hWnd); User32DLL.java
int GetWindowTextLength(HWND hWnd); User32DLL.java
BOOL SetForegroundWindow(HWND hWnd); User32DLL.java
BOOL IsIconic(HWND hWnd); User32DLL.java
BOOL IsWindowUnicode(HWND hWnd); User32DLL.java
HDC GetDCEx(HWND hWnd, HRGN hrgnClip, int flags); User32DLL.java
BOOL InternetSetOption(HANDLE hInternet, DWORD dwOption, LPVOID lpBuffer, DWORD dwBufferLength); WininetDLL.java
int WSAStartup(int wVersionRequested, WSADATA lpWSAData); Ws2_32DLL.java
int WSACleanup(); Ws2_32DLL.java
int socket(int af, int type, int protocol); Ws2_32DLL.java
int closesocket(int s); Ws2_32DLL.java
int connect(int s, SOCKADDR.ByReference name, int namelen); Ws2_32DLL.java
int bind(int s, SOCKADDR.ByReference name, int namelen); Ws2_32DLL.java
int accept(int s, SOCKADDR.ByReference addr, IntByReference addrlen); Ws2_32DLL.java
int listen(int s, int backlog); Ws2_32DLL.java
int gethostname(byte[] name, int namelen); Ws2_32DLL.java
hostent gethostbyname(String name); Ws2_32DLL.java
int send(int s, String buf, int len, int flags); Ws2_32DLL.java
int recv(int s, Pointer buf, int len, int flags); Ws2_32DLL.java
int shutdown(int s, int how); Ws2_32DLL.java