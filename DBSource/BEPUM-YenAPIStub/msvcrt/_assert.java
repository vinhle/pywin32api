/**
 * Project: BE-PUMv2
 * Package name: v2.org.analysis.apihandle.winapi.msvcrt.functions
 * File name: _assert.java
 * Created date: Jul 12, 2016
 * Description:
 */
package v2.org.analysis.apihandle.winapi.msvcrt.functions;

import v2.org.analysis.apihandle.winapi.msvcrt.MSVCRTAPI;

/**
 * @author Yen Nguyen
 *
 */
public class _assert extends MSVCRTAPI {

	public _assert() {
		super();
		NUM_OF_PARMS = 1;
	}


	@Override
	public void execute() {
		// TODO: I have not decided what I need to do in this API yet
	}

}
