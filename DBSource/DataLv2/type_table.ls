{
  "LPCDVTARGETDEVICE": [
    "DVTARGETDEVICE", 
    "DVTARGETDEVICE[]"
  ], 
  "ENUM_SERVICE_STATUS*": [
    "ENUM_SERVICE_STATUS", 
    "ENUM_SERVICE_STATUS[]"
  ], 
  "PCSTR*": [
    "String[]", 
    "PointerByReference"
  ], 
  "LPPROCESSOR_NUMBER*": [
    "PointerByReference"
  ], 
  "ACL*": [
    "ACL", 
    "ACL[]"
  ], 
  "NETINFOSTRUCT": [
    "NETINFOSTRUCT"
  ], 
  "SHARE_INFO_501*": [
    "SHARE_INFO_501", 
    "SHARE_INFO_501[]"
  ], 
  "PTPMPARAMS": [
    "TPMPARAMS", 
    "TPMPARAMS[]"
  ], 
  "USER_INFO_3": [
    "USER_INFO_3"
  ], 
  "LOGBRUSH**": [
    "PointerByReference"
  ], 
  "PCURSORINFO": [
    "CURSORINFO", 
    "CURSORINFO[]"
  ], 
  "SYSTEM_POWER_LEVEL**": [
    "PointerByReference"
  ], 
  "LPTimeSysInfo": [
    "IntByReference", 
    "int[]"
  ], 
  "PPIXELFORMATDESCRIPTOR": [
    "PIXELFORMATDESCRIPTOR", 
    "PIXELFORMATDESCRIPTOR[]"
  ], 
  "PNET_DISPLAY_USER*": [
    "PointerByReference"
  ], 
  "LPPIXELFORMATDESCRIPTOR": [
    "PIXELFORMATDESCRIPTOR", 
    "PIXELFORMATDESCRIPTOR[]"
  ], 
  "PLCID": [
    "IntByReference", 
    "long[]"
  ], 
  "LPCCONNECTION_INFO_1": [
    "CONNECTION_INFO_1", 
    "CONNECTION_INFO_1[]"
  ], 
  "LPCCONNECTION_INFO_0": [
    "CONNECTION_INFO_0", 
    "CONNECTION_INFO_0[]"
  ], 
  "LPNET_DISPLAY_MACHINE*": [
    "PointerByReference"
  ], 
  "LPGET_FILEEX_INFO_LEVELS": [
    "IntByReference", 
    "int[]"
  ], 
  "PUSER_INFO_1008*": [
    "PointerByReference"
  ], 
  "PSERVER_INFO_101*": [
    "PointerByReference"
  ], 
  "PLONG_PTR*": [
    "PointerByReference"
  ], 
  "TRIVERTEX": [
    "TRIVERTEX"
  ], 
  "TRACE_GUID_PROPERTIES*": [
    "TRACE_GUID_PROPERTIES", 
    "TRACE_GUID_PROPERTIES[]"
  ], 
  "COMMTIMEOUTS*": [
    "COMMTIMEOUTS", 
    "COMMTIMEOUTS[]"
  ], 
  "LPFILE_INFO_3*": [
    "PointerByReference"
  ], 
  "EVENTLOGRECORD*": [
    "EVENTLOGRECORD", 
    "EVENTLOGRECORD[]"
  ], 
  "LPPDH_COUNTER_PATH_ELEMENTS*": [
    "PointerByReference"
  ], 
  "CONSOLE_FONT_INFOEX": [
    "CONSOLE_FONT_INFOEX"
  ], 
  "CCHAR*": [
    "byte"
  ], 
  "JOBOBJECT_LIMIT_VIOLATION_INFORMATION*": [
    "JOBOBJECT_LIMIT_VIOLATION_INFORMATION", 
    "JOBOBJECT_LIMIT_VIOLATION_INFORMATION[]"
  ], 
  "CONSOLE_HISTORY_INFO": [
    "CONSOLE_HISTORY_INFO"
  ], 
  "LPCFONTSIGNATURE": [
    "FONTSIGNATURE", 
    "FONTSIGNATURE[]"
  ], 
  "CHAR_INFO*": [
    "CHAR_INFO", 
    "CHAR_INFO[]"
  ], 
  "LPDRAWTEXTPARAMS": [
    "DRAWTEXTPARAMS", 
    "DRAWTEXTPARAMS[]"
  ], 
  "LPSMALL_RECT": [
    "SMALL_RECT", 
    "SMALL_RECT[]"
  ], 
  "POVERLAPPED_ENTRY": [
    "OVERLAPPED_ENTRY", 
    "OVERLAPPED_ENTRY[]"
  ], 
  "PEXECUTION_STATE": [
    "IntByReference", 
    "long[]"
  ], 
  "PPOWER_ACTION_POLICY": [
    "POWER_ACTION_POLICY", 
    "POWER_ACTION_POLICY[]"
  ], 
  "LPSYSTEM_LOGICAL_PROCESSOR_INFORMATION": [
    "SYSTEM_LOGICAL_PROCESSOR_INFORMATION", 
    "SYSTEM_LOGICAL_PROCESSOR_INFORMATION[]"
  ], 
  "HRGN*": [
    "HANDLEByReference"
  ], 
  "LPRGNDATA*": [
    "PointerByReference"
  ], 
  "LPWINDOW_BUFFER_SIZE_RECORD*": [
    "PointerByReference"
  ], 
  "PITEMIDLIST*": [
    "PointerByReference"
  ], 
  "LOGPALETTE**": [
    "PointerByReference"
  ], 
  "LONG64": [
    "long"
  ], 
  "PFILE_NOTIFY_INFORMATION": [
    "FILE_NOTIFY_INFORMATION", 
    "FILE_NOTIFY_INFORMATION[]"
  ], 
  "LPALTTABINFO*": [
    "PointerByReference"
  ], 
  "MONITORENUMPROC": [
    "WinUser.MONITORENUMPROC"
  ], 
  "LPVIDEOPARAMETERS": [
    "VIDEOPARAMETERS", 
    "VIDEOPARAMETERS[]"
  ], 
  "LPCETW_BUFFER_CONTEXT": [
    "ETW_BUFFER_CONTEXT", 
    "ETW_BUFFER_CONTEXT[]"
  ], 
  "SERVICE_STATUS**": [
    "PointerByReference"
  ], 
  "LPCGLOBAL_POWER_POLICY": [
    "GLOBAL_POWER_POLICY", 
    "GLOBAL_POWER_POLICY[]"
  ], 
  "LPHGLOBAL": [
    "HANDLEByReference"
  ], 
  "AXESLIST*": [
    "AXESLIST", 
    "AXESLIST[]"
  ], 
  "LPCSHITEMID": [
    "SHITEMID", 
    "SHITEMID[]"
  ], 
  "LPDEVMODE*": [
    "PointerByReference"
  ], 
  "WPARAM**": [
    "PointerByReference"
  ], 
  "PFONTSIGNATURE*": [
    "PointerByReference"
  ], 
  "LPCUSER_INFO_3": [
    "USER_INFO_3", 
    "USER_INFO_3[]"
  ], 
  "PROVIDOR_INFO_2**": [
    "PointerByReference"
  ], 
  "LPCUSER_INFO_1": [
    "USER_INFO_1", 
    "USER_INFO_1[]"
  ], 
  "LPCUSER_INFO_4": [
    "USER_INFO_4", 
    "USER_INFO_4[]"
  ], 
  "LPEFS_HASH_BLOB": [
    "EFS_HASH_BLOB", 
    "EFS_HASH_BLOB[]"
  ], 
  "LPCSHARE_INFO_0": [
    "SHARE_INFO_0", 
    "SHARE_INFO_0[]"
  ], 
  "LPCSHARE_INFO_2": [
    "SHARE_INFO_2", 
    "SHARE_INFO_2[]"
  ], 
  "LPDFS_INFO_50*": [
    "PointerByReference"
  ], 
  "LPHEAPLIST32*": [
    "PointerByReference"
  ], 
  "PPROCESS_MEMORY_COUNTERS": [
    "PROCESS_MEMORY_COUNTERS", 
    "PROCESS_MEMORY_COUNTERS[]"
  ], 
  "PGUID": [
    "GUID", 
    "GUID[]"
  ], 
  "LPCDISCDLGSTRUCT": [
    "DISCDLGSTRUCT", 
    "DISCDLGSTRUCT[]"
  ], 
  "DFS_INFO_102": [
    "DFS_INFO_102"
  ], 
  "PWSAPROTOCOLCHAIN": [
    "WSAPROTOCOLCHAIN", 
    "WSAPROTOCOLCHAIN[]"
  ], 
  "LPMENUITEMTEMPLATE": [
    "MENUITEMTEMPLATE", 
    "MENUITEMTEMPLATE[]"
  ], 
  "SERVICE_DESCRIPTION": [
    "SERVICE_DESCRIPTION"
  ], 
  "PDEBUGHOOKINFO": [
    "DEBUGHOOKINFO", 
    "DEBUGHOOKINFO[]"
  ], 
  "DFS_INFO_103": [
    "DFS_INFO_103"
  ], 
  "FLASHWINFO**": [
    "PointerByReference"
  ], 
  "LPPRINTER_INFO_8": [
    "PRINTER_INFO_8", 
    "PRINTER_INFO_8[]"
  ], 
  "DWORDLONG*": [
    "LongByReference", 
    "__int64[]"
  ], 
  "DFS_INFO_150": [
    "DFS_INFO_150"
  ], 
  "LPCMENUEX_TEMPLATE_ITEM": [
    "MENUEX_TEMPLATE_ITEM", 
    "MENUEX_TEMPLATE_ITEM[]"
  ], 
  "LPBOOLEAN*": [
    "byte"
  ], 
  "PPROCESS_HEAP_ENTRY*": [
    "PointerByReference"
  ], 
  "SYSTEM_POWER_LEVEL*": [
    "SYSTEM_POWER_LEVEL", 
    "SYSTEM_POWER_LEVEL[]"
  ], 
  "HDESK": [
    "HANDLE"
  ], 
  "PWCRANGE": [
    "WCRANGE", 
    "WCRANGE[]"
  ], 
  "LPSECURITY_ATTRIBUTES": [
    "SECURITY_ATTRIBUTES", 
    "SECURITY_ATTRIBUTES[]"
  ], 
  "LPSHARE_INFO_501*": [
    "PointerByReference"
  ], 
  "NET_DISPLAY_GROUP**": [
    "PointerByReference"
  ], 
  "WSAPROTOCOL_INFO*": [
    "WSAPROTOCOL_INFO", 
    "WSAPROTOCOL_INFO[]"
  ], 
  "PROCESS_INFORMATION**": [
    "PointerByReference"
  ], 
  "EVENTMSG": [
    "EVENTMSG"
  ], 
  "OSVERSIONINFOEX*": [
    "OSVERSIONINFOEX", 
    "OSVERSIONINFOEX[]"
  ], 
  "LPMACHINE_PROCESSOR_POWER_POLICY*": [
    "PointerByReference"
  ], 
  "LPMDICREATESTRUCT": [
    "MDICREATESTRUCT", 
    "MDICREATESTRUCT[]"
  ], 
  "SYSGEOCLASS**": [
    "PointerByReference"
  ], 
  "LPCTRACKMOUSEEVENT": [
    "TRACKMOUSEEVENT", 
    "TRACKMOUSEEVENT[]"
  ], 
  "AUDIODESCRIPTION*": [
    "AUDIODESCRIPTION", 
    "AUDIODESCRIPTION[]"
  ], 
  "NETINFOSTRUCT**": [
    "PointerByReference"
  ], 
  "LPNET_DISPLAY_USER": [
    "NET_DISPLAY_USER", 
    "NET_DISPLAY_USER[]"
  ], 
  "PRAWINPUTDEVICE": [
    "RAWINPUTDEVICE", 
    "RAWINPUTDEVICE[]"
  ], 
  "LPUSER_INFO_1020": [
    "USER_INFO_1020", 
    "USER_INFO_1020[]"
  ], 
  "LPCONSOLE_READCONSOLE_CONTROL*": [
    "PointerByReference"
  ], 
  "SIZE_T**": [
    "PointerByReference"
  ], 
  "PRINTER_NOTIFY_INFO*": [
    "PRINTER_NOTIFY_INFO", 
    "PRINTER_NOTIFY_INFO[]"
  ], 
  "CLIPFORMAT*": [
    "IntByReference", 
    "int[]"
  ], 
  "LPAXISINFO": [
    "AXISINFO", 
    "AXISINFO[]"
  ], 
  "LOGFONT": [
    "LOGFONT"
  ], 
  "PPRINTER_ENUM_VALUES*": [
    "PointerByReference"
  ], 
  "LPMSG*": [
    "PointerByReference"
  ], 
  "PLOGPALETTE*": [
    "PointerByReference"
  ], 
  "PPRINTER_INFO_8*": [
    "PointerByReference"
  ], 
  "LPAUDIODESCRIPTION": [
    "AUDIODESCRIPTION", 
    "AUDIODESCRIPTION[]"
  ], 
  "LPPDH_FMT_COUNTERVALUE_ITEM": [
    "PDH_FMT_COUNTERVALUE_ITEM", 
    "PDH_FMT_COUNTERVALUE_ITEM[]"
  ], 
  "WKSTA_USER_INFO_1101": [
    "WKSTA_USER_INFO_1101"
  ], 
  "OVERLAPPED_ENTRY": [
    "OVERLAPPED_ENTRY"
  ], 
  "USER_INFO_1024**": [
    "PointerByReference"
  ], 
  "PJOBOBJECT_LIMIT_VIOLATION_INFORMATION*": [
    "PointerByReference"
  ], 
  "PSERVICE_SID_INFO*": [
    "PointerByReference"
  ], 
  "PSHSTOCKICONID*": [
    "PointerByReference"
  ], 
  "ENUMTEXTMETRIC*": [
    "ENUMTEXTMETRIC", 
    "ENUMTEXTMETRIC[]"
  ], 
  "PPAINTSTRUCT*": [
    "PointerByReference"
  ], 
  "LPSSIZE_T": [
    "IntByReference", 
    "long[]"
  ], 
  "LPCFILE_INFO_2": [
    "FILE_INFO_2", 
    "FILE_INFO_2[]"
  ], 
  "ACL_INFORMATION_CLASS**": [
    "PointerByReference"
  ], 
  "LPNEWTEXTMETRICEX*": [
    "PointerByReference"
  ], 
  "LPLCID*": [
    "PointerByReference"
  ], 
  "PHANDLETABLE": [
    "HANDLETABLE", 
    "HANDLETABLE[]"
  ], 
  "LPPDH_COUNTER_PATH_ELEMENTS": [
    "PDH_COUNTER_PATH_ELEMENTS", 
    "PDH_COUNTER_PATH_ELEMENTS[]"
  ], 
  "LPDFS_STORAGE_INFO*": [
    "PointerByReference"
  ], 
  "LPDFS_INFO_103*": [
    "PointerByReference"
  ], 
  "PJOB_INFO_3*": [
    "PointerByReference"
  ], 
  "LPICONINFO*": [
    "PointerByReference"
  ], 
  "PORT_INFO_1**": [
    "PointerByReference"
  ], 
  "PNTMS_NOTIFICATIONINFORMATION": [
    "NTMS_NOTIFICATIONINFORMATION", 
    "NTMS_NOTIFICATIONINFORMATION[]"
  ], 
  "LPWER_REGISTER_FILE_TYPE*": [
    "PointerByReference"
  ], 
  "SMALL_RECT**": [
    "PointerByReference"
  ], 
  "LPDFS_TARGET_PRIORITY": [
    "DFS_TARGET_PRIORITY", 
    "DFS_TARGET_PRIORITY[]"
  ], 
  "REGSAM": [
    "int", 
    "WORD"
  ], 
  "PPDH_FMT_COUNTERVALUE": [
    "PDH_FMT_COUNTERVALUE", 
    "PDH_FMT_COUNTERVALUE[]"
  ], 
  "PLCID*": [
    "PointerByReference"
  ], 
  "NONCLIENTMETRICS": [
    "NONCLIENTMETRICS"
  ], 
  "DFS_INFO_104*": [
    "DFS_INFO_104", 
    "DFS_INFO_104[]"
  ], 
  "USE_INFO_2": [
    "USE_INFO_2"
  ], 
  "USE_INFO_1": [
    "USE_INFO_1"
  ], 
  "LPCPINFOEX*": [
    "PointerByReference"
  ], 
  "PANOSE*": [
    "PANOSE", 
    "PANOSE[]"
  ], 
  "LPJOBOBJECT_NOTIFICATION_LIMIT_INFORMATION": [
    "JOBOBJECT_NOTIFICATION_LIMIT_INFORMATION", 
    "JOBOBJECT_NOTIFICATION_LIMIT_INFORMATION[]"
  ], 
  "LPCUSER_INFO_21": [
    "USER_INFO_21", 
    "USER_INFO_21[]"
  ], 
  "LPCUSER_INFO_20": [
    "USER_INFO_20", 
    "USER_INFO_20[]"
  ], 
  "LPCUSER_INFO_23": [
    "USER_INFO_23", 
    "USER_INFO_23[]"
  ], 
  "LPCUSER_INFO_22": [
    "USER_INFO_22", 
    "USER_INFO_22[]"
  ], 
  "SIZEL": [
    "SIZE"
  ], 
  "LPACCESS_MASK*": [
    "PointerByReference"
  ], 
  "PAT_ENUM": [
    "AT_ENUM", 
    "AT_ENUM[]"
  ], 
  "DRIVER_INFO_6*": [
    "DRIVER_INFO_6", 
    "DRIVER_INFO_6[]"
  ], 
  "LPGROUP_AFFINITY*": [
    "PointerByReference"
  ], 
  "PLPCSTR": [
    "byte"
  ], 
  "LPGEOCLASS*": [
    "IntByReference", 
    "long[]"
  ], 
  "LPACCEL": [
    "ACCEL", 
    "ACCEL[]"
  ], 
  "UMS_CREATE_THREAD_ATTRIBUTES": [
    "UMS_CREATE_THREAD_ATTRIBUTES"
  ], 
  "PMENUINFO": [
    "MENUINFO", 
    "MENUINFO[]"
  ], 
  "LPMONITOR_INFO_2*": [
    "PointerByReference"
  ], 
  "HENHMETAFILE": [
    "HANDLE"
  ], 
  "SHFILEINFO*": [
    "SHFILEINFO", 
    "SHFILEINFO[]"
  ], 
  "PMDICREATESTRUCT*": [
    "PointerByReference"
  ], 
  "RAWINPUTDEVICELIST": [
    "RAWINPUTDEVICELIST"
  ], 
  "WER_DUMP_TYPE": [
    "int"
  ], 
  "PMONITORINFO*": [
    "PointerByReference"
  ], 
  "LPCEVENTLOG_FULL_INFORMATION": [
    "EVENTLOG_FULL_INFORMATION", 
    "EVENTLOG_FULL_INFORMATION[]"
  ], 
  "PDRAWTEXTPARAMS*": [
    "PointerByReference"
  ], 
  "LPCLOGFONT": [
    "LOGFONT", 
    "LOGFONT[]"
  ], 
  "DYNAMIC_TIME_ZONE_INFORMATION*": [
    "DYNAMIC_TIME_ZONE_INFORMATION", 
    "DYNAMIC_TIME_ZONE_INFORMATION[]"
  ], 
  "LPCSC_ACTION_TYPE": [
    "IntByReference", 
    "int[]"
  ], 
  "LPCWER_DUMP_TYPE": [
    "IntByReference", 
    "int[]"
  ], 
  "LPRAWINPUTHEADER": [
    "RAWINPUTHEADER", 
    "RAWINPUTHEADER[]"
  ], 
  "LPTOKEN_DEFAULT_DACL*": [
    "PointerByReference"
  ], 
  "CPINFOEX*": [
    "CPINFOEX", 
    "CPINFOEX[]"
  ], 
  "PJOBOBJECTINFOCLASS*": [
    "PointerByReference"
  ], 
  "AUDIODESCRIPTION": [
    "AUDIODESCRIPTION"
  ], 
  "LPHIGHCONTRAST*": [
    "PointerByReference"
  ], 
  "PMETAFILEPICT*": [
    "PointerByReference"
  ], 
  "LPCWIN32_STREAM_ID": [
    "WIN32_STREAM_ID", 
    "WIN32_STREAM_ID[]"
  ], 
  "SERVICE_PREFERRED_NODE_INFO**": [
    "PointerByReference"
  ], 
  "FONTSIGNATURE*": [
    "FONTSIGNATURE", 
    "FONTSIGNATURE[]"
  ], 
  "PWIN32_STREAM_ID*": [
    "PointerByReference"
  ], 
  "USER_INFO_4*": [
    "USER_INFO_4", 
    "USER_INFO_4[]"
  ], 
  "LPCOVERLAPPED": [
    "OVERLAPPED", 
    "OVERLAPPED[]"
  ], 
  "LPSERVICE_SID_INFO": [
    "SERVICE_SID_INFO", 
    "SERVICE_SID_INFO[]"
  ], 
  "LPATOM": [
    "ShortByReference", 
    "short[]"
  ], 
  "PSTREAM_INFO_LEVELS*": [
    "PointerByReference"
  ], 
  "LPDFS_INFO_150*": [
    "PointerByReference"
  ], 
  "LPCMONITORINFO": [
    "MONITORINFO", 
    "MONITORINFO[]"
  ], 
  "PLMSTR": [
    "char"
  ], 
  "LOCALGROUP_INFO_1002*": [
    "LOCALGROUP_INFO_1002", 
    "LOCALGROUP_INFO_1002[]"
  ], 
  "LPPRINTER_INFO_6*": [
    "PointerByReference"
  ], 
  "PDH_RAW_COUNTER_ITEM**": [
    "PointerByReference"
  ], 
  "PNET_DISPLAY_GROUP": [
    "NET_DISPLAY_GROUP", 
    "NET_DISPLAY_GROUP[]"
  ], 
  "PENUM_SERVICE_STATUS": [
    "ENUM_SERVICE_STATUS", 
    "ENUM_SERVICE_STATUS[]"
  ], 
  "PZZWSTR*": [
    "WString[]", 
    "PointerByReference"
  ], 
  "LPTASKDIALOG_COMMON_BUTTON_FLAGS": [
    "IntByReference", 
    "int[]"
  ], 
  "PBOOL*": [
    "PointerByReference"
  ], 
  "SERVICE_STATUS_HANDLE": [
    "HANDLE"
  ], 
  "USER_INFO_1020*": [
    "USER_INFO_1020", 
    "USER_INFO_1020[]"
  ], 
  "CONNECTION_INFO_1": [
    "CONNECTION_INFO_1"
  ], 
  "UNIVERSAL_NAME_INFO": [
    "UNIVERSAL_NAME_INFO"
  ], 
  "LPSHDESCRIPTIONID": [
    "SHDESCRIPTIONID", 
    "SHDESCRIPTIONID[]"
  ], 
  "LPSERVICE_SID_INFO*": [
    "PointerByReference"
  ], 
  "WSAPROTOCOLCHAIN*": [
    "WSAPROTOCOLCHAIN", 
    "WSAPROTOCOLCHAIN[]"
  ], 
  "PPCSTR": [
    "byte"
  ], 
  "CONNECTION_INFO_0": [
    "CONNECTION_INFO_0"
  ], 
  "FILE_SEGMENT_ELEMENT*": [
    "FILE_SEGMENT_ELEMENT", 
    "FILE_SEGMENT_ELEMENT[]"
  ], 
  "LPTimeSysInfo*": [
    "PointerByReference"
  ], 
  "SIZE*": [
    "SIZE", 
    "SIZE[]"
  ], 
  "LPCOMMCONFIG*": [
    "PointerByReference"
  ], 
  "METAFILEPICT*": [
    "METAFILEPICT", 
    "METAFILEPICT[]"
  ], 
  "PANIMATIONINFO*": [
    "PointerByReference"
  ], 
  "USER_INFO_1014*": [
    "USER_INFO_1014", 
    "USER_INFO_1014[]"
  ], 
  "GEOCLASS***": [
    "IntByReference", 
    "long[]"
  ], 
  "USER_INFO_21**": [
    "PointerByReference"
  ], 
  "SERVICE_FAILURE_ACTIONS**": [
    "PointerByReference"
  ], 
  "PUSER_INFO_21*": [
    "PointerByReference"
  ], 
  "FILETIME*": [
    "FILETIME", 
    "FILETIME[]"
  ], 
  "WER_REGISTER_FILE_TYPE*": [
    "IntByReference", 
    "int[]"
  ], 
  "LPTAPE_SET_DRIVE_PARAMETERS*": [
    "PointerByReference"
  ], 
  "LPCTRIVERTEX": [
    "TRIVERTEX", 
    "TRIVERTEX[]"
  ], 
  "LARGE_INTEGER": [
    "LARGE_INTEGER"
  ], 
  "LPCREATESTRUCT*": [
    "PointerByReference"
  ], 
  "SHELLFLAGSTATE": [
    "SHELLFLAGSTATE"
  ], 
  "HICON": [
    "HANDLE"
  ], 
  "LPCSTICKYKEYS": [
    "STICKYKEYS", 
    "STICKYKEYS[]"
  ], 
  "CLIENTCREATESTRUCT*": [
    "CLIENTCREATESTRUCT", 
    "CLIENTCREATESTRUCT[]"
  ], 
  "RAWMOUSE*": [
    "RAWMOUSE", 
    "RAWMOUSE[]"
  ], 
  "HBRUSH*": [
    "HANDLEByReference"
  ], 
  "CWPSTRUCT": [
    "CWPSTRUCT"
  ], 
  "CONSOLE_HISTORY_INFO**": [
    "PointerByReference"
  ], 
  "LPCEMR": [
    "EMR", 
    "EMR[]"
  ], 
  "COORD**": [
    "PointerByReference"
  ], 
  "WKSTA_INFO_101**": [
    "PointerByReference"
  ], 
  "PTAPE_GET_DRIVE_PARAMETERS": [
    "TAPE_GET_DRIVE_PARAMETERS", 
    "TAPE_GET_DRIVE_PARAMETERS[]"
  ], 
  "LPCONSOLE_READCONSOLE_CONTROL": [
    "CONSOLE_READCONSOLE_CONTROL", 
    "CONSOLE_READCONSOLE_CONTROL[]"
  ], 
  "LPJOBOBJECT_BASIC_UI_RESTRICTIONS": [
    "JOBOBJECT_BASIC_UI_RESTRICTIONS", 
    "JOBOBJECT_BASIC_UI_RESTRICTIONS[]"
  ], 
  "PUSER_INFO_2*": [
    "PointerByReference"
  ], 
  "LPHW_PROFILE_INFO": [
    "HW_PROFILE_INFO", 
    "HW_PROFILE_INFO[]"
  ], 
  "LPULONG": [
    "IntByReference", 
    "long[]"
  ], 
  "LPJOBOBJECT_BASIC_ACCOUNTING_INFORMATION*": [
    "PointerByReference"
  ], 
  "LPCPRINTER_INFO_2": [
    "PRINTER_INFO_2", 
    "PRINTER_INFO_2[]"
  ], 
  "LPCPRINTER_INFO_3": [
    "PRINTER_INFO_3", 
    "PRINTER_INFO_3[]"
  ], 
  "LPCPRINTER_INFO_4": [
    "PRINTER_INFO_4", 
    "PRINTER_INFO_4[]"
  ], 
  "LPCPRINTER_INFO_5": [
    "PRINTER_INFO_5", 
    "PRINTER_INFO_5[]"
  ], 
  "LPCPRINTER_INFO_6": [
    "PRINTER_INFO_6", 
    "PRINTER_INFO_6[]"
  ], 
  "CWPRETSTRUCT*": [
    "CWPRETSTRUCT", 
    "CWPRETSTRUCT[]"
  ], 
  "LPCPRINTER_INFO_8": [
    "PRINTER_INFO_8", 
    "PRINTER_INFO_8[]"
  ], 
  "EVENTMSG**": [
    "PointerByReference"
  ], 
  "PNDDESHAREINFO": [
    "NDDESHAREINFO", 
    "NDDESHAREINFO[]"
  ], 
  "PAINTSTRUCT": [
    "PAINTSTRUCT"
  ], 
  "LPDFS_INFO_105*": [
    "PointerByReference"
  ], 
  "PSTARTUPINFO": [
    "STARTUPINFO", 
    "STARTUPINFO[]"
  ], 
  "METAFILEPICT**": [
    "PointerByReference"
  ], 
  "PACCESSTIMEOUT": [
    "ACCESSTIMEOUT", 
    "ACCESSTIMEOUT[]"
  ], 
  "LPSHARE_INFO_1005*": [
    "PointerByReference"
  ], 
  "USER_MODALS_INFO_1005**": [
    "PointerByReference"
  ], 
  "LPPDH_RAW_COUNTER_ITEM": [
    "PDH_RAW_COUNTER_ITEM", 
    "PDH_RAW_COUNTER_ITEM[]"
  ], 
  "LPCUSER_POWER_POLICY": [
    "USER_POWER_POLICY", 
    "USER_POWER_POLICY[]"
  ], 
  "WPARAM*": [
    "IntByReference", 
    "int[]"
  ], 
  "PCHAR_INFO": [
    "CHAR_INFO", 
    "CHAR_INFO[]"
  ], 
  "CPINFO*": [
    "CPINFO", 
    "CPINFO[]"
  ], 
  "LPPRINTER_NOTIFY_OPTIONS": [
    "PRINTER_NOTIFY_OPTIONS", 
    "PRINTER_NOTIFY_OPTIONS[]"
  ], 
  "LPENUMLOGFONT*": [
    "PointerByReference"
  ], 
  "PPROCESSOR_POWER_POLICY_INFO*": [
    "PointerByReference"
  ], 
  "MONITOR_INFO_1*": [
    "MONITOR_INFO_1", 
    "MONITOR_INFO_1[]"
  ], 
  "PRINTER_NOTIFY_OPTIONS**": [
    "PointerByReference"
  ], 
  "PPROCESS_MEMORY_COUNTERS_EX": [
    "PROCESS_MEMORY_COUNTERS_EX", 
    "PROCESS_MEMORY_COUNTERS_EX[]"
  ], 
  "PDESIGNVECTOR*": [
    "PointerByReference"
  ], 
  "LPCONSOLE_CURSOR_INFO*": [
    "PointerByReference"
  ], 
  "USER_MODALS_INFO_1001*": [
    "USER_MODALS_INFO_1001", 
    "USER_MODALS_INFO_1001[]"
  ], 
  "USER_MODALS_INFO_1003**": [
    "PointerByReference"
  ], 
  "PUINT_PTR*": [
    "PointerByReference"
  ], 
  "STICKYKEYS*": [
    "STICKYKEYS", 
    "STICKYKEYS[]"
  ], 
  "LOCALGROUP_INFO_0**": [
    "PointerByReference"
  ], 
  "PDFS_INFO_200*": [
    "PointerByReference"
  ], 
  "LPWSAPROTOCOL_INFO*": [
    "PointerByReference"
  ], 
  "PSCROLLINFO": [
    "SCROLLINFO", 
    "SCROLLINFO[]"
  ], 
  "HKEY": [
    "HANDLE"
  ], 
  "LPUSER_INFO_1003*": [
    "PointerByReference"
  ], 
  "LPCRECTL": [
    "RECTL", 
    "RECTL[]"
  ], 
  "PRINTER_NOTIFY_OPTIONS_TYPE": [
    "PRINTER_NOTIFY_OPTIONS_TYPE"
  ], 
  "PCWPRETSTRUCT": [
    "CWPRETSTRUCT", 
    "CWPRETSTRUCT[]"
  ], 
  "LPCHAR": [
    "byte"
  ], 
  "PPDH_COUNTER_INFO": [
    "PDH_COUNTER_INFO", 
    "PDH_COUNTER_INFO[]"
  ], 
  "MENUINFO*": [
    "MENUINFO", 
    "MENUINFO[]"
  ], 
  "LPJOBOBJECT_EXTENDED_LIMIT_INFORMATION*": [
    "PointerByReference"
  ], 
  "size_t**": [
    "PointerByReference"
  ], 
  "PGUITHREADINFO": [
    "GUITHREADINFO", 
    "GUITHREADINFO[]"
  ], 
  "FILE_INFO_BY_HANDLE_CLASS": [
    "int"
  ], 
  "LPFORMATETC": [
    "FORMATETC", 
    "FORMATETC[]"
  ], 
  "SHARE_INFO_1501": [
    "SHARE_INFO_1501"
  ], 
  "PROCESSOR_POWER_POLICY_INFO**": [
    "PointerByReference"
  ], 
  "ICONMETRICS": [
    "ICONMETRICS"
  ], 
  "WKSTA_INFO_102*": [
    "WKSTA_INFO_102", 
    "WKSTA_INFO_102[]"
  ], 
  "LPLOGBRUSH*": [
    "PointerByReference"
  ], 
  "SERVER_INFO_102": [
    "SERVER_INFO_102"
  ], 
  "SERVER_INFO_101": [
    "SERVER_INFO_101"
  ], 
  "SERVER_INFO_100": [
    "SERVER_INFO_100"
  ], 
  "LOCALGROUP_INFO_1": [
    "LOCALGROUP_INFO_1"
  ], 
  "LOCALGROUP_INFO_0": [
    "LOCALGROUP_INFO_0"
  ], 
  "SHARE_INFO_501": [
    "SHARE_INFO_501"
  ], 
  "LPDFS_STORAGE_INFO": [
    "DFS_STORAGE_INFO", 
    "DFS_STORAGE_INFO[]"
  ], 
  "PCURRENCYFMT*": [
    "PointerByReference"
  ], 
  "PROPERTYKEY": [
    "PROPERTYKEY"
  ], 
  "PRINTER_INFO_8*": [
    "PRINTER_INFO_8", 
    "PRINTER_INFO_8[]"
  ], 
  "LPDISCDLGSTRUCT*": [
    "PointerByReference"
  ], 
  "PROCESS_MEMORY_COUNTERS": [
    "PROCESS_MEMORY_COUNTERS"
  ], 
  "RM_APP_TYPE**": [
    "PointerByReference"
  ], 
  "PSC_ACTION*": [
    "PointerByReference"
  ], 
  "PSERVICE_CONTROL_STATUS_REASON_PARAMS": [
    "SERVICE_CONTROL_STATUS_REASON_PARAMS", 
    "SERVICE_CONTROL_STATUS_REASON_PARAMS[]"
  ], 
  "LPLCTYPE*": [
    "PointerByReference"
  ], 
  "PCOMBOBOXINFO*": [
    "PointerByReference"
  ], 
  "SECURITY_CONTEXT_TRACKING_MODE": [
    "byte"
  ], 
  "LPSYSTEM_POWER_LEVEL*": [
    "PointerByReference"
  ], 
  "LPDWORD*": [
    "PointerByReference"
  ], 
  "LPGROUP_AFFINITY": [
    "GROUP_AFFINITY", 
    "GROUP_AFFINITY[]"
  ], 
  "LPPDH_FMT_COUNTERVALUE*": [
    "PointerByReference"
  ], 
  "LPPDH_RAW_COUNTER": [
    "PDH_RAW_COUNTER", 
    "PDH_RAW_COUNTER[]"
  ], 
  "NONCLIENTMETRICS*": [
    "NONCLIENTMETRICS", 
    "NONCLIENTMETRICS[]"
  ], 
  "PNTMS_ALLOCATION_INFORMATION*": [
    "PointerByReference"
  ], 
  "MENUITEMINFO**": [
    "PointerByReference"
  ], 
  "PFILE_INFO_BY_HANDLE_CLASS": [
    "IntByReference", 
    "int[]"
  ], 
  "DEBUGHOOKINFO*": [
    "DEBUGHOOKINFO", 
    "DEBUGHOOKINFO[]"
  ], 
  "PRINTER_NOTIFY_INFO**": [
    "PointerByReference"
  ], 
  "PNEWTEXTMETRIC*": [
    "PointerByReference"
  ], 
  "LPCMENU_EVENT_RECORD": [
    "MENU_EVENT_RECORD", 
    "MENU_EVENT_RECORD[]"
  ], 
  "PDRIVER_INFO_6*": [
    "PointerByReference"
  ], 
  "PEFS_HASH_BLOB": [
    "EFS_HASH_BLOB", 
    "EFS_HASH_BLOB[]"
  ], 
  "GEOCLASS****": [
    "PointerByReference"
  ], 
  "USER_MODALS_INFO_2": [
    "USER_MODALS_INFO_2"
  ], 
  "USER_MODALS_INFO_3": [
    "USER_MODALS_INFO_3"
  ], 
  "USER_MODALS_INFO_0": [
    "USER_MODALS_INFO_0"
  ], 
  "USER_MODALS_INFO_1": [
    "USER_MODALS_INFO_1"
  ], 
  "PVS_FIXEDFILEINFO": [
    "VS_FIXEDFILEINFO", 
    "VS_FIXEDFILEINFO[]"
  ], 
  "PEVENT_TRACE_PROPERTIES": [
    "EVENT_TRACE_PROPERTIES", 
    "EVENT_TRACE_PROPERTIES[]"
  ], 
  "LPCONSOLE_HISTORY_INFO": [
    "CONSOLE_HISTORY_INFO", 
    "CONSOLE_HISTORY_INFO[]"
  ], 
  "LPCUSE_INFO_2": [
    "USE_INFO_2", 
    "USE_INFO_2[]"
  ], 
  "PACCESSTIMEOUT*": [
    "PointerByReference"
  ], 
  "PTRACE_GUID_PROPERTIES*": [
    "PointerByReference"
  ], 
  "LPGENERIC_MAPPING": [
    "GENERIC_MAPPING", 
    "GENERIC_MAPPING[]"
  ], 
  "LPLOCALGROUP_USERS_INFO_0": [
    "LOCALGROUP_USERS_INFO_0", 
    "LOCALGROUP_USERS_INFO_0[]"
  ], 
  "LPTAPE_SET_DRIVE_PARAMETERS": [
    "TAPE_SET_DRIVE_PARAMETERS", 
    "TAPE_SET_DRIVE_PARAMETERS[]"
  ], 
  "LPCINPUT": [
    "INPUT", 
    "INPUT[]"
  ], 
  "LPGROUP_INFO_1002": [
    "GROUP_INFO_1002", 
    "GROUP_INFO_1002[]"
  ], 
  "LPGROUP_INFO_1005": [
    "GROUP_INFO_1005", 
    "GROUP_INFO_1005[]"
  ], 
  "POWER_ACTION_POLICY": [
    "POWER_ACTION_POLICY"
  ], 
  "CHAR_INFO**": [
    "PointerByReference"
  ], 
  "LPPOWER_ACTION_POLICY": [
    "POWER_ACTION_POLICY", 
    "POWER_ACTION_POLICY[]"
  ], 
  "LPCCONSOLE_SELECTION_INFO": [
    "CONSOLE_SELECTION_INFO", 
    "CONSOLE_SELECTION_INFO[]"
  ], 
  "LPPGEOCLASS": [
    "IntByReference", 
    "long[]"
  ], 
  "LPEVENTLOG_FULL_INFORMATION": [
    "EVENTLOG_FULL_INFORMATION", 
    "EVENTLOG_FULL_INFORMATION[]"
  ], 
  "LATENCY_TIME": [
    "int"
  ], 
  "SECURITY_ATTRIBUTES**": [
    "PointerByReference"
  ], 
  "PROCESS_HEAP_ENTRY*": [
    "PROCESS_HEAP_ENTRY", 
    "PROCESS_HEAP_ENTRY[]"
  ], 
  "PBY_HANDLE_FILE_INFORMATION": [
    "BY_HANDLE_FILE_INFORMATION", 
    "BY_HANDLE_FILE_INFORMATION[]"
  ], 
  "PSERVER_INFO_102*": [
    "PointerByReference"
  ], 
  "LPCStringTable": [
    "StringTable", 
    "StringTable[]"
  ], 
  "NEWTEXTMETRICEX": [
    "NEWTEXTMETRICEX"
  ], 
  "LPCTpcGetSamplesArgs": [
    "TpcGetSamplesArgs", 
    "TpcGetSamplesArgs[]"
  ], 
  "FOCUS_EVENT_RECORD": [
    "FOCUS_EVENT_RECORD"
  ], 
  "PWER_REPORT_INFORMATION": [
    "WER_REPORT_INFORMATION", 
    "WER_REPORT_INFORMATION[]"
  ], 
  "LPSESSION_INFO_1*": [
    "PointerByReference"
  ], 
  "LPHDWP": [
    "HANDLEByReference"
  ], 
  "LPHGDIOBJ": [
    "HANDLEByReference"
  ], 
  "LPHEAPLIST32": [
    "HEAPLIST32", 
    "HEAPLIST32[]"
  ], 
  "LPCOMMTIMEOUTS": [
    "COMMTIMEOUTS", 
    "COMMTIMEOUTS[]"
  ], 
  "PMENUBARINFO*": [
    "PointerByReference"
  ], 
  "SYSTEM_LOGICAL_PROCESSOR_INFORMATION*": [
    "SYSTEM_LOGICAL_PROCESSOR_INFORMATION", 
    "SYSTEM_LOGICAL_PROCESSOR_INFORMATION[]"
  ], 
  "LPUINT64*": [
    "PointerByReference"
  ], 
  "RAWKEYBOARD": [
    "RAWKEYBOARD"
  ], 
  "PSHARE_INFO_1004*": [
    "PointerByReference"
  ], 
  "RGNDATA**": [
    "PointerByReference"
  ], 
  "PSERVER_INFO_403*": [
    "PointerByReference"
  ], 
  "LPCPROCESS_INFORMATION": [
    "PROCESS_INFORMATION", 
    "PROCESS_INFORMATION[]"
  ], 
  "PJOBOBJECT_CPU_RATE_CONTROL_INFORMATION": [
    "JOBOBJECT_CPU_RATE_CONTROL_INFORMATION", 
    "JOBOBJECT_CPU_RATE_CONTROL_INFORMATION[]"
  ], 
  "LPUINT32*": [
    "PointerByReference"
  ], 
  "LPVS_FIXEDFILEINFO*": [
    "PointerByReference"
  ], 
  "POWER_ACTION_POLICY**": [
    "PointerByReference"
  ], 
  "LPCSCNRT_STATUS": [
    "IntByReference", 
    "int[]"
  ], 
  "PPRINTER_NOTIFY_INFO*": [
    "PointerByReference"
  ], 
  "SERVER_INFO_100**": [
    "PointerByReference"
  ], 
  "SERVICE_STATUS_PROCESS": [
    "SERVICE_STATUS_PROCESS"
  ], 
  "ETW_BUFFER_CONTEXT": [
    "ETW_BUFFER_CONTEXT"
  ], 
  "MDICREATESTRUCT": [
    "MDICREATESTRUCT"
  ], 
  "PSIGDN": [
    "IntByReference", 
    "int[]"
  ], 
  "HPEN": [
    "HANDLE"
  ], 
  "PMACHINE_PROCESSOR_POWER_POLICY*": [
    "PointerByReference"
  ], 
  "LPCUSER_OTHER_INFO": [
    "USER_OTHER_INFO", 
    "USER_OTHER_INFO[]"
  ], 
  "HANDLETABLE": [
    "HANDLETABLE"
  ], 
  "KERNINGPAIR*": [
    "KERNINGPAIR", 
    "KERNINGPAIR[]"
  ], 
  "PUSER_INFO_4*": [
    "PointerByReference"
  ], 
  "PPROCESSOR_POWER_POLICY": [
    "PROCESSOR_POWER_POLICY", 
    "PROCESSOR_POWER_POLICY[]"
  ], 
  "LPMONITORINFO*": [
    "PointerByReference"
  ], 
  "LPSC_HANDLE": [
    "HANDLEByReference"
  ], 
  "PNONCLIENTMETRICS*": [
    "PointerByReference"
  ], 
  "LPUSEROBJECTFLAGS": [
    "USEROBJECTFLAGS", 
    "USEROBJECTFLAGS[]"
  ], 
  "PMOUSEHOOKSTRUCT*": [
    "PointerByReference"
  ], 
  "WKSTA_INFO_502": [
    "WKSTA_INFO_502"
  ], 
  "PPRINTER_NOTIFY_INFO_DATA": [
    "PRINTER_NOTIFY_INFO_DATA", 
    "PRINTER_NOTIFY_INFO_DATA[]"
  ], 
  "PLANGID": [
    "ShortByReference", 
    "short[]"
  ], 
  "PABC*": [
    "PointerByReference"
  ], 
  "ABCFLOAT**": [
    "PointerByReference"
  ], 
  "PZZWSTR": [
    "CHARByReference", 
    "wchar_t[]", 
    "WString"
  ], 
  "LPCGROUP_AFFINITY": [
    "GROUP_AFFINITY", 
    "GROUP_AFFINITY[]"
  ], 
  "LPSERVICE_FAILURE_ACTIONS": [
    "SERVICE_FAILURE_ACTIONS", 
    "SERVICE_FAILURE_ACTIONS[]"
  ], 
  "STARTUPINFO": [
    "STARTUPINFO"
  ], 
  "SHARE_INFO_1004*": [
    "SHARE_INFO_1004", 
    "SHARE_INFO_1004[]"
  ], 
  "PCALID": [
    "IntByReference", 
    "long[]"
  ], 
  "CONSOLE_SELECTION_INFO*": [
    "CONSOLE_SELECTION_INFO", 
    "CONSOLE_SELECTION_INFO[]"
  ], 
  "LPUSHORT": [
    "ShortByReference", 
    "short[]"
  ], 
  "LPWER_REPORT_TYPE*": [
    "PointerByReference"
  ], 
  "LPFONTSIGNATURE": [
    "FONTSIGNATURE", 
    "FONTSIGNATURE[]"
  ], 
  "PFLASHWINFO*": [
    "PointerByReference"
  ], 
  "MENUITEMTEMPLATEHEADER": [
    "MENUITEMTEMPLATEHEADER"
  ], 
  "PSERVICE_FAILURE_ACTIONS_FLAG": [
    "SERVICE_FAILURE_ACTIONS_FLAG", 
    "SERVICE_FAILURE_ACTIONS_FLAG[]"
  ], 
  "LPSSIZE_T*": [
    "PointerByReference"
  ], 
  "PSYSTEM_INFO": [
    "SYSTEM_INFO", 
    "SYSTEM_INFO[]"
  ], 
  "INT16*": [
    "ShortByReference", 
    "short[]"
  ], 
  "PDWORD64*": [
    "PointerByReference"
  ], 
  "PLONG32": [
    "IntByReference", 
    "int[]"
  ], 
  "LPCGLOBAL_USER_POWER_POLICY": [
    "GLOBAL_USER_POWER_POLICY", 
    "GLOBAL_USER_POWER_POLICY[]"
  ], 
  "SHARE_INFO_502**": [
    "PointerByReference"
  ], 
  "ENUMLOGFONTEXDV": [
    "ENUMLOGFONTEXDV"
  ], 
  "LPCICONINFO": [
    "ICONINFO", 
    "ICONINFO[]"
  ], 
  "LPSHARE_INFO_502*": [
    "PointerByReference"
  ], 
  "LPQUERY_SERVICE_CONFIG": [
    "QUERY_SERVICE_CONFIG", 
    "QUERY_SERVICE_CONFIG[]"
  ], 
  "LOCALGROUP_MEMBERS_INFO_1": [
    "LOCALGROUP_MEMBERS_INFO_1"
  ], 
  "LOCALGROUP_MEMBERS_INFO_0": [
    "LOCALGROUP_MEMBERS_INFO_0"
  ], 
  "LOCALGROUP_MEMBERS_INFO_3": [
    "LOCALGROUP_MEMBERS_INFO_3"
  ], 
  "LPRASTERIZER_STATUS": [
    "RASTERIZER_STATUS", 
    "RASTERIZER_STATUS[]"
  ], 
  "PCOMBOBOXINFO": [
    "COMBOBOXINFO", 
    "COMBOBOXINFO[]"
  ], 
  "LPDISCDLGSTRUCT": [
    "DISCDLGSTRUCT", 
    "DISCDLGSTRUCT[]"
  ], 
  "MOUSEKEYS*": [
    "MOUSEKEYS", 
    "MOUSEKEYS[]"
  ], 
  "PROCESSOR_POWER_POLICY_INFO": [
    "PROCESSOR_POWER_POLICY_INFO"
  ], 
  "SIZE_T": [
    "int"
  ], 
  "LPPDH_COUNTER_INFO": [
    "PDH_COUNTER_INFO", 
    "PDH_COUNTER_INFO[]"
  ], 
  "LPEVENT_TRACE_PROPERTIES*": [
    "PointerByReference"
  ], 
  "LPCHANDLETABLE": [
    "HANDLETABLE", 
    "HANDLETABLE[]"
  ], 
  "LPCUSER_INFO_2": [
    "USER_INFO_2", 
    "USER_INFO_2[]"
  ], 
  "PADDJOB_INFO_1*": [
    "PointerByReference"
  ], 
  "PLPWSTR": [
    "char"
  ], 
  "LPMINIMIZEDMETRICS*": [
    "PointerByReference"
  ], 
  "GLOBAL_MACHINE_POWER_POLICY": [
    "GLOBAL_MACHINE_POWER_POLICY"
  ], 
  "PEMRALPHABLEND": [
    "EMRALPHABLEND", 
    "EMRALPHABLEND[]"
  ], 
  "LPAT_ENUM": [
    "AT_ENUM", 
    "AT_ENUM[]"
  ], 
  "PSSIZE_T": [
    "IntByReference", 
    "long[]"
  ], 
  "COMMTIMEOUTS**": [
    "PointerByReference"
  ], 
  "BATTERY_REPORTING_SCALE*": [
    "BATTERY_REPORTING_SCALE", 
    "BATTERY_REPORTING_SCALE[]"
  ], 
  "LPCOMMPROP*": [
    "PointerByReference"
  ], 
  "LPPCSTR*": [
    "byte"
  ], 
  "PRINTER_INFO_7*": [
    "PRINTER_INFO_7", 
    "PRINTER_INFO_7[]"
  ], 
  "PPRINTER_INFO_9*": [
    "PointerByReference"
  ], 
  "LPPRINTER_ENUM_VALUES": [
    "PRINTER_ENUM_VALUES", 
    "PRINTER_ENUM_VALUES[]"
  ], 
  "LPCPRINTER_NOTIFY_OPTIONS": [
    "PRINTER_NOTIFY_OPTIONS", 
    "PRINTER_NOTIFY_OPTIONS[]"
  ], 
  "PFLS_CALLBACK_FUNCTION": [
    "Callback"
  ], 
  "LPRAWINPUT*": [
    "PointerByReference"
  ], 
  "JOBOBJECT_BASIC_AND_IO_ACCOUNTING_INFORMATION*": [
    "JOBOBJECT_BASIC_AND_IO_ACCOUNTING_INFORMATION", 
    "JOBOBJECT_BASIC_AND_IO_ACCOUNTING_INFORMATION[]"
  ], 
  "SYSTEM_POWER_STATUS**": [
    "PointerByReference"
  ], 
  "ULONG_PTR*": [
    "IntByReference", 
    "long[]"
  ], 
  "LPSHITEMID": [
    "SHITEMID", 
    "SHITEMID[]"
  ], 
  "BATTERY_REPORTING_SCALE**": [
    "PointerByReference"
  ], 
  "LPLPCGEOCLASS": [
    "IntByReference", 
    "long[]"
  ], 
  "LPJOBOBJECT_BASIC_LIMIT_INFORMATION*": [
    "PointerByReference"
  ], 
  "LPEXECUTION_STATE": [
    "IntByReference", 
    "long[]"
  ], 
  "LPTPMPARAMS*": [
    "PointerByReference"
  ], 
  "PCREATESTRUCT": [
    "CREATESTRUCT", 
    "CREATESTRUCT[]"
  ], 
  "PJOBOBJECT_BASIC_UI_RESTRICTIONS": [
    "JOBOBJECT_BASIC_UI_RESTRICTIONS", 
    "JOBOBJECT_BASIC_UI_RESTRICTIONS[]"
  ], 
  "PFE_EXPORT_FUNC": [
    "WinBase.FE_EXPORT_FUNC"
  ], 
  "PFILE_INFO_2*": [
    "PointerByReference"
  ], 
  "Psize_t*": [
    "PointerByReference"
  ], 
  "IO_COUNTERS*": [
    "IO_COUNTERS", 
    "IO_COUNTERS[]"
  ], 
  "PKBDLLHOOKSTRUCT*": [
    "PointerByReference"
  ], 
  "LPCHIGHCONTRAST": [
    "HIGHCONTRAST", 
    "HIGHCONTRAST[]"
  ], 
  "PRM_PROCESS_INFO*": [
    "PointerByReference"
  ], 
  "LPCSHARE_INFO_1": [
    "SHARE_INFO_1", 
    "SHARE_INFO_1[]"
  ], 
  "LPTRACE_GUID_REGISTRATION": [
    "TRACE_GUID_REGISTRATION", 
    "TRACE_GUID_REGISTRATION[]"
  ], 
  "USER_MODALS_INFO_1004*": [
    "USER_MODALS_INFO_1004", 
    "USER_MODALS_INFO_1004[]"
  ], 
  "EVENT_INSTANCE_HEADER*": [
    "EVENT_INSTANCE_HEADER", 
    "EVENT_INSTANCE_HEADER[]"
  ], 
  "PGLYPHMETRICS*": [
    "PointerByReference"
  ], 
  "PSHDESCRIPTIONID": [
    "SHDESCRIPTIONID", 
    "SHDESCRIPTIONID[]"
  ], 
  "PWNDCLASSEX": [
    "WNDCLASSEX", 
    "WNDCLASSEX[]"
  ], 
  "LPDWORDLONG*": [
    "PointerByReference"
  ], 
  "PSC_ACTION_TYPE": [
    "IntByReference", 
    "int[]"
  ], 
  "LPPROCESS_HEAP_ENTRY*": [
    "PointerByReference"
  ], 
  "INT8*": [
    "byte"
  ], 
  "PCBT_CREATEWND*": [
    "PointerByReference"
  ], 
  "LPFOCUS_EVENT_RECORD*": [
    "PointerByReference"
  ], 
  "LPCJOBOBJECT_EXTENDED_LIMIT_INFORMATION": [
    "JOBOBJECT_EXTENDED_LIMIT_INFORMATION", 
    "JOBOBJECT_EXTENDED_LIMIT_INFORMATION[]"
  ], 
  "DFS_INFO_105*": [
    "DFS_INFO_105", 
    "DFS_INFO_105[]"
  ], 
  "LPULARGE_INTEGER": [
    "ULARGE_INTEGER", 
    "ULARGE_INTEGER[]"
  ], 
  "LPCWINDOWINFO": [
    "WINDOWINFO", 
    "WINDOWINFO[]"
  ], 
  "LPCHEAPLIST32": [
    "HEAPLIST32", 
    "HEAPLIST32[]"
  ], 
  "SID_AND_ATTRIBUTES": [
    "SID_AND_ATTRIBUTES"
  ], 
  "PPDH_FMT_COUNTERVALUE_ITEM*": [
    "PointerByReference"
  ], 
  "HRAWINPUT": [
    "HANDLE"
  ], 
  "PLONGLONG": [
    "DoubleByReference", 
    "double[]"
  ], 
  "RID_DEVICE_INFO_MOUSE*": [
    "RID_DEVICE_INFO_MOUSE", 
    "RID_DEVICE_INFO_MOUSE[]"
  ], 
  "LPCEFS_HASH_BLOB": [
    "EFS_HASH_BLOB", 
    "EFS_HASH_BLOB[]"
  ], 
  "LPOUTLINETEXTMETRIC*": [
    "PointerByReference"
  ], 
  "PMAT2*": [
    "PointerByReference"
  ], 
  "SERVER_INFO_101*": [
    "SERVER_INFO_101", 
    "SERVER_INFO_101[]"
  ], 
  "CLIENTCREATESTRUCT": [
    "CLIENTCREATESTRUCT"
  ], 
  "LPCGROUP_INFO_1002": [
    "GROUP_INFO_1002", 
    "GROUP_INFO_1002[]"
  ], 
  "PWER_REPORT_INFORMATION*": [
    "PointerByReference"
  ], 
  "MSG_INFO_0**": [
    "PointerByReference"
  ], 
  "RAWINPUTDEVICELIST**": [
    "PointerByReference"
  ], 
  "LPCPROCESS_MEMORY_COUNTERS_EX": [
    "PROCESS_MEMORY_COUNTERS_EX", 
    "PROCESS_MEMORY_COUNTERS_EX[]"
  ], 
  "LPCGROUP_INFO_1005": [
    "GROUP_INFO_1005", 
    "GROUP_INFO_1005[]"
  ], 
  "LPCCONSOLE_SCREEN_BUFFER_INFOEX": [
    "CONSOLE_SCREEN_BUFFER_INFOEX", 
    "CONSOLE_SCREEN_BUFFER_INFOEX[]"
  ], 
  "PSECURITY_CONTEXT_TRACKING_MODE": [
    "byte"
  ], 
  "USER_MODALS_INFO_0**": [
    "PointerByReference"
  ], 
  "METARECORD*": [
    "METARECORD", 
    "METARECORD[]"
  ], 
  "PPROCESS_MEMORY_COUNTERS*": [
    "PointerByReference"
  ], 
  "USER_INFO_1012*": [
    "USER_INFO_1012", 
    "USER_INFO_1012[]"
  ], 
  "LPFILE_SEGMENT_ELEMENT": [
    "FILE_SEGMENT_ELEMENT", 
    "FILE_SEGMENT_ELEMENT[]"
  ], 
  "PLOCALGROUP_USERS_INFO_0": [
    "LOCALGROUP_USERS_INFO_0", 
    "LOCALGROUP_USERS_INFO_0[]"
  ], 
  "wchar_t": [
    "char"
  ], 
  "LPPDH_FMT_COUNTERVALUE": [
    "PDH_FMT_COUNTERVALUE", 
    "PDH_FMT_COUNTERVALUE[]"
  ], 
  "PGROUP_AFFINITY": [
    "GROUP_AFFINITY", 
    "GROUP_AFFINITY[]"
  ], 
  "LPEVENT_TRACE": [
    "EVENT_TRACE", 
    "EVENT_TRACE[]"
  ], 
  "LONG_PTR**": [
    "PointerByReference"
  ], 
  "LPWER_FILE_TYPE*": [
    "PointerByReference"
  ], 
  "LPCDFS_INFO_150": [
    "DFS_INFO_150", 
    "DFS_INFO_150[]"
  ], 
  "LPCURSORINFO": [
    "CURSORINFO", 
    "CURSORINFO[]"
  ], 
  "MENUBARINFO": [
    "MENUBARINFO"
  ], 
  "PENUMTEXTMETRIC*": [
    "PointerByReference"
  ], 
  "LPNEWTEXTMETRICEX": [
    "NEWTEXTMETRICEX", 
    "NEWTEXTMETRICEX[]"
  ], 
  "LPCSESSION_INFO_2": [
    "SESSION_INFO_2", 
    "SESSION_INFO_2[]"
  ], 
  "LPCMEMORYSTATUSEX": [
    "MEMORYSTATUSEX", 
    "MEMORYSTATUSEX[]"
  ], 
  "PCONSOLE_READCONSOLE_CONTROL": [
    "CONSOLE_READCONSOLE_CONTROL", 
    "CONSOLE_READCONSOLE_CONTROL[]"
  ], 
  "PDFS_INFO_3*": [
    "PointerByReference"
  ], 
  "PCLIENTCREATESTRUCT": [
    "CLIENTCREATESTRUCT", 
    "CLIENTCREATESTRUCT[]"
  ], 
  "ACCEL*": [
    "ACCEL", 
    "ACCEL[]"
  ], 
  "LPCCOMSTAT": [
    "COMSTAT", 
    "COMSTAT[]"
  ], 
  "LPETW_BUFFER_CONTEXT*": [
    "PointerByReference"
  ], 
  "LPSYSTEM_POWER_STATE*": [
    "PointerByReference"
  ], 
  "PCLIPFORMAT": [
    "IntByReference", 
    "int[]"
  ], 
  "LPRID_DEVICE_INFO_MOUSE*": [
    "PointerByReference"
  ], 
  "LPPRINTER_INFO_7*": [
    "PointerByReference"
  ], 
  "MENUEX_TEMPLATE_ITEM**": [
    "PointerByReference"
  ], 
  "PENUMLOGFONTEX*": [
    "PointerByReference"
  ], 
  "PFILE_INFO_3*": [
    "PointerByReference"
  ], 
  "PEVENTMSG": [
    "EVENTMSG", 
    "EVENTMSG[]"
  ], 
  "LPINT16*": [
    "PointerByReference"
  ], 
  "LPCSTR": [
    "ByteByReference", 
    "char[]", 
    "String"
  ], 
  "LPDFS_INFO_1*": [
    "PointerByReference"
  ], 
  "PUSER_INFO_1003": [
    "USER_INFO_1003", 
    "USER_INFO_1003[]"
  ], 
  "WIN32_FIND_DATA**": [
    "PointerByReference"
  ], 
  "PDH_COUNTER_PATH_ELEMENTS": [
    "PDH_COUNTER_PATH_ELEMENTS"
  ], 
  "DFS_INFO_7*": [
    "DFS_INFO_7", 
    "DFS_INFO_7[]"
  ], 
  "PDFS_INFO_100*": [
    "PointerByReference"
  ], 
  "LPGROUP_INFO_2": [
    "GROUP_INFO_2", 
    "GROUP_INFO_2[]"
  ], 
  "LPGROUP_INFO_1": [
    "GROUP_INFO_1", 
    "GROUP_INFO_1[]"
  ], 
  "LPGROUP_INFO_0": [
    "GROUP_INFO_0", 
    "GROUP_INFO_0[]"
  ], 
  "JOBOBJECT_BASIC_LIMIT_INFORMATION*": [
    "JOBOBJECT_BASIC_LIMIT_INFORMATION", 
    "JOBOBJECT_BASIC_LIMIT_INFORMATION[]"
  ], 
  "PGEOTYPE***": [
    "PointerByReference"
  ], 
  "PUSER_INFO_1009": [
    "USER_INFO_1009", 
    "USER_INFO_1009[]"
  ], 
  "PUSER_INFO_1008": [
    "USER_INFO_1008", 
    "USER_INFO_1008[]"
  ], 
  "LPDFS_INFO_3*": [
    "PointerByReference"
  ], 
  "LPPRINTER_DEFAULTS*": [
    "PointerByReference"
  ], 
  "WAITCHAIN_NODE_INFO": [
    "WAITCHAIN_NODE_INFO"
  ], 
  "JOBOBJECT_BASIC_ACCOUNTING_INFORMATION": [
    "JOBOBJECT_BASIC_ACCOUNTING_INFORMATION"
  ], 
  "LPTRIVERTEX": [
    "TRIVERTEX", 
    "TRIVERTEX[]"
  ], 
  "JOBOBJECT_EXTENDED_LIMIT_INFORMATION": [
    "JOBOBJECT_EXTENDED_LIMIT_INFORMATION"
  ], 
  "PRINTER_INFO_4**": [
    "PointerByReference"
  ], 
  "PDFS_INFO_101*": [
    "PointerByReference"
  ], 
  "PNEWTEXTMETRIC": [
    "NEWTEXTMETRIC", 
    "NEWTEXTMETRIC[]"
  ], 
  "PPANOSE*": [
    "PointerByReference"
  ], 
  "LPWIN32_FIND_DATA": [
    "WIN32_FIND_DATA", 
    "WIN32_FIND_DATA[]"
  ], 
  "OVERLAPPED_ENTRY**": [
    "PointerByReference"
  ], 
  "PEXTENDED_NAME_FORMAT*": [
    "PointerByReference"
  ], 
  "LPCRAWHID": [
    "RAWHID", 
    "RAWHID[]"
  ], 
  "LPDFS_INFO_300*": [
    "PointerByReference"
  ], 
  "UMS_CREATE_THREAD_ATTRIBUTES*": [
    "UMS_CREATE_THREAD_ATTRIBUTES", 
    "UMS_CREATE_THREAD_ATTRIBUTES[]"
  ], 
  "LPCONNECTION_INFO_0*": [
    "PointerByReference"
  ], 
  "PMEMORYSTATUS*": [
    "PointerByReference"
  ], 
  "LPSHELLEXECUTEINFO*": [
    "PointerByReference"
  ], 
  "WER_REGISTER_FILE_TYPE**": [
    "PointerByReference"
  ], 
  "PLPGEOTYPE**": [
    "PointerByReference"
  ], 
  "LPJOBOBJECT_NOTIFICATION_LIMIT_INFORMATION*": [
    "PointerByReference"
  ], 
  "PBYTE": [
    "byte", 
    "byte[]"
  ], 
  "LPCPROCESSOR_POWER_POLICY_INFO": [
    "PROCESSOR_POWER_POLICY_INFO", 
    "PROCESSOR_POWER_POLICY_INFO[]"
  ], 
  "INT**": [
    "PointerByReference"
  ], 
  "LPENUMLOGFONTEX*": [
    "PointerByReference"
  ], 
  "HWND*": [
    "HANDLEByReference"
  ], 
  "PLOGBRUSH*": [
    "PointerByReference"
  ], 
  "LPBITMAPINFO*": [
    "PointerByReference"
  ], 
  "PINT": [
    "IntByReference", 
    "int[]"
  ], 
  "PCURSORINFO*": [
    "PointerByReference"
  ], 
  "PINPUT_RECORD": [
    "INPUT_RECORD", 
    "INPUT_RECORD[]"
  ], 
  "TRACKMOUSEEVENT": [
    "TRACKMOUSEEVENT"
  ], 
  "PSERVICE_PREFERRED_NODE_INFO*": [
    "PointerByReference"
  ], 
  "LPJOBOBJECT_LIMIT_VIOLATION_INFORMATION": [
    "JOBOBJECT_LIMIT_VIOLATION_INFORMATION", 
    "JOBOBJECT_LIMIT_VIOLATION_INFORMATION[]"
  ], 
  "LPSHSTOCKICONINFO*": [
    "PointerByReference"
  ], 
  "PEVENTLOGRECORD": [
    "EVENTLOGRECORD", 
    "EVENTLOGRECORD[]"
  ], 
  "LPGLYPHMETRICS": [
    "GLYPHMETRICS", 
    "GLYPHMETRICS[]"
  ], 
  "PCOMMPROP": [
    "COMMPROP", 
    "COMMPROP[]"
  ], 
  "USER_INFO_24**": [
    "PointerByReference"
  ], 
  "HCRYPTPROV*": [
    "ULONG_PTRByReference"
  ], 
  "MONITOR_INFO_2*": [
    "MONITOR_INFO_2", 
    "MONITOR_INFO_2[]"
  ], 
  "PCONSOLE_HISTORY_INFO": [
    "CONSOLE_HISTORY_INFO", 
    "CONSOLE_HISTORY_INFO[]"
  ], 
  "PDFS_INFO_100": [
    "DFS_INFO_100", 
    "DFS_INFO_100[]"
  ], 
  "LPMEMORYSTATUSEX": [
    "MEMORYSTATUSEX", 
    "MEMORYSTATUSEX[]"
  ], 
  "CONNECTDLGSTRUCT*": [
    "CONNECTDLGSTRUCT", 
    "CONNECTDLGSTRUCT[]"
  ], 
  "LPGENERIC_MAPPING*": [
    "PointerByReference"
  ], 
  "LPFLOAT": [
    "FloatByReference", 
    "float[]"
  ], 
  "HGLOBAL": [
    "HANDLE", 
    "Pointer"
  ], 
  "PEVENT_INSTANCE_INFO": [
    "EVENT_INSTANCE_INFO", 
    "EVENT_INSTANCE_INFO[]"
  ], 
  "LPCRID_DEVICE_INFO_KEYBOARD": [
    "RID_DEVICE_INFO_KEYBOARD", 
    "RID_DEVICE_INFO_KEYBOARD[]"
  ], 
  "LPUSER_INFO_1051*": [
    "PointerByReference"
  ], 
  "LPCUSER_INFO_0": [
    "USER_INFO_0", 
    "USER_INFO_0[]"
  ], 
  "PUSER_INFO_1*": [
    "PointerByReference"
  ], 
  "DCB": [
    "DCB"
  ], 
  "TIMERPROC": [
    "Callback"
  ], 
  "REFKNOWNFOLDERID": [
    "Guid.GUID"
  ], 
  "LPDLGTEMPLATE": [
    "DLGTEMPLATE", 
    "DLGTEMPLATE[]"
  ], 
  "HREPORT": [
    "HANDLE"
  ], 
  "PWCT_OBJECT_STATUS": [
    "IntByReference", 
    "int[]"
  ], 
  "WNDENUMPROC": [
    "WinUser.WNDENUMPROC"
  ], 
  "LONG32": [
    "int"
  ], 
  "AXESLIST**": [
    "PointerByReference"
  ], 
  "AUDIODESCRIPTION**": [
    "PointerByReference"
  ], 
  "WKSTA_INFO_101*": [
    "WKSTA_INFO_101", 
    "WKSTA_INFO_101[]"
  ], 
  "LGRPID": [
    "int"
  ], 
  "USER_INFO_1008": [
    "USER_INFO_1008"
  ], 
  "PLOCALGROUP_MEMBERS_INFO_1*": [
    "PointerByReference"
  ], 
  "COMBOBOXINFO*": [
    "COMBOBOXINFO", 
    "COMBOBOXINFO[]"
  ], 
  "USER_INFO_1009": [
    "USER_INFO_1009"
  ], 
  "LPSERVICE_TRIGGER*": [
    "PointerByReference"
  ], 
  "LPCFILE_SEGMENT_ELEMENT": [
    "FILE_SEGMENT_ELEMENT", 
    "FILE_SEGMENT_ELEMENT[]"
  ], 
  "PSECURITY_ATTRIBUTES": [
    "SECURITY_ATTRIBUTES", 
    "SECURITY_ATTRIBUTES[]"
  ], 
  "MACHINE_PROCESSOR_POWER_POLICY": [
    "MACHINE_PROCESSOR_POWER_POLICY"
  ], 
  "LPPERFORMANCE_INFORMATION": [
    "PERFORMANCE_INFORMATION", 
    "PERFORMANCE_INFORMATION[]"
  ], 
  "BITMAPINFO": [
    "BITMAPINFO"
  ], 
  "LPENUM_SERVICE_STATUS": [
    "ENUM_SERVICE_STATUS", 
    "ENUM_SERVICE_STATUS[]"
  ], 
  "LPCWIN32_FIND_DATA": [
    "WIN32_FIND_DATA", 
    "WIN32_FIND_DATA[]"
  ], 
  "USER_MODALS_INFO_1006**": [
    "PointerByReference"
  ], 
  "LPVIDEOPARAMETERS*": [
    "PointerByReference"
  ], 
  "LPHICON": [
    "HANDLEByReference"
  ], 
  "COLORADJUSTMENT**": [
    "PointerByReference"
  ], 
  "LPCNONCLIENTMETRICS": [
    "NONCLIENTMETRICS", 
    "NONCLIENTMETRICS[]"
  ], 
  "DFS_INFO_50": [
    "DFS_INFO_50"
  ], 
  "PGETPROPERTYSTOREFLAGS*": [
    "PointerByReference"
  ], 
  "StringTable*": [
    "StringTable", 
    "StringTable[]"
  ], 
  "PMENUEX_TEMPLATE_ITEM": [
    "MENUEX_TEMPLATE_ITEM", 
    "MENUEX_TEMPLATE_ITEM[]"
  ], 
  "LPPRINTER_NOTIFY_INFO_DATA": [
    "PRINTER_NOTIFY_INFO_DATA", 
    "PRINTER_NOTIFY_INFO_DATA[]"
  ], 
  "LPEVENTMSG": [
    "EVENTMSG", 
    "EVENTMSG[]"
  ], 
  "LPEVENT_INSTANCE_INFO": [
    "EVENT_INSTANCE_INFO", 
    "EVENT_INSTANCE_INFO[]"
  ], 
  "PJOBOBJECT_BASIC_PROCESS_ID_LIST*": [
    "PointerByReference"
  ], 
  "LPKBDLLHOOKSTRUCT": [
    "KBDLLHOOKSTRUCT", 
    "KBDLLHOOKSTRUCT[]"
  ], 
  "EVENT_INSTANCE_HEADER": [
    "EVENT_INSTANCE_HEADER"
  ], 
  "ADDJOB_INFO_1": [
    "ADDJOB_INFO_1"
  ], 
  "PSC_ACTION": [
    "SC_ACTION", 
    "SC_ACTION[]"
  ], 
  "LPCWSAPROTOCOL_INFO": [
    "WSAPROTOCOL_INFO", 
    "WSAPROTOCOL_INFO[]"
  ], 
  "LPLCTYPE": [
    "IntByReference", 
    "long[]"
  ], 
  "PStringTable*": [
    "PointerByReference"
  ], 
  "DFS_INFO_8**": [
    "PointerByReference"
  ], 
  "LPDRIVER_INFO_6*": [
    "PointerByReference"
  ], 
  "MEMORYSTATUS**": [
    "PointerByReference"
  ], 
  "LPSHARE_INFO_1006*": [
    "PointerByReference"
  ], 
  "LPACL*": [
    "PointerByReference"
  ], 
  "JOBOBJECT_SECURITY_LIMIT_INFORMATION": [
    "JOBOBJECT_SECURITY_LIMIT_INFORMATION"
  ], 
  "WKSTA_INFO_101": [
    "WKSTA_INFO_101"
  ], 
  "PSYSTEM_POWER_LEVEL": [
    "SYSTEM_POWER_LEVEL", 
    "SYSTEM_POWER_LEVEL[]"
  ], 
  "WKSTA_INFO_102": [
    "WKSTA_INFO_102"
  ], 
  "GROUP_INFO_1002**": [
    "PointerByReference"
  ], 
  "CONSOLE_CURSOR_INFO": [
    "CONSOLE_CURSOR_INFO"
  ], 
  "MONITORINFO": [
    "MONITORINFO"
  ], 
  "PINT16*": [
    "PointerByReference"
  ], 
  "MONITOR_INFO_2**": [
    "PointerByReference"
  ], 
  "EFS_HASH_BLOB": [
    "EFS_HASH_BLOB"
  ], 
  "INPUT**": [
    "PointerByReference"
  ], 
  "EVENT_INSTANCE_INFO**": [
    "PointerByReference"
  ], 
  "LPTAPE_GET_DRIVE_PARAMETERS": [
    "TAPE_GET_DRIVE_PARAMETERS", 
    "TAPE_GET_DRIVE_PARAMETERS[]"
  ], 
  "CPINFO**": [
    "PointerByReference"
  ], 
  "LPMETARECORD*": [
    "PointerByReference"
  ], 
  "COLOR16**": [
    "PointerByReference"
  ], 
  "PPOINTL": [
    "POINTL", 
    "POINTL[]"
  ], 
  "PSYSTEM_LOGICAL_PROCESSOR_INFORMATION*": [
    "PointerByReference"
  ], 
  "PSERVICE_TRIGGER": [
    "SERVICE_TRIGGER", 
    "SERVICE_TRIGGER[]"
  ], 
  "PROCESSOR_NUMBER": [
    "PROCESSOR_NUMBER"
  ], 
  "LPULONG*": [
    "PointerByReference"
  ], 
  "PHBRUSH": [
    "HANDLEByReference"
  ], 
  "GCP_RESULTS": [
    "GCP_RESULTS"
  ], 
  "PSERVER_INFO_402*": [
    "PointerByReference"
  ], 
  "LPCONVINFO*": [
    "PointerByReference"
  ], 
  "PJOBOBJECT_BASIC_ACCOUNTING_INFORMATION": [
    "JOBOBJECT_BASIC_ACCOUNTING_INFORMATION", 
    "JOBOBJECT_BASIC_ACCOUNTING_INFORMATION[]"
  ], 
  "LPDEBUGHOOKINFO": [
    "DEBUGHOOKINFO", 
    "DEBUGHOOKINFO[]"
  ], 
  "PSHARE_INFO_1005*": [
    "PointerByReference"
  ], 
  "CONSOLE_FONT_INFO": [
    "CONSOLE_FONT_INFO"
  ], 
  "LPPRINTER_INFO_3": [
    "PRINTER_INFO_3", 
    "PRINTER_INFO_3[]"
  ], 
  "ETW_BUFFER_CONTEXT**": [
    "PointerByReference"
  ], 
  "LPUSER_INFO_1024": [
    "USER_INFO_1024", 
    "USER_INFO_1024[]"
  ], 
  "LPGRADIENT_TRIANGLE": [
    "GRADIENT_TRIANGLE", 
    "GRADIENT_TRIANGLE[]"
  ], 
  "GUITHREADINFO": [
    "GUITHREADINFO"
  ], 
  "TRIVERTEX*": [
    "TRIVERTEX", 
    "TRIVERTEX[]"
  ], 
  "PRAWINPUTDEVICE*": [
    "PointerByReference"
  ], 
  "LPUSER_INFO_1020*": [
    "PointerByReference"
  ], 
  "PSERVICE_TIMECHANGE_INFO": [
    "SERVICE_TIMECHANGE_INFO", 
    "SERVICE_TIMECHANGE_INFO[]"
  ], 
  "ULARGE_INTEGER": [
    "ULARGE_INTEGER"
  ], 
  "LPCPDH_STATISTICS": [
    "PDH_STATISTICS", 
    "PDH_STATISTICS[]"
  ], 
  "LPCONSOLE_SCREEN_BUFFER_INFO": [
    "CONSOLE_SCREEN_BUFFER_INFO", 
    "CONSOLE_SCREEN_BUFFER_INFO[]"
  ], 
  "MEMORYSTATUS*": [
    "MEMORYSTATUS", 
    "MEMORYSTATUS[]"
  ], 
  "MSG_INFO_1*": [
    "MSG_INFO_1", 
    "MSG_INFO_1[]"
  ], 
  "LPGEOCLASS***": [
    "PointerByReference"
  ], 
  "LPCSTR*": [
    "String[]", 
    "PointerByReference"
  ], 
  "LPSERVICE_STATUS": [
    "SERVICE_STATUS", 
    "SERVICE_STATUS[]"
  ], 
  "PDH_RAW_COUNTER**": [
    "PointerByReference"
  ], 
  "INPUT": [
    "INPUT"
  ], 
  "PUSER_INFO_1020*": [
    "PointerByReference"
  ], 
  "LPSECURITY_CONTEXT_TRACKING_MODE*": [
    "byte"
  ], 
  "ENUMLOGFONT*": [
    "ENUMLOGFONT", 
    "ENUMLOGFONT[]"
  ], 
  "LANGID**": [
    "PointerByReference"
  ], 
  "LPSHORT*": [
    "PointerByReference"
  ], 
  "DESIGNVECTOR**": [
    "PointerByReference"
  ], 
  "DFS_STORAGE_INFO_1": [
    "DFS_STORAGE_INFO_1"
  ], 
  "SHFILEOPSTRUCT*": [
    "SHFILEOPSTRUCT", 
    "SHFILEOPSTRUCT[]"
  ], 
  "LPHREPORT": [
    "HANDLEByReference"
  ], 
  "PUMS_CREATE_THREAD_ATTRIBUTES": [
    "UMS_CREATE_THREAD_ATTRIBUTES", 
    "UMS_CREATE_THREAD_ATTRIBUTES[]"
  ], 
  "PPGEOCLASS*": [
    "IntByReference", 
    "long[]"
  ], 
  "USER_INFO_20*": [
    "USER_INFO_20", 
    "USER_INFO_20[]"
  ], 
  "RAWINPUTDEVICE*": [
    "RAWINPUTDEVICE", 
    "RAWINPUTDEVICE[]"
  ], 
  "LPCVALENT": [
    "VALENT", 
    "VALENT[]"
  ], 
  "PTASKDIALOG_FLAGS": [
    "IntByReference", 
    "int[]"
  ], 
  "SYSTEM_BATTERY_STATE**": [
    "PointerByReference"
  ], 
  "LPCBY_HANDLE_FILE_INFORMATION": [
    "BY_HANDLE_FILE_INFORMATION", 
    "BY_HANDLE_FILE_INFORMATION[]"
  ], 
  "PNETRESOURCE": [
    "NETRESOURCE", 
    "NETRESOURCE[]"
  ], 
  "LPCPDH_DATA_ITEM_PATH_ELEMENTS": [
    "PDH_DATA_ITEM_PATH_ELEMENTS", 
    "PDH_DATA_ITEM_PATH_ELEMENTS[]"
  ], 
  "PPIXELFORMATDESCRIPTOR*": [
    "PointerByReference"
  ], 
  "PTRACE_GUID_REGISTRATION*": [
    "PointerByReference"
  ], 
  "SHSTOCKICONINFO**": [
    "PointerByReference"
  ], 
  "SHARE_INFO_1005*": [
    "SHARE_INFO_1005", 
    "SHARE_INFO_1005[]"
  ], 
  "DOC_INFO_1*": [
    "DOC_INFO_1", 
    "DOC_INFO_1[]"
  ], 
  "LPCWNODE_HEADER": [
    "WNODE_HEADER", 
    "WNODE_HEADER[]"
  ], 
  "LPTEXTMETRIC": [
    "TEXTMETRIC", 
    "TEXTMETRIC[]"
  ], 
  "LPBOOL*": [
    "PointerByReference"
  ], 
  "PMODULEINFO": [
    "MODULEINFO", 
    "MODULEINFO[]"
  ], 
  "LPENUMLOGFONTEXDV*": [
    "PointerByReference"
  ], 
  "ENUMTEXTMETRIC": [
    "ENUMTEXTMETRIC"
  ], 
  "SHARE_INFO_503**": [
    "PointerByReference"
  ], 
  "VOID*": [
    "Pointer", 
    "LPVOID", 
    "PointerByReference"
  ], 
  "FORM_INFO_1*": [
    "FORM_INFO_1", 
    "FORM_INFO_1[]"
  ], 
  "PMODULEENTRY32": [
    "MODULEENTRY32", 
    "MODULEENTRY32[]"
  ], 
  "ACL_INFORMATION_CLASS": [
    "int"
  ], 
  "LPOVERLAPPED_ENTRY": [
    "OVERLAPPED_ENTRY", 
    "OVERLAPPED_ENTRY[]"
  ], 
  "LPCJOBOBJECT_BASIC_UI_RESTRICTIONS": [
    "JOBOBJECT_BASIC_UI_RESTRICTIONS", 
    "JOBOBJECT_BASIC_UI_RESTRICTIONS[]"
  ], 
  "LPCRID_DEVICE_INFO": [
    "RID_DEVICE_INFO", 
    "RID_DEVICE_INFO[]"
  ], 
  "DFS_INFO_105**": [
    "PointerByReference"
  ], 
  "MSG_INFO_1": [
    "MSG_INFO_1"
  ], 
  "MSG_INFO_0": [
    "MSG_INFO_0"
  ], 
  "LPPORT_INFO_2": [
    "PORT_INFO_2", 
    "PORT_INFO_2[]"
  ], 
  "LPPORT_INFO_3": [
    "PORT_INFO_3", 
    "PORT_INFO_3[]"
  ], 
  "USN*": [
    "DoubleByReference", 
    "double[]"
  ], 
  "LPLGRPID*": [
    "PointerByReference"
  ], 
  "BSMINFO*": [
    "BSMINFO", 
    "BSMINFO[]"
  ], 
  "HCRYPTKEY*": [
    "ULONG_PTRByReference"
  ], 
  "PCONSOLE_CURSOR_INFO": [
    "CONSOLE_CURSOR_INFO", 
    "CONSOLE_CURSOR_INFO[]"
  ], 
  "LPCHAR_INFO": [
    "CHAR_INFO", 
    "CHAR_INFO[]"
  ], 
  "LPCWKSTA_USER_INFO_1101": [
    "WKSTA_USER_INFO_1101", 
    "WKSTA_USER_INFO_1101[]"
  ], 
  "PMENUITEMTEMPLATEHEADER*": [
    "PointerByReference"
  ], 
  "KERNINGPAIR": [
    "KERNINGPAIR"
  ], 
  "LPMENUITEMINFO": [
    "MENUITEMINFO", 
    "MENUITEMINFO[]"
  ], 
  "JOBOBJECT_BASIC_LIMIT_INFORMATION**": [
    "PointerByReference"
  ], 
  "LPCPROCESSOR_NUMBER": [
    "PROCESSOR_NUMBER", 
    "PROCESSOR_NUMBER[]"
  ], 
  "PSECURITY_ATTRIBUTES*": [
    "PointerByReference"
  ], 
  "LPHPEN": [
    "HANDLEByReference"
  ], 
  "LPSHARE_INFO_1501*": [
    "PointerByReference"
  ], 
  "EFS_CERTIFICATE_BLOB": [
    "EFS_CERTIFICATE_BLOB"
  ], 
  "PRAWINPUTHEADER": [
    "RAWINPUTHEADER", 
    "RAWINPUTHEADER[]"
  ], 
  "LPDFS_INFO_1": [
    "DFS_INFO_1", 
    "DFS_INFO_1[]"
  ], 
  "LPDFS_INFO_3": [
    "DFS_INFO_3", 
    "DFS_INFO_3[]"
  ], 
  "POINTL*": [
    "POINTL", 
    "POINTL[]"
  ], 
  "JOBOBJECT_END_OF_JOB_TIME_INFORMATION": [
    "JOBOBJECT_END_OF_JOB_TIME_INFORMATION"
  ], 
  "LPDFS_INFO_4": [
    "DFS_INFO_4", 
    "DFS_INFO_4[]"
  ], 
  "LPDFS_INFO_7": [
    "DFS_INFO_7", 
    "DFS_INFO_7[]"
  ], 
  "LPDFS_INFO_6": [
    "DFS_INFO_6", 
    "DFS_INFO_6[]"
  ], 
  "LPDFS_INFO_9": [
    "DFS_INFO_9", 
    "DFS_INFO_9[]"
  ], 
  "LPDFS_INFO_8": [
    "DFS_INFO_8", 
    "DFS_INFO_8[]"
  ], 
  "BYTE": [
    "byte", 
    "BYTE"
  ], 
  "EVENT_TRACE_HEADER*": [
    "EVENT_TRACE_HEADER", 
    "EVENT_TRACE_HEADER[]"
  ], 
  "CCHAR**": [
    "byte"
  ], 
  "PRINTER_INFO_4*": [
    "PRINTER_INFO_4", 
    "PRINTER_INFO_4[]"
  ], 
  "JOBOBJECT_BASIC_AND_IO_ACCOUNTING_INFORMATION**": [
    "PointerByReference"
  ], 
  "LPHMODULE": [
    "HANDLEByReference"
  ], 
  "MENUITEMTEMPLATE*": [
    "MENUITEMTEMPLATE", 
    "MENUITEMTEMPLATE[]"
  ], 
  "KEY_EVENT_RECORD**": [
    "PointerByReference"
  ], 
  "PWER_DUMP_CUSTOM_OPTIONS*": [
    "PointerByReference"
  ], 
  "USER_POWER_POLICY**": [
    "PointerByReference"
  ], 
  "PRINTER_DEFAULTS*": [
    "PRINTER_DEFAULTS", 
    "PRINTER_DEFAULTS[]"
  ], 
  "NDDESHAREINFO*": [
    "NDDESHAREINFO", 
    "NDDESHAREINFO[]"
  ], 
  "PVALENT": [
    "VALENT", 
    "VALENT[]"
  ], 
  "SYSTEM_POWER_INFORMATION": [
    "SYSTEM_POWER_INFORMATION"
  ], 
  "PINPUT*": [
    "PointerByReference"
  ], 
  "USER_INFO_20": [
    "USER_INFO_20"
  ], 
  "LPWCHAR*": [
    "char"
  ], 
  "LPCSESSION_INFO_10": [
    "SESSION_INFO_10", 
    "SESSION_INFO_10[]"
  ], 
  "LPRM_PROCESS_INFO*": [
    "PointerByReference"
  ], 
  "PDFS_TARGET_PRIORITY*": [
    "PointerByReference"
  ], 
  "MENUBARINFO*": [
    "MENUBARINFO", 
    "MENUBARINFO[]"
  ], 
  "PMOUSE_EVENT_RECORD": [
    "MOUSE_EVENT_RECORD", 
    "MOUSE_EVENT_RECORD[]"
  ], 
  "LCTYPE": [
    "int", 
    "DWORD"
  ], 
  "LPASSOC_FILTER*": [
    "PointerByReference"
  ], 
  "LPTOKEN_GROUPS": [
    "TOKEN_GROUPS", 
    "TOKEN_GROUPS[]"
  ], 
  "PSIZE_T": [
    "IntByReference", 
    "long[]"
  ], 
  "SERVER_INFO_102*": [
    "SERVER_INFO_102", 
    "SERVER_INFO_102[]"
  ], 
  "LPPDH_RAW_COUNTER*": [
    "PointerByReference"
  ], 
  "LPCRGBQUAD": [
    "RGBQUAD", 
    "RGBQUAD[]"
  ], 
  "LPCMONITOR_INFO_2": [
    "MONITOR_INFO_2", 
    "MONITOR_INFO_2[]"
  ], 
  "JOBOBJECTINFOCLASS*": [
    "IntByReference", 
    "int[]"
  ], 
  "LPCMONITOR_INFO_1": [
    "MONITOR_INFO_1", 
    "MONITOR_INFO_1[]"
  ], 
  "GROUP_INFO_2**": [
    "PointerByReference"
  ], 
  "LPLPGEOTYPE**": [
    "PointerByReference"
  ], 
  "GUID": [
    "GUID"
  ], 
  "LPCGROUP_INFO_1": [
    "GROUP_INFO_1", 
    "GROUP_INFO_1[]"
  ], 
  "LPCGROUP_INFO_0": [
    "GROUP_INFO_0", 
    "GROUP_INFO_0[]"
  ], 
  "LPCGROUP_INFO_3": [
    "GROUP_INFO_3", 
    "GROUP_INFO_3[]"
  ], 
  "LPCGROUP_INFO_2": [
    "GROUP_INFO_2", 
    "GROUP_INFO_2[]"
  ], 
  "SHARE_INFO_1501*": [
    "SHARE_INFO_1501", 
    "SHARE_INFO_1501[]"
  ], 
  "DFS_TARGET_PRIORITY": [
    "DFS_TARGET_PRIORITY"
  ], 
  "LPNTMS_NOTIFICATIONINFORMATION*": [
    "PointerByReference"
  ], 
  "LPHW_PROFILE_INFO*": [
    "PointerByReference"
  ], 
  "LPCPINFO": [
    "CPINFO", 
    "CPINFO[]"
  ], 
  "STREAM_INFO_LEVELS*": [
    "IntByReference", 
    "int[]"
  ], 
  "ENHMETARECORD*": [
    "ENHMETARECORD", 
    "ENHMETARECORD[]"
  ], 
  "PLRESULT*": [
    "PointerByReference"
  ], 
  "DWORD32**": [
    "PointerByReference"
  ], 
  "MEMORY_BASIC_INFORMATION": [
    "MEMORY_BASIC_INFORMATION"
  ], 
  "RAWKEYBOARD*": [
    "RAWKEYBOARD", 
    "RAWKEYBOARD[]"
  ], 
  "PPOLYTEXT": [
    "POLYTEXT", 
    "POLYTEXT[]"
  ], 
  "LPCUSE_INFO_0": [
    "USE_INFO_0", 
    "USE_INFO_0[]"
  ], 
  "LPHWINSTA": [
    "HANDLEByReference"
  ], 
  "GEOID**": [
    "PointerByReference"
  ], 
  "MOUSE_EVENT_RECORD**": [
    "PointerByReference"
  ], 
  "PGUID*": [
    "PointerByReference"
  ], 
  "LPCGLYPHSET": [
    "GLYPHSET", 
    "GLYPHSET[]"
  ], 
  "PTRIVERTEX*": [
    "PointerByReference"
  ], 
  "LPCOMMPROP": [
    "COMMPROP", 
    "COMMPROP[]"
  ], 
  "POWERBROADCAST_SETTING*": [
    "POWERBROADCAST_SETTING", 
    "POWERBROADCAST_SETTING[]"
  ], 
  "LPCNET_DISPLAY_GROUP": [
    "NET_DISPLAY_GROUP", 
    "NET_DISPLAY_GROUP[]"
  ], 
  "LPOFSTRUCT*": [
    "PointerByReference"
  ], 
  "LPCVOID": [
    "Pointer", 
    "byte[]", 
    "LPVOID"
  ], 
  "MOUSEHOOKSTRUCT*": [
    "MOUSEHOOKSTRUCT", 
    "MOUSEHOOKSTRUCT[]"
  ], 
  "LPGEOTYPE*": [
    "IntByReference", 
    "long[]"
  ], 
  "SYSTEM_INFO*": [
    "SYSTEM_INFO", 
    "SYSTEM_INFO[]"
  ], 
  "PSTAT_SERVER_0": [
    "STAT_SERVER_0", 
    "STAT_SERVER_0[]"
  ], 
  "LPQWORD*": [
    "PointerByReference"
  ], 
  "PORT_INFO_2*": [
    "PORT_INFO_2", 
    "PORT_INFO_2[]"
  ], 
  "TAPE_SET_DRIVE_PARAMETERS**": [
    "PointerByReference"
  ], 
  "PDFS_INFO_2*": [
    "PointerByReference"
  ], 
  "SYSGEOTYPE**": [
    "PointerByReference"
  ], 
  "PPORT_INFO_1*": [
    "PointerByReference"
  ], 
  "LPPRINTER_INFO_4*": [
    "PointerByReference"
  ], 
  "LPCGRADIENT_TRIANGLE": [
    "GRADIENT_TRIANGLE", 
    "GRADIENT_TRIANGLE[]"
  ], 
  "ENUMLOGFONT": [
    "ENUMLOGFONT"
  ], 
  "LPPGEOTYPE*": [
    "IntByReference", 
    "long[]"
  ], 
  "DFS_TARGET_PRIORITY_CLASS**": [
    "PointerByReference"
  ], 
  "LPENUM_SERVICE_STATUS*": [
    "PointerByReference"
  ], 
  "PUSER_INFO_1011": [
    "USER_INFO_1011", 
    "USER_INFO_1011[]"
  ], 
  "PUSER_INFO_1012": [
    "USER_INFO_1012", 
    "USER_INFO_1012[]"
  ], 
  "LPSYSGEOCLASS": [
    "IntByReference", 
    "int[]"
  ], 
  "PUSER_INFO_1014": [
    "USER_INFO_1014", 
    "USER_INFO_1014[]"
  ], 
  "PCONVINFO*": [
    "PointerByReference"
  ], 
  "LPCBITMAPINFO": [
    "BITMAPINFO", 
    "BITMAPINFO[]"
  ], 
  "CPINFOEX**": [
    "PointerByReference"
  ], 
  "PUSE_INFO_0*": [
    "PointerByReference"
  ], 
  "LPDFS_INFO_2*": [
    "PointerByReference"
  ], 
  "SC_ACTION_TYPE*": [
    "IntByReference", 
    "int[]"
  ], 
  "LPDYNAMIC_TIME_ZONE_INFORMATION": [
    "DYNAMIC_TIME_ZONE_INFORMATION", 
    "DYNAMIC_TIME_ZONE_INFORMATION[]"
  ], 
  "HREPORT*": [
    "HANDLEByReference"
  ], 
  "USER_INFO_1003": [
    "USER_INFO_1003"
  ], 
  "CREATESTRUCT*": [
    "CREATESTRUCT", 
    "CREATESTRUCT[]"
  ], 
  "LPCGENERIC_MAPPING": [
    "GENERIC_MAPPING", 
    "GENERIC_MAPPING[]"
  ], 
  "USER_INFO_1006": [
    "USER_INFO_1006"
  ], 
  "USER_INFO_1007": [
    "USER_INFO_1007"
  ], 
  "USER_INFO_1005": [
    "USER_INFO_1005"
  ], 
  "LPCPAINTSTRUCT": [
    "PAINTSTRUCT", 
    "PAINTSTRUCT[]"
  ], 
  "PPROCESSOR_NUMBER*": [
    "PointerByReference"
  ], 
  "LPCTPMPARAMS": [
    "TPMPARAMS", 
    "TPMPARAMS[]"
  ], 
  "LPPWSTR": [
    "char"
  ], 
  "PIXELFORMATDESCRIPTOR**": [
    "PointerByReference"
  ], 
  "PCOLOR16*": [
    "PointerByReference"
  ], 
  "PROCESS_MEMORY_COUNTERS**": [
    "PointerByReference"
  ], 
  "ACCESSTIMEOUT*": [
    "ACCESSTIMEOUT", 
    "ACCESSTIMEOUT[]"
  ], 
  "PLPCWSTR": [
    "char"
  ], 
  "PUSER_INFO_23*": [
    "PointerByReference"
  ], 
  "GEOTYPE*": [
    "IntByReference", 
    "long[]"
  ], 
  "LPINT64": [
    "LongByReference", 
    "__int64[]"
  ], 
  "COLORREF*": [
    "IntByReference", 
    "long[]"
  ], 
  "JOBOBJECT_BASIC_PROCESS_ID_LIST*": [
    "JOBOBJECT_BASIC_PROCESS_ID_LIST", 
    "JOBOBJECT_BASIC_PROCESS_ID_LIST[]"
  ], 
  "LPCSTD_ALERT": [
    "STD_ALERT", 
    "STD_ALERT[]"
  ], 
  "TOKEN_GROUPS": [
    "TOKEN_GROUPS"
  ], 
  "LPCONNECTION_INFO_1*": [
    "PointerByReference"
  ], 
  "LPENHMETARECORD": [
    "ENHMETARECORD", 
    "ENHMETARECORD[]"
  ], 
  "LPPANOSE*": [
    "PointerByReference"
  ], 
  "PROPERTYKEY*": [
    "PROPERTYKEY", 
    "PROPERTYKEY[]"
  ], 
  "LPWMIDPREQUESTCODE": [
    "IntByReference", 
    "int[]"
  ], 
  "PCPINFO*": [
    "PointerByReference"
  ], 
  "__int64**": [
    "PointerByReference"
  ], 
  "LPOSVERSIONINFO": [
    "WinNT.OSVERSIONINFOEX"
  ], 
  "LPWCHAR": [
    "char"
  ], 
  "PLOGPALETTE": [
    "LOGPALETTE", 
    "LOGPALETTE[]"
  ], 
  "PUSHORT": [
    "ShortByReference", 
    "short[]"
  ], 
  "SIZE**": [
    "PointerByReference"
  ], 
  "ENUMLOGFONTEX*": [
    "ENUMLOGFONTEX", 
    "ENUMLOGFONTEX[]"
  ], 
  "PNTMS_ALLOCATION_INFORMATION": [
    "NTMS_ALLOCATION_INFORMATION", 
    "NTMS_ALLOCATION_INFORMATION[]"
  ], 
  "PGROUP_INFO_2*": [
    "PointerByReference"
  ], 
  "LPCMODULEENTRY32": [
    "MODULEENTRY32", 
    "MODULEENTRY32[]"
  ], 
  "SHARE_INFO_1*": [
    "SHARE_INFO_1", 
    "SHARE_INFO_1[]"
  ], 
  "WKSTA_INFO_100*": [
    "WKSTA_INFO_100", 
    "WKSTA_INFO_100[]"
  ], 
  "PSYSTEM_POWER_INFORMATION": [
    "SYSTEM_POWER_INFORMATION", 
    "SYSTEM_POWER_INFORMATION[]"
  ], 
  "PCONSOLE_FONT_INFO": [
    "CONSOLE_FONT_INFO", 
    "CONSOLE_FONT_INFO[]"
  ], 
  "LPCGUID": [
    "GUID", 
    "GUID[]"
  ], 
  "PERFORMANCE_INFORMATION**": [
    "PointerByReference"
  ], 
  "LPINT32*": [
    "PointerByReference"
  ], 
  "LPUSER_MODALS_INFO_1002*": [
    "PointerByReference"
  ], 
  "PRINTER_INFO_1*": [
    "PRINTER_INFO_1", 
    "PRINTER_INFO_1[]"
  ], 
  "JOBOBJECT_RATE_CONTROL_TOLERANCE": [
    "int"
  ], 
  "LPSERVER_INFO_402": [
    "SERVER_INFO_402", 
    "SERVER_INFO_402[]"
  ], 
  "SERVICE_TIMECHANGE_INFO**": [
    "PointerByReference"
  ], 
  "PDH_RAW_COUNTER_ITEM": [
    "PDH_RAW_COUNTER_ITEM"
  ], 
  "KERNINGPAIR**": [
    "PointerByReference"
  ], 
  "LPCGETPROPERTYSTOREFLAGS": [
    "IntByReference", 
    "int[]"
  ], 
  "USE_INFO_0": [
    "USE_INFO_0"
  ], 
  "MACHINE_POWER_POLICY": [
    "MACHINE_POWER_POLICY"
  ], 
  "LPStringTable*": [
    "PointerByReference"
  ], 
  "PPROCESSOR_POWER_POLICY_INFO": [
    "PROCESSOR_POWER_POLICY_INFO", 
    "PROCESSOR_POWER_POLICY_INFO[]"
  ], 
  "PLOCALGROUP_MEMBERS_INFO_2*": [
    "PointerByReference"
  ], 
  "LPCUSER_INFO_24": [
    "USER_INFO_24", 
    "USER_INFO_24[]"
  ], 
  "PUSER_INFO_0*": [
    "PointerByReference"
  ], 
  "LPPROCESS_MEMORY_COUNTERS*": [
    "PointerByReference"
  ], 
  "LPUSER_INFO_1005*": [
    "PointerByReference"
  ], 
  "ENUMLOGFONT**": [
    "PointerByReference"
  ], 
  "MENU_EVENT_RECORD": [
    "MENU_EVENT_RECORD"
  ], 
  "LPCLARGE_INTEGER": [
    "LARGE_INTEGER", 
    "LARGE_INTEGER[]"
  ], 
  "LPCTOKEN_DEFAULT_DACL": [
    "TOKEN_DEFAULT_DACL", 
    "TOKEN_DEFAULT_DACL[]"
  ], 
  "LPPCTSTR*": [
    "char"
  ], 
  "LPCUSER_INFO_1017": [
    "USER_INFO_1017", 
    "USER_INFO_1017[]"
  ], 
  "PHW_PROFILE_INFO*": [
    "PointerByReference"
  ], 
  "LPCUSER_INFO_1011": [
    "USER_INFO_1011", 
    "USER_INFO_1011[]"
  ], 
  "LPCUSER_INFO_1010": [
    "USER_INFO_1010", 
    "USER_INFO_1010[]"
  ], 
  "PPDH_RAW_LOG_RECORD*": [
    "PointerByReference"
  ], 
  "XFORM": [
    "XFORM"
  ], 
  "LPSYSTEM_BATTERY_STATE*": [
    "PointerByReference"
  ], 
  "PGLOBAL_USER_POWER_POLICY*": [
    "PointerByReference"
  ], 
  "LPTAPE_SET_MEDIA_PARAMETERS*": [
    "PointerByReference"
  ], 
  "MAT2": [
    "MAT2"
  ], 
  "PDLGTEMPLATE": [
    "DLGTEMPLATE", 
    "DLGTEMPLATE[]"
  ], 
  "PPTSTR*": [
    "char"
  ], 
  "PXFORM": [
    "XFORM", 
    "XFORM[]"
  ], 
  "PUSER_INFO_1014*": [
    "PointerByReference"
  ], 
  "PHPEN": [
    "HANDLEByReference"
  ], 
  "SERVICE_TRIGGER**": [
    "PointerByReference"
  ], 
  "DFS_TARGET_PRIORITY_CLASS": [
    "int"
  ], 
  "PCOMMCONFIG": [
    "COMMCONFIG", 
    "COMMCONFIG[]"
  ], 
  "PROCESSOR_NUMBER**": [
    "PointerByReference"
  ], 
  "ADDJOB_INFO_1*": [
    "ADDJOB_INFO_1", 
    "ADDJOB_INFO_1[]"
  ], 
  "PPOINT": [
    "POINT", 
    "POINT[]"
  ], 
  "LPMONITOR_INFO_1": [
    "MONITOR_INFO_1", 
    "MONITOR_INFO_1[]"
  ], 
  "REMOTE_NAME_INFO*": [
    "REMOTE_NAME_INFO", 
    "REMOTE_NAME_INFO[]"
  ], 
  "LPWKSTA_INFO_502*": [
    "PointerByReference"
  ], 
  "MINIMIZEDMETRICS**": [
    "PointerByReference"
  ], 
  "LPCSIZE": [
    "SIZE", 
    "SIZE[]"
  ], 
  "ASSOC_FILTER": [
    "int"
  ], 
  "LPCONSOLE_SCREEN_BUFFER_INFOEX": [
    "CONSOLE_SCREEN_BUFFER_INFOEX", 
    "CONSOLE_SCREEN_BUFFER_INFOEX[]"
  ], 
  "PPRINTPAGERANGE": [
    "PRINTPAGERANGE", 
    "PRINTPAGERANGE[]"
  ], 
  "DFS_INFO_2**": [
    "PointerByReference"
  ], 
  "PSYSTEM_POWER_INFORMATION*": [
    "PointerByReference"
  ], 
  "WER_REPORT_INFORMATION**": [
    "PointerByReference"
  ], 
  "LPTITLEBARINFO": [
    "TITLEBARINFO", 
    "TITLEBARINFO[]"
  ], 
  "PMENUBARINFO": [
    "MENUBARINFO", 
    "MENUBARINFO[]"
  ], 
  "PDESIGNVECTOR": [
    "DESIGNVECTOR", 
    "DESIGNVECTOR[]"
  ], 
  "PDH_RAW_COUNTER*": [
    "PDH_RAW_COUNTER", 
    "PDH_RAW_COUNTER[]"
  ], 
  "GEOCLASS": [
    "int"
  ], 
  "LPSHARE_INFO_503*": [
    "PointerByReference"
  ], 
  "LPCNEWTEXTMETRIC": [
    "NEWTEXTMETRIC", 
    "NEWTEXTMETRIC[]"
  ], 
  "PMEMORY_BASIC_INFORMATION*": [
    "PointerByReference"
  ], 
  "COMMTIMEOUTS": [
    "COMMTIMEOUTS"
  ], 
  "HINTERNET": [
    "HANDLE"
  ], 
  "LPPOLYTEXT*": [
    "PointerByReference"
  ], 
  "METAFILEPICT": [
    "METAFILEPICT"
  ], 
  "HWINSTA*": [
    "HANDLEByReference"
  ], 
  "USER_MODALS_INFO_2*": [
    "USER_MODALS_INFO_2", 
    "USER_MODALS_INFO_2[]"
  ], 
  "PSERVICE_TIMECHANGE_INFO*": [
    "PointerByReference"
  ], 
  "LPNETRESOURCE*": [
    "PointerByReference"
  ], 
  "USER_INFO_1008**": [
    "PointerByReference"
  ], 
  "TOGGLEKEYS*": [
    "TOGGLEKEYS", 
    "TOGGLEKEYS[]"
  ], 
  "CONSOLE_FONT_INFOEX**": [
    "PointerByReference"
  ], 
  "LPCREMOTE_NAME_INFO": [
    "REMOTE_NAME_INFO", 
    "REMOTE_NAME_INFO[]"
  ], 
  "LPCCOMBOBOXINFO": [
    "COMBOBOXINFO", 
    "COMBOBOXINFO[]"
  ], 
  "FILTERKEYS*": [
    "FILTERKEYS", 
    "FILTERKEYS[]"
  ], 
  "PKBDLLHOOKSTRUCT": [
    "KBDLLHOOKSTRUCT", 
    "KBDLLHOOKSTRUCT[]"
  ], 
  "LPCSTR**": [
    "byte"
  ], 
  "LPPROCESS_HEAP_ENTRY": [
    "PROCESS_HEAP_ENTRY", 
    "PROCESS_HEAP_ENTRY[]"
  ], 
  "LPCUSER_INFO_1014": [
    "USER_INFO_1014", 
    "USER_INFO_1014[]"
  ], 
  "PPRINTER_NOTIFY_OPTIONS_TYPE*": [
    "PointerByReference"
  ], 
  "LPCDEBUGHOOKINFO": [
    "DEBUGHOOKINFO", 
    "DEBUGHOOKINFO[]"
  ], 
  "PPDH_TIME_INFO": [
    "PDH_TIME_INFO", 
    "PDH_TIME_INFO[]"
  ], 
  "TpcGetSamplesArgs*": [
    "TpcGetSamplesArgs", 
    "TpcGetSamplesArgs[]"
  ], 
  "SHQUERYRBINFO**": [
    "PointerByReference"
  ], 
  "PSERVICE_STATUS_PROCESS": [
    "SERVICE_STATUS_PROCESS", 
    "SERVICE_STATUS_PROCESS[]"
  ], 
  "LPCONNECTDLGSTRUCT": [
    "CONNECTDLGSTRUCT", 
    "CONNECTDLGSTRUCT[]"
  ], 
  "UINT8**": [
    "byte"
  ], 
  "LPOVERLAPPED_ENTRY*": [
    "PointerByReference"
  ], 
  "HLOCAL": [
    "HANDLE", 
    "Pointer"
  ], 
  "EVENTLOG_FULL_INFORMATION": [
    "EVENTLOG_FULL_INFORMATION"
  ], 
  "WER_CONSENT": [
    "int"
  ], 
  "DFS_INFO_106**": [
    "PointerByReference"
  ], 
  "LPITEMIDLIST*": [
    "PointerByReference"
  ], 
  "PWKSTA_USER_INFO_1101": [
    "WKSTA_USER_INFO_1101", 
    "WKSTA_USER_INFO_1101[]"
  ], 
  "LPRAWMOUSE*": [
    "PointerByReference"
  ], 
  "FINDEX_INFO_LEVELS**": [
    "PointerByReference"
  ], 
  "DOC_INFO_1": [
    "DOC_INFO_1"
  ], 
  "POLYTEXT**": [
    "PointerByReference"
  ], 
  "PFONTSIGNATURE": [
    "FONTSIGNATURE", 
    "FONTSIGNATURE[]"
  ], 
  "PLPGEOCLASS": [
    "IntByReference", 
    "long[]"
  ], 
  "LPCUSER_INFO_1012": [
    "USER_INFO_1012", 
    "USER_INFO_1012[]"
  ], 
  "LPCEMRALPHABLEND": [
    "EMRALPHABLEND", 
    "EMRALPHABLEND[]"
  ], 
  "LPCBSMINFO": [
    "BSMINFO", 
    "BSMINFO[]"
  ], 
  "LPCTSTR*": [
    "String[]"
  ], 
  "HRGN": [
    "HANDLE"
  ], 
  "USER_INFO_23*": [
    "USER_INFO_23", 
    "USER_INFO_23[]"
  ], 
  "PPGEOCLASS": [
    "IntByReference", 
    "long[]"
  ], 
  "LPCLOCALGROUP_USERS_INFO_0": [
    "LOCALGROUP_USERS_INFO_0", 
    "LOCALGROUP_USERS_INFO_0[]"
  ], 
  "PPROVIDOR_INFO_1": [
    "PROVIDOR_INFO_1", 
    "PROVIDOR_INFO_1[]"
  ], 
  "LPDFS_INFO_104*": [
    "PointerByReference"
  ], 
  "LPCPROVIDOR_INFO_1": [
    "PROVIDOR_INFO_1", 
    "PROVIDOR_INFO_1[]"
  ], 
  "LPCWKSTA_INFO_502": [
    "WKSTA_INFO_502", 
    "WKSTA_INFO_502[]"
  ], 
  "MSG_INFO_0*": [
    "MSG_INFO_0", 
    "MSG_INFO_0[]"
  ], 
  "DRIVER_INFO_6**": [
    "PointerByReference"
  ], 
  "LPFIXED*": [
    "PointerByReference"
  ], 
  "LPMOUSEMOVEPOINT": [
    "MOUSEMOVEPOINT", 
    "MOUSEMOVEPOINT[]"
  ], 
  "LPCPDH_RAW_COUNTER": [
    "PDH_RAW_COUNTER", 
    "PDH_RAW_COUNTER[]"
  ], 
  "PGUITHREADINFO*": [
    "PointerByReference"
  ], 
  "LPCPDH_FMT_COUNTERVALUE_ITEM": [
    "PDH_FMT_COUNTERVALUE_ITEM", 
    "PDH_FMT_COUNTERVALUE_ITEM[]"
  ], 
  "PPROCESS_INFORMATION": [
    "PROCESS_INFORMATION", 
    "PROCESS_INFORMATION[]"
  ], 
  "LPCSYSTEM_POWER_STATE": [
    "IntByReference", 
    "int[]"
  ], 
  "PTSTR**": [
    "char"
  ], 
  "LOGPALETTE": [
    "LOGPALETTE"
  ], 
  "LPGETPROPERTYSTOREFLAGS": [
    "IntByReference", 
    "int[]"
  ], 
  "LPNETINFOSTRUCT": [
    "NETINFOSTRUCT", 
    "NETINFOSTRUCT[]"
  ], 
  "LPMOUSEMOVEPOINT*": [
    "PointerByReference"
  ], 
  "LPACL_INFORMATION_CLASS*": [
    "PointerByReference"
  ], 
  "LPCCONNECTDLGSTRUCT": [
    "CONNECTDLGSTRUCT", 
    "CONNECTDLGSTRUCT[]"
  ], 
  "PPROCESS_INFORMATION*": [
    "PointerByReference"
  ], 
  "LPXFORM*": [
    "PointerByReference"
  ], 
  "PLOGFONT": [
    "LOGFONT", 
    "LOGFONT[]"
  ], 
  "LPCMENUITEMTEMPLATEHEADER": [
    "MENUITEMTEMPLATEHEADER", 
    "MENUITEMTEMPLATEHEADER[]"
  ], 
  "LPEVENTMSG*": [
    "PointerByReference"
  ], 
  "JOBOBJECT_BASIC_PROCESS_ID_LIST": [
    "JOBOBJECT_BASIC_PROCESS_ID_LIST"
  ], 
  "PSIZE": [
    "SIZE", 
    "SIZE[]"
  ], 
  "LPHANDLE*": [
    "HANDLEByReference"
  ], 
  "DRIVER_INFO_6": [
    "DRIVER_INFO_6"
  ], 
  "LPCTIME_ZONE_INFORMATION": [
    "TIME_ZONE_INFORMATION", 
    "TIME_ZONE_INFORMATION[]"
  ], 
  "LPDFS_INFO_200": [
    "DFS_INFO_200", 
    "DFS_INFO_200[]"
  ], 
  "WINDOW_BUFFER_SIZE_RECORD*": [
    "WINDOW_BUFFER_SIZE_RECORD", 
    "WINDOW_BUFFER_SIZE_RECORD[]"
  ], 
  "PDISPLAY_DEVICE": [
    "DISPLAY_DEVICE", 
    "DISPLAY_DEVICE[]"
  ], 
  "RAWHID**": [
    "PointerByReference"
  ], 
  "SYSGEOCLASS*": [
    "IntByReference", 
    "int[]"
  ], 
  "LPLONG32": [
    "IntByReference", 
    "int[]"
  ], 
  "PFINDEX_SEARCH_OPS": [
    "IntByReference", 
    "int[]"
  ], 
  "PADDJOB_INFO_1": [
    "ADDJOB_INFO_1", 
    "ADDJOB_INFO_1[]"
  ], 
  "EXTENDED_NAME_FORMAT**": [
    "PointerByReference"
  ], 
  "LPSHARE_INFO_1501": [
    "SHARE_INFO_1501", 
    "SHARE_INFO_1501[]"
  ], 
  "PRINTPROCESSOR_INFO_1": [
    "PRINTPROCESSOR_INFO_1"
  ], 
  "SHARE_INFO_1006*": [
    "SHARE_INFO_1006", 
    "SHARE_INFO_1006[]"
  ], 
  "SYSTEM_LOGICAL_PROCESSOR_INFORMATION": [
    "SYSTEM_LOGICAL_PROCESSOR_INFORMATION"
  ], 
  "ENHMETAHEADER": [
    "ENHMETAHEADER"
  ], 
  "PGPFIDL_FLAGS": [
    "IntByReference", 
    "int[]"
  ], 
  "GROUP_INFO_1005**": [
    "PointerByReference"
  ], 
  "OSVERSIONINFOEX": [
    "OSVERSIONINFOEX"
  ], 
  "LPHLOCAL": [
    "HANDLEByReference"
  ], 
  "GROUP_USERS_INFO_0": [
    "GROUP_USERS_INFO_0"
  ], 
  "GROUP_USERS_INFO_1": [
    "GROUP_USERS_INFO_1"
  ], 
  "MediaLabelInfo**": [
    "PointerByReference"
  ], 
  "PHRAWINPUT": [
    "HANDLEByReference"
  ], 
  "WINDOWINFO": [
    "WINDOWINFO"
  ], 
  "PPRINTER_NOTIFY_OPTIONS*": [
    "PointerByReference"
  ], 
  "KBDLLHOOKSTRUCT**": [
    "PointerByReference"
  ], 
  "PSERVICE_CONTROL_STATUS_REASON_PARAMS*": [
    "PointerByReference"
  ], 
  "USER_MODALS_INFO_1001**": [
    "PointerByReference"
  ], 
  "MOUSEHOOKSTRUCT**": [
    "PointerByReference"
  ], 
  "ABC**": [
    "PointerByReference"
  ], 
  "PDH_FMT_COUNTERVALUE_ITEM**": [
    "PointerByReference"
  ], 
  "USHORT": [
    "short"
  ], 
  "LPCTHREADENTRY32": [
    "THREADENTRY32", 
    "THREADENTRY32[]"
  ], 
  "EMRALPHABLEND*": [
    "EMRALPHABLEND", 
    "EMRALPHABLEND[]"
  ], 
  "LPSERVER_INFO_100*": [
    "PointerByReference"
  ], 
  "HRAWINPUT*": [
    "HANDLEByReference"
  ], 
  "UINT32**": [
    "PointerByReference"
  ], 
  "LPLPCSTR": [
    "byte"
  ], 
  "LPSERVICE_STATUS*": [
    "PointerByReference"
  ], 
  "LRESULT": [
    "int"
  ], 
  "PJOBOBJECT_BASIC_UI_RESTRICTIONS*": [
    "PointerByReference"
  ], 
  "ENUM_SERVICE_STATUS": [
    "ENUM_SERVICE_STATUS"
  ], 
  "PGETPROPERTYSTOREFLAGS": [
    "IntByReference", 
    "int[]"
  ], 
  "BOOLEAN*": [
    "byte"
  ], 
  "PPTSTR": [
    "char"
  ], 
  "POWERBROADCAST_SETTING**": [
    "PointerByReference"
  ], 
  "PSERVICE_STATUS*": [
    "PointerByReference"
  ], 
  "LPMONITOR_INFO_2": [
    "MONITOR_INFO_2", 
    "MONITOR_INFO_2[]"
  ], 
  "PGROUP_INFO_0*": [
    "PointerByReference"
  ], 
  "LPUNIVERSAL_NAME_INFO": [
    "UNIVERSAL_NAME_INFO", 
    "UNIVERSAL_NAME_INFO[]"
  ], 
  "CWPRETSTRUCT**": [
    "PointerByReference"
  ], 
  "BSMINFO**": [
    "PointerByReference"
  ], 
  "LPCWER_REPORT_INFORMATION": [
    "WER_REPORT_INFORMATION", 
    "WER_REPORT_INFORMATION[]"
  ], 
  "PJOBOBJECT_SECURITY_LIMIT_INFORMATION": [
    "JOBOBJECT_SECURITY_LIMIT_INFORMATION", 
    "JOBOBJECT_SECURITY_LIMIT_INFORMATION[]"
  ], 
  "LPSESSION_INFO_0*": [
    "PointerByReference"
  ], 
  "WCT_OBJECT_STATUS**": [
    "PointerByReference"
  ], 
  "LPUSER_INFO_1053": [
    "USER_INFO_1053", 
    "USER_INFO_1053[]"
  ], 
  "LPAXESLIST": [
    "AXESLIST", 
    "AXESLIST[]"
  ], 
  "LPUSER_INFO_1051": [
    "USER_INFO_1051", 
    "USER_INFO_1051[]"
  ], 
  "DFS_INFO_1**": [
    "PointerByReference"
  ], 
  "LPDVTARGETDEVICE": [
    "DVTARGETDEVICE", 
    "DVTARGETDEVICE[]"
  ], 
  "PVS_FIXEDFILEINFO*": [
    "PointerByReference"
  ], 
  "LPDFS_INFO_150": [
    "DFS_INFO_150", 
    "DFS_INFO_150[]"
  ], 
  "PTOGGLEKEYS*": [
    "PointerByReference"
  ], 
  "LPSIGDN*": [
    "PointerByReference"
  ], 
  "NEWTEXTMETRIC": [
    "NEWTEXTMETRIC"
  ], 
  "PPAINTSTRUCT": [
    "PAINTSTRUCT", 
    "PAINTSTRUCT[]"
  ], 
  "LPHRESULT*": [
    "PointerByReference"
  ], 
  "DFS_INFO_106*": [
    "DFS_INFO_106", 
    "DFS_INFO_106[]"
  ], 
  "LPDWORDLONG": [
    "LongByReference", 
    "__int64[]"
  ], 
  "HW_PROFILE_INFO*": [
    "HW_PROFILE_INFO", 
    "HW_PROFILE_INFO[]"
  ], 
  "LPNTMS_ALLOCATION_INFORMATION*": [
    "PointerByReference"
  ], 
  "PLOCALGROUP_MEMBERS_INFO_3": [
    "LOCALGROUP_MEMBERS_INFO_3", 
    "LOCALGROUP_MEMBERS_INFO_3[]"
  ], 
  "PDH_COUNTER_INFO*": [
    "PDH_COUNTER_INFO", 
    "PDH_COUNTER_INFO[]"
  ], 
  "SHARE_INFO_1005**": [
    "PointerByReference"
  ], 
  "PULONG*": [
    "PointerByReference"
  ], 
  "DFS_TARGET_PRIORITY_CLASS*": [
    "IntByReference", 
    "int[]"
  ], 
  "LPQUERY_SERVICE_CONFIG*": [
    "PointerByReference"
  ], 
  "LPACCESSTIMEOUT": [
    "ACCESSTIMEOUT", 
    "ACCESSTIMEOUT[]"
  ], 
  "PDVTARGETDEVICE*": [
    "PointerByReference"
  ], 
  "PEFS_CERTIFICATE_BLOB": [
    "EFS_CERTIFICATE_BLOB", 
    "EFS_CERTIFICATE_BLOB[]"
  ], 
  "LPCQUERY_SERVICE_CONFIG": [
    "QUERY_SERVICE_CONFIG", 
    "QUERY_SERVICE_CONFIG[]"
  ], 
  "PMENUEX_TEMPLATE_HEADER*": [
    "PointerByReference"
  ], 
  "CONSOLE_FONT_INFO*": [
    "CONSOLE_FONT_INFO", 
    "CONSOLE_FONT_INFO[]"
  ], 
  "LPTIME_ZONE_INFORMATION*": [
    "PointerByReference"
  ], 
  "QUERY_USER_NOTIFICATION_STATE": [
    "int"
  ], 
  "PRAWHID*": [
    "PointerByReference"
  ], 
  "LPTCHAR*": [
    "char"
  ], 
  "PQUERY_SERVICE_LOCK_STATUS*": [
    "PointerByReference"
  ], 
  "RAWKEYBOARD**": [
    "PointerByReference"
  ], 
  "PENHMETAHEADER*": [
    "PointerByReference"
  ], 
  "PPOINTL*": [
    "PointerByReference"
  ], 
  "MOUSEMOVEPOINT**": [
    "PointerByReference"
  ], 
  "LPSERVICE_PRESHUTDOWN_INFO": [
    "SERVICE_PRESHUTDOWN_INFO", 
    "SERVICE_PRESHUTDOWN_INFO[]"
  ], 
  "DFS_INFO_107*": [
    "DFS_INFO_107", 
    "DFS_INFO_107[]"
  ], 
  "ENUMLOGFONTEX**": [
    "PointerByReference"
  ], 
  "LPDFS_STORAGE_INFO_1": [
    "DFS_STORAGE_INFO_1", 
    "DFS_STORAGE_INFO_1[]"
  ], 
  "SYSTEM_POWER_CAPABILITIES": [
    "SYSTEM_POWER_CAPABILITIES"
  ], 
  "HIGHCONTRAST**": [
    "PointerByReference"
  ], 
  "LPLDT_ENTRY": [
    "LDT_ENTRY", 
    "LDT_ENTRY[]"
  ], 
  "LPCSHARE_INFO_1005": [
    "SHARE_INFO_1005", 
    "SHARE_INFO_1005[]"
  ], 
  "LOGBRUSH": [
    "LOGBRUSH"
  ], 
  "SCROLLINFO**": [
    "PointerByReference"
  ], 
  "PWCHAR*": [
    "char"
  ], 
  "PTRACE_GUID_REGISTRATION": [
    "TRACE_GUID_REGISTRATION", 
    "TRACE_GUID_REGISTRATION[]"
  ], 
  "LPCPOWER_ACTION_POLICY": [
    "POWER_ACTION_POLICY", 
    "POWER_ACTION_POLICY[]"
  ], 
  "GUITHREADINFO*": [
    "GUITHREADINFO", 
    "GUITHREADINFO[]"
  ], 
  "PRINTER_NOTIFY_INFO_DATA**": [
    "PointerByReference"
  ], 
  "PSYSTEM_BATTERY_STATE": [
    "SYSTEM_BATTERY_STATE", 
    "SYSTEM_BATTERY_STATE[]"
  ], 
  "PFILETIME*": [
    "PointerByReference"
  ], 
  "LPNUMBERFMT": [
    "NUMBERFMT", 
    "NUMBERFMT[]"
  ], 
  "LOCALGROUP_MEMBERS_INFO_1**": [
    "PointerByReference"
  ], 
  "LPENUM_SERVICE_STATUS_PROCESS*": [
    "PointerByReference"
  ], 
  "LPCWAITCHAIN_NODE_INFO": [
    "WAITCHAIN_NODE_INFO", 
    "WAITCHAIN_NODE_INFO[]"
  ], 
  "PINPUT_RECORD*": [
    "PointerByReference"
  ], 
  "LPGUITHREADINFO": [
    "GUITHREADINFO", 
    "GUITHREADINFO[]"
  ], 
  "LPHIGHCONTRAST": [
    "HIGHCONTRAST", 
    "HIGHCONTRAST[]"
  ], 
  "SYSTEM_POWER_INFORMATION*": [
    "SYSTEM_POWER_INFORMATION", 
    "SYSTEM_POWER_INFORMATION[]"
  ], 
  "PDLGTEMPLATE*": [
    "PointerByReference"
  ], 
  "CPINFOEX": [
    "CPINFOEX"
  ], 
  "USER_OTHER_INFO*": [
    "USER_OTHER_INFO", 
    "USER_OTHER_INFO[]"
  ], 
  "PDFS_INFO_50": [
    "DFS_INFO_50", 
    "DFS_INFO_50[]"
  ], 
  "USER_INFO_11": [
    "USER_INFO_11"
  ], 
  "PAINTSTRUCT**": [
    "PointerByReference"
  ], 
  "SHSTOCKICONINFO*": [
    "SHSTOCKICONINFO", 
    "SHSTOCKICONINFO[]"
  ], 
  "LPCSHDESCRIPTIONID": [
    "SHDESCRIPTIONID", 
    "SHDESCRIPTIONID[]"
  ], 
  "LPPSTR": [
    "byte"
  ], 
  "WER_REPORT_UI*": [
    "IntByReference", 
    "int[]"
  ], 
  "LPGETPROPERTYSTOREFLAGS*": [
    "PointerByReference"
  ], 
  "LPHEAPENTRY32": [
    "HEAPENTRY32", 
    "HEAPENTRY32[]"
  ], 
  "SHFILEOPSTRUCT**": [
    "PointerByReference"
  ], 
  "LPCWKSTA_TRANSPORT_INFO_0": [
    "WKSTA_TRANSPORT_INFO_0", 
    "WKSTA_TRANSPORT_INFO_0[]"
  ], 
  "OVERLAPPED**": [
    "PointerByReference"
  ], 
  "PCREATESTRUCT*": [
    "PointerByReference"
  ], 
  "LPCBITMAP": [
    "BITMAP", 
    "BITMAP[]"
  ], 
  "LPHFONT": [
    "HANDLEByReference"
  ], 
  "LPCNTMS_NOTIFICATIONINFORMATION": [
    "NTMS_NOTIFICATIONINFORMATION", 
    "NTMS_NOTIFICATIONINFORMATION[]"
  ], 
  "LPNTMS_NOTIFICATIONINFORMATION": [
    "NTMS_NOTIFICATIONINFORMATION", 
    "NTMS_NOTIFICATIONINFORMATION[]"
  ], 
  "SERVICE_TRIGGER_SPECIFIC_DATA_ITEM*": [
    "SERVICE_TRIGGER_SPECIFIC_DATA_ITEM", 
    "SERVICE_TRIGGER_SPECIFIC_DATA_ITEM[]"
  ], 
  "PDFS_INFO_5*": [
    "PointerByReference"
  ], 
  "LPCWPRETSTRUCT*": [
    "PointerByReference"
  ], 
  "PSERVICE_FAILURE_ACTIONS_FLAG*": [
    "PointerByReference"
  ], 
  "LPCRM_PROCESS_INFO": [
    "RM_PROCESS_INFO", 
    "RM_PROCESS_INFO[]"
  ], 
  "GROUP_AFFINITY": [
    "GROUP_AFFINITY"
  ], 
  "AXISINFO**": [
    "PointerByReference"
  ], 
  "PUSER_INFO_1024": [
    "USER_INFO_1024", 
    "USER_INFO_1024[]"
  ], 
  "JOBOBJECT_BASIC_ACCOUNTING_INFORMATION*": [
    "JOBOBJECT_BASIC_ACCOUNTING_INFORMATION", 
    "JOBOBJECT_BASIC_ACCOUNTING_INFORMATION[]"
  ], 
  "PMEMORYSTATUSEX*": [
    "PointerByReference"
  ], 
  "BITMAPINFO*": [
    "BITMAPINFO", 
    "BITMAPINFO[]"
  ], 
  "PDOC_INFO_1*": [
    "PointerByReference"
  ], 
  "PQUERY_SERVICE_CONFIG": [
    "QUERY_SERVICE_CONFIG", 
    "QUERY_SERVICE_CONFIG[]"
  ], 
  "LPCCONVINFO": [
    "CONVINFO", 
    "CONVINFO[]"
  ], 
  "LPEFS_CERTIFICATE_BLOB": [
    "EFS_CERTIFICATE_BLOB", 
    "EFS_CERTIFICATE_BLOB[]"
  ], 
  "LPCJOBOBJECT_BASIC_ACCOUNTING_INFORMATION": [
    "JOBOBJECT_BASIC_ACCOUNTING_INFORMATION", 
    "JOBOBJECT_BASIC_ACCOUNTING_INFORMATION[]"
  ], 
  "SESSION_INFO_0*": [
    "SESSION_INFO_0", 
    "SESSION_INFO_0[]"
  ], 
  "LPNET_DISPLAY_MACHINE": [
    "NET_DISPLAY_MACHINE", 
    "NET_DISPLAY_MACHINE[]"
  ], 
  "PROCESSOR_POWER_POLICY": [
    "PROCESSOR_POWER_POLICY"
  ], 
  "USER_INFO_1011": [
    "USER_INFO_1011"
  ], 
  "USER_INFO_1010": [
    "USER_INFO_1010"
  ], 
  "PMOUSEKEYS*": [
    "PointerByReference"
  ], 
  "PHMENU": [
    "HANDLEByReference"
  ], 
  "LPPROCESS_MEMORY_COUNTERS": [
    "PROCESS_MEMORY_COUNTERS", 
    "PROCESS_MEMORY_COUNTERS[]"
  ], 
  "USER_INFO_1014": [
    "USER_INFO_1014"
  ], 
  "USER_INFO_1017": [
    "USER_INFO_1017"
  ], 
  "PUSER_MODALS_INFO_1*": [
    "PointerByReference"
  ], 
  "LPRGBQUAD": [
    "RGBQUAD", 
    "RGBQUAD[]"
  ], 
  "GLYPHSET": [
    "GLYPHSET"
  ], 
  "APPBARDATA": [
    "APPBARDATA"
  ], 
  "USER_INFO_1011*": [
    "USER_INFO_1011", 
    "USER_INFO_1011[]"
  ], 
  "long**": [
    "PointerByReference"
  ], 
  "PUSER_INFO_22*": [
    "PointerByReference"
  ], 
  "NTMS_ALLOCATION_INFORMATION*": [
    "NTMS_ALLOCATION_INFORMATION", 
    "NTMS_ALLOCATION_INFORMATION[]"
  ], 
  "PRID_DEVICE_INFO*": [
    "PointerByReference"
  ], 
  "GROUP_USERS_INFO_0**": [
    "PointerByReference"
  ], 
  "PRID_DEVICE_INFO_KEYBOARD": [
    "RID_DEVICE_INFO_KEYBOARD", 
    "RID_DEVICE_INFO_KEYBOARD[]"
  ], 
  "PWIN32_STREAM_ID": [
    "WIN32_STREAM_ID", 
    "WIN32_STREAM_ID[]"
  ], 
  "EXECUTION_STATE**": [
    "PointerByReference"
  ], 
  "PACL*": [
    "PointerByReference"
  ], 
  "NTMS_ALLOCATION_INFORMATION**": [
    "PointerByReference"
  ], 
  "LPCLIPFORMAT*": [
    "PointerByReference"
  ], 
  "DOC_INFO_1**": [
    "PointerByReference"
  ], 
  "RASTERIZER_STATUS*": [
    "RASTERIZER_STATUS", 
    "RASTERIZER_STATUS[]"
  ], 
  "DFS_INFO_1*": [
    "DFS_INFO_1", 
    "DFS_INFO_1[]"
  ], 
  "LPMSG": [
    "MSG", 
    "MSG[]"
  ], 
  "PDISPLAY_DEVICE*": [
    "PointerByReference"
  ], 
  "CONSOLE_SCREEN_BUFFER_INFOEX*": [
    "CONSOLE_SCREEN_BUFFER_INFOEX", 
    "CONSOLE_SCREEN_BUFFER_INFOEX[]"
  ], 
  "PSERVICE_STATUS": [
    "SERVICE_STATUS", 
    "SERVICE_STATUS[]"
  ], 
  "LPBSMINFO": [
    "BSMINFO", 
    "BSMINFO[]"
  ], 
  "PRINTER_ENUM_VALUES*": [
    "PRINTER_ENUM_VALUES", 
    "PRINTER_ENUM_VALUES[]"
  ], 
  "DESIGNVECTOR*": [
    "DESIGNVECTOR", 
    "DESIGNVECTOR[]"
  ], 
  "LPEMR*": [
    "PointerByReference"
  ], 
  "CLIPFORMAT": [
    "int"
  ], 
  "PDFS_INFO_150": [
    "DFS_INFO_150", 
    "DFS_INFO_150[]"
  ], 
  "LPSYSTEMTIME*": [
    "PointerByReference"
  ], 
  "LPDISPLAY_DEVICE*": [
    "PointerByReference"
  ], 
  "PCOLORADJUSTMENT*": [
    "PointerByReference"
  ], 
  "SCROLLBARINFO*": [
    "SCROLLBARINFO", 
    "SCROLLBARINFO[]"
  ], 
  "NUMBERFMT**": [
    "PointerByReference"
  ], 
  "KBDLLHOOKSTRUCT": [
    "KBDLLHOOKSTRUCT"
  ], 
  "LPSCNRT_STATUS": [
    "IntByReference", 
    "int[]"
  ], 
  "LARGE_INTEGER*": [
    "LARGE_INTEGER", 
    "LARGE_INTEGER[]", 
    "LONGLONGByReference"
  ], 
  "PALTTABINFO": [
    "ALTTABINFO", 
    "ALTTABINFO[]"
  ], 
  "TPMPARAMS**": [
    "PointerByReference"
  ], 
  "PLOCALGROUP_MEMBERS_INFO_3*": [
    "PointerByReference"
  ], 
  "LPCMOUSEMOVEPOINT": [
    "MOUSEMOVEPOINT", 
    "MOUSEMOVEPOINT[]"
  ], 
  "LPCONSOLE_SCREEN_BUFFER_INFO*": [
    "PointerByReference"
  ], 
  "ENUM_SERVICE_STATUS**": [
    "PointerByReference"
  ], 
  "LPACCESSTIMEOUT*": [
    "PointerByReference"
  ], 
  "DFS_INFO_200*": [
    "DFS_INFO_200", 
    "DFS_INFO_200[]"
  ], 
  "LPHANDLE": [
    "HANDLE"
  ], 
  "PWNODE_HEADER": [
    "WNODE_HEADER", 
    "WNODE_HEADER[]"
  ], 
  "POVERLAPPED*": [
    "PointerByReference"
  ], 
  "LPUSER_MODALS_INFO_1001*": [
    "PointerByReference"
  ], 
  "LPHDC": [
    "HANDLEByReference"
  ], 
  "PLPGEOCLASS*": [
    "PointerByReference"
  ], 
  "LPEFS_HASH_BLOB*": [
    "PointerByReference"
  ], 
  "PHEAPLIST32*": [
    "PointerByReference"
  ], 
  "LPCCOMMTIMEOUTS": [
    "COMMTIMEOUTS", 
    "COMMTIMEOUTS[]"
  ], 
  "LPUSER_INFO_1006*": [
    "PointerByReference"
  ], 
  "GRADIENT_RECT*": [
    "GRADIENT_RECT", 
    "GRADIENT_RECT[]"
  ], 
  "PCONSOLE_SELECTION_INFO*": [
    "PointerByReference"
  ], 
  "PFORM_INFO_1*": [
    "PointerByReference"
  ], 
  "PUSER_MODALS_INFO_2": [
    "USER_MODALS_INFO_2", 
    "USER_MODALS_INFO_2[]"
  ], 
  "PUSER_MODALS_INFO_1": [
    "USER_MODALS_INFO_1", 
    "USER_MODALS_INFO_1[]"
  ], 
  "PUSER_MODALS_INFO_0": [
    "USER_MODALS_INFO_0", 
    "USER_MODALS_INFO_0[]"
  ], 
  "LPSHFILEOPSTRUCT": [
    "SHFILEOPSTRUCT", 
    "SHFILEOPSTRUCT[]"
  ], 
  "LPUSER_INFO_24*": [
    "PointerByReference"
  ], 
  "HANDLETABLE**": [
    "PointerByReference"
  ], 
  "PFILE_NOTIFY_INFORMATION*": [
    "PointerByReference"
  ], 
  "VALENT**": [
    "PointerByReference"
  ], 
  "LPNET_DISPLAY_USER*": [
    "PointerByReference"
  ], 
  "SESSION_INFO_10*": [
    "SESSION_INFO_10", 
    "SESSION_INFO_10[]"
  ], 
  "PENUMLOGFONT*": [
    "PointerByReference"
  ], 
  "PUSER_INFO_3*": [
    "PointerByReference"
  ], 
  "LPCUSER_INFO_1008": [
    "USER_INFO_1008", 
    "USER_INFO_1008[]"
  ], 
  "PENUMLOGFONTEXDV": [
    "ENUMLOGFONTEXDV", 
    "ENUMLOGFONTEXDV[]"
  ], 
  "LPCUSER_INFO_1006": [
    "USER_INFO_1006", 
    "USER_INFO_1006[]"
  ], 
  "LPCUSER_INFO_1007": [
    "USER_INFO_1007", 
    "USER_INFO_1007[]"
  ], 
  "PMETARECORD*": [
    "PointerByReference"
  ], 
  "LPCUSER_INFO_1005": [
    "USER_INFO_1005", 
    "USER_INFO_1005[]"
  ], 
  "LPCUSER_INFO_1003": [
    "USER_INFO_1003", 
    "USER_INFO_1003[]"
  ], 
  "LPCWCT_OBJECT_STATUS": [
    "IntByReference", 
    "int[]"
  ], 
  "PNETINFOSTRUCT*": [
    "PointerByReference"
  ], 
  "LASTINPUTINFO": [
    "LASTINPUTINFO"
  ], 
  "LPPIXELFORMATDESCRIPTOR*": [
    "PointerByReference"
  ], 
  "TEXTMETRIC*": [
    "TEXTMETRIC", 
    "TEXTMETRIC[]"
  ], 
  "LPPRINTER_NOTIFY_INFO_DATA*": [
    "PointerByReference"
  ], 
  "PGLYPHSET": [
    "GLYPHSET", 
    "GLYPHSET[]"
  ], 
  "FLASHWINFO*": [
    "FLASHWINFO", 
    "FLASHWINFO[]"
  ], 
  "PREMOTE_NAME_INFO": [
    "REMOTE_NAME_INFO", 
    "REMOTE_NAME_INFO[]"
  ], 
  "WNDCLASSEX*": [
    "WNDCLASSEX", 
    "WNDCLASSEX[]"
  ], 
  "PMSLLHOOKSTRUCT": [
    "MSLLHOOKSTRUCT", 
    "MSLLHOOKSTRUCT[]"
  ], 
  "MOUSEMOVEPOINT": [
    "MOUSEMOVEPOINT"
  ], 
  "PPRINTER_INFO_6": [
    "PRINTER_INFO_6", 
    "PRINTER_INFO_6[]"
  ], 
  "GROUP_INFO_1**": [
    "PointerByReference"
  ], 
  "PVOID64": [
    "Pointer", 
    "LPVOID"
  ], 
  "LPGLOBAL_POWER_POLICY*": [
    "PointerByReference"
  ], 
  "PGRADIENT_RECT": [
    "GRADIENT_RECT", 
    "GRADIENT_RECT[]"
  ], 
  "UINT32*": [
    "IntByReference", 
    "int[]"
  ], 
  "LPMEMORY_BASIC_INFORMATION*": [
    "PointerByReference"
  ], 
  "LPRGBQUAD*": [
    "PointerByReference"
  ], 
  "PLPGEOTYPE": [
    "IntByReference", 
    "long[]"
  ], 
  "JOBOBJECT_CPU_RATE_CONTROL_INFORMATION*": [
    "JOBOBJECT_CPU_RATE_CONTROL_INFORMATION", 
    "JOBOBJECT_CPU_RATE_CONTROL_INFORMATION[]"
  ], 
  "LPMETARECORD": [
    "METARECORD", 
    "METARECORD[]"
  ], 
  "SERVER_TRANSPORT_INFO_3*": [
    "SERVER_TRANSPORT_INFO_3", 
    "SERVER_TRANSPORT_INFO_3[]"
  ], 
  "COLORADJUSTMENT*": [
    "COLORADJUSTMENT", 
    "COLORADJUSTMENT[]"
  ], 
  "INT": [
    "int"
  ], 
  "PPRINTER_INFO_2": [
    "PRINTER_INFO_2", 
    "PRINTER_INFO_2[]"
  ], 
  "SERVICE_LAUNCH_PROTECTED_INFO": [
    "SERVICE_LAUNCH_PROTECTED_INFO"
  ], 
  "LPSERVICE_TRIGGER_SPECIFIC_DATA_ITEM*": [
    "PointerByReference"
  ], 
  "LPSERVICE_PREFERRED_NODE_INFO": [
    "SERVICE_PREFERRED_NODE_INFO", 
    "SERVICE_PREFERRED_NODE_INFO[]"
  ], 
  "LPHMENU": [
    "HANDLEByReference"
  ], 
  "USER_INFO_1011**": [
    "PointerByReference"
  ], 
  "LPRAWINPUTHEADER*": [
    "PointerByReference"
  ], 
  "LPCPRIVILEGE_SET": [
    "PRIVILEGE_SET", 
    "PRIVILEGE_SET[]"
  ], 
  "PFILTERKEYS": [
    "FILTERKEYS", 
    "FILTERKEYS[]"
  ], 
  "PSERVER_TRANSPORT_INFO_1": [
    "SERVER_TRANSPORT_INFO_1", 
    "SERVER_TRANSPORT_INFO_1[]"
  ], 
  "LPGRADIENT_TRIANGLE*": [
    "PointerByReference"
  ], 
  "PUSER_INFO_1": [
    "USER_INFO_1", 
    "USER_INFO_1[]"
  ], 
  "PUSER_INFO_0": [
    "USER_INFO_0", 
    "USER_INFO_0[]"
  ], 
  "PUSER_INFO_3": [
    "USER_INFO_3", 
    "USER_INFO_3[]"
  ], 
  "LPCGEOTYPE*": [
    "IntByReference", 
    "long[]"
  ], 
  "LPCSHSTOCKICONID": [
    "IntByReference", 
    "int[]"
  ], 
  "LPGROUP_INFO_1*": [
    "PointerByReference"
  ], 
  "PMONITOR_INFO_1": [
    "MONITOR_INFO_1", 
    "MONITOR_INFO_1[]"
  ], 
  "LPCXFORM": [
    "XFORM", 
    "XFORM[]"
  ], 
  "PDFS_INFO_8": [
    "DFS_INFO_8", 
    "DFS_INFO_8[]"
  ], 
  "PDFS_INFO_9": [
    "DFS_INFO_9", 
    "DFS_INFO_9[]"
  ], 
  "LPCRID_DEVICE_INFO_MOUSE": [
    "RID_DEVICE_INFO_MOUSE", 
    "RID_DEVICE_INFO_MOUSE[]"
  ], 
  "ENUM_SERVICE_STATUS_PROCESS": [
    "ENUM_SERVICE_STATUS_PROCESS"
  ], 
  "MODULEENTRY32*": [
    "MODULEENTRY32", 
    "MODULEENTRY32[]"
  ], 
  "PDFS_INFO_2": [
    "DFS_INFO_2", 
    "DFS_INFO_2[]"
  ], 
  "PROCESSOR_POWER_POLICY_INFO*": [
    "PROCESSOR_POWER_POLICY_INFO", 
    "PROCESSOR_POWER_POLICY_INFO[]"
  ], 
  "USER_INFO_4": [
    "USER_INFO_4"
  ], 
  "PDFS_INFO_1": [
    "DFS_INFO_1", 
    "DFS_INFO_1[]"
  ], 
  "PDFS_INFO_6": [
    "DFS_INFO_6", 
    "DFS_INFO_6[]"
  ], 
  "PDFS_INFO_7": [
    "DFS_INFO_7", 
    "DFS_INFO_7[]"
  ], 
  "PDFS_INFO_4": [
    "DFS_INFO_4", 
    "DFS_INFO_4[]"
  ], 
  "USER_INFO_1": [
    "USER_INFO_1"
  ], 
  "PPRINTER_INFO_4*": [
    "PointerByReference"
  ], 
  "WIN32_FIND_DATA": [
    "WIN32_FIND_DATA"
  ], 
  "PSECURITY_DESCRIPTOR*": [
    "PointerByReference"
  ], 
  "PWINDOWINFO*": [
    "PointerByReference"
  ], 
  "LPSTD_ALERT": [
    "STD_ALERT", 
    "STD_ALERT[]"
  ], 
  "Psize_t": [
    "IntByReference", 
    "long[]"
  ], 
  "PRINTPROCESSOR_INFO_1**": [
    "PointerByReference"
  ], 
  "HEAPENTRY32": [
    "HEAPENTRY32"
  ], 
  "SECURITY_ATTRIBUTES": [
    "SECURITY_ATTRIBUTES"
  ], 
  "LPGEOID": [
    "IntByReference", 
    "long[]"
  ], 
  "PWER_CONSENT*": [
    "PointerByReference"
  ], 
  "PSIGDN*": [
    "PointerByReference"
  ], 
  "SIZE": [
    "SIZE"
  ], 
  "FILE_NOTIFY_INFORMATION*": [
    "FILE_NOTIFY_INFORMATION", 
    "FILE_NOTIFY_INFORMATION[]"
  ], 
  "HW_PROFILE_INFO": [
    "HW_PROFILE_INFO"
  ], 
  "LPDFS_INFO_300": [
    "DFS_INFO_300", 
    "DFS_INFO_300[]"
  ], 
  "PSID": [
    "WinNT.PSID", 
    "Pointer"
  ], 
  "USER_INFO_22*": [
    "USER_INFO_22", 
    "USER_INFO_22[]"
  ], 
  "PWCT_OBJECT_TYPE": [
    "IntByReference", 
    "int[]"
  ], 
  "LPUSER_INFO_1010": [
    "USER_INFO_1010", 
    "USER_INFO_1010[]"
  ], 
  "LPQUERY_SERVICE_LOCK_STATUS": [
    "QUERY_SERVICE_LOCK_STATUS", 
    "QUERY_SERVICE_LOCK_STATUS[]"
  ], 
  "LPJOBOBJECT_BASIC_ACCOUNTING_INFORMATION": [
    "JOBOBJECT_BASIC_ACCOUNTING_INFORMATION", 
    "JOBOBJECT_BASIC_ACCOUNTING_INFORMATION[]"
  ], 
  "DFS_INFO_300*": [
    "DFS_INFO_300", 
    "DFS_INFO_300[]"
  ], 
  "WIN32_FIND_STREAM_DATA**": [
    "PointerByReference"
  ], 
  "PDH_RAW_COUNTER": [
    "PDH_RAW_COUNTER"
  ], 
  "RID_DEVICE_INFO_HID*": [
    "RID_DEVICE_INFO_HID", 
    "RID_DEVICE_INFO_HID[]"
  ], 
  "PCOLORADJUSTMENT": [
    "COLORADJUSTMENT", 
    "COLORADJUSTMENT[]"
  ], 
  "WKSTA_INFO_502**": [
    "PointerByReference"
  ], 
  "HSZ*": [
    "HANDLEByReference"
  ], 
  "COLORREF": [
    "int", 
    "DWORD"
  ], 
  "QUERY_SERVICE_CONFIG": [
    "QUERY_SERVICE_CONFIG"
  ], 
  "COMSTAT": [
    "COMSTAT"
  ], 
  "SERVER_INFO_402*": [
    "SERVER_INFO_402", 
    "SERVER_INFO_402[]"
  ], 
  "PUSER_OTHER_INFO": [
    "USER_OTHER_INFO", 
    "USER_OTHER_INFO[]"
  ], 
  "LPCONSOLE_FONT_INFOEX": [
    "CONSOLE_FONT_INFOEX", 
    "CONSOLE_FONT_INFOEX[]"
  ], 
  "SYSTEM_POWER_STATE**": [
    "PointerByReference"
  ], 
  "ENHMETARECORD**": [
    "PointerByReference"
  ], 
  "CONSOLE_SELECTION_INFO**": [
    "PointerByReference"
  ], 
  "SERVICE_DELAYED_AUTO_START_INFO**": [
    "PointerByReference"
  ], 
  "LPDRAWTEXTPARAMS*": [
    "PointerByReference"
  ], 
  "HEAPENTRY32**": [
    "PointerByReference"
  ], 
  "RGNDATA": [
    "RGNDATA"
  ], 
  "GLOBAL_USER_POWER_POLICY*": [
    "GLOBAL_USER_POWER_POLICY", 
    "GLOBAL_USER_POWER_POLICY[]"
  ], 
  "LPWKSTA_INFO_102*": [
    "PointerByReference"
  ], 
  "DFS_STORAGE_INFO": [
    "DFS_STORAGE_INFO"
  ], 
  "LPCCLIENTCREATESTRUCT": [
    "CLIENTCREATESTRUCT", 
    "CLIENTCREATESTRUCT[]"
  ], 
  "GEOCLASS**": [
    "IntByReference", 
    "long[]"
  ], 
  "NUMBERFMT*": [
    "NUMBERFMT", 
    "NUMBERFMT[]"
  ], 
  "PAINTSTRUCT*": [
    "PAINTSTRUCT", 
    "PAINTSTRUCT[]"
  ], 
  "JOBOBJECT_NOTIFICATION_LIMIT_INFORMATION*": [
    "JOBOBJECT_NOTIFICATION_LIMIT_INFORMATION", 
    "JOBOBJECT_NOTIFICATION_LIMIT_INFORMATION[]"
  ], 
  "MENUEX_TEMPLATE_HEADER*": [
    "MENUEX_TEMPLATE_HEADER", 
    "MENUEX_TEMPLATE_HEADER[]"
  ], 
  "PPCWSTR*": [
    "char"
  ], 
  "LPUSER_INFO_1008": [
    "USER_INFO_1008", 
    "USER_INFO_1008[]"
  ], 
  "SERVICE_DELAYED_AUTO_START_INFO*": [
    "SERVICE_DELAYED_AUTO_START_INFO", 
    "SERVICE_DELAYED_AUTO_START_INFO[]"
  ], 
  "PEVENT_TRACE": [
    "EVENT_TRACE", 
    "EVENT_TRACE[]"
  ], 
  "EXTENDED_NAME_FORMAT*": [
    "EXTENDED_NAME_FORMAT", 
    "EXTENDED_NAME_FORMAT[]"
  ], 
  "LPCSHARE_INFO_502": [
    "SHARE_INFO_502", 
    "SHARE_INFO_502[]"
  ], 
  "LPCSHARE_INFO_503": [
    "SHARE_INFO_503", 
    "SHARE_INFO_503[]"
  ], 
  "GRADIENT_RECT**": [
    "PointerByReference"
  ], 
  "SYSTEM_BATTERY_STATE": [
    "SYSTEM_BATTERY_STATE"
  ], 
  "LPCPRINTER_NOTIFY_OPTIONS_TYPE": [
    "PRINTER_NOTIFY_OPTIONS_TYPE", 
    "PRINTER_NOTIFY_OPTIONS_TYPE[]"
  ], 
  "AVRT_PRIORITY**": [
    "PointerByReference"
  ], 
  "LPCPRINTER_ENUM_VALUES": [
    "PRINTER_ENUM_VALUES", 
    "PRINTER_ENUM_VALUES[]"
  ], 
  "LPCAVRT_PRIORITY": [
    "IntByReference", 
    "int[]"
  ], 
  "PGRADIENT_TRIANGLE*": [
    "PointerByReference"
  ], 
  "PRAWKEYBOARD": [
    "RAWKEYBOARD", 
    "RAWKEYBOARD[]"
  ], 
  "LPCENUMLOGFONT": [
    "ENUMLOGFONT", 
    "ENUMLOGFONT[]"
  ], 
  "LPCRASTERIZER_STATUS": [
    "RASTERIZER_STATUS", 
    "RASTERIZER_STATUS[]"
  ], 
  "PFLASHWINFO": [
    "FLASHWINFO", 
    "FLASHWINFO[]"
  ], 
  "LPCJOB_INFO_4": [
    "JOB_INFO_4", 
    "JOB_INFO_4[]"
  ], 
  "LPCJOB_INFO_3": [
    "JOB_INFO_3", 
    "JOB_INFO_3[]"
  ], 
  "LPCJOB_INFO_2": [
    "JOB_INFO_2", 
    "JOB_INFO_2[]"
  ], 
  "LPCJOB_INFO_1": [
    "JOB_INFO_1", 
    "JOB_INFO_1[]"
  ], 
  "LPUSER_MODALS_INFO_3": [
    "USER_MODALS_INFO_3", 
    "USER_MODALS_INFO_3[]"
  ], 
  "LPWER_CONSENT*": [
    "PointerByReference"
  ], 
  "PPDH_RAW_COUNTER*": [
    "PointerByReference"
  ], 
  "PRINTPROCESSOR_INFO_1*": [
    "PRINTPROCESSOR_INFO_1", 
    "PRINTPROCESSOR_INFO_1[]"
  ], 
  "NEWTEXTMETRICEX**": [
    "PointerByReference"
  ], 
  "LPCSERVICE_FAILURE_ACTIONS": [
    "SERVICE_FAILURE_ACTIONS", 
    "SERVICE_FAILURE_ACTIONS[]"
  ], 
  "ULARGE_INTEGER*": [
    "ULARGE_INTEGER", 
    "ULARGE_INTEGER[]"
  ], 
  "ACCESS_MASK**": [
    "PointerByReference"
  ], 
  "LPKAFFINITY*": [
    "PointerByReference"
  ], 
  "PCOORD": [
    "COORD", 
    "COORD[]"
  ], 
  "MOUSEMOVEPOINT*": [
    "MOUSEMOVEPOINT", 
    "MOUSEMOVEPOINT[]"
  ], 
  "CWPSTRUCT**": [
    "PointerByReference"
  ], 
  "LPGROUP_USERS_INFO_1": [
    "GROUP_USERS_INFO_1", 
    "GROUP_USERS_INFO_1[]"
  ], 
  "LPCPRINTER_DEFAULTS": [
    "PRINTER_DEFAULTS", 
    "PRINTER_DEFAULTS[]"
  ], 
  "WINDOWPLACEMENT": [
    "WINDOWPLACEMENT"
  ], 
  "LPCCWPSTRUCT": [
    "CWPSTRUCT", 
    "CWPSTRUCT[]"
  ], 
  "LPTOGGLEKEYS*": [
    "PointerByReference"
  ], 
  "LPGEOID*": [
    "PointerByReference"
  ], 
  "LPWINDOWPLACEMENT*": [
    "PointerByReference"
  ], 
  "PLOCALGROUP_INFO_1002": [
    "LOCALGROUP_INFO_1002", 
    "LOCALGROUP_INFO_1002[]"
  ], 
  "LPINT": [
    "IntByReference", 
    "int[]"
  ], 
  "LPNETCONNECTINFOSTRUCT*": [
    "PointerByReference"
  ], 
  "LPCINPUT_RECORD": [
    "INPUT_RECORD", 
    "INPUT_RECORD[]"
  ], 
  "FILE_INFO_BY_HANDLE_CLASS**": [
    "PointerByReference"
  ], 
  "PFINDEX_SEARCH_OPS*": [
    "PointerByReference"
  ], 
  "UCHAR**": [
    "byte"
  ], 
  "SHELLFLAGSTATE**": [
    "PointerByReference"
  ], 
  "LPSESSION_INFO_10": [
    "SESSION_INFO_10", 
    "SESSION_INFO_10[]"
  ], 
  "PCOMMTIMEOUTS": [
    "COMMTIMEOUTS", 
    "COMMTIMEOUTS[]"
  ], 
  "LATENCY_TIME**": [
    "PointerByReference"
  ], 
  "LPSERVICE_LAUNCH_PROTECTED_INFO*": [
    "PointerByReference"
  ], 
  "PSERVICE_TRIGGER_INFO": [
    "SERVICE_TRIGGER_INFO", 
    "SERVICE_TRIGGER_INFO[]"
  ], 
  "CONSOLE_SCREEN_BUFFER_INFO": [
    "CONSOLE_SCREEN_BUFFER_INFO"
  ], 
  "PTIME_ZONE_INFORMATION": [
    "TIME_ZONE_INFORMATION", 
    "TIME_ZONE_INFORMATION[]"
  ], 
  "DISCDLGSTRUCT**": [
    "PointerByReference"
  ], 
  "NTMS_ALLOCATION_INFORMATION": [
    "NTMS_ALLOCATION_INFORMATION"
  ], 
  "LPCONNECTION_INFO_1": [
    "CONNECTION_INFO_1", 
    "CONNECTION_INFO_1[]"
  ], 
  "LPCONNECTION_INFO_0": [
    "CONNECTION_INFO_0", 
    "CONNECTION_INFO_0[]"
  ], 
  "PENHMETARECORD": [
    "ENHMETARECORD", 
    "ENHMETARECORD[]"
  ], 
  "LPUSER_INFO_1007": [
    "USER_INFO_1007", 
    "USER_INFO_1007[]"
  ], 
  "PTOGGLEKEYS": [
    "TOGGLEKEYS", 
    "TOGGLEKEYS[]"
  ], 
  "LPUSER_INFO_1014*": [
    "PointerByReference"
  ], 
  "SERVICE_PRESHUTDOWN_INFO**": [
    "PointerByReference"
  ], 
  "LPWCRANGE": [
    "WCRANGE", 
    "WCRANGE[]"
  ], 
  "PINPUT": [
    "INPUT", 
    "INPUT[]"
  ], 
  "TOKEN_DEFAULT_DACL": [
    "TOKEN_DEFAULT_DACL"
  ], 
  "PSHARE_INFO_1006*": [
    "PointerByReference"
  ], 
  "LPPDH_TIME_INFO*": [
    "PointerByReference"
  ], 
  "PDH_RAW_LOG_RECORD*": [
    "PDH_RAW_LOG_RECORD", 
    "PDH_RAW_LOG_RECORD[]"
  ], 
  "LPPDH_FMT_COUNTERVALUE_ITEM*": [
    "PointerByReference"
  ], 
  "BY_HANDLE_FILE_INFORMATION*": [
    "BY_HANDLE_FILE_INFORMATION", 
    "BY_HANDLE_FILE_INFORMATION[]"
  ], 
  "PWER_DUMP_TYPE": [
    "IntByReference", 
    "int[]"
  ], 
  "LPAUDIODESCRIPTION*": [
    "PointerByReference"
  ], 
  "PCONSOLE_FONT_INFOEX*": [
    "PointerByReference"
  ], 
  "PTRACE_LOGFILE_HEADER": [
    "TRACE_LOGFILE_HEADER", 
    "TRACE_LOGFILE_HEADER[]"
  ], 
  "PDFS_STORAGE_INFO": [
    "DFS_STORAGE_INFO", 
    "DFS_STORAGE_INFO[]"
  ], 
  "LPRGNDATA": [
    "RGNDATA", 
    "RGNDATA[]"
  ], 
  "INPUT_RECORD": [
    "INPUT_RECORD"
  ], 
  "COORD*": [
    "COORD", 
    "COORD[]"
  ], 
  "PLATENCY_TIME": [
    "IntByReference", 
    "int[]"
  ], 
  "GROUP_INFO_2": [
    "GROUP_INFO_2"
  ], 
  "LPINPUT_RECORD*": [
    "PointerByReference"
  ], 
  "PPSAPI_WS_WATCH_INFORMATION": [
    "PSAPI_WS_WATCH_INFORMATION", 
    "PSAPI_WS_WATCH_INFORMATION[]"
  ], 
  "LPOVERLAPPED*": [
    "PointerByReference"
  ], 
  "LPWKSTA_INFO_102": [
    "WKSTA_INFO_102", 
    "WKSTA_INFO_102[]"
  ], 
  "LPWKSTA_INFO_101": [
    "WKSTA_INFO_101", 
    "WKSTA_INFO_101[]"
  ], 
  "LPWKSTA_INFO_100": [
    "WKSTA_INFO_100", 
    "WKSTA_INFO_100[]"
  ], 
  "CURRENCYFMT*": [
    "CURRENCYFMT", 
    "CURRENCYFMT[]"
  ], 
  "SOUNDSENTRY*": [
    "SOUNDSENTRY", 
    "SOUNDSENTRY[]"
  ], 
  "LPSYSTEMTIME": [
    "SYSTEMTIME", 
    "SYSTEMTIME[]"
  ], 
  "DESIGNVECTOR": [
    "DESIGNVECTOR"
  ], 
  "PSESSION_INFO_502": [
    "SESSION_INFO_502", 
    "SESSION_INFO_502[]"
  ], 
  "PPDH_TIME_INFO*": [
    "PointerByReference"
  ], 
  "LPCPRINTER_INFO_1": [
    "PRINTER_INFO_1", 
    "PRINTER_INFO_1[]"
  ], 
  "LPCUSEROBJECTFLAGS": [
    "USEROBJECTFLAGS", 
    "USEROBJECTFLAGS[]"
  ], 
  "LPCABC": [
    "ABC", 
    "ABC[]"
  ], 
  "LPLPSTR": [
    "byte"
  ], 
  "PLPARAM": [
    "IntByReference", 
    "long[]"
  ], 
  "LPHFILE": [
    "IntByReference", 
    "int[]"
  ], 
  "MONITORINFOEX": [
    "MONITORINFOEX"
  ], 
  "MENU_EVENT_RECORD**": [
    "PointerByReference"
  ], 
  "GLYPHMETRICS": [
    "GLYPHMETRICS"
  ], 
  "PJOBOBJECT_RATE_CONTROL_TOLERANCE_INTERVAL*": [
    "PointerByReference"
  ], 
  "WINDOWPLACEMENT**": [
    "PointerByReference"
  ], 
  "SHELLFLAGSTATE*": [
    "SHELLFLAGSTATE", 
    "SHELLFLAGSTATE[]"
  ], 
  "MSLLHOOKSTRUCT**": [
    "PointerByReference"
  ], 
  "PRINTER_NOTIFY_INFO_DATA": [
    "PRINTER_NOTIFY_INFO_DATA"
  ], 
  "LPDISPLAY_DEVICE": [
    "DISPLAY_DEVICE", 
    "DISPLAY_DEVICE[]"
  ], 
  "LPCDFS_INFO_7": [
    "DFS_INFO_7", 
    "DFS_INFO_7[]"
  ], 
  "LPCDFS_INFO_6": [
    "DFS_INFO_6", 
    "DFS_INFO_6[]"
  ], 
  "LPCDFS_INFO_5": [
    "DFS_INFO_5", 
    "DFS_INFO_5[]"
  ], 
  "float": [
    "float"
  ], 
  "LPCDFS_INFO_3": [
    "DFS_INFO_3", 
    "DFS_INFO_3[]"
  ], 
  "LPCDFS_INFO_2": [
    "DFS_INFO_2", 
    "DFS_INFO_2[]"
  ], 
  "LPCDFS_INFO_1": [
    "DFS_INFO_1", 
    "DFS_INFO_1[]"
  ], 
  "USER_INFO_1024*": [
    "USER_INFO_1024", 
    "USER_INFO_1024[]"
  ], 
  "LPCGEOCLASS*": [
    "IntByReference", 
    "long[]"
  ], 
  "LPCTOKEN_GROUPS": [
    "TOKEN_GROUPS", 
    "TOKEN_GROUPS[]"
  ], 
  "LPCPRINTER_INFO_7": [
    "PRINTER_INFO_7", 
    "PRINTER_INFO_7[]"
  ], 
  "TRACE_LOGFILE_HEADER*": [
    "TRACE_LOGFILE_HEADER", 
    "TRACE_LOGFILE_HEADER[]"
  ], 
  "LPCDFS_INFO_9": [
    "DFS_INFO_9", 
    "DFS_INFO_9[]"
  ], 
  "LPCDFS_INFO_8": [
    "DFS_INFO_8", 
    "DFS_INFO_8[]"
  ], 
  "GROUP_INFO_1005*": [
    "GROUP_INFO_1005", 
    "GROUP_INFO_1005[]"
  ], 
  "EMRALPHABLEND": [
    "EMRALPHABLEND"
  ], 
  "SERVICE_PREFERRED_NODE_INFO": [
    "SERVICE_PREFERRED_NODE_INFO"
  ], 
  "LPCPRINTER_INFO_9": [
    "PRINTER_INFO_9", 
    "PRINTER_INFO_9[]"
  ], 
  "LPCOMSTAT": [
    "COMSTAT", 
    "COMSTAT[]"
  ], 
  "PDFS_INFO_4*": [
    "PointerByReference"
  ], 
  "PRINTER_DEFAULTS**": [
    "PointerByReference"
  ], 
  "PBITMAPINFO": [
    "BITMAPINFO", 
    "BITMAPINFO[]"
  ], 
  "LPPRINTER_INFO_2*": [
    "PointerByReference"
  ], 
  "USER_INFO_10*": [
    "USER_INFO_10", 
    "USER_INFO_10[]"
  ], 
  "XFORM**": [
    "PointerByReference"
  ], 
  "LPCRGNDATA": [
    "RGNDATA", 
    "RGNDATA[]"
  ], 
  "MONITORINFO**": [
    "PointerByReference"
  ], 
  "PROCESSENTRY32": [
    "PROCESSENTRY32"
  ], 
  "LPWIN32_STREAM_ID": [
    "WIN32_STREAM_ID", 
    "WIN32_STREAM_ID[]"
  ], 
  "JOBOBJECT_EXTENDED_LIMIT_INFORMATION*": [
    "JOBOBJECT_EXTENDED_LIMIT_INFORMATION", 
    "JOBOBJECT_EXTENDED_LIMIT_INFORMATION[]"
  ], 
  "PMOUSEHOOKSTRUCT": [
    "MOUSEHOOKSTRUCT", 
    "MOUSEHOOKSTRUCT[]"
  ], 
  "GROUP_USERS_INFO_0*": [
    "GROUP_USERS_INFO_0", 
    "GROUP_USERS_INFO_0[]"
  ], 
  "DISPLAY_DEVICE*": [
    "DISPLAY_DEVICE", 
    "DISPLAY_DEVICE[]"
  ], 
  "PPORT_INFO_3*": [
    "PointerByReference"
  ], 
  "PCOMSTAT": [
    "COMSTAT", 
    "COMSTAT[]"
  ], 
  "SIGDN": [
    "int"
  ], 
  "LPTRACE_GUID_REGISTRATION*": [
    "PointerByReference"
  ], 
  "LPJOBOBJECT_RATE_CONTROL_TOLERANCE_INTERVAL": [
    "IntByReference", 
    "int[]"
  ], 
  "CONSOLE_FONT_INFOEX*": [
    "CONSOLE_FONT_INFOEX", 
    "CONSOLE_FONT_INFOEX[]"
  ], 
  "PPRINTPROCESSOR_INFO_1*": [
    "PointerByReference"
  ], 
  "LOGPEN*": [
    "LOGPEN", 
    "LOGPEN[]"
  ], 
  "LPBATTERY_REPORTING_SCALE*": [
    "PointerByReference"
  ], 
  "ULONG32": [
    "int"
  ], 
  "PDFS_INFO_103*": [
    "PointerByReference"
  ], 
  "LPCDFS_INFO_300": [
    "DFS_INFO_300", 
    "DFS_INFO_300[]"
  ], 
  "LPCSYSGEOCLASS": [
    "IntByReference", 
    "int[]"
  ], 
  "NET_DISPLAY_USER**": [
    "PointerByReference"
  ], 
  "FILE_INFO_2**": [
    "PointerByReference"
  ], 
  "ICONINFO*": [
    "ICONINFO", 
    "ICONINFO[]"
  ], 
  "LPSTR*": [
    "String[]", 
    "PointerByReference"
  ], 
  "USER_INFO_1010*": [
    "USER_INFO_1010", 
    "USER_INFO_1010[]"
  ], 
  "USER_INFO_4**": [
    "PointerByReference"
  ], 
  "STICKYKEYS": [
    "STICKYKEYS"
  ], 
  "RAWINPUTDEVICELIST*": [
    "RAWINPUTDEVICELIST", 
    "RAWINPUTDEVICELIST[]"
  ], 
  "LPCNET_DISPLAY_USER": [
    "NET_DISPLAY_USER", 
    "NET_DISPLAY_USER[]"
  ], 
  "LPREMOTE_NAME_INFO*": [
    "PointerByReference"
  ], 
  "COMMPROP": [
    "COMMPROP"
  ], 
  "PHDDEDATA": [
    "HANDLEByReference"
  ], 
  "LPCMEMORY_BASIC_INFORMATION": [
    "MEMORY_BASIC_INFORMATION", 
    "MEMORY_BASIC_INFORMATION[]"
  ], 
  "LPCURRENCYFMT*": [
    "PointerByReference"
  ], 
  "PDFS_INFO_150*": [
    "PointerByReference"
  ], 
  "OVERLAPPED*": [
    "OVERLAPPED", 
    "OVERLAPPED[]"
  ], 
  "RAWHID*": [
    "RAWHID", 
    "RAWHID[]"
  ], 
  "PFILE_SEGMENT_ELEMENT*": [
    "PointerByReference"
  ], 
  "PRID_DEVICE_INFO_KEYBOARD*": [
    "PointerByReference"
  ], 
  "PDATATYPES_INFO_1": [
    "DATATYPES_INFO_1", 
    "DATATYPES_INFO_1[]"
  ], 
  "PLOGPEN": [
    "LOGPEN", 
    "LOGPEN[]"
  ], 
  "UINT**": [
    "PointerByReference"
  ], 
  "LPCWPSTRUCT": [
    "CWPSTRUCT", 
    "CWPSTRUCT[]"
  ], 
  "PDFS_STORAGE_INFO_1*": [
    "PointerByReference"
  ], 
  "LPMENU_EVENT_RECORD*": [
    "PointerByReference"
  ], 
  "LPHFILE*": [
    "PointerByReference"
  ], 
  "LPCCONSOLE_CURSOR_INFO": [
    "CONSOLE_CURSOR_INFO", 
    "CONSOLE_CURSOR_INFO[]"
  ], 
  "PRINTER_INFO_7**": [
    "PointerByReference"
  ], 
  "JOBOBJECT_BASIC_LIMIT_INFORMATION": [
    "JOBOBJECT_BASIC_LIMIT_INFORMATION"
  ], 
  "PCONNECTION_INFO_1*": [
    "PointerByReference"
  ], 
  "EFS_HASH_BLOB*": [
    "EFS_HASH_BLOB", 
    "EFS_HASH_BLOB[]"
  ], 
  "CONVCONTEXT**": [
    "PointerByReference"
  ], 
  "PHWINSTA": [
    "HANDLEByReference"
  ], 
  "LPLOCALGROUP_INFO_1": [
    "LOCALGROUP_INFO_1", 
    "LOCALGROUP_INFO_1[]"
  ], 
  "LPLOCALGROUP_INFO_0": [
    "LOCALGROUP_INFO_0", 
    "LOCALGROUP_INFO_0[]"
  ], 
  "PPRIVILEGE_SET": [
    "PRIVILEGE_SET", 
    "PRIVILEGE_SET[]"
  ], 
  "PRINTER_INFO_5**": [
    "PointerByReference"
  ], 
  "PCOMMPROP*": [
    "PointerByReference"
  ], 
  "USER_INFO_1007*": [
    "USER_INFO_1007", 
    "USER_INFO_1007[]"
  ], 
  "PSYSTEM_INFO*": [
    "PointerByReference"
  ], 
  "PGEOTYPE*": [
    "IntByReference", 
    "long[]"
  ], 
  "PULONG32*": [
    "PointerByReference"
  ], 
  "PHINSTANCE": [
    "HANDLEByReference"
  ], 
  "LPMENUEX_TEMPLATE_ITEM*": [
    "PointerByReference"
  ], 
  "PAT_INFO*": [
    "PointerByReference"
  ], 
  "LPCACL_INFORMATION_CLASS": [
    "IntByReference", 
    "int[]"
  ], 
  "PSHSTOCKICONINFO*": [
    "PointerByReference"
  ], 
  "LPCOPENASINFO": [
    "OPENASINFO", 
    "OPENASINFO[]"
  ], 
  "PMEMORYSTATUSEX": [
    "MEMORYSTATUSEX", 
    "MEMORYSTATUSEX[]"
  ], 
  "DRAWTEXTPARAMS*": [
    "DRAWTEXTPARAMS", 
    "DRAWTEXTPARAMS[]"
  ], 
  "DCB**": [
    "PointerByReference"
  ], 
  "MENUITEMINFO*": [
    "MENUITEMINFO", 
    "MENUITEMINFO[]"
  ], 
  "PDH_DATA_ITEM_PATH_ELEMENTS**": [
    "PointerByReference"
  ], 
  "LPWSAPROTOCOLCHAIN*": [
    "PointerByReference"
  ], 
  "PWMIDPREQUESTCODE": [
    "IntByReference", 
    "int[]"
  ], 
  "PPOWER_POLICY": [
    "POWER_POLICY", 
    "POWER_POLICY[]"
  ], 
  "OVERLAPPED": [
    "OVERLAPPED"
  ], 
  "PWKSTA_USER_INFO_1": [
    "WKSTA_USER_INFO_1", 
    "WKSTA_USER_INFO_1[]"
  ], 
  "NETCONNECTINFOSTRUCT**": [
    "PointerByReference"
  ], 
  "LPSHSTOCKICONID*": [
    "PointerByReference"
  ], 
  "LPUSER_INFO_1007*": [
    "PointerByReference"
  ], 
  "PSERVICE_DESCRIPTION*": [
    "PointerByReference"
  ], 
  "PUSER_INFO_20": [
    "USER_INFO_20", 
    "USER_INFO_20[]"
  ], 
  "PUSER_INFO_21": [
    "USER_INFO_21", 
    "USER_INFO_21[]"
  ], 
  "PUSER_INFO_22": [
    "USER_INFO_22", 
    "USER_INFO_22[]"
  ], 
  "PUSER_INFO_23": [
    "USER_INFO_23", 
    "USER_INFO_23[]"
  ], 
  "LPCCONVCONTEXT": [
    "CONVCONTEXT", 
    "CONVCONTEXT[]"
  ], 
  "PDH_FMT_COUNTERVALUE_ITEM*": [
    "PDH_FMT_COUNTERVALUE_ITEM", 
    "PDH_FMT_COUNTERVALUE_ITEM[]"
  ], 
  "DOCINFO**": [
    "PointerByReference"
  ], 
  "LPCDYNAMIC_TIME_ZONE_INFORMATION": [
    "DYNAMIC_TIME_ZONE_INFORMATION", 
    "DYNAMIC_TIME_ZONE_INFORMATION[]"
  ], 
  "KEY_INFORMATION_CLASS": [
    "int"
  ], 
  "LPCCPINFO": [
    "CPINFO", 
    "CPINFO[]"
  ], 
  "wchar_t*": [
    "char"
  ], 
  "LPPOWER_POLICY*": [
    "PointerByReference"
  ], 
  "LPPRINTER_INFO_9*": [
    "PointerByReference"
  ], 
  "USER_INFO_1024": [
    "USER_INFO_1024"
  ], 
  "WER_REGISTER_FILE_TYPE": [
    "int"
  ], 
  "AT_ENUM": [
    "AT_ENUM"
  ], 
  "PSHELLFLAGSTATE*": [
    "PointerByReference"
  ], 
  "USER_INFO_1020": [
    "USER_INFO_1020"
  ], 
  "LPLMSTR": [
    "char"
  ], 
  "RGNDATA*": [
    "RGNDATA", 
    "RGNDATA[]"
  ], 
  "LPUSER_POWER_POLICY": [
    "USER_POWER_POLICY", 
    "USER_POWER_POLICY[]"
  ], 
  "RECT**": [
    "PointerByReference"
  ], 
  "PHDWP": [
    "HANDLEByReference"
  ], 
  "LPKBDLLHOOKSTRUCT*": [
    "PointerByReference"
  ], 
  "LPEVENTLOGRECORD": [
    "EVENTLOGRECORD", 
    "EVENTLOGRECORD[]"
  ], 
  "LPUSER_INFO_1009*": [
    "PointerByReference"
  ], 
  "SERVER_INFO_403*": [
    "SERVER_INFO_403", 
    "SERVER_INFO_403[]"
  ], 
  "LPJOBOBJECT_BASIC_AND_IO_ACCOUNTING_INFORMATION": [
    "JOBOBJECT_BASIC_AND_IO_ACCOUNTING_INFORMATION", 
    "JOBOBJECT_BASIC_AND_IO_ACCOUNTING_INFORMATION[]"
  ], 
  "SIGDN*": [
    "IntByReference", 
    "int[]"
  ], 
  "LPCSYSTEM_POWER_STATUS": [
    "SYSTEM_POWER_STATUS", 
    "SYSTEM_POWER_STATUS[]"
  ], 
  "LPSTAT_SERVER_0*": [
    "PointerByReference"
  ], 
  "LPTRIVERTEX*": [
    "PointerByReference"
  ], 
  "LPTOKEN_DEFAULT_DACL": [
    "TOKEN_DEFAULT_DACL", 
    "TOKEN_DEFAULT_DACL[]"
  ], 
  "LPHMETAFILE": [
    "HANDLEByReference"
  ], 
  "WNODE_HEADER": [
    "WNODE_HEADER"
  ], 
  "LPSID_AND_ATTRIBUTES*": [
    "PointerByReference"
  ], 
  "PDATATYPES_INFO_1*": [
    "PointerByReference"
  ], 
  "CURRENCYFMT**": [
    "PointerByReference"
  ], 
  "PEVENT_INSTANCE_HEADER": [
    "EVENT_INSTANCE_HEADER", 
    "EVENT_INSTANCE_HEADER[]"
  ], 
  "WKSTA_USER_INFO_1101*": [
    "WKSTA_USER_INFO_1101", 
    "WKSTA_USER_INFO_1101[]"
  ], 
  "PJOBOBJECT_LIMIT_VIOLATION_INFORMATION": [
    "JOBOBJECT_LIMIT_VIOLATION_INFORMATION", 
    "JOBOBJECT_LIMIT_VIOLATION_INFORMATION[]"
  ], 
  "LPCCWPRETSTRUCT": [
    "CWPRETSTRUCT", 
    "CWPRETSTRUCT[]"
  ], 
  "LPJOBOBJECT_RATE_CONTROL_TOLERANCE": [
    "IntByReference", 
    "int[]"
  ], 
  "WKSTA_TRANSPORT_INFO_0": [
    "WKSTA_TRANSPORT_INFO_0"
  ], 
  "LPCRM_UNIQUE_PROCESS": [
    "RM_UNIQUE_PROCESS", 
    "RM_UNIQUE_PROCESS[]"
  ], 
  "THREADENTRY32": [
    "THREADENTRY32"
  ], 
  "LPCONSOLE_FONT_INFO": [
    "CONSOLE_FONT_INFO", 
    "CONSOLE_FONT_INFO[]"
  ], 
  "QUERY_USER_NOTIFICATION_STATE*": [
    "IntByReference", 
    "int[]"
  ], 
  "STICKYKEYS**": [
    "PointerByReference"
  ], 
  "DFS_INFO_300**": [
    "PointerByReference"
  ], 
  "LPCPDH_RAW_LOG_RECORD": [
    "PDH_RAW_LOG_RECORD", 
    "PDH_RAW_LOG_RECORD[]"
  ], 
  "LPMOUSE_EVENT_RECORD": [
    "MOUSE_EVENT_RECORD", 
    "MOUSE_EVENT_RECORD[]"
  ], 
  "LPSYSTEM_INFO": [
    "SYSTEM_INFO", 
    "SYSTEM_INFO[]"
  ], 
  "PSERVER_INFO_402": [
    "SERVER_INFO_402", 
    "SERVER_INFO_402[]"
  ], 
  "LPCDFS_INFO_200": [
    "DFS_INFO_200", 
    "DFS_INFO_200[]"
  ], 
  "LPFILETIME*": [
    "PointerByReference"
  ], 
  "SHORT*": [
    "ShortByReference", 
    "short[]"
  ], 
  "DATATYPES_INFO_1*": [
    "DATATYPES_INFO_1", 
    "DATATYPES_INFO_1[]"
  ], 
  "LPCHAR_INFO*": [
    "PointerByReference"
  ], 
  "WSAPROTOCOL_INFO": [
    "WSAPROTOCOL_INFO"
  ], 
  "SCROLLBARINFO": [
    "SCROLLBARINFO"
  ], 
  "PCOLORREF*": [
    "PointerByReference"
  ], 
  "LPMODULEENTRY32": [
    "MODULEENTRY32", 
    "MODULEENTRY32[]"
  ], 
  "LPCLIENTCREATESTRUCT*": [
    "PointerByReference"
  ], 
  "LPGROUP_INFO_0*": [
    "PointerByReference"
  ], 
  "LPMEMORYSTATUSEX*": [
    "PointerByReference"
  ], 
  "PUSER_INFO_1005*": [
    "PointerByReference"
  ], 
  "DFS_INFO_50*": [
    "DFS_INFO_50", 
    "DFS_INFO_50[]"
  ], 
  "LPUSER_INFO_23*": [
    "PointerByReference"
  ], 
  "PWKSTA_INFO_100": [
    "WKSTA_INFO_100", 
    "WKSTA_INFO_100[]"
  ], 
  "PWKSTA_INFO_101": [
    "WKSTA_INFO_101", 
    "WKSTA_INFO_101[]"
  ], 
  "PWKSTA_INFO_102": [
    "WKSTA_INFO_102", 
    "WKSTA_INFO_102[]"
  ], 
  "LPACCESS_MASK": [
    "IntByReference", 
    "long[]"
  ], 
  "SERVER_INFO_403**": [
    "PointerByReference"
  ], 
  "TAPE_GET_MEDIA_PARAMETERS**": [
    "PointerByReference"
  ], 
  "PUINT64*": [
    "PointerByReference"
  ], 
  "MOUSEKEYS": [
    "MOUSEKEYS"
  ], 
  "PSSIZE_T*": [
    "PointerByReference"
  ], 
  "LPAT_ENUM*": [
    "PointerByReference"
  ], 
  "FLOAT*": [
    "FloatByReference", 
    "float[]"
  ], 
  "LPDOCINFO": [
    "DOCINFO", 
    "DOCINFO[]"
  ], 
  "CWPRETSTRUCT": [
    "CWPRETSTRUCT"
  ], 
  "WELL_KNOWN_SID_TYPE": [
    "int"
  ], 
  "LPSTAT_SERVER_0": [
    "STAT_SERVER_0", 
    "STAT_SERVER_0[]"
  ], 
  "PMSG*": [
    "PointerByReference"
  ], 
  "LPCSID_AND_ATTRIBUTES": [
    "SID_AND_ATTRIBUTES", 
    "SID_AND_ATTRIBUTES[]"
  ], 
  "SMALL_RECT": [
    "SMALL_RECT"
  ], 
  "BOOLEAN": [
    "byte"
  ], 
  "PALETTEENTRY*": [
    "PALETTEENTRY", 
    "PALETTEENTRY[]"
  ], 
  "SESSION_INFO_502": [
    "SESSION_INFO_502"
  ], 
  "SYSTEM_BATTERY_STATE*": [
    "SYSTEM_BATTERY_STATE", 
    "SYSTEM_BATTERY_STATE[]"
  ], 
  "FINDEX_SEARCH_OPS*": [
    "IntByReference", 
    "int[]"
  ], 
  "PERFORMANCE_INFORMATION": [
    "PERFORMANCE_INFORMATION"
  ], 
  "AVRT_PRIORITY": [
    "int"
  ], 
  "PUSER_MODALS_INFO_1007*": [
    "PointerByReference"
  ], 
  "LPPRINTER_ENUM_VALUES*": [
    "PointerByReference"
  ], 
  "LPUINT*": [
    "PointerByReference"
  ], 
  "LPRAWINPUTDEVICE*": [
    "PointerByReference"
  ], 
  "PKEY_EVENT_RECORD": [
    "KEY_EVENT_RECORD", 
    "KEY_EVENT_RECORD[]"
  ], 
  "LPCCHAR*": [
    "byte"
  ], 
  "BITMAPINFO**": [
    "PointerByReference"
  ], 
  "LPRID_DEVICE_INFO*": [
    "PointerByReference"
  ], 
  "TAPE_SET_MEDIA_PARAMETERS": [
    "TAPE_SET_MEDIA_PARAMETERS"
  ], 
  "CONSOLE_SCREEN_BUFFER_INFOEX": [
    "CONSOLE_SCREEN_BUFFER_INFOEX"
  ], 
  "LANGID": [
    "short"
  ], 
  "FORMATETC": [
    "FORMATETC"
  ], 
  "PQUERY_SERVICE_CONFIG*": [
    "PointerByReference"
  ], 
  "LPSTR**": [
    "byte"
  ], 
  "LPPCTSTR": [
    "char"
  ], 
  "OUTLINETEXTMETRIC*": [
    "OUTLINETEXTMETRIC", 
    "OUTLINETEXTMETRIC[]"
  ], 
  "LPCBLENDFUNCTION": [
    "BLENDFUNCTION", 
    "BLENDFUNCTION[]"
  ], 
  "LPKEY_EVENT_RECORD*": [
    "PointerByReference"
  ], 
  "WKSTA_TRANSPORT_INFO_0**": [
    "PointerByReference"
  ], 
  "PLPGEOTYPE*": [
    "IntByReference", 
    "long[]"
  ], 
  "SYSTEM_POWER_POLICY": [
    "SYSTEM_POWER_POLICY"
  ], 
  "PASSOC_FILTER*": [
    "PointerByReference"
  ], 
  "PWKSTA_TRANSPORT_INFO_0": [
    "WKSTA_TRANSPORT_INFO_0", 
    "WKSTA_TRANSPORT_INFO_0[]"
  ], 
  "PBATTERY_REPORTING_SCALE*": [
    "PointerByReference"
  ], 
  "LPPRINTER_INFO_3*": [
    "PointerByReference"
  ], 
  "USHORT*": [
    "ShortByReference", 
    "short[]"
  ], 
  "HDC*": [
    "HANDLEByReference"
  ], 
  "RID_DEVICE_INFO_MOUSE": [
    "RID_DEVICE_INFO_MOUSE"
  ], 
  "RID_DEVICE_INFO_KEYBOARD*": [
    "RID_DEVICE_INFO_KEYBOARD", 
    "RID_DEVICE_INFO_KEYBOARD[]"
  ], 
  "INT*": [
    "IntByReference", 
    "int[]"
  ], 
  "LPOVERLAPPED": [
    "OVERLAPPED", 
    "OVERLAPPED[]", 
    "Pointer"
  ], 
  "LPINPUT_RECORD": [
    "INPUT_RECORD", 
    "INPUT_RECORD[]"
  ], 
  "LPPDH_RAW_COUNTER_ITEM*": [
    "PointerByReference"
  ], 
  "UINT8*": [
    "byte"
  ], 
  "LONGLONG*": [
    "DoubleByReference", 
    "double[]"
  ], 
  "LPHBRUSH": [
    "HANDLEByReference"
  ], 
  "INT8": [
    "byte"
  ], 
  "OSVERSIONINFOEX**": [
    "PointerByReference"
  ], 
  "LPHCURSOR": [
    "HANDLEByReference"
  ], 
  "LPSYSTEM_POWER_LEVEL": [
    "SYSTEM_POWER_LEVEL", 
    "SYSTEM_POWER_LEVEL[]"
  ], 
  "SERVICE_DELAYED_AUTO_START_INFO": [
    "SERVICE_DELAYED_AUTO_START_INFO"
  ], 
  "JOB_INFO_2*": [
    "JOB_INFO_2", 
    "JOB_INFO_2[]"
  ], 
  "SERVICE_STATUS_HANDLE*": [
    "HANDLEByReference"
  ], 
  "LPRM_APP_TYPE": [
    "IntByReference", 
    "int[]"
  ], 
  "LPWER_DUMP_CUSTOM_OPTIONS*": [
    "PointerByReference"
  ], 
  "PLONG32*": [
    "PointerByReference"
  ], 
  "LPRAWINPUTDEVICELIST*": [
    "PointerByReference"
  ], 
  "PWER_DUMP_CUSTOM_OPTIONS": [
    "WER_DUMP_CUSTOM_OPTIONS", 
    "WER_DUMP_CUSTOM_OPTIONS[]"
  ], 
  "PHDROP": [
    "HANDLEByReference"
  ], 
  "FORMATETC*": [
    "FORMATETC", 
    "FORMATETC[]"
  ], 
  "LPSECURITY_ATTRIBUTES*": [
    "PointerByReference"
  ], 
  "PGLYPHSET*": [
    "PointerByReference"
  ], 
  "FIXED": [
    "FIXED"
  ], 
  "IShellFolder*": [
    "PointerByReference"
  ], 
  "PTOKEN_PRIVILEGES*": [
    "PointerByReference"
  ], 
  "PUSEROBJECTFLAGS*": [
    "PointerByReference"
  ], 
  "LPSOUNDSENTRY": [
    "SOUNDSENTRY", 
    "SOUNDSENTRY[]"
  ], 
  "LPCONSOLE_HISTORY_INFO*": [
    "PointerByReference"
  ], 
  "PRINTER_NOTIFY_OPTIONS_TYPE**": [
    "PointerByReference"
  ], 
  "CURSORINFO**": [
    "PointerByReference"
  ], 
  "SHITEMID**": [
    "PointerByReference"
  ], 
  "DFS_INFO_150*": [
    "DFS_INFO_150", 
    "DFS_INFO_150[]"
  ], 
  "PSHQUERYRBINFO*": [
    "PointerByReference"
  ], 
  "LPCENUM_SERVICE_STATUS_PROCESS": [
    "ENUM_SERVICE_STATUS_PROCESS", 
    "ENUM_SERVICE_STATUS_PROCESS[]"
  ], 
  "CHAR**": [
    "byte"
  ], 
  "PLOCALGROUP_MEMBERS_INFO_0*": [
    "PointerByReference"
  ], 
  "LPStringTable": [
    "StringTable", 
    "StringTable[]"
  ], 
  "PDH_COUNTER_PATH_ELEMENTS*": [
    "PDH_COUNTER_PATH_ELEMENTS", 
    "PDH_COUNTER_PATH_ELEMENTS[]"
  ], 
  "JOB_INFO_4*": [
    "JOB_INFO_4", 
    "JOB_INFO_4[]"
  ], 
  "LOCALGROUP_INFO_1002**": [
    "PointerByReference"
  ], 
  "LPSERVICE_DESCRIPTION*": [
    "PointerByReference"
  ], 
  "RAWMOUSE": [
    "RAWMOUSE"
  ], 
  "LPRECT": [
    "RECT", 
    "RECT[]"
  ], 
  "LPDFS_STORAGE_INFO_1*": [
    "PointerByReference"
  ], 
  "LPSESSION_INFO_502*": [
    "PointerByReference"
  ], 
  "COLORADJUSTMENT": [
    "COLORADJUSTMENT"
  ], 
  "STREAM_INFO_LEVELS": [
    "int"
  ], 
  "WINDOWINFO**": [
    "PointerByReference"
  ], 
  "PMOUSEMOVEPOINT": [
    "MOUSEMOVEPOINT", 
    "MOUSEMOVEPOINT[]"
  ], 
  "LPENHMETAHEADER*": [
    "PointerByReference"
  ], 
  "LPWINDOWINFO": [
    "WINDOWINFO", 
    "WINDOWINFO[]"
  ], 
  "MENUITEMTEMPLATEHEADER**": [
    "PointerByReference"
  ], 
  "MACHINE_PROCESSOR_POWER_POLICY*": [
    "MACHINE_PROCESSOR_POWER_POLICY", 
    "MACHINE_PROCESSOR_POWER_POLICY[]"
  ], 
  "PSYSTEM_POWER_CAPABILITIES*": [
    "PointerByReference"
  ], 
  "PPDH_STATISTICS*": [
    "PointerByReference"
  ], 
  "LCID**": [
    "PointerByReference"
  ], 
  "STARTUPINFO**": [
    "PointerByReference"
  ], 
  "LPMENUEX_TEMPLATE_ITEM": [
    "MENUEX_TEMPLATE_ITEM", 
    "MENUEX_TEMPLATE_ITEM[]"
  ], 
  "SERVICE_PREFERRED_NODE_INFO*": [
    "SERVICE_PREFERRED_NODE_INFO", 
    "SERVICE_PREFERRED_NODE_INFO[]"
  ], 
  "LPDEBUGHOOKINFO*": [
    "PointerByReference"
  ], 
  "PSERVICE_TRIGGER*": [
    "PointerByReference"
  ], 
  "LPCLDT_ENTRY": [
    "LDT_ENTRY", 
    "LDT_ENTRY[]"
  ], 
  "LPPROVIDOR_INFO_1": [
    "PROVIDOR_INFO_1", 
    "PROVIDOR_INFO_1[]"
  ], 
  "LPPROVIDOR_INFO_2": [
    "PROVIDOR_INFO_2", 
    "PROVIDOR_INFO_2[]"
  ], 
  "LPCFINDEX_SEARCH_OPS": [
    "IntByReference", 
    "int[]"
  ], 
  "LPCSERVER_INFO_402": [
    "SERVER_INFO_402", 
    "SERVER_INFO_402[]"
  ], 
  "LPCSERVER_INFO_403": [
    "SERVER_INFO_403", 
    "SERVER_INFO_403[]"
  ], 
  "PWCT_OBJECT_STATUS*": [
    "PointerByReference"
  ], 
  "PENUMLOGFONT": [
    "ENUMLOGFONT", 
    "ENUMLOGFONT[]"
  ], 
  "LPCSHQUERYRBINFO": [
    "SHQUERYRBINFO", 
    "SHQUERYRBINFO[]"
  ], 
  "SHARE_INFO_503": [
    "SHARE_INFO_503"
  ], 
  "LPPROPERTYKEY*": [
    "PointerByReference"
  ], 
  "POPENASINFO*": [
    "PointerByReference"
  ], 
  "PTSTR": [
    "CHARByReference", 
    "wchar_t[]", 
    "WString", 
    "String"
  ], 
  "LPCTAPE_SET_MEDIA_PARAMETERS": [
    "TAPE_SET_MEDIA_PARAMETERS", 
    "TAPE_SET_MEDIA_PARAMETERS[]"
  ], 
  "LPUINT16": [
    "ShortByReference", 
    "short[]"
  ], 
  "RECTL**": [
    "PointerByReference"
  ], 
  "SMALL_RECT*": [
    "SMALL_RECT", 
    "SMALL_RECT[]"
  ], 
  "USE_INFO_1*": [
    "USE_INFO_1", 
    "USE_INFO_1[]"
  ], 
  "LPHWND": [
    "HANDLEByReference"
  ], 
  "LPTIME_ZONE_INFORMATION": [
    "TIME_ZONE_INFORMATION", 
    "TIME_ZONE_INFORMATION[]"
  ], 
  "LPPANOSE": [
    "PANOSE", 
    "PANOSE[]"
  ], 
  "HEAP_INFORMATION_CLASS*": [
    "IntByReference", 
    "int[]"
  ], 
  "LPLRESULT*": [
    "PointerByReference"
  ], 
  "LPTOGGLEKEYS": [
    "TOGGLEKEYS", 
    "TOGGLEKEYS[]"
  ], 
  "TRACKMOUSEEVENT*": [
    "TRACKMOUSEEVENT", 
    "TRACKMOUSEEVENT[]"
  ], 
  "LPCGLYPHMETRICS": [
    "GLYPHMETRICS", 
    "GLYPHMETRICS[]"
  ], 
  "LPNDDESHAREINFO": [
    "NDDESHAREINFO", 
    "NDDESHAREINFO[]"
  ], 
  "ULONG64*": [
    "LongByReference", 
    "__int64[]"
  ], 
  "LPTOP_LEVEL_EXCEPTION_FILTER": [
    "Callback"
  ], 
  "PNETRESOURCE*": [
    "PointerByReference"
  ], 
  "PFORMATETC*": [
    "PointerByReference"
  ], 
  "LPUSER_OTHER_INFO*": [
    "PointerByReference"
  ], 
  "PTAPE_SET_DRIVE_PARAMETERS*": [
    "PointerByReference"
  ], 
  "PMENUITEMTEMPLATE": [
    "MENUITEMTEMPLATE", 
    "MENUITEMTEMPLATE[]"
  ], 
  "LPUINT8*": [
    "byte"
  ], 
  "LPINPUT": [
    "INPUT", 
    "INPUT[]"
  ], 
  "LPCGRADIENT_RECT": [
    "GRADIENT_RECT", 
    "GRADIENT_RECT[]"
  ], 
  "LPCRM_FILTER_ACTION": [
    "IntByReference", 
    "int[]"
  ], 
  "LPCDFS_TARGET_PRIORITY_CLASS": [
    "IntByReference", 
    "int[]"
  ], 
  "PSHARE_INFO_1*": [
    "PointerByReference"
  ], 
  "LPCAT_ENUM": [
    "AT_ENUM", 
    "AT_ENUM[]"
  ], 
  "LPUMS_CREATE_THREAD_ATTRIBUTES": [
    "UMS_CREATE_THREAD_ATTRIBUTES", 
    "UMS_CREATE_THREAD_ATTRIBUTES[]"
  ], 
  "LPCWER_REGISTER_FILE_TYPE": [
    "IntByReference", 
    "int[]"
  ], 
  "ACCESS_MASK": [
    "int"
  ], 
  "PSTICKYKEYS": [
    "STICKYKEYS", 
    "STICKYKEYS[]"
  ], 
  "LPGLOBAL_USER_POWER_POLICY": [
    "GLOBAL_USER_POWER_POLICY", 
    "GLOBAL_USER_POWER_POLICY[]"
  ], 
  "short**": [
    "PointerByReference"
  ], 
  "USER_INFO_1012**": [
    "PointerByReference"
  ], 
  "LPCPALETTEENTRY": [
    "PALETTEENTRY", 
    "PALETTEENTRY[]"
  ], 
  "JOBOBJECT_END_OF_JOB_TIME_INFORMATION**": [
    "PointerByReference"
  ], 
  "LPCOMBOBOXINFO": [
    "COMBOBOXINFO", 
    "COMBOBOXINFO[]"
  ], 
  "LPCOORD": [
    "COORD", 
    "COORD[]"
  ], 
  "LPCURSORINFO*": [
    "PointerByReference"
  ], 
  "LPCURRENCYFMT": [
    "CURRENCYFMT", 
    "CURRENCYFMT[]"
  ], 
  "PCOLOR16": [
    "ShortByReference", 
    "short[]"
  ], 
  "LPMINIMIZEDMETRICS": [
    "MINIMIZEDMETRICS", 
    "MINIMIZEDMETRICS[]"
  ], 
  "char**": [
    "byte"
  ], 
  "PWIN32_FIND_DATA*": [
    "PointerByReference"
  ], 
  "LPCSERVER_TRANSPORT_INFO_0": [
    "SERVER_TRANSPORT_INFO_0", 
    "SERVER_TRANSPORT_INFO_0[]"
  ], 
  "LPCSERVER_TRANSPORT_INFO_1": [
    "SERVER_TRANSPORT_INFO_1", 
    "SERVER_TRANSPORT_INFO_1[]"
  ], 
  "LPCSERVER_TRANSPORT_INFO_2": [
    "SERVER_TRANSPORT_INFO_2", 
    "SERVER_TRANSPORT_INFO_2[]"
  ], 
  "LPCSERVER_TRANSPORT_INFO_3": [
    "SERVER_TRANSPORT_INFO_3", 
    "SERVER_TRANSPORT_INFO_3[]"
  ], 
  "SERVICE_STATUS": [
    "SERVICE_STATUS"
  ], 
  "PROVIDOR_INFO_1**": [
    "PointerByReference"
  ], 
  "PEXTENDED_NAME_FORMAT": [
    "EXTENDED_NAME_FORMAT", 
    "EXTENDED_NAME_FORMAT[]"
  ], 
  "PWKSTA_USER_INFO_0": [
    "WKSTA_USER_INFO_0", 
    "WKSTA_USER_INFO_0[]"
  ], 
  "PCONVINFO": [
    "CONVINFO", 
    "CONVINFO[]"
  ], 
  "PCOMSTAT*": [
    "PointerByReference"
  ], 
  "LPGLYPHMETRICS*": [
    "PointerByReference"
  ], 
  "PPALETTEENTRY": [
    "PALETTEENTRY", 
    "PALETTEENTRY[]"
  ], 
  "USER_INFO_1052**": [
    "PointerByReference"
  ], 
  "ENUMTEXTMETRIC**": [
    "PointerByReference"
  ], 
  "LPNET_DISPLAY_GROUP": [
    "NET_DISPLAY_GROUP", 
    "NET_DISPLAY_GROUP[]"
  ], 
  "LPRM_UNIQUE_PROCESS": [
    "RM_UNIQUE_PROCESS", 
    "RM_UNIQUE_PROCESS[]"
  ], 
  "RID_DEVICE_INFO_KEYBOARD": [
    "RID_DEVICE_INFO_KEYBOARD"
  ], 
  "PUSER_INFO_1051*": [
    "PointerByReference"
  ], 
  "LPCCOORD": [
    "COORD", 
    "COORD[]"
  ], 
  "LPCRAWMOUSE": [
    "RAWMOUSE", 
    "RAWMOUSE[]"
  ], 
  "LPFORM_INFO_2*": [
    "PointerByReference"
  ], 
  "LPPROCESSOR_POWER_POLICY*": [
    "PointerByReference"
  ], 
  "PITEMIDLIST": [
    "ITEMIDLIST", 
    "ITEMIDLIST[]"
  ], 
  "LPHSZ": [
    "HANDLEByReference"
  ], 
  "EMR": [
    "EMR"
  ], 
  "PPRINTER_NOTIFY_OPTIONS": [
    "PRINTER_NOTIFY_OPTIONS", 
    "PRINTER_NOTIFY_OPTIONS[]"
  ], 
  "TASKDIALOG_COMMON_BUTTON_FLAGS": [
    "int"
  ], 
  "LPPRINTPROCESSOR_INFO_1*": [
    "PointerByReference"
  ], 
  "TRACE_GUID_REGISTRATION**": [
    "PointerByReference"
  ], 
  "PRINTER_NOTIFY_OPTIONS*": [
    "PRINTER_NOTIFY_OPTIONS", 
    "PRINTER_NOTIFY_OPTIONS[]"
  ], 
  "LPSCROLLINFO*": [
    "PointerByReference"
  ], 
  "TEXTMETRIC**": [
    "PointerByReference"
  ], 
  "LPWER_REPORT_UI*": [
    "PointerByReference"
  ], 
  "PNET_DISPLAY_GROUP*": [
    "PointerByReference"
  ], 
  "PHANDLE": [
    "HANDLEByReference"
  ], 
  "PPORT_INFO_2*": [
    "PointerByReference"
  ], 
  "LPCNDDESHAREINFO": [
    "NDDESHAREINFO", 
    "NDDESHAREINFO[]"
  ], 
  "PWKSTA_INFO_502*": [
    "PointerByReference"
  ], 
  "PPRINTER_INFO_7": [
    "PRINTER_INFO_7", 
    "PRINTER_INFO_7[]"
  ], 
  "PDCB*": [
    "PointerByReference"
  ], 
  "LPLONG64*": [
    "PointerByReference"
  ], 
  "PWKSTA_INFO_101*": [
    "PointerByReference"
  ], 
  "LPWKSTA_USER_INFO_1": [
    "WKSTA_USER_INFO_1", 
    "WKSTA_USER_INFO_1[]"
  ], 
  "LPWKSTA_USER_INFO_0": [
    "WKSTA_USER_INFO_0", 
    "WKSTA_USER_INFO_0[]"
  ], 
  "PDFS_INFO_104*": [
    "PointerByReference"
  ], 
  "PSHITEMID*": [
    "PointerByReference"
  ], 
  "PRINTER_INFO_9*": [
    "PRINTER_INFO_9", 
    "PRINTER_INFO_9[]"
  ], 
  "PPROCESSOR_POWER_POLICY*": [
    "PointerByReference"
  ], 
  "PRM_APP_TYPE*": [
    "PointerByReference"
  ], 
  "TRACE_GUID_PROPERTIES": [
    "TRACE_GUID_PROPERTIES"
  ], 
  "LPINT32": [
    "IntByReference", 
    "int[]"
  ], 
  "SYSTEM_POWER_CAPABILITIES*": [
    "SYSTEM_POWER_CAPABILITIES", 
    "SYSTEM_POWER_CAPABILITIES[]"
  ], 
  "LPMOUSEKEYS*": [
    "PointerByReference"
  ], 
  "POWER_ACTION**": [
    "PointerByReference"
  ], 
  "PRAWMOUSE*": [
    "PointerByReference"
  ], 
  "PBY_HANDLE_FILE_INFORMATION*": [
    "PointerByReference"
  ], 
  "LPCSYSTEM_POWER_INFORMATION": [
    "SYSTEM_POWER_INFORMATION", 
    "SYSTEM_POWER_INFORMATION[]"
  ], 
  "PSYSTEMTIME*": [
    "PointerByReference"
  ], 
  "PLOGBRUSH": [
    "LOGBRUSH", 
    "LOGBRUSH[]"
  ], 
  "PLANGID*": [
    "PointerByReference"
  ], 
  "LPCDFS_INFO_102": [
    "DFS_INFO_102", 
    "DFS_INFO_102[]"
  ], 
  "PDLGITEMTEMPLATE": [
    "DLGITEMTEMPLATE", 
    "DLGITEMTEMPLATE[]"
  ], 
  "QUERY_SERVICE_LOCK_STATUS*": [
    "QUERY_SERVICE_LOCK_STATUS", 
    "QUERY_SERVICE_LOCK_STATUS[]"
  ], 
  "LPCCONSOLE_FONT_INFO": [
    "CONSOLE_FONT_INFO", 
    "CONSOLE_FONT_INFO[]"
  ], 
  "LPDFS_INFO_9*": [
    "PointerByReference"
  ], 
  "CONVCONTEXT*": [
    "CONVCONTEXT", 
    "CONVCONTEXT[]"
  ], 
  "PEVENT_INSTANCE_HEADER*": [
    "PointerByReference"
  ], 
  "CONSOLE_READCONSOLE_CONTROL**": [
    "PointerByReference"
  ], 
  "PHW_PROFILE_INFO": [
    "HW_PROFILE_INFO", 
    "HW_PROFILE_INFO[]"
  ], 
  "HFILE*": [
    "IntByReference", 
    "int[]"
  ], 
  "AXISINFO": [
    "AXISINFO"
  ], 
  "HPALETTE*": [
    "HANDLEByReference"
  ], 
  "DFS_INFO_3*": [
    "DFS_INFO_3", 
    "DFS_INFO_3[]"
  ], 
  "PBOOLEAN": [
    "byte"
  ], 
  "USER_INFO_23**": [
    "PointerByReference"
  ], 
  "PTOKEN_GROUPS": [
    "TOKEN_GROUPS", 
    "TOKEN_GROUPS[]"
  ], 
  "XFORM*": [
    "XFORM", 
    "XFORM[]", 
    "Pointer"
  ], 
  "LPLPCGEOTYPE*": [
    "PointerByReference"
  ], 
  "JOBOBJECT_CPU_RATE_CONTROL_INFORMATION": [
    "JOBOBJECT_CPU_RATE_CONTROL_INFORMATION"
  ], 
  "LPCBATTERY_REPORTING_SCALE": [
    "BATTERY_REPORTING_SCALE", 
    "BATTERY_REPORTING_SCALE[]"
  ], 
  "PTHREADENTRY32": [
    "THREADENTRY32", 
    "THREADENTRY32[]"
  ], 
  "LPCGCP_RESULTS": [
    "GCP_RESULTS", 
    "GCP_RESULTS[]"
  ], 
  "LPSERVICE_TRIGGER_INFO": [
    "SERVICE_TRIGGER_INFO", 
    "SERVICE_TRIGGER_INFO[]"
  ], 
  "LPCTAPE_GET_MEDIA_PARAMETERS": [
    "TAPE_GET_MEDIA_PARAMETERS", 
    "TAPE_GET_MEDIA_PARAMETERS[]"
  ], 
  "LPJOBOBJECT_END_OF_JOB_TIME_INFORMATION*": [
    "PointerByReference"
  ], 
  "LPCUSER_INFO_1024": [
    "USER_INFO_1024", 
    "USER_INFO_1024[]"
  ], 
  "COMBOBOXINFO": [
    "COMBOBOXINFO"
  ], 
  "USER_INFO_11**": [
    "PointerByReference"
  ], 
  "LPCUSER_INFO_1020": [
    "USER_INFO_1020", 
    "USER_INFO_1020[]"
  ], 
  "PATOM": [
    "ShortByReference", 
    "short[]"
  ], 
  "HFILE**": [
    "PointerByReference"
  ], 
  "MEMORY_BASIC_INFORMATION*": [
    "MEMORY_BASIC_INFORMATION", 
    "MEMORY_BASIC_INFORMATION[]"
  ], 
  "SHARE_INFO_2*": [
    "SHARE_INFO_2", 
    "SHARE_INFO_2[]"
  ], 
  "PPCTSTR*": [
    "char"
  ], 
  "LPPRINTER_NOTIFY_INFO*": [
    "PointerByReference"
  ], 
  "PSERVICE_TRIGGER_SPECIFIC_DATA_ITEM*": [
    "PointerByReference"
  ], 
  "PASSOC_FILTER": [
    "IntByReference", 
    "int[]"
  ], 
  "LPMENUITEMTEMPLATEHEADER": [
    "MENUITEMTEMPLATEHEADER", 
    "MENUITEMTEMPLATEHEADER[]"
  ], 
  "PGLOBAL_POWER_POLICY*": [
    "PointerByReference"
  ], 
  "HDEVNOTIFY": [
    "WinUser.HDEVNOTIFY"
  ], 
  "LONG": [
    "int", 
    "LONG"
  ], 
  "LDT_ENTRY**": [
    "PointerByReference"
  ], 
  "RASTERIZER_STATUS": [
    "RASTERIZER_STATUS"
  ], 
  "LPCOLORADJUSTMENT*": [
    "PointerByReference"
  ], 
  "SERVICE_FAILURE_ACTIONS_FLAG**": [
    "PointerByReference"
  ], 
  "LPCJOBOBJECT_CPU_RATE_CONTROL_INFORMATION": [
    "JOBOBJECT_CPU_RATE_CONTROL_INFORMATION", 
    "JOBOBJECT_CPU_RATE_CONTROL_INFORMATION[]"
  ], 
  "CURSORINFO*": [
    "CURSORINFO", 
    "CURSORINFO[]"
  ], 
  "PSC_HANDLE": [
    "HANDLEByReference"
  ], 
  "KAFFINITY": [
    "int"
  ], 
  "POFSTRUCT": [
    "OFSTRUCT", 
    "OFSTRUCT[]"
  ], 
  "GUITHREADINFO**": [
    "PointerByReference"
  ], 
  "CONSOLE_SCREEN_BUFFER_INFO*": [
    "CONSOLE_SCREEN_BUFFER_INFO", 
    "CONSOLE_SCREEN_BUFFER_INFO[]"
  ], 
  "LPOUTLINETEXTMETRIC": [
    "OUTLINETEXTMETRIC", 
    "OUTLINETEXTMETRIC[]"
  ], 
  "HPALETTE": [
    "HANDLE"
  ], 
  "LPWINDOWINFO*": [
    "PointerByReference"
  ], 
  "FILE_INFO_3**": [
    "PointerByReference"
  ], 
  "PVIDEOPARAMETERS*": [
    "PointerByReference"
  ], 
  "PDVTARGETDEVICE": [
    "DVTARGETDEVICE", 
    "DVTARGETDEVICE[]"
  ], 
  "POWER_POLICY*": [
    "POWER_POLICY", 
    "POWER_POLICY[]"
  ], 
  "LPPROVIDOR_INFO_1*": [
    "PointerByReference"
  ], 
  "LPCOSVERSIONINFOEX": [
    "OSVERSIONINFOEX", 
    "OSVERSIONINFOEX[]"
  ], 
  "MOUSE_EVENT_RECORD*": [
    "MOUSE_EVENT_RECORD", 
    "MOUSE_EVENT_RECORD[]"
  ], 
  "LPCEFS_CERTIFICATE_BLOB": [
    "EFS_CERTIFICATE_BLOB", 
    "EFS_CERTIFICATE_BLOB[]"
  ], 
  "LPCMOUSE_EVENT_RECORD": [
    "MOUSE_EVENT_RECORD", 
    "MOUSE_EVENT_RECORD[]"
  ], 
  "PUSER_INFO_1011*": [
    "PointerByReference"
  ], 
  "MEMORY_BASIC_INFORMATION**": [
    "PointerByReference"
  ], 
  "PTCHAR*": [
    "char"
  ], 
  "DIBSECTION*": [
    "DIBSECTION", 
    "DIBSECTION[]"
  ], 
  "RAWINPUTHEADER**": [
    "PointerByReference"
  ], 
  "LPFILE_SEGMENT_ELEMENT*": [
    "PointerByReference"
  ], 
  "UINT_PTR": [
    "int", 
    "UINT_PTR"
  ], 
  "INT32**": [
    "PointerByReference"
  ], 
  "PRAWINPUTHEADER*": [
    "PointerByReference"
  ], 
  "PDOCINFO*": [
    "PointerByReference"
  ], 
  "PDFS_INFO_7*": [
    "PointerByReference"
  ], 
  "PBOOLEAN*": [
    "byte"
  ], 
  "PCONNECTDLGSTRUCT*": [
    "PointerByReference"
  ], 
  "LPSYSGEOCLASS*": [
    "PointerByReference"
  ], 
  "LPCWKSTA_INFO_100": [
    "WKSTA_INFO_100", 
    "WKSTA_INFO_100[]"
  ], 
  "char": [
    "byte"
  ], 
  "VALENT": [
    "VALENT"
  ], 
  "PCOMMTIMEOUTS*": [
    "PointerByReference"
  ], 
  "GLYPHSET*": [
    "GLYPHSET", 
    "GLYPHSET[]"
  ], 
  "USEROBJECTFLAGS*": [
    "USEROBJECTFLAGS", 
    "USEROBJECTFLAGS[]"
  ], 
  "HCURSOR*": [
    "HANDLEByReference"
  ], 
  "LPGRADIENT_RECT*": [
    "PointerByReference"
  ], 
  "FILTERKEYS**": [
    "PointerByReference"
  ], 
  "LPARAM": [
    "int", 
    "Pointer", 
    "long"
  ], 
  "PMETAFILEPICT": [
    "METAFILEPICT", 
    "METAFILEPICT[]"
  ], 
  "LPACL": [
    "ACL", 
    "ACL[]"
  ], 
  "LPCALID*": [
    "PointerByReference"
  ], 
  "DFS_INFO_107**": [
    "PointerByReference"
  ], 
  "LPINT8*": [
    "byte"
  ], 
  "LPUSER_INFO_1052": [
    "USER_INFO_1052", 
    "USER_INFO_1052[]"
  ], 
  "LPCMDICREATESTRUCT": [
    "MDICREATESTRUCT", 
    "MDICREATESTRUCT[]"
  ], 
  "PMENUINFO*": [
    "PointerByReference"
  ], 
  "JOB_INFO_2": [
    "JOB_INFO_2"
  ], 
  "PRINTER_NOTIFY_INFO_DATA*": [
    "PRINTER_NOTIFY_INFO_DATA", 
    "PRINTER_NOTIFY_INFO_DATA[]"
  ], 
  "USER_INFO_22**": [
    "PointerByReference"
  ], 
  "LPGROUP_INFO_3*": [
    "PointerByReference"
  ], 
  "PCTSTR": [
    "CHARByReference", 
    "wchar_t[]", 
    "WString"
  ], 
  "AT_INFO": [
    "AT_INFO"
  ], 
  "SHARE_INFO_502": [
    "SHARE_INFO_502"
  ], 
  "PRINTER_INFO_5*": [
    "PRINTER_INFO_5", 
    "PRINTER_INFO_5[]"
  ], 
  "RID_DEVICE_INFO_HID**": [
    "PointerByReference"
  ], 
  "LPJOBOBJECT_BASIC_AND_IO_ACCOUNTING_INFORMATION*": [
    "PointerByReference"
  ], 
  "PUSER_INFO_1006*": [
    "PointerByReference"
  ], 
  "LPLDT_ENTRY*": [
    "PointerByReference"
  ], 
  "PLOCALGROUP_INFO_0": [
    "LOCALGROUP_INFO_0", 
    "LOCALGROUP_INFO_0[]"
  ], 
  "PLOCALGROUP_INFO_1": [
    "LOCALGROUP_INFO_1", 
    "LOCALGROUP_INFO_1[]"
  ], 
  "CONSOLE_READCONSOLE_CONTROL": [
    "CONSOLE_READCONSOLE_CONTROL"
  ], 
  "WIN32_STREAM_ID**": [
    "PointerByReference"
  ], 
  "PROCESSENTRY32*": [
    "PROCESSENTRY32", 
    "PROCESSENTRY32[]"
  ], 
  "RECT": [
    "RECT"
  ], 
  "PHCOLORSPACE": [
    "HANDLEByReference"
  ], 
  "LPCRAWINPUTDEVICELIST": [
    "RAWINPUTDEVICELIST", 
    "RAWINPUTDEVICELIST[]"
  ], 
  "LPPROPERTYKEY": [
    "PROPERTYKEY", 
    "PROPERTYKEY[]"
  ], 
  "SESSION_INFO_1**": [
    "PointerByReference"
  ], 
  "QUERY_USER_NOTIFICATION_STATE**": [
    "PointerByReference"
  ], 
  "SESSION_INFO_0**": [
    "PointerByReference"
  ], 
  "POPENASINFO": [
    "OPENASINFO", 
    "OPENASINFO[]"
  ], 
  "LPDOC_INFO_1*": [
    "PointerByReference"
  ], 
  "SHARE_INFO_501**": [
    "PointerByReference"
  ], 
  "RECTL*": [
    "RECTL", 
    "RECTL[]"
  ], 
  "WKSTA_TRANSPORT_INFO_0*": [
    "WKSTA_TRANSPORT_INFO_0", 
    "WKSTA_TRANSPORT_INFO_0[]"
  ], 
  "LPMSLLHOOKSTRUCT*": [
    "PointerByReference"
  ], 
  "USER_INFO_0*": [
    "USER_INFO_0", 
    "USER_INFO_0[]"
  ], 
  "PINT32*": [
    "PointerByReference"
  ], 
  "LCID": [
    "int", 
    "LCID"
  ], 
  "LPRID_DEVICE_INFO_HID*": [
    "PointerByReference"
  ], 
  "LPCENUM_SERVICE_STATUS": [
    "ENUM_SERVICE_STATUS", 
    "ENUM_SERVICE_STATUS[]"
  ], 
  "DEVMODE": [
    "DEVMODE"
  ], 
  "LPSERVICE_TRIGGER_INFO*": [
    "PointerByReference"
  ], 
  "PUSER_MODALS_INFO_1006*": [
    "PointerByReference"
  ], 
  "LPDFS_INFO_6*": [
    "PointerByReference"
  ], 
  "PUSER_INFO_1024*": [
    "PointerByReference"
  ], 
  "LPCLOCALGROUP_INFO_1002": [
    "LOCALGROUP_INFO_1002", 
    "LOCALGROUP_INFO_1002[]"
  ], 
  "PAVRT_PRIORITY": [
    "IntByReference", 
    "int[]"
  ], 
  "SHSTOCKICONID**": [
    "PointerByReference"
  ], 
  "PDFS_STORAGE_INFO_1": [
    "DFS_STORAGE_INFO_1", 
    "DFS_STORAGE_INFO_1[]"
  ], 
  "DWORD": [
    "int"
  ], 
  "LPJOBOBJECT_EXTENDED_LIMIT_INFORMATION": [
    "JOBOBJECT_EXTENDED_LIMIT_INFORMATION", 
    "JOBOBJECT_EXTENDED_LIMIT_INFORMATION[]"
  ], 
  "SERVICE_TRIGGER_INFO": [
    "SERVICE_TRIGGER_INFO"
  ], 
  "SHARE_INFO_1**": [
    "PointerByReference"
  ], 
  "LPLPCWSTR": [
    "char"
  ], 
  "DFS_INFO_200": [
    "DFS_INFO_200"
  ], 
  "USER_MODALS_INFO_1002*": [
    "USER_MODALS_INFO_1002", 
    "USER_MODALS_INFO_1002[]"
  ], 
  "HCONVLIST*": [
    "HANDLEByReference"
  ], 
  "PLARGE_INTEGER*": [
    "PointerByReference"
  ], 
  "LPLARGE_INTEGER": [
    "LARGE_INTEGER", 
    "LARGE_INTEGER[]"
  ], 
  "PCONSOLE_SCREEN_BUFFER_INFOEX*": [
    "PointerByReference"
  ], 
  "LPDWORD64": [
    "LongByReference", 
    "__int64[]"
  ], 
  "USER_INFO_1053*": [
    "USER_INFO_1053", 
    "USER_INFO_1053[]"
  ], 
  "LPSTREAM_INFO_LEVELS*": [
    "PointerByReference"
  ], 
  "LPPOINT": [
    "POINT", 
    "POINT[]"
  ], 
  "PHEAP_INFORMATION_CLASS*": [
    "PointerByReference"
  ], 
  "MINIMIZEDMETRICS": [
    "MINIMIZEDMETRICS"
  ], 
  "LOCALGROUP_MEMBERS_INFO_0*": [
    "LOCALGROUP_MEMBERS_INFO_0", 
    "LOCALGROUP_MEMBERS_INFO_0[]"
  ], 
  "LPMOUSEKEYS": [
    "MOUSEKEYS", 
    "MOUSEKEYS[]"
  ], 
  "LPHRGN": [
    "HANDLEByReference"
  ], 
  "PRINTER_INFO_4": [
    "PRINTER_INFO_4"
  ], 
  "MONITOR_INFO_1**": [
    "PointerByReference"
  ], 
  "DLGTEMPLATE": [
    "DLGTEMPLATE"
  ], 
  "PRINTER_INFO_7": [
    "PRINTER_INFO_7"
  ], 
  "PRINTER_INFO_1": [
    "PRINTER_INFO_1"
  ], 
  "PRINTER_INFO_2": [
    "PRINTER_INFO_2"
  ], 
  "PRINTER_INFO_3": [
    "PRINTER_INFO_3"
  ], 
  "GLOBAL_USER_POWER_POLICY": [
    "GLOBAL_USER_POWER_POLICY"
  ], 
  "SERVICE_TIMECHANGE_INFO*": [
    "SERVICE_TIMECHANGE_INFO", 
    "SERVICE_TIMECHANGE_INFO[]"
  ], 
  "PSHITEMID": [
    "SHITEMID", 
    "SHITEMID[]"
  ], 
  "COMMCONFIG*": [
    "COMMCONFIG", 
    "COMMCONFIG[]"
  ], 
  "PRINTER_INFO_8": [
    "PRINTER_INFO_8"
  ], 
  "PRINTER_INFO_9": [
    "PRINTER_INFO_9"
  ], 
  "DFS_INFO_105": [
    "DFS_INFO_105"
  ], 
  "WSAPROTOCOLCHAIN**": [
    "PointerByReference"
  ], 
  "TIME_ZONE_INFORMATION**": [
    "PointerByReference"
  ], 
  "MENUEX_TEMPLATE_HEADER**": [
    "PointerByReference"
  ], 
  "LPPROCESSENTRY32": [
    "PROCESSENTRY32", 
    "PROCESSENTRY32[]"
  ], 
  "DFS_INFO_4**": [
    "PointerByReference"
  ], 
  "LPSHARE_INFO_501": [
    "SHARE_INFO_501", 
    "SHARE_INFO_501[]"
  ], 
  "SECURITY_CONTEXT_TRACKING_MODE*": [
    "byte"
  ], 
  "MSLLHOOKSTRUCT*": [
    "MSLLHOOKSTRUCT", 
    "MSLLHOOKSTRUCT[]"
  ], 
  "LPPRINTER_NOTIFY_OPTIONS*": [
    "PointerByReference"
  ], 
  "NET_DISPLAY_MACHINE*": [
    "NET_DISPLAY_MACHINE", 
    "NET_DISPLAY_MACHINE[]"
  ], 
  "LPFILE_INFO_BY_HANDLE_CLASS*": [
    "PointerByReference"
  ], 
  "PLONG64*": [
    "PointerByReference"
  ], 
  "LPWNODE_HEADER": [
    "WNODE_HEADER", 
    "WNODE_HEADER[]"
  ], 
  "PPOWER_ACTION_POLICY*": [
    "PointerByReference"
  ], 
  "PSYSGEOTYPE": [
    "IntByReference", 
    "int[]"
  ], 
  "LPSERVICE_TIMECHANGE_INFO*": [
    "PointerByReference"
  ], 
  "SYSTEM_INFO**": [
    "PointerByReference"
  ], 
  "LPGLOBAL_POWER_POLICY": [
    "GLOBAL_POWER_POLICY", 
    "GLOBAL_POWER_POLICY[]"
  ], 
  "PMSLLHOOKSTRUCT*": [
    "PointerByReference"
  ], 
  "RM_UNIQUE_PROCESS**": [
    "PointerByReference"
  ], 
  "PDH_DATA_ITEM_PATH_ELEMENTS*": [
    "PDH_DATA_ITEM_PATH_ELEMENTS", 
    "PDH_DATA_ITEM_PATH_ELEMENTS[]"
  ], 
  "TAPE_SET_MEDIA_PARAMETERS**": [
    "PointerByReference"
  ], 
  "LPDIBSECTION*": [
    "PointerByReference"
  ], 
  "LPSHARE_INFO_502": [
    "SHARE_INFO_502", 
    "SHARE_INFO_502[]"
  ], 
  "PAT_ENUM*": [
    "PointerByReference"
  ], 
  "LPEFS_CERTIFICATE_BLOB*": [
    "PointerByReference"
  ], 
  "PDISCDLGSTRUCT*": [
    "PointerByReference"
  ], 
  "PMSG": [
    "MSG", 
    "MSG[]"
  ], 
  "BITMAP**": [
    "PointerByReference"
  ], 
  "LPFORM_INFO_1*": [
    "PointerByReference"
  ], 
  "LPCSHFILEOPSTRUCT": [
    "SHFILEOPSTRUCT", 
    "SHFILEOPSTRUCT[]"
  ], 
  "PPRINTER_INFO_2*": [
    "PointerByReference"
  ], 
  "LPCENHMETAHEADER": [
    "ENHMETAHEADER", 
    "ENHMETAHEADER[]"
  ], 
  "PEVENT_TRACE_PROPERTIES*": [
    "PointerByReference"
  ], 
  "WAITCHAIN_NODE_INFO*": [
    "WAITCHAIN_NODE_INFO", 
    "WAITCHAIN_NODE_INFO[]"
  ], 
  "HEAPLIST32": [
    "HEAPLIST32"
  ], 
  "PPCTSTR": [
    "char"
  ], 
  "PFILE_INFO_2": [
    "FILE_INFO_2", 
    "FILE_INFO_2[]"
  ], 
  "PFILE_INFO_3": [
    "FILE_INFO_3", 
    "FILE_INFO_3[]"
  ], 
  "PCPINFO": [
    "CPINFO", 
    "CPINFO[]"
  ], 
  "LPSHARE_INFO_503": [
    "SHARE_INFO_503", 
    "SHARE_INFO_503[]"
  ], 
  "PSID_NAME_USE": [
    "PointerByReference"
  ], 
  "PMENUITEMINFO*": [
    "PointerByReference"
  ], 
  "USE_INFO_0**": [
    "PointerByReference"
  ], 
  "HDROP": [
    "HANDLE"
  ], 
  "LPFOCUS_EVENT_RECORD": [
    "FOCUS_EVENT_RECORD", 
    "FOCUS_EVENT_RECORD[]"
  ], 
  "PRGNDATA*": [
    "PointerByReference"
  ], 
  "PULONG_PTR*": [
    "PointerByReference"
  ], 
  "PSERVICE_SID_INFO": [
    "SERVICE_SID_INFO", 
    "SERVICE_SID_INFO[]"
  ], 
  "PERFORMANCE_INFORMATION*": [
    "PERFORMANCE_INFORMATION", 
    "PERFORMANCE_INFORMATION[]"
  ], 
  "PGROUP_USERS_INFO_0*": [
    "PointerByReference"
  ], 
  "LPDFS_INFO_103": [
    "DFS_INFO_103", 
    "DFS_INFO_103[]"
  ], 
  "LPDFS_INFO_100": [
    "DFS_INFO_100", 
    "DFS_INFO_100[]"
  ], 
  "LPDFS_INFO_101": [
    "DFS_INFO_101", 
    "DFS_INFO_101[]"
  ], 
  "LPDFS_INFO_106": [
    "DFS_INFO_106", 
    "DFS_INFO_106[]"
  ], 
  "LPICONINFO": [
    "ICONINFO", 
    "ICONINFO[]"
  ], 
  "LPDFS_INFO_104": [
    "DFS_INFO_104", 
    "DFS_INFO_104[]"
  ], 
  "LPDFS_INFO_105": [
    "DFS_INFO_105", 
    "DFS_INFO_105[]"
  ], 
  "OUTLINETEXTMETRIC**": [
    "PointerByReference"
  ], 
  "HIGHCONTRAST*": [
    "HIGHCONTRAST", 
    "HIGHCONTRAST[]"
  ], 
  "MODULEINFO**": [
    "PointerByReference"
  ], 
  "PCONNECTDLGSTRUCT": [
    "CONNECTDLGSTRUCT", 
    "CONNECTDLGSTRUCT[]"
  ], 
  "PUSER_INFO_1009*": [
    "PointerByReference"
  ], 
  "PLONG64": [
    "LongByReference", 
    "__int64[]"
  ], 
  "LPHBITMAP": [
    "HANDLEByReference"
  ], 
  "SERVER_TRANSPORT_INFO_2*": [
    "SERVER_TRANSPORT_INFO_2", 
    "SERVER_TRANSPORT_INFO_2[]"
  ], 
  "LPCLATENCY_TIME": [
    "IntByReference", 
    "int[]"
  ], 
  "PCOMMCONFIG*": [
    "PointerByReference"
  ], 
  "PPRIVILEGE_SET*": [
    "PointerByReference"
  ], 
  "HANDLE*": [
    "HANDLEByReference"
  ], 
  "PRGNDATA": [
    "RGNDATA", 
    "RGNDATA[]"
  ], 
  "USE_INFO_0*": [
    "USE_INFO_0", 
    "USE_INFO_0[]"
  ], 
  "LPCENUMTEXTMETRIC": [
    "ENUMTEXTMETRIC", 
    "ENUMTEXTMETRIC[]"
  ], 
  "LPMSLLHOOKSTRUCT": [
    "MSLLHOOKSTRUCT", 
    "MSLLHOOKSTRUCT[]"
  ], 
  "PGENERIC_MAPPING*": [
    "PointerByReference"
  ], 
  "LPHDROP": [
    "HANDLEByReference"
  ], 
  "PICONMETRICS*": [
    "PointerByReference"
  ], 
  "LPLONG64": [
    "LongByReference", 
    "__int64[]"
  ], 
  "LPUSE_INFO_0*": [
    "PointerByReference"
  ], 
  "WIN32_FIND_STREAM_DATA*": [
    "WIN32_FIND_STREAM_DATA", 
    "WIN32_FIND_STREAM_DATA[]"
  ], 
  "CWPSTRUCT*": [
    "CWPSTRUCT", 
    "CWPSTRUCT[]"
  ], 
  "PCONSOLE_HISTORY_INFO*": [
    "PointerByReference"
  ], 
  "RM_FILTER_ACTION": [
    "int"
  ], 
  "LPJOBOBJECT_END_OF_JOB_TIME_INFORMATION": [
    "JOBOBJECT_END_OF_JOB_TIME_INFORMATION", 
    "JOBOBJECT_END_OF_JOB_TIME_INFORMATION[]"
  ], 
  "WCT_OBJECT_STATUS*": [
    "IntByReference", 
    "int[]"
  ], 
  "LPCOVERLAPPED_ENTRY": [
    "OVERLAPPED_ENTRY", 
    "OVERLAPPED_ENTRY[]"
  ], 
  "LPCAPPBARDATA": [
    "APPBARDATA", 
    "APPBARDATA[]"
  ], 
  "LPPROCESSOR_POWER_POLICY": [
    "PROCESSOR_POWER_POLICY", 
    "PROCESSOR_POWER_POLICY[]"
  ], 
  "PNET_DISPLAY_USER": [
    "NET_DISPLAY_USER", 
    "NET_DISPLAY_USER[]"
  ], 
  "HEAPENTRY32*": [
    "HEAPENTRY32", 
    "HEAPENTRY32[]"
  ], 
  "CONVINFO": [
    "CONVINFO"
  ], 
  "TOKEN_PRIVILEGES**": [
    "PointerByReference"
  ], 
  "LPMEMORYSTATUS": [
    "MEMORYSTATUS", 
    "MEMORYSTATUS[]"
  ], 
  "int**": [
    "PointerByReference"
  ], 
  "PSCNRT_STATUS": [
    "IntByReference", 
    "int[]"
  ], 
  "PSHARE_INFO_0*": [
    "PointerByReference"
  ], 
  "PLPCGEOTYPE": [
    "IntByReference", 
    "long[]"
  ], 
  "PORT_INFO_3**": [
    "PointerByReference"
  ], 
  "PHEAPENTRY32*": [
    "PointerByReference"
  ], 
  "LPFLASHWINFO": [
    "FLASHWINFO", 
    "FLASHWINFO[]"
  ], 
  "PGROUP_INFO_2": [
    "GROUP_INFO_2", 
    "GROUP_INFO_2[]"
  ], 
  "PGROUP_INFO_3": [
    "GROUP_INFO_3", 
    "GROUP_INFO_3[]"
  ], 
  "FILE_NOTIFY_INFORMATION**": [
    "PointerByReference"
  ], 
  "LPCOFSTRUCT": [
    "OFSTRUCT", 
    "OFSTRUCT[]"
  ], 
  "JOBOBJECT_END_OF_JOB_TIME_INFORMATION*": [
    "JOBOBJECT_END_OF_JOB_TIME_INFORMATION", 
    "JOBOBJECT_END_OF_JOB_TIME_INFORMATION[]"
  ], 
  "NTMS_NOTIFICATIONINFORMATION*": [
    "NTMS_NOTIFICATIONINFORMATION", 
    "NTMS_NOTIFICATIONINFORMATION[]"
  ], 
  "LPCONSOLE_CURSOR_INFO": [
    "CONSOLE_CURSOR_INFO", 
    "CONSOLE_CURSOR_INFO[]"
  ], 
  "LPMACHINE_POWER_POLICY*": [
    "PointerByReference"
  ], 
  "INT16**": [
    "PointerByReference"
  ], 
  "ABCFLOAT": [
    "ABCFLOAT"
  ], 
  "GLYPHMETRICS**": [
    "PointerByReference"
  ], 
  "OPENASINFO**": [
    "PointerByReference"
  ], 
  "LOCALGROUP_USERS_INFO_0*": [
    "LOCALGROUP_USERS_INFO_0", 
    "LOCALGROUP_USERS_INFO_0[]"
  ], 
  "LOGBRUSH*": [
    "LOGBRUSH", 
    "LOGBRUSH[]"
  ], 
  "LPSIZE_T*": [
    "PointerByReference"
  ], 
  "LPPERFORMANCE_INFORMATION*": [
    "PointerByReference"
  ], 
  "LPUSER_MODALS_INFO_1007*": [
    "PointerByReference"
  ], 
  "LPASSOC_FILTER": [
    "IntByReference", 
    "int[]"
  ], 
  "TASKDIALOG_COMMON_BUTTON_FLAGS*": [
    "IntByReference", 
    "int[]"
  ], 
  "LPVS_FIXEDFILEINFO": [
    "VS_FIXEDFILEINFO", 
    "VS_FIXEDFILEINFO[]"
  ], 
  "PROCESSOR_NUMBER*": [
    "PROCESSOR_NUMBER", 
    "PROCESSOR_NUMBER[]"
  ], 
  "LONG_PTR*": [
    "IntByReference", 
    "long[]"
  ], 
  "CLIENTCREATESTRUCT**": [
    "PointerByReference"
  ], 
  "WINDOW_BUFFER_SIZE_RECORD**": [
    "PointerByReference"
  ], 
  "OVERLAPPED_ENTRY*": [
    "OVERLAPPED_ENTRY", 
    "OVERLAPPED_ENTRY[]"
  ], 
  "PDWORD64": [
    "LongByReference", 
    "__int64[]"
  ], 
  "PTAPE_SET_DRIVE_PARAMETERS": [
    "TAPE_SET_DRIVE_PARAMETERS", 
    "TAPE_SET_DRIVE_PARAMETERS[]"
  ], 
  "EVENT_TRACE_PROPERTIES*": [
    "EVENT_TRACE_PROPERTIES", 
    "EVENT_TRACE_PROPERTIES[]"
  ], 
  "UCHAR": [
    "byte"
  ], 
  "LPGUID": [
    "GUID", 
    "GUID[]"
  ], 
  "HLOCAL*": [
    "HANDLEByReference"
  ], 
  "PVOID": [
    "Pointer", 
    "LPVOID", 
    "byte[]", 
    "Structure", 
    "PVOID", 
    "Buffer"
  ], 
  "HCRYPTKEY": [
    "ULONG_PTR"
  ], 
  "LPCSHSTOCKICONINFO": [
    "SHSTOCKICONINFO", 
    "SHSTOCKICONINFO[]"
  ], 
  "LPLPARAM*": [
    "PointerByReference"
  ], 
  "PSERVICE_LAUNCH_PROTECTED_INFO": [
    "SERVICE_LAUNCH_PROTECTED_INFO", 
    "SERVICE_LAUNCH_PROTECTED_INFO[]"
  ], 
  "LPMODULEINFO": [
    "MODULEINFO", 
    "MODULEINFO[]"
  ], 
  "PGEOID": [
    "IntByReference", 
    "long[]"
  ], 
  "LPHCONV": [
    "HANDLEByReference"
  ], 
  "PGEOCLASS***": [
    "PointerByReference"
  ], 
  "LPTCH": [
    "Pointer"
  ], 
  "PQWORD*": [
    "PointerByReference"
  ], 
  "FIXED*": [
    "FIXED", 
    "FIXED[]"
  ], 
  "LPCWNDCLASSEX": [
    "WNDCLASSEX", 
    "WNDCLASSEX[]"
  ], 
  "LPGROUP_INFO_1002*": [
    "PointerByReference"
  ], 
  "TimeSysInfo*": [
    "IntByReference", 
    "int[]"
  ], 
  "UINT64**": [
    "PointerByReference"
  ], 
  "PWKSTA_INFO_100*": [
    "PointerByReference"
  ], 
  "LPNETRESOURCE": [
    "NETRESOURCE", 
    "NETRESOURCE[]"
  ], 
  "TAPE_GET_MEDIA_PARAMETERS": [
    "TAPE_GET_MEDIA_PARAMETERS"
  ], 
  "PRECT": [
    "RECT", 
    "RECT[]"
  ], 
  "LPTAPE_GET_MEDIA_PARAMETERS": [
    "TAPE_GET_MEDIA_PARAMETERS", 
    "TAPE_GET_MEDIA_PARAMETERS[]"
  ], 
  "LPDATATYPES_INFO_1": [
    "DATATYPES_INFO_1", 
    "DATATYPES_INFO_1[]"
  ], 
  "USER_INFO_1*": [
    "USER_INFO_1", 
    "USER_INFO_1[]"
  ], 
  "LPCREATESTRUCT": [
    "CREATESTRUCT", 
    "CREATESTRUCT[]"
  ], 
  "BOOL*": [
    "IntByReference", 
    "int[]"
  ], 
  "PHANDLER_ROUTINE": [
    "Callback"
  ], 
  "LPFORM_INFO_1": [
    "FORM_INFO_1", 
    "FORM_INFO_1[]"
  ], 
  "PWKSTA_USER_INFO_0*": [
    "PointerByReference"
  ], 
  "LPCNETRESOURCE": [
    "NETRESOURCE", 
    "NETRESOURCE[]"
  ], 
  "LPFORM_INFO_2": [
    "FORM_INFO_2", 
    "FORM_INFO_2[]"
  ], 
  "PNUMBERFMT*": [
    "PointerByReference"
  ], 
  "LPENUM_SERVICE_STATUS_PROCESS": [
    "ENUM_SERVICE_STATUS_PROCESS", 
    "ENUM_SERVICE_STATUS_PROCESS[]"
  ], 
  "LPRM_FILTER_ACTION": [
    "IntByReference", 
    "int[]"
  ], 
  "LPCVS_FIXEDFILEINFO": [
    "VS_FIXEDFILEINFO", 
    "VS_FIXEDFILEINFO[]"
  ], 
  "PRAWINPUT*": [
    "PointerByReference"
  ], 
  "wchar_t**": [
    "char"
  ], 
  "PWIN32_FIND_DATA": [
    "WIN32_FIND_DATA", 
    "WIN32_FIND_DATA[]"
  ], 
  "PDH_COUNTER_INFO**": [
    "PointerByReference"
  ], 
  "PGROUP_INFO_1002*": [
    "PointerByReference"
  ], 
  "MOUSE_EVENT_RECORD": [
    "MOUSE_EVENT_RECORD"
  ], 
  "LPBYTE": [
    "byte", 
    "byte[]", 
    "Pointer", 
    "Buffer"
  ], 
  "LPDFS_INFO_8*": [
    "PointerByReference"
  ], 
  "PAPPBARDATA*": [
    "PointerByReference"
  ], 
  "LPPROCESS_MEMORY_COUNTERS_EX*": [
    "PointerByReference"
  ], 
  "PTRACE_GUID_PROPERTIES": [
    "TRACE_GUID_PROPERTIES", 
    "TRACE_GUID_PROPERTIES[]"
  ], 
  "LPCALTTABINFO": [
    "ALTTABINFO", 
    "ALTTABINFO[]"
  ], 
  "LPCENUMLOGFONTEX": [
    "ENUMLOGFONTEX", 
    "ENUMLOGFONTEX[]"
  ], 
  "PSERVICE_TRIGGER_SPECIFIC_DATA_ITEM": [
    "SERVICE_TRIGGER_SPECIFIC_DATA_ITEM", 
    "SERVICE_TRIGGER_SPECIFIC_DATA_ITEM[]"
  ], 
  "RID_DEVICE_INFO_HID": [
    "RID_DEVICE_INFO_HID"
  ], 
  "SERVICE_FAILURE_ACTIONS": [
    "SERVICE_FAILURE_ACTIONS"
  ], 
  "USER_INFO_1006**": [
    "PointerByReference"
  ], 
  "PJOBOBJECT_END_OF_JOB_TIME_INFORMATION": [
    "JOBOBJECT_END_OF_JOB_TIME_INFORMATION", 
    "JOBOBJECT_END_OF_JOB_TIME_INFORMATION[]"
  ], 
  "DFS_INFO_2*": [
    "DFS_INFO_2", 
    "DFS_INFO_2[]"
  ], 
  "PWSAPROTOCOLCHAIN*": [
    "PointerByReference"
  ], 
  "WCT_OBJECT_TYPE": [
    "int"
  ], 
  "PLCTYPE*": [
    "PointerByReference"
  ], 
  "PRINTER_ENUM_VALUES**": [
    "PointerByReference"
  ], 
  "PLPCWSTR*": [
    "char"
  ], 
  "PCONSOLE_SELECTION_INFO": [
    "CONSOLE_SELECTION_INFO", 
    "CONSOLE_SELECTION_INFO[]"
  ], 
  "PHLOCAL": [
    "HANDLEByReference"
  ], 
  "SHSTOCKICONID*": [
    "IntByReference", 
    "int[]"
  ], 
  "PCLIENTCREATESTRUCT*": [
    "PointerByReference"
  ], 
  "LPHRAWINPUT": [
    "HANDLEByReference"
  ], 
  "USER_INFO_23": [
    "USER_INFO_23"
  ], 
  "AXESLIST": [
    "AXESLIST"
  ], 
  "USER_INFO_24": [
    "USER_INFO_24"
  ], 
  "LPWSAPROTOCOLCHAIN": [
    "WSAPROTOCOLCHAIN", 
    "WSAPROTOCOLCHAIN[]"
  ], 
  "SERVICE_TIMECHANGE_INFO": [
    "SERVICE_TIMECHANGE_INFO"
  ], 
  "NUMBERFMT": [
    "NUMBERFMT"
  ], 
  "LPCGEOTYPE": [
    "int"
  ], 
  "ICONMETRICS*": [
    "ICONMETRICS", 
    "ICONMETRICS[]"
  ], 
  "WMIDPREQUESTCODE": [
    "int"
  ], 
  "LPKAFFINITY": [
    "IntByReference", 
    "long[]"
  ], 
  "LMSTR*": [
    "char"
  ], 
  "PFINDEX_INFO_LEVELS*": [
    "PointerByReference"
  ], 
  "GROUP_AFFINITY**": [
    "PointerByReference"
  ], 
  "PENUM_SERVICE_STATUS_PROCESS*": [
    "PointerByReference"
  ], 
  "PJOBOBJECT_END_OF_JOB_TIME_INFORMATION*": [
    "PointerByReference"
  ], 
  "TCHAR": [
    "char"
  ], 
  "PRINTPAGERANGE**": [
    "PointerByReference"
  ], 
  "FILE_NOTIFY_INFORMATION": [
    "FILE_NOTIFY_INFORMATION"
  ], 
  "LPSYSTEM_POWER_STATE": [
    "IntByReference", 
    "int[]"
  ], 
  "LPUINT_PTR*": [
    "PointerByReference"
  ], 
  "LPPAINTSTRUCT": [
    "PAINTSTRUCT", 
    "PAINTSTRUCT[]"
  ], 
  "PSERVER_TRANSPORT_INFO_3*": [
    "PointerByReference"
  ], 
  "MENUITEMINFO": [
    "MENUITEMINFO"
  ], 
  "LPDVTARGETDEVICE*": [
    "PointerByReference"
  ], 
  "HCRYPTPROV": [
    "ULONG_PTR"
  ], 
  "LPUSER_MODALS_INFO_1006*": [
    "PointerByReference"
  ], 
  "EVENTLOGRECORD**": [
    "PointerByReference"
  ], 
  "USER_INFO_1003**": [
    "PointerByReference"
  ], 
  "DEVMODE*": [
    "DEVMODE", 
    "DEVMODE[]"
  ], 
  "PDFS_INFO_106": [
    "DFS_INFO_106", 
    "DFS_INFO_106[]"
  ], 
  "LPCJOBOBJECT_LIMIT_VIOLATION_INFORMATION": [
    "JOBOBJECT_LIMIT_VIOLATION_INFORMATION", 
    "JOBOBJECT_LIMIT_VIOLATION_INFORMATION[]"
  ], 
  "PDFS_INFO_101": [
    "DFS_INFO_101", 
    "DFS_INFO_101[]"
  ], 
  "PMOUSEKEYS": [
    "MOUSEKEYS", 
    "MOUSEKEYS[]"
  ], 
  "LPWKSTA_TRANSPORT_INFO_0": [
    "WKSTA_TRANSPORT_INFO_0", 
    "WKSTA_TRANSPORT_INFO_0[]"
  ], 
  "LPUSER_INFO_10*": [
    "PointerByReference"
  ], 
  "LPEVENT_TRACE_HEADER": [
    "EVENT_TRACE_HEADER", 
    "EVENT_TRACE_HEADER[]"
  ], 
  "USER_INFO_1010**": [
    "PointerByReference"
  ], 
  "LPENHMETAHEADER": [
    "ENHMETAHEADER", 
    "ENHMETAHEADER[]"
  ], 
  "LATENCY_TIME*": [
    "IntByReference", 
    "int[]"
  ], 
  "LPPSAPI_WS_WATCH_INFORMATION*": [
    "PointerByReference"
  ], 
  "PWPARAM": [
    "IntByReference", 
    "int[]"
  ], 
  "CALTYPE**": [
    "PointerByReference"
  ], 
  "HFONT*": [
    "HANDLEByReference"
  ], 
  "PDFS_INFO_102": [
    "DFS_INFO_102", 
    "DFS_INFO_102[]"
  ], 
  "PLPCGEOTYPE*": [
    "PointerByReference"
  ], 
  "PAUDIODESCRIPTION*": [
    "PointerByReference"
  ], 
  "CONSOLE_READCONSOLE_CONTROL*": [
    "CONSOLE_READCONSOLE_CONTROL", 
    "CONSOLE_READCONSOLE_CONTROL[]"
  ], 
  "MSLLHOOKSTRUCT": [
    "MSLLHOOKSTRUCT"
  ], 
  "PMediaLabelInfo*": [
    "PointerByReference"
  ], 
  "PUINT16": [
    "ShortByReference", 
    "short[]"
  ], 
  "LPEVENT_INSTANCE_INFO*": [
    "PointerByReference"
  ], 
  "PJOB_INFO_4*": [
    "PointerByReference"
  ], 
  "LPNUMBERFMT*": [
    "PointerByReference"
  ], 
  "PUSER_INFO_1010*": [
    "PointerByReference"
  ], 
  "PJOBOBJECT_BASIC_AND_IO_ACCOUNTING_INFORMATION*": [
    "PointerByReference"
  ], 
  "PJOBOBJECT_BASIC_LIMIT_INFORMATION*": [
    "PointerByReference"
  ], 
  "PSTD_ALERT": [
    "STD_ALERT", 
    "STD_ALERT[]"
  ], 
  "LPLONG_PTR*": [
    "PointerByReference"
  ], 
  "PMENU_EVENT_RECORD*": [
    "PointerByReference"
  ], 
  "LPPTSTR*": [
    "char"
  ], 
  "SYSTEM_POWER_INFORMATION**": [
    "PointerByReference"
  ], 
  "LPCFIXED": [
    "FIXED", 
    "FIXED[]"
  ], 
  "METARECORD**": [
    "PointerByReference"
  ], 
  "LPOPEN_AS_INFO_FLAGS": [
    "IntByReference", 
    "int[]"
  ], 
  "ENUMLOGFONTEX": [
    "ENUMLOGFONTEX"
  ], 
  "LPSERVICE_TIMECHANGE_INFO": [
    "SERVICE_TIMECHANGE_INFO", 
    "SERVICE_TIMECHANGE_INFO[]"
  ], 
  "LPCJOBOBJECT_RATE_CONTROL_TOLERANCE_INTERVAL": [
    "IntByReference", 
    "int[]"
  ], 
  "LPRAWINPUTDEVICELIST": [
    "RAWINPUTDEVICELIST", 
    "RAWINPUTDEVICELIST[]"
  ], 
  "CALTYPE": [
    "int"
  ], 
  "PDH_RAW_COUNTER_ITEM*": [
    "PDH_RAW_COUNTER_ITEM", 
    "PDH_RAW_COUNTER_ITEM[]"
  ], 
  "LPLOGFONT*": [
    "PointerByReference"
  ], 
  "SCNRT_STATUS": [
    "int"
  ], 
  "PTRACKMOUSEEVENT*": [
    "PointerByReference"
  ], 
  "LPJOBOBJECT_BASIC_PROCESS_ID_LIST": [
    "JOBOBJECT_BASIC_PROCESS_ID_LIST", 
    "JOBOBJECT_BASIC_PROCESS_ID_LIST[]"
  ], 
  "PGENERIC_MAPPING": [
    "GENERIC_MAPPING", 
    "GENERIC_MAPPING[]"
  ], 
  "PBITMAP*": [
    "PointerByReference"
  ], 
  "HANDLETABLE*": [
    "HANDLETABLE", 
    "HANDLETABLE[]"
  ], 
  "LPOFSTRUCT": [
    "OFSTRUCT", 
    "OFSTRUCT[]"
  ], 
  "HCONV": [
    "HANDLE"
  ], 
  "PACL_INFORMATION_CLASS*": [
    "PointerByReference"
  ], 
  "PSERVICE_FAILURE_ACTIONS*": [
    "PointerByReference"
  ], 
  "ENUM_SERVICE_STATUS_PROCESS**": [
    "PointerByReference"
  ], 
  "SYSTEM_POWER_STATE*": [
    "IntByReference", 
    "int[]"
  ], 
  "LPPRIVILEGE_SET*": [
    "PointerByReference"
  ], 
  "LPTAPE_SET_MEDIA_PARAMETERS": [
    "TAPE_SET_MEDIA_PARAMETERS", 
    "TAPE_SET_MEDIA_PARAMETERS[]"
  ], 
  "HOOKPROC": [
    "WinUser.HOOKPROC"
  ], 
  "FORM_INFO_2*": [
    "FORM_INFO_2", 
    "FORM_INFO_2[]"
  ], 
  "PGEOTYPE": [
    "IntByReference", 
    "long[]"
  ], 
  "PCHAR": [
    "byte"
  ], 
  "WER_DUMP_TYPE*": [
    "IntByReference", 
    "int[]"
  ], 
  "GROUP_INFO_0**": [
    "PointerByReference"
  ], 
  "ULONGLONG": [
    "double"
  ], 
  "PFILE_INFO_BY_HANDLE_CLASS*": [
    "PointerByReference"
  ], 
  "LOGFONT**": [
    "PointerByReference"
  ], 
  "PWAITCHAIN_NODE_INFO": [
    "WAITCHAIN_NODE_INFO", 
    "WAITCHAIN_NODE_INFO[]"
  ], 
  "PUSER_INFO_1007*": [
    "PointerByReference"
  ], 
  "VS_FIXEDFILEINFO**": [
    "PointerByReference"
  ], 
  "DFS_INFO_7**": [
    "PointerByReference"
  ], 
  "CONSOLE_FONT_INFO**": [
    "PointerByReference"
  ], 
  "POUTLINETEXTMETRIC": [
    "OUTLINETEXTMETRIC", 
    "OUTLINETEXTMETRIC[]"
  ], 
  "USER_MODALS_INFO_2**": [
    "PointerByReference"
  ], 
  "LPGLYPHSET*": [
    "PointerByReference"
  ], 
  "WER_REPORT_TYPE*": [
    "IntByReference", 
    "int[]"
  ], 
  "PMAT2": [
    "MAT2", 
    "MAT2[]"
  ], 
  "ACL**": [
    "PointerByReference"
  ], 
  "PHEAPENTRY32": [
    "HEAPENTRY32", 
    "HEAPENTRY32[]"
  ], 
  "LPBLENDFUNCTION*": [
    "PointerByReference"
  ], 
  "PEMR": [
    "EMR", 
    "EMR[]"
  ], 
  "LPLANGID": [
    "ShortByReference", 
    "short[]"
  ], 
  "PDFS_INFO_102*": [
    "PointerByReference"
  ], 
  "LPTHREADENTRY32*": [
    "PointerByReference"
  ], 
  "LPPOWERBROADCAST_SETTING*": [
    "PointerByReference"
  ], 
  "LPSHARE_INFO_1*": [
    "PointerByReference"
  ], 
  "ALTTABINFO**": [
    "PointerByReference"
  ], 
  "PSHFILEINFO*": [
    "PointerByReference"
  ], 
  "PALTTABINFO*": [
    "PointerByReference"
  ], 
  "PDH_DATA_ITEM_PATH_ELEMENTS": [
    "PDH_DATA_ITEM_PATH_ELEMENTS"
  ], 
  "HCONV*": [
    "HANDLEByReference"
  ], 
  "DATATYPES_INFO_1": [
    "DATATYPES_INFO_1"
  ], 
  "CONVCONTEXT": [
    "CONVCONTEXT"
  ], 
  "LPIO_COUNTERS": [
    "IO_COUNTERS", 
    "IO_COUNTERS[]"
  ], 
  "BITMAP*": [
    "BITMAP", 
    "BITMAP[]"
  ], 
  "HSZ": [
    "HANDLE"
  ], 
  "GEOCLASS*": [
    "IntByReference", 
    "long[]"
  ], 
  "PUSER_MODALS_INFO_1005*": [
    "PointerByReference"
  ], 
  "PWORD*": [
    "PointerByReference"
  ], 
  "LPCTAPE_GET_DRIVE_PARAMETERS": [
    "TAPE_GET_DRIVE_PARAMETERS", 
    "TAPE_GET_DRIVE_PARAMETERS[]"
  ], 
  "WINSTA": [
    "HANDLE"
  ], 
  "ICONMETRICS**": [
    "PointerByReference"
  ], 
  "USER_INFO_10": [
    "USER_INFO_10"
  ], 
  "PTimeSysInfo": [
    "IntByReference", 
    "int[]"
  ], 
  "PHDC": [
    "HANDLEByReference"
  ], 
  "PMODULEINFO*": [
    "PointerByReference"
  ], 
  "LPCFORM_INFO_2": [
    "FORM_INFO_2", 
    "FORM_INFO_2[]"
  ], 
  "SERVICE_CONTROL_STATUS_REASON_PARAMS**": [
    "PointerByReference"
  ], 
  "PSIZE_T*": [
    "PointerByReference"
  ], 
  "SERVER_TRANSPORT_INFO_3**": [
    "PointerByReference"
  ], 
  "LPFILTERKEYS": [
    "FILTERKEYS", 
    "FILTERKEYS[]"
  ], 
  "KEY_EVENT_RECORD*": [
    "KEY_EVENT_RECORD", 
    "KEY_EVENT_RECORD[]"
  ], 
  "MENUBARINFO**": [
    "PointerByReference"
  ], 
  "DWORD_PTR": [
    "int"
  ], 
  "PETW_BUFFER_CONTEXT*": [
    "PointerByReference"
  ], 
  "HBITMAP*": [
    "HANDLEByReference"
  ], 
  "DWORD_PTR**": [
    "PointerByReference"
  ], 
  "LPRM_APP_TYPE*": [
    "PointerByReference"
  ], 
  "PANIMATIONINFO": [
    "ANIMATIONINFO", 
    "ANIMATIONINFO[]"
  ], 
  "ULARGE_INTEGER**": [
    "PointerByReference"
  ], 
  "LPGROUP_INFO_3": [
    "GROUP_INFO_3", 
    "GROUP_INFO_3[]"
  ], 
  "LPGEOTYPE***": [
    "PointerByReference"
  ], 
  "LPMODULEINFO*": [
    "PointerByReference"
  ], 
  "WER_REPORT_INFORMATION*": [
    "WER_REPORT_INFORMATION", 
    "WER_REPORT_INFORMATION[]"
  ], 
  "PSTR**": [
    "byte"
  ], 
  "CONVINFO*": [
    "CONVINFO", 
    "CONVINFO[]"
  ], 
  "INT64**": [
    "PointerByReference"
  ], 
  "LPCSERVICE_FAILURE_ACTIONS_FLAG": [
    "SERVICE_FAILURE_ACTIONS_FLAG", 
    "SERVICE_FAILURE_ACTIONS_FLAG[]"
  ], 
  "PLONGLONG*": [
    "PointerByReference"
  ], 
  "LPCHEAPENTRY32": [
    "HEAPENTRY32", 
    "HEAPENTRY32[]"
  ], 
  "LPCPROPERTYKEY": [
    "PROPERTYKEY", 
    "PROPERTYKEY[]"
  ], 
  "SYSTEM_INFO": [
    "SYSTEM_INFO"
  ], 
  "GRADIENT_TRIANGLE": [
    "GRADIENT_TRIANGLE"
  ], 
  "LOCALGROUP_MEMBERS_INFO_1*": [
    "LOCALGROUP_MEMBERS_INFO_1", 
    "LOCALGROUP_MEMBERS_INFO_1[]"
  ], 
  "LPCOLORADJUSTMENT": [
    "COLORADJUSTMENT", 
    "COLORADJUSTMENT[]"
  ], 
  "LPPOWER_ACTION": [
    "IntByReference", 
    "int[]"
  ], 
  "SERVICE_LAUNCH_PROTECTED_INFO*": [
    "SERVICE_LAUNCH_PROTECTED_INFO", 
    "SERVICE_LAUNCH_PROTECTED_INFO[]"
  ], 
  "PSHARE_INFO_503": [
    "SHARE_INFO_503", 
    "SHARE_INFO_503[]"
  ], 
  "PSHARE_INFO_502": [
    "SHARE_INFO_502", 
    "SHARE_INFO_502[]"
  ], 
  "PSHARE_INFO_501": [
    "SHARE_INFO_501", 
    "SHARE_INFO_501[]"
  ], 
  "PORT_INFO_2": [
    "PORT_INFO_2"
  ], 
  "PMONITORINFO": [
    "MONITORINFO", 
    "MONITORINFO[]"
  ], 
  "PQUERY_USER_NOTIFICATION_STATE*": [
    "PointerByReference"
  ], 
  "USER_MODALS_INFO_1003*": [
    "USER_MODALS_INFO_1003", 
    "USER_MODALS_INFO_1003[]"
  ], 
  "GLOBAL_POWER_POLICY*": [
    "GLOBAL_POWER_POLICY", 
    "GLOBAL_POWER_POLICY[]"
  ], 
  "WER_REPORT_UI**": [
    "PointerByReference"
  ], 
  "PEFS_HASH_BLOB*": [
    "PointerByReference"
  ], 
  "void*": [
    "Pointer", 
    "LPVOID"
  ], 
  "EMRALPHABLEND**": [
    "PointerByReference"
  ], 
  "FILE_SEGMENT_ELEMENT**": [
    "PointerByReference"
  ], 
  "PICONMETRICS": [
    "ICONMETRICS", 
    "ICONMETRICS[]"
  ], 
  "LPRID_DEVICE_INFO_KEYBOARD*": [
    "PointerByReference"
  ], 
  "LPPGEOTYPE**": [
    "PointerByReference"
  ], 
  "LPTAPE_GET_DRIVE_PARAMETERS*": [
    "PointerByReference"
  ], 
  "PSESSION_INFO_1": [
    "SESSION_INFO_1", 
    "SESSION_INFO_1[]"
  ], 
  "PSESSION_INFO_0": [
    "SESSION_INFO_0", 
    "SESSION_INFO_0[]"
  ], 
  "LPADDJOB_INFO_1": [
    "ADDJOB_INFO_1", 
    "ADDJOB_INFO_1[]"
  ], 
  "PTSTR*": [
    "WString[]", 
    "PointerByReference"
  ], 
  "CONNECTION_INFO_0**": [
    "PointerByReference"
  ], 
  "DFS_INFO_300": [
    "DFS_INFO_300"
  ], 
  "DRAWTEXTPARAMS**": [
    "PointerByReference"
  ], 
  "USER_INFO_21*": [
    "USER_INFO_21", 
    "USER_INFO_21[]"
  ], 
  "DLGTEMPLATE**": [
    "PointerByReference"
  ], 
  "PREMOTE_NAME_INFO*": [
    "PointerByReference"
  ], 
  "PPGEOCLASS**": [
    "PointerByReference"
  ], 
  "LPSERVER_INFO_403*": [
    "PointerByReference"
  ], 
  "AT_ENUM*": [
    "AT_ENUM", 
    "AT_ENUM[]"
  ], 
  "PWINDOW_BUFFER_SIZE_RECORD*": [
    "PointerByReference"
  ], 
  "GLOBAL_POWER_POLICY**": [
    "PointerByReference"
  ], 
  "PTpcGetSamplesArgs": [
    "TpcGetSamplesArgs", 
    "TpcGetSamplesArgs[]"
  ], 
  "LPUSHORT*": [
    "PointerByReference"
  ], 
  "FORM_INFO_2**": [
    "PointerByReference"
  ], 
  "LPCMENUITEMINFO": [
    "MENUITEMINFO", 
    "MENUITEMINFO[]"
  ], 
  "DFS_STORAGE_INFO_1**": [
    "PointerByReference"
  ], 
  "PACL": [
    "ACL", 
    "ACL[]", 
    "Pointer"
  ], 
  "PRINTER_INFO_3**": [
    "PointerByReference"
  ], 
  "RAWHID": [
    "RAWHID"
  ], 
  "USER_INFO_1009*": [
    "USER_INFO_1009", 
    "USER_INFO_1009[]"
  ], 
  "PDH_STATISTICS**": [
    "PointerByReference"
  ], 
  "DFS_INFO_102**": [
    "PointerByReference"
  ], 
  "PPRINTER_INFO_3*": [
    "PointerByReference"
  ], 
  "PEMR*": [
    "PointerByReference"
  ], 
  "SESSION_INFO_0": [
    "SESSION_INFO_0"
  ], 
  "SESSION_INFO_1": [
    "SESSION_INFO_1"
  ], 
  "SESSION_INFO_2": [
    "SESSION_INFO_2"
  ], 
  "FINDEX_SEARCH_OPS**": [
    "PointerByReference"
  ], 
  "LPCSHFILEINFO": [
    "SHFILEINFO", 
    "SHFILEINFO[]"
  ], 
  "PROVIDOR_INFO_1*": [
    "PROVIDOR_INFO_1", 
    "PROVIDOR_INFO_1[]"
  ], 
  "LPCFORMATETC": [
    "FORMATETC", 
    "FORMATETC[]"
  ], 
  "PGPFIDL_FLAGS*": [
    "PointerByReference"
  ], 
  "PGROUP_USERS_INFO_1*": [
    "PointerByReference"
  ], 
  "LPCDFS_INFO_50": [
    "DFS_INFO_50", 
    "DFS_INFO_50[]"
  ], 
  "OPEN_AS_INFO_FLAGS": [
    "int"
  ], 
  "PFINDEX_INFO_LEVELS": [
    "IntByReference", 
    "int[]"
  ], 
  "MODULEINFO": [
    "MODULEINFO"
  ], 
  "PCONSOLE_SCREEN_BUFFER_INFOEX": [
    "CONSOLE_SCREEN_BUFFER_INFOEX", 
    "CONSOLE_SCREEN_BUFFER_INFOEX[]"
  ], 
  "PDH_FMT_COUNTERVALUE_ITEM": [
    "PDH_FMT_COUNTERVALUE_ITEM"
  ], 
  "LPPRINTER_INFO_5*": [
    "PointerByReference"
  ], 
  "USER_INFO_3**": [
    "PointerByReference"
  ], 
  "SC_ACTION*": [
    "SC_ACTION", 
    "SC_ACTION[]"
  ], 
  "LPCAUDIODESCRIPTION": [
    "AUDIODESCRIPTION", 
    "AUDIODESCRIPTION[]"
  ], 
  "LPCSECURITY_ATTRIBUTES": [
    "SECURITY_ATTRIBUTES", 
    "SECURITY_ATTRIBUTES[]"
  ], 
  "PSYSGEOCLASS*": [
    "PointerByReference"
  ], 
  "SERVER_TRANSPORT_INFO_1*": [
    "SERVER_TRANSPORT_INFO_1", 
    "SERVER_TRANSPORT_INFO_1[]"
  ], 
  "LPCALTYPE": [
    "IntByReference", 
    "long[]"
  ], 
  "LPWINDOW_BUFFER_SIZE_RECORD": [
    "WINDOW_BUFFER_SIZE_RECORD", 
    "WINDOW_BUFFER_SIZE_RECORD[]"
  ], 
  "LPCULARGE_INTEGER": [
    "ULARGE_INTEGER", 
    "ULARGE_INTEGER[]"
  ], 
  "LPSYSGEOTYPE": [
    "IntByReference", 
    "int[]"
  ], 
  "LPJOB_INFO_1*": [
    "PointerByReference"
  ], 
  "LPTEXTMETRIC*": [
    "PointerByReference"
  ], 
  "LPENUMLOGFONTEXDV": [
    "ENUMLOGFONTEXDV", 
    "ENUMLOGFONTEXDV[]"
  ], 
  "DFS_INFO_104**": [
    "PointerByReference"
  ], 
  "ICONINFO**": [
    "PointerByReference"
  ], 
  "SERVER_INFO_102**": [
    "PointerByReference"
  ], 
  "WER_CONSENT**": [
    "PointerByReference"
  ], 
  "SERVER_TRANSPORT_INFO_0**": [
    "PointerByReference"
  ], 
  "SYSTEM_POWER_CAPABILITIES**": [
    "PointerByReference"
  ], 
  "LPHANDLETABLE": [
    "HANDLETABLE", 
    "HANDLETABLE[]"
  ], 
  "PSERVICE_LAUNCH_PROTECTED_INFO*": [
    "PointerByReference"
  ], 
  "LPCPROCESS_HEAP_ENTRY": [
    "PROCESS_HEAP_ENTRY", 
    "PROCESS_HEAP_ENTRY[]"
  ], 
  "LPSECURITY_QUALITY_OF_SERVICE*": [
    "PointerByReference"
  ], 
  "WER_REPORT_INFORMATION": [
    "WER_REPORT_INFORMATION"
  ], 
  "LPENHMETARECORD*": [
    "PointerByReference"
  ], 
  "LPDOC_INFO_1": [
    "DOC_INFO_1", 
    "DOC_INFO_1[]"
  ], 
  "PWIN32_FIND_STREAM_DATA": [
    "WIN32_FIND_STREAM_DATA", 
    "WIN32_FIND_STREAM_DATA[]"
  ], 
  "PSHARE_INFO_1004": [
    "SHARE_INFO_1004", 
    "SHARE_INFO_1004[]"
  ], 
  "PSHARE_INFO_1005": [
    "SHARE_INFO_1005", 
    "SHARE_INFO_1005[]"
  ], 
  "PSHARE_INFO_1006": [
    "SHARE_INFO_1006", 
    "SHARE_INFO_1006[]"
  ], 
  "PRID_DEVICE_INFO_HID": [
    "RID_DEVICE_INFO_HID", 
    "RID_DEVICE_INFO_HID[]"
  ], 
  "PSCROLLBARINFO": [
    "SCROLLBARINFO", 
    "SCROLLBARINFO[]"
  ], 
  "SC_HANDLE*": [
    "HANDLEByReference"
  ], 
  "PHGDIOBJ": [
    "HANDLEByReference"
  ], 
  "PROPERTYKEY**": [
    "PointerByReference"
  ], 
  "LPJOBOBJECT_RATE_CONTROL_TOLERANCE*": [
    "PointerByReference"
  ], 
  "PCBT_CREATEWND": [
    "CBT_CREATEWND", 
    "CBT_CREATEWND[]"
  ], 
  "PPORT_INFO_1": [
    "PORT_INFO_1", 
    "PORT_INFO_1[]"
  ], 
  "PPORT_INFO_3": [
    "PORT_INFO_3", 
    "PORT_INFO_3[]"
  ], 
  "PPORT_INFO_2": [
    "PORT_INFO_2", 
    "PORT_INFO_2[]"
  ], 
  "LPUSER_MODALS_INFO_1*": [
    "PointerByReference"
  ], 
  "PENUMLOGFONTEX": [
    "ENUMLOGFONTEX", 
    "ENUMLOGFONTEX[]"
  ], 
  "LPCNETCONNECTINFOSTRUCT": [
    "NETCONNECTINFOSTRUCT", 
    "NETCONNECTINFOSTRUCT[]"
  ], 
  "SYSGEOTYPE*": [
    "IntByReference", 
    "int[]"
  ], 
  "PSERVICE_DELAYED_AUTO_START_INFO": [
    "SERVICE_DELAYED_AUTO_START_INFO", 
    "SERVICE_DELAYED_AUTO_START_INFO[]"
  ], 
  "PICONINFO*": [
    "PointerByReference"
  ], 
  "LPBSMINFO*": [
    "PointerByReference"
  ], 
  "LPMENUITEMTEMPLATE*": [
    "PointerByReference"
  ], 
  "LPCSESSION_INFO_502": [
    "SESSION_INFO_502", 
    "SESSION_INFO_502[]"
  ], 
  "MODULEINFO*": [
    "MODULEINFO", 
    "MODULEINFO[]"
  ], 
  "PMDICREATESTRUCT": [
    "MDICREATESTRUCT", 
    "MDICREATESTRUCT[]"
  ], 
  "PPRINTPROCESSOR_INFO_1": [
    "PRINTPROCESSOR_INFO_1", 
    "PRINTPROCESSOR_INFO_1[]"
  ], 
  "PRINTPAGERANGE*": [
    "PRINTPAGERANGE", 
    "PRINTPAGERANGE[]"
  ], 
  "PLGRPID*": [
    "PointerByReference"
  ], 
  "MEMORYSTATUSEX*": [
    "MEMORYSTATUSEX", 
    "MEMORYSTATUSEX[]"
  ], 
  "POSVERSIONINFOEX": [
    "OSVERSIONINFOEX", 
    "OSVERSIONINFOEX[]"
  ], 
  "HMODULE*": [
    "HANDLEByReference"
  ], 
  "LPPRINTER_DEFAULTS": [
    "PRINTER_DEFAULTS", 
    "PRINTER_DEFAULTS[]"
  ], 
  "PMSG_INFO_1*": [
    "PointerByReference"
  ], 
  "PHICON": [
    "HANDLEByReference"
  ], 
  "PDFS_INFO_106*": [
    "PointerByReference"
  ], 
  "PLOGPEN*": [
    "PointerByReference"
  ], 
  "JOB_INFO_1**": [
    "PointerByReference"
  ], 
  "LPJOBOBJECT_BASIC_LIMIT_INFORMATION": [
    "JOBOBJECT_BASIC_LIMIT_INFORMATION", 
    "JOBOBJECT_BASIC_LIMIT_INFORMATION[]"
  ], 
  "PCALID*": [
    "PointerByReference"
  ], 
  "PGROUP_INFO_3*": [
    "PointerByReference"
  ], 
  "LPPRINTER_NOTIFY_OPTIONS_TYPE*": [
    "PointerByReference"
  ], 
  "GETPROPERTYSTOREFLAGS*": [
    "IntByReference", 
    "int[]"
  ], 
  "LPCDFS_STORAGE_INFO": [
    "DFS_STORAGE_INFO", 
    "DFS_STORAGE_INFO[]"
  ], 
  "SYSTEM_POWER_LEVEL": [
    "SYSTEM_POWER_LEVEL"
  ], 
  "LPPRINTER_INFO_1*": [
    "PointerByReference"
  ], 
  "SE_OBJECT_TYPE": [
    "int"
  ], 
  "PCWPRETSTRUCT*": [
    "PointerByReference"
  ], 
  "PPDH_RAW_COUNTER": [
    "PDH_RAW_COUNTER", 
    "PDH_RAW_COUNTER[]"
  ], 
  "LPSHSTOCKICONINFO": [
    "SHSTOCKICONINFO", 
    "SHSTOCKICONINFO[]"
  ], 
  "GCP_RESULTS**": [
    "PointerByReference"
  ], 
  "SERVICE_TRIGGER_INFO*": [
    "SERVICE_TRIGGER_INFO", 
    "SERVICE_TRIGGER_INFO[]"
  ], 
  "PWORD": [
    "ShortByReference", 
    "short[]"
  ], 
  "LPPROCESS_INFORMATION*": [
    "PointerByReference"
  ], 
  "SECURITY_INFORMATION": [
    "int"
  ], 
  "LPLRESULT": [
    "IntByReference", 
    "long[]"
  ], 
  "GROUP_INFO_1*": [
    "GROUP_INFO_1", 
    "GROUP_INFO_1[]"
  ], 
  "LPDRIVER_INFO_6": [
    "DRIVER_INFO_6", 
    "DRIVER_INFO_6[]"
  ], 
  "PSHDESCRIPTIONID*": [
    "PointerByReference"
  ], 
  "LPPDH_DATA_ITEM_PATH_ELEMENTS*": [
    "PointerByReference"
  ], 
  "LPTOKEN_PRIVILEGES": [
    "TOKEN_PRIVILEGES", 
    "TOKEN_PRIVILEGES[]"
  ], 
  "PPWSTR": [
    "char"
  ], 
  "LPCSOUNDSENTRY": [
    "SOUNDSENTRY", 
    "SOUNDSENTRY[]"
  ], 
  "THREADENTRY32**": [
    "PointerByReference"
  ], 
  "MENUINFO": [
    "MENUINFO"
  ], 
  "LPSHSTOCKICONID": [
    "IntByReference", 
    "int[]"
  ], 
  "UINT16**": [
    "PointerByReference"
  ], 
  "PMETARECORD": [
    "METARECORD", 
    "METARECORD[]"
  ], 
  "LPDFS_INFO_200*": [
    "PointerByReference"
  ], 
  "SC_ACTION**": [
    "PointerByReference"
  ], 
  "PWKSTA_USER_INFO_1*": [
    "PointerByReference"
  ], 
  "SHELLEXECUTEINFO": [
    "SHELLEXECUTEINFO"
  ], 
  "GENERIC_MAPPING": [
    "GENERIC_MAPPING"
  ], 
  "SHARE_INFO_0*": [
    "SHARE_INFO_0", 
    "SHARE_INFO_0[]"
  ], 
  "PRM_PROCESS_INFO": [
    "RM_PROCESS_INFO", 
    "RM_PROCESS_INFO[]"
  ], 
  "HMETAFILE": [
    "HANDLE"
  ], 
  "PCWPSTRUCT": [
    "CWPSTRUCT", 
    "CWPSTRUCT[]"
  ], 
  "DWORD**": [
    "PointerByReference"
  ], 
  "OUTLINETEXTMETRIC": [
    "OUTLINETEXTMETRIC"
  ], 
  "PSYSTEM_LOGICAL_PROCESSOR_INFORMATION": [
    "SYSTEM_LOGICAL_PROCESSOR_INFORMATION", 
    "SYSTEM_LOGICAL_PROCESSOR_INFORMATION[]", 
    "Pointer"
  ], 
  "PWER_REPORT_TYPE*": [
    "PointerByReference"
  ], 
  "LPCOMBOBOXINFO*": [
    "PointerByReference"
  ], 
  "LPWER_CONSENT": [
    "IntByReference", 
    "int[]"
  ], 
  "HGDIOBJ*": [
    "HANDLEByReference"
  ], 
  "PBSMINFO*": [
    "PointerByReference"
  ], 
  "PULARGE_INTEGER*": [
    "PointerByReference"
  ], 
  "PFILETIME": [
    "FILETIME", 
    "FILETIME[]"
  ], 
  "LPPCSTR": [
    "byte"
  ], 
  "USER_INFO_1017**": [
    "PointerByReference"
  ], 
  "DFS_INFO_5*": [
    "DFS_INFO_5", 
    "DFS_INFO_5[]"
  ], 
  "PDFS_INFO_9*": [
    "PointerByReference"
  ], 
  "PPALETTEENTRY*": [
    "PointerByReference"
  ], 
  "FILTERKEYS": [
    "FILTERKEYS"
  ], 
  "LPSYSTEM_BATTERY_STATE": [
    "SYSTEM_BATTERY_STATE", 
    "SYSTEM_BATTERY_STATE[]"
  ], 
  "BY_HANDLE_FILE_INFORMATION": [
    "BY_HANDLE_FILE_INFORMATION"
  ], 
  "LPSERVICE_DESCRIPTION": [
    "SERVICE_DESCRIPTION", 
    "SERVICE_DESCRIPTION[]"
  ], 
  "LPWINDOWPLACEMENT": [
    "WINDOWPLACEMENT", 
    "WINDOWPLACEMENT[]"
  ], 
  "MOUSEHOOKSTRUCT": [
    "MOUSEHOOKSTRUCT"
  ], 
  "LMSTR**": [
    "char"
  ], 
  "LPUSER_MODALS_INFO_1005*": [
    "PointerByReference"
  ], 
  "TOKEN_PRIVILEGES": [
    "TOKEN_PRIVILEGES"
  ], 
  "LPMENUBARINFO*": [
    "PointerByReference"
  ], 
  "LPCLOGBRUSH": [
    "LOGBRUSH", 
    "LOGBRUSH[]"
  ], 
  "SERVICE_CONTROL_STATUS_REASON_PARAMS": [
    "SERVICE_CONTROL_STATUS_REASON_PARAMS"
  ], 
  "PVIDEOPARAMETERS": [
    "VIDEOPARAMETERS", 
    "VIDEOPARAMETERS[]"
  ], 
  "LPJOB_INFO_1": [
    "JOB_INFO_1", 
    "JOB_INFO_1[]"
  ], 
  "HDESK*": [
    "HANDLEByReference"
  ], 
  "LPCICONMETRICS": [
    "ICONMETRICS", 
    "ICONMETRICS[]"
  ], 
  "EVENT_INSTANCE_INFO": [
    "EVENT_INSTANCE_INFO"
  ], 
  "USER_INFO_1012": [
    "USER_INFO_1012"
  ], 
  "PSERVER_TRANSPORT_INFO_2*": [
    "PointerByReference"
  ], 
  "LPLANGID*": [
    "PointerByReference"
  ], 
  "PLASTINPUTINFO": [
    "LASTINPUTINFO", 
    "LASTINPUTINFO[]"
  ], 
  "TITLEBARINFO**": [
    "PointerByReference"
  ], 
  "LPPGEOCLASS**": [
    "PointerByReference"
  ], 
  "LOCALGROUP_MEMBERS_INFO_2": [
    "LOCALGROUP_MEMBERS_INFO_2"
  ], 
  "PWER_FILE_TYPE*": [
    "PointerByReference"
  ], 
  "PTAPE_GET_MEDIA_PARAMETERS": [
    "TAPE_GET_MEDIA_PARAMETERS", 
    "TAPE_GET_MEDIA_PARAMETERS[]"
  ], 
  "LPUSE_INFO_2*": [
    "PointerByReference"
  ], 
  "PSERVICE_PRESHUTDOWN_INFO": [
    "SERVICE_PRESHUTDOWN_INFO", 
    "SERVICE_PRESHUTDOWN_INFO[]"
  ], 
  "LPMENU_EVENT_RECORD": [
    "MENU_EVENT_RECORD", 
    "MENU_EVENT_RECORD[]"
  ], 
  "GCP_RESULTS*": [
    "GCP_RESULTS", 
    "GCP_RESULTS[]"
  ], 
  "PCONVCONTEXT": [
    "CONVCONTEXT", 
    "CONVCONTEXT[]"
  ], 
  "USER_INFO_1005**": [
    "PointerByReference"
  ], 
  "LPCNTMS_ALLOCATION_INFORMATION": [
    "NTMS_ALLOCATION_INFORMATION", 
    "NTMS_ALLOCATION_INFORMATION[]"
  ], 
  "PNEWTEXTMETRICEX": [
    "NEWTEXTMETRICEX", 
    "NEWTEXTMETRICEX[]"
  ], 
  "LPUSER_INFO_11*": [
    "PointerByReference"
  ], 
  "PTITLEBARINFO*": [
    "PointerByReference"
  ], 
  "LPUSER_INFO_4": [
    "USER_INFO_4", 
    "USER_INFO_4[]"
  ], 
  "TRACE_LOGFILE_HEADER": [
    "TRACE_LOGFILE_HEADER"
  ], 
  "LPLPWSTR*": [
    "char"
  ], 
  "LPUSER_INFO_0": [
    "USER_INFO_0", 
    "USER_INFO_0[]"
  ], 
  "LPUSER_INFO_1": [
    "USER_INFO_1", 
    "USER_INFO_1[]"
  ], 
  "LPUSER_INFO_2": [
    "USER_INFO_2", 
    "USER_INFO_2[]"
  ], 
  "LPUSER_INFO_3": [
    "USER_INFO_3", 
    "USER_INFO_3[]"
  ], 
  "PFILTERKEYS*": [
    "PointerByReference"
  ], 
  "PSCNRT_STATUS*": [
    "PointerByReference"
  ], 
  "UINT64*": [
    "LongByReference", 
    "__int64[]"
  ], 
  "PCOORD*": [
    "PointerByReference"
  ], 
  "FIXED**": [
    "PointerByReference"
  ], 
  "LPPRINTPAGERANGE*": [
    "PointerByReference"
  ], 
  "PTOKEN_DEFAULT_DACL": [
    "TOKEN_DEFAULT_DACL", 
    "TOKEN_DEFAULT_DACL[]"
  ], 
  "LPRID_DEVICE_INFO": [
    "RID_DEVICE_INFO", 
    "RID_DEVICE_INFO[]"
  ], 
  "PJOBOBJECT_BASIC_AND_IO_ACCOUNTING_INFORMATION": [
    "JOBOBJECT_BASIC_AND_IO_ACCOUNTING_INFORMATION", 
    "JOBOBJECT_BASIC_AND_IO_ACCOUNTING_INFORMATION[]"
  ], 
  "LPSIGDN": [
    "IntByReference", 
    "int[]"
  ], 
  "ANIMATIONINFO": [
    "ANIMATIONINFO"
  ], 
  "RM_FILTER_ACTION*": [
    "IntByReference", 
    "int[]"
  ], 
  "PSECURITY_DESCRIPTOR": [
    "Pointer"
  ], 
  "MENUEX_TEMPLATE_HEADER": [
    "MENUEX_TEMPLATE_HEADER"
  ], 
  "LPCGUITHREADINFO": [
    "GUITHREADINFO", 
    "GUITHREADINFO[]"
  ], 
  "LPCSERVICE_PREFERRED_NODE_INFO": [
    "SERVICE_PREFERRED_NODE_INFO", 
    "SERVICE_PREFERRED_NODE_INFO[]"
  ], 
  "LPCCONSOLE_SCREEN_BUFFER_INFO": [
    "CONSOLE_SCREEN_BUFFER_INFO", 
    "CONSOLE_SCREEN_BUFFER_INFO[]"
  ], 
  "LPWIN32_FIND_STREAM_DATA": [
    "WIN32_FIND_STREAM_DATA", 
    "WIN32_FIND_STREAM_DATA[]"
  ], 
  "LPCKEY_EVENT_RECORD": [
    "KEY_EVENT_RECORD", 
    "KEY_EVENT_RECORD[]"
  ], 
  "LONG32*": [
    "IntByReference", 
    "int[]"
  ], 
  "SERVICE_DESCRIPTION**": [
    "PointerByReference"
  ], 
  "ACL_INFORMATION_CLASS*": [
    "IntByReference", 
    "int[]"
  ], 
  "LPSHELLFLAGSTATE": [
    "SHELLFLAGSTATE", 
    "SHELLFLAGSTATE[]"
  ], 
  "LPHCOLORSPACE": [
    "HANDLEByReference"
  ], 
  "LPCSERVICE_DESCRIPTION": [
    "SERVICE_DESCRIPTION", 
    "SERVICE_DESCRIPTION[]"
  ], 
  "LPCMINIMIZEDMETRICS": [
    "MINIMIZEDMETRICS", 
    "MINIMIZEDMETRICS[]"
  ], 
  "PSID_AND_ATTRIBUTES": [
    "SID_AND_ATTRIBUTES", 
    "SID_AND_ATTRIBUTES[]"
  ], 
  "ALTTABINFO": [
    "ALTTABINFO"
  ], 
  "short": [
    "short"
  ], 
  "LPFLOAT*": [
    "PointerByReference"
  ], 
  "EVENT_TRACE_HEADER**": [
    "PointerByReference"
  ], 
  "LPLPHANDLE": [
    "HANDLEByReference"
  ], 
  "HCURSOR": [
    "HANDLE", 
    "HCURSOR"
  ], 
  "LPOPENASINFO*": [
    "PointerByReference"
  ], 
  "APPBARDATA*": [
    "APPBARDATA", 
    "APPBARDATA[]"
  ], 
  "DOCINFO": [
    "DOCINFO"
  ], 
  "HKL*": [
    "HANDLEByReference"
  ], 
  "LPTRACKMOUSEEVENT*": [
    "PointerByReference"
  ], 
  "PPGEOTYPE*": [
    "IntByReference", 
    "long[]"
  ], 
  "long long*": [
    "LongByReference", 
    "long long[]"
  ], 
  "DWORD*": [
    "IntByReference", 
    "long[]", 
    "DWORDByReference"
  ], 
  "PSHFILEINFO": [
    "SHFILEINFO", 
    "SHFILEINFO[]"
  ], 
  "PDYNAMIC_TIME_ZONE_INFORMATION": [
    "DYNAMIC_TIME_ZONE_INFORMATION", 
    "DYNAMIC_TIME_ZONE_INFORMATION[]"
  ], 
  "LPFONTSIGNATURE*": [
    "PointerByReference"
  ], 
  "WMIDPREQUESTCODE*": [
    "IntByReference", 
    "int[]"
  ], 
  "PDH_TIME_INFO**": [
    "PointerByReference"
  ], 
  "VIDEOPARAMETERS**": [
    "PointerByReference"
  ], 
  "LPSHARE_INFO_2*": [
    "PointerByReference"
  ], 
  "PGEOTYPE**": [
    "IntByReference", 
    "long[]"
  ], 
  "LPPALETTEENTRY": [
    "PALETTEENTRY", 
    "PALETTEENTRY[]"
  ], 
  "LPCONSOLE_SELECTION_INFO": [
    "CONSOLE_SELECTION_INFO", 
    "CONSOLE_SELECTION_INFO[]"
  ], 
  "LPKERNINGPAIR": [
    "KERNINGPAIR", 
    "KERNINGPAIR[]"
  ], 
  "LPUMS_CREATE_THREAD_ATTRIBUTES*": [
    "PointerByReference"
  ], 
  "PDFS_INFO_50*": [
    "PointerByReference"
  ], 
  "DWORD64": [
    "long"
  ], 
  "LPLOCALGROUP_MEMBERS_INFO_0*": [
    "PointerByReference"
  ], 
  "HHOOK": [
    "HANDLE"
  ], 
  "LOCALGROUP_INFO_0*": [
    "LOCALGROUP_INFO_0", 
    "LOCALGROUP_INFO_0[]"
  ], 
  "PWINDOW_BUFFER_SIZE_RECORD": [
    "WINDOW_BUFFER_SIZE_RECORD", 
    "WINDOW_BUFFER_SIZE_RECORD[]"
  ], 
  "LPWINSTA": [
    "HANDLEByReference"
  ], 
  "ULONG_PTR": [
    "int", 
    "Pointer"
  ], 
  "POFSTRUCT*": [
    "PointerByReference"
  ], 
  "NETRESOURCE**": [
    "PointerByReference"
  ], 
  "TOKEN_DEFAULT_DACL**": [
    "PointerByReference"
  ], 
  "LPACL_INFORMATION_CLASS": [
    "IntByReference", 
    "int[]"
  ], 
  "LPSHARE_INFO_0*": [
    "PointerByReference"
  ], 
  "PRID_DEVICE_INFO_HID*": [
    "PointerByReference"
  ], 
  "RECTL": [
    "RECTL"
  ], 
  "PLPWSTR*": [
    "char"
  ], 
  "LPINT16": [
    "ShortByReference", 
    "short[]"
  ], 
  "UINT16": [
    "short"
  ], 
  "LPCWCT_OBJECT_TYPE": [
    "IntByReference", 
    "int[]"
  ], 
  "WAITCHAIN_NODE_INFO**": [
    "PointerByReference"
  ], 
  "PMENUEX_TEMPLATE_HEADER": [
    "MENUEX_TEMPLATE_HEADER", 
    "MENUEX_TEMPLATE_HEADER[]"
  ], 
  "PLOCALGROUP_MEMBERS_INFO_1": [
    "LOCALGROUP_MEMBERS_INFO_1", 
    "LOCALGROUP_MEMBERS_INFO_1[]"
  ], 
  "SERVER_INFO_402": [
    "SERVER_INFO_402"
  ], 
  "SERVER_INFO_403": [
    "SERVER_INFO_403"
  ], 
  "LPWER_REPORT_TYPE": [
    "IntByReference", 
    "int[]"
  ], 
  "PCHAR*": [
    "byte"
  ], 
  "PWINDOWINFO": [
    "WINDOWINFO", 
    "WINDOWINFO[]"
  ], 
  "PGEOCLASS**": [
    "IntByReference", 
    "long[]"
  ], 
  "PSTR": [
    "ByteByReference", 
    "char[]", 
    "String"
  ], 
  "GRADIENT_RECT": [
    "GRADIENT_RECT"
  ], 
  "POINT**": [
    "PointerByReference"
  ], 
  "UMS_CREATE_THREAD_ATTRIBUTES**": [
    "PointerByReference"
  ], 
  "LPCWSTR": [
    "CHARByReference", 
    "wchar_t[]", 
    "WString"
  ], 
  "LPGROUP_USERS_INFO_0*": [
    "PointerByReference"
  ], 
  "PRINTER_ENUM_VALUES": [
    "PRINTER_ENUM_VALUES"
  ], 
  "LPHRSRC": [
    "HANDLEByReference"
  ], 
  "SCNRT_STATUS**": [
    "PointerByReference"
  ], 
  "LOCALGROUP_MEMBERS_INFO_2*": [
    "LOCALGROUP_MEMBERS_INFO_2", 
    "LOCALGROUP_MEMBERS_INFO_2[]"
  ], 
  "LPGCP_RESULTS*": [
    "PointerByReference"
  ], 
  "TRACKMOUSEEVENT**": [
    "PointerByReference"
  ], 
  "LPRID_DEVICE_INFO_HID": [
    "RID_DEVICE_INFO_HID", 
    "RID_DEVICE_INFO_HID[]"
  ], 
  "PENUM_SERVICE_STATUS*": [
    "PointerByReference"
  ], 
  "PRIVILEGE_SET*": [
    "PRIVILEGE_SET", 
    "PRIVILEGE_SET[]"
  ], 
  "LPUSE_INFO_1": [
    "USE_INFO_1", 
    "USE_INFO_1[]"
  ], 
  "GROUP_AFFINITY*": [
    "GROUP_AFFINITY", 
    "GROUP_AFFINITY[]"
  ], 
  "USHORT**": [
    "PointerByReference"
  ], 
  "MONITORINFO*": [
    "MONITORINFO", 
    "MONITORINFO[]"
  ], 
  "LPCTRACE_LOGFILE_HEADER": [
    "TRACE_LOGFILE_HEADER", 
    "TRACE_LOGFILE_HEADER[]"
  ], 
  "USER_INFO_1051*": [
    "USER_INFO_1051", 
    "USER_INFO_1051[]"
  ], 
  "PHHOOK": [
    "HANDLEByReference"
  ], 
  "HMODULE": [
    "HANDLE", 
    "HMODULE"
  ], 
  "LPVOID": [
    "Pointer", 
    "LPVOID", 
    "String", 
    "Structure", 
    "byte[]", 
    "ByteBuffer"
  ], 
  "PPSAPI_WS_WATCH_INFORMATION*": [
    "PointerByReference"
  ], 
  "PHSZ": [
    "HANDLEByReference"
  ], 
  "PCHAR_INFO*": [
    "PointerByReference"
  ], 
  "WORD**": [
    "PointerByReference"
  ], 
  "SERVICE_STATUS_PROCESS**": [
    "PointerByReference"
  ], 
  "USEROBJECTFLAGS": [
    "USEROBJECTFLAGS"
  ], 
  "EVENT_TRACE*": [
    "EVENT_TRACE", 
    "EVENT_TRACE[]"
  ], 
  "LPPZZWSTR": [
    "char"
  ], 
  "PSHARE_INFO_502*": [
    "PointerByReference"
  ], 
  "POWER_ACTION": [
    "int"
  ], 
  "LPCJOBOBJECT_BASIC_LIMIT_INFORMATION": [
    "JOBOBJECT_BASIC_LIMIT_INFORMATION", 
    "JOBOBJECT_BASIC_LIMIT_INFORMATION[]"
  ], 
  "PDFS_TARGET_PRIORITY": [
    "DFS_TARGET_PRIORITY", 
    "DFS_TARGET_PRIORITY[]"
  ], 
  "PTHREADENTRY32*": [
    "PointerByReference"
  ], 
  "LPPDH_COUNTER_INFO*": [
    "PointerByReference"
  ], 
  "PJOBOBJECT_BASIC_PROCESS_ID_LIST": [
    "JOBOBJECT_BASIC_PROCESS_ID_LIST", 
    "JOBOBJECT_BASIC_PROCESS_ID_LIST[]"
  ], 
  "STAT_SERVER_0**": [
    "PointerByReference"
  ], 
  "LPCGEOCLASS": [
    "int"
  ], 
  "SSIZE_T**": [
    "PointerByReference"
  ], 
  "PPDH_RAW_COUNTER_ITEM": [
    "PDH_RAW_COUNTER_ITEM", 
    "PDH_RAW_COUNTER_ITEM[]"
  ], 
  "LPMENUITEMTEMPLATEHEADER*": [
    "PointerByReference"
  ], 
  "PKAFFINITY": [
    "IntByReference", 
    "long[]"
  ], 
  "LPLGRPID": [
    "IntByReference", 
    "long[]"
  ], 
  "PCCHAR": [
    "byte"
  ], 
  "PSYSGEOTYPE*": [
    "PointerByReference"
  ], 
  "PUSER_POWER_POLICY*": [
    "PointerByReference"
  ], 
  "EMR*": [
    "EMR", 
    "EMR[]"
  ], 
  "LPMAT2": [
    "MAT2", 
    "MAT2[]"
  ], 
  "HDC": [
    "HANDLE"
  ], 
  "LPEVENTLOG_FULL_INFORMATION*": [
    "PointerByReference"
  ], 
  "PUSEROBJECTFLAGS": [
    "USEROBJECTFLAGS", 
    "USEROBJECTFLAGS[]"
  ], 
  "LPSHELLFLAGSTATE*": [
    "PointerByReference"
  ], 
  "INT64*": [
    "LongByReference", 
    "__int64[]"
  ], 
  "ACCEL**": [
    "PointerByReference"
  ], 
  "HDWP*": [
    "HANDLEByReference"
  ], 
  "LPCDOCINFO": [
    "DOCINFO", 
    "DOCINFO[]"
  ], 
  "LPJOB_INFO_4*": [
    "PointerByReference"
  ], 
  "MENUITEMTEMPLATEHEADER*": [
    "MENUITEMTEMPLATEHEADER", 
    "MENUITEMTEMPLATEHEADER[]"
  ], 
  "SERVICE_FAILURE_ACTIONS_FLAG": [
    "SERVICE_FAILURE_ACTIONS_FLAG"
  ], 
  "LPSERVER_INFO_402*": [
    "PointerByReference"
  ], 
  "LPLPCGEOCLASS*": [
    "PointerByReference"
  ], 
  "PHGLOBAL": [
    "HANDLEByReference"
  ], 
  "LPCLOCALGROUP_INFO_1": [
    "LOCALGROUP_INFO_1", 
    "LOCALGROUP_INFO_1[]"
  ], 
  "LPWER_FILE_TYPE": [
    "IntByReference", 
    "int[]"
  ], 
  "LPTPMPARAMS": [
    "TPMPARAMS", 
    "TPMPARAMS[]"
  ], 
  "PORT_INFO_3": [
    "PORT_INFO_3"
  ], 
  "PORT_INFO_1": [
    "PORT_INFO_1"
  ], 
  "MENUITEMTEMPLATE**": [
    "PointerByReference"
  ], 
  "LPSESSION_INFO_2*": [
    "PointerByReference"
  ], 
  "JOB_INFO_1*": [
    "JOB_INFO_1", 
    "JOB_INFO_1[]"
  ], 
  "PCTSTR**": [
    "char"
  ], 
  "AT_INFO**": [
    "PointerByReference"
  ], 
  "DFS_INFO_9": [
    "DFS_INFO_9"
  ], 
  "LPCWER_FILE_TYPE": [
    "IntByReference", 
    "int[]"
  ], 
  "LPEMR": [
    "EMR", 
    "EMR[]"
  ], 
  "ULONGLONG**": [
    "PointerByReference"
  ], 
  "LPUSER_MODALS_INFO_2*": [
    "PointerByReference"
  ], 
  "DFS_INFO_3": [
    "DFS_INFO_3"
  ], 
  "DFS_INFO_2": [
    "DFS_INFO_2"
  ], 
  "DFS_INFO_1": [
    "DFS_INFO_1"
  ], 
  "WER_REPORT_TYPE**": [
    "PointerByReference"
  ], 
  "PDH_COUNTER_INFO": [
    "PDH_COUNTER_INFO"
  ], 
  "DFS_INFO_6": [
    "DFS_INFO_6"
  ], 
  "LPSYSTEM_POWER_STATUS*": [
    "PointerByReference"
  ], 
  "DFS_INFO_4": [
    "DFS_INFO_4"
  ], 
  "LPWKSTA_USER_INFO_1101*": [
    "PointerByReference"
  ], 
  "LPWCT_OBJECT_TYPE*": [
    "PointerByReference"
  ], 
  "LPCTOKEN_PRIVILEGES": [
    "TOKEN_PRIVILEGES", 
    "TOKEN_PRIVILEGES[]"
  ], 
  "LPLATENCY_TIME*": [
    "PointerByReference"
  ], 
  "HFILE": [
    "int"
  ], 
  "WER_FILE_TYPE*": [
    "IntByReference", 
    "int[]"
  ], 
  "HRSRC": [
    "HANDLE", 
    "HRSRC"
  ], 
  "TAPE_SET_MEDIA_PARAMETERS*": [
    "TAPE_SET_MEDIA_PARAMETERS", 
    "TAPE_SET_MEDIA_PARAMETERS[]"
  ], 
  "LPCEVENT_TRACE": [
    "EVENT_TRACE", 
    "EVENT_TRACE[]"
  ], 
  "LPANIMATIONINFO": [
    "ANIMATIONINFO", 
    "ANIMATIONINFO[]"
  ], 
  "PUSER_INFO_1053*": [
    "PointerByReference"
  ], 
  "FORM_INFO_1**": [
    "PointerByReference"
  ], 
  "GENERIC_MAPPING**": [
    "PointerByReference"
  ], 
  "SERVER_TRANSPORT_INFO_0*": [
    "SERVER_TRANSPORT_INFO_0", 
    "SERVER_TRANSPORT_INFO_0[]"
  ], 
  "LPPDH_STATISTICS*": [
    "PointerByReference"
  ], 
  "TOGGLEKEYS": [
    "TOGGLEKEYS"
  ], 
  "LPCUSER_MODALS_INFO_1": [
    "USER_MODALS_INFO_1", 
    "USER_MODALS_INFO_1[]"
  ], 
  "USE_INFO_2*": [
    "USE_INFO_2", 
    "USE_INFO_2[]"
  ], 
  "LPCRAWKEYBOARD": [
    "RAWKEYBOARD", 
    "RAWKEYBOARD[]"
  ], 
  "LPMEMORY_BASIC_INFORMATION": [
    "MEMORY_BASIC_INFORMATION", 
    "MEMORY_BASIC_INFORMATION[]"
  ], 
  "LPMACHINE_PROCESSOR_POWER_POLICY": [
    "MACHINE_PROCESSOR_POWER_POLICY", 
    "MACHINE_PROCESSOR_POWER_POLICY[]"
  ], 
  "WCHAR": [
    "char"
  ], 
  "PRECTL*": [
    "PointerByReference"
  ], 
  "WINDOW_BUFFER_SIZE_RECORD": [
    "WINDOW_BUFFER_SIZE_RECORD"
  ], 
  "USER_INFO_1005*": [
    "USER_INFO_1005", 
    "USER_INFO_1005[]"
  ], 
  "ENHMETAHEADER*": [
    "ENHMETAHEADER", 
    "ENHMETAHEADER[]"
  ], 
  "NTMS_NOTIFICATIONINFORMATION**": [
    "PointerByReference"
  ], 
  "LONGLONG": [
    "double"
  ], 
  "PSHARE_INFO_1501*": [
    "PointerByReference"
  ], 
  "LPCMOUSEHOOKSTRUCT": [
    "MOUSEHOOKSTRUCT", 
    "MOUSEHOOKSTRUCT[]"
  ], 
  "GET_FILEEX_INFO_LEVELS*": [
    "IntByReference", 
    "int[]"
  ], 
  "PULONG64*": [
    "PointerByReference"
  ], 
  "DEBUGHOOKINFO": [
    "DEBUGHOOKINFO"
  ], 
  "StringTable": [
    "StringTable"
  ], 
  "PCONNECTION_INFO_0": [
    "CONNECTION_INFO_0", 
    "CONNECTION_INFO_0[]"
  ], 
  "PCONNECTION_INFO_1": [
    "CONNECTION_INFO_1", 
    "CONNECTION_INFO_1[]"
  ], 
  "PDH_FMT_COUNTERVALUE": [
    "PDH_FMT_COUNTERVALUE"
  ], 
  "PTITLEBARINFO": [
    "TITLEBARINFO", 
    "TITLEBARINFO[]"
  ], 
  "LPUSER_INFO_10": [
    "USER_INFO_10", 
    "USER_INFO_10[]"
  ], 
  "LPUSER_INFO_11": [
    "USER_INFO_11", 
    "USER_INFO_11[]"
  ], 
  "PSERVICE_PRESHUTDOWN_INFO*": [
    "PointerByReference"
  ], 
  "PSHARE_INFO_2*": [
    "PointerByReference"
  ], 
  "PTRACKMOUSEEVENT": [
    "TRACKMOUSEEVENT", 
    "TRACKMOUSEEVENT[]"
  ], 
  "LPTRACE_LOGFILE_HEADER*": [
    "PointerByReference"
  ], 
  "FOCUS_EVENT_RECORD*": [
    "FOCUS_EVENT_RECORD", 
    "FOCUS_EVENT_RECORD[]"
  ], 
  "POWER_POLICY**": [
    "PointerByReference"
  ], 
  "USER_MODALS_INFO_3**": [
    "PointerByReference"
  ], 
  "EVENTLOG_FULL_INFORMATION*": [
    "EVENTLOG_FULL_INFORMATION", 
    "EVENTLOG_FULL_INFORMATION[]"
  ], 
  "LPCDLGTEMPLATE": [
    "DLGTEMPLATE", 
    "DLGTEMPLATE[]"
  ], 
  "JOBOBJECT_BASIC_ACCOUNTING_INFORMATION**": [
    "PointerByReference"
  ], 
  "PACL_INFORMATION_CLASS": [
    "IntByReference", 
    "int[]"
  ], 
  "LPCTOGGLEKEYS": [
    "TOGGLEKEYS", 
    "TOGGLEKEYS[]"
  ], 
  "LPCEVENTMSG": [
    "EVENTMSG", 
    "EVENTMSG[]"
  ], 
  "DFS_INFO_103**": [
    "PointerByReference"
  ], 
  "PPOWERBROADCAST_SETTING": [
    "POWERBROADCAST_SETTING", 
    "POWERBROADCAST_SETTING[]"
  ], 
  "LPCLOGPEN": [
    "LOGPEN", 
    "LOGPEN[]"
  ], 
  "PFIXED": [
    "FIXED", 
    "FIXED[]"
  ], 
  "LPCENUMLOGFONTEXDV": [
    "ENUMLOGFONTEXDV", 
    "ENUMLOGFONTEXDV[]"
  ], 
  "SHARE_INFO_1004**": [
    "PointerByReference"
  ], 
  "PHIGHCONTRAST": [
    "HIGHCONTRAST", 
    "HIGHCONTRAST[]"
  ], 
  "POVERLAPPED_ENTRY*": [
    "PointerByReference"
  ], 
  "POPEN_AS_INFO_FLAGS*": [
    "PointerByReference"
  ], 
  "PUSN": [
    "DoubleByReference", 
    "double[]"
  ], 
  "PLPHANDLE": [
    "HANDLEByReference"
  ], 
  "PABCFLOAT": [
    "ABCFLOAT", 
    "ABCFLOAT[]"
  ], 
  "PDFS_INFO_107*": [
    "PointerByReference"
  ], 
  "LPSYSTEM_POWER_POLICY": [
    "SYSTEM_POWER_POLICY", 
    "SYSTEM_POWER_POLICY[]"
  ], 
  "HANDLE": [
    "HANDLE", 
    "Pointer"
  ], 
  "LPJOBOBJECT_RATE_CONTROL_TOLERANCE_INTERVAL*": [
    "PointerByReference"
  ], 
  "LPPGEOCLASS*": [
    "IntByReference", 
    "long[]"
  ], 
  "LPCSYSTEM_POWER_LEVEL": [
    "SYSTEM_POWER_LEVEL", 
    "SYSTEM_POWER_LEVEL[]"
  ], 
  "PPROCESS_MEMORY_COUNTERS_EX*": [
    "PointerByReference"
  ], 
  "LPGLOBAL_MACHINE_POWER_POLICY*": [
    "PointerByReference"
  ], 
  "PULONG64": [
    "LongByReference", 
    "__int64[]"
  ], 
  "PDWORDLONG": [
    "LongByReference", 
    "__int64[]"
  ], 
  "LPCFINDEX_INFO_LEVELS": [
    "IntByReference", 
    "int[]"
  ], 
  "LPRAWMOUSE": [
    "RAWMOUSE", 
    "RAWMOUSE[]"
  ], 
  "LOCALGROUP_MEMBERS_INFO_0**": [
    "PointerByReference"
  ], 
  "LPPROCESSENTRY32*": [
    "PointerByReference"
  ], 
  "HBITMAP": [
    "HANDLE"
  ], 
  "LPABCFLOAT*": [
    "PointerByReference"
  ], 
  "HMENU*": [
    "HANDLEByReference"
  ], 
  "LPAPPBARDATA*": [
    "PointerByReference"
  ], 
  "PPERFORMANCE_INFORMATION": [
    "PERFORMANCE_INFORMATION", 
    "PERFORMANCE_INFORMATION[]"
  ], 
  "PATOM*": [
    "PointerByReference"
  ], 
  "PTRIVERTEX": [
    "TRIVERTEX", 
    "TRIVERTEX[]"
  ], 
  "PUNIVERSAL_NAME_INFO*": [
    "PointerByReference"
  ], 
  "PINT64": [
    "LongByReference", 
    "__int64[]"
  ], 
  "LPDWORD_PTR": [
    "IntByReference", 
    "long[]"
  ], 
  "LPWER_REPORT_INFORMATION": [
    "WER_REPORT_INFORMATION", 
    "WER_REPORT_INFORMATION[]"
  ], 
  "MEMORYSTATUSEX**": [
    "PointerByReference"
  ], 
  "PBLENDFUNCTION*": [
    "PointerByReference"
  ], 
  "LPWKSTA_TRANSPORT_INFO_0*": [
    "PointerByReference"
  ], 
  "ASSOC_FILTER**": [
    "PointerByReference"
  ], 
  "LPJOBOBJECT_BASIC_UI_RESTRICTIONS*": [
    "PointerByReference"
  ], 
  "MACHINE_PROCESSOR_POWER_POLICY**": [
    "PointerByReference"
  ], 
  "TimeSysInfo**": [
    "PointerByReference"
  ], 
  "LPLOCALGROUP_MEMBERS_INFO_3": [
    "LOCALGROUP_MEMBERS_INFO_3", 
    "LOCALGROUP_MEMBERS_INFO_3[]"
  ], 
  "LPLOCALGROUP_MEMBERS_INFO_2": [
    "LOCALGROUP_MEMBERS_INFO_2", 
    "LOCALGROUP_MEMBERS_INFO_2[]"
  ], 
  "LPLOCALGROUP_MEMBERS_INFO_1": [
    "LOCALGROUP_MEMBERS_INFO_1", 
    "LOCALGROUP_MEMBERS_INFO_1[]"
  ], 
  "DFS_INFO_101**": [
    "PointerByReference"
  ], 
  "LPSTR": [
    "ByteByReference", 
    "char[]", 
    "String"
  ], 
  "GPFIDL_FLAGS*": [
    "IntByReference", 
    "int[]"
  ], 
  "SECURITY_QUALITY_OF_SERVICE": [
    "SECURITY_QUALITY_OF_SERVICE"
  ], 
  "LOGPEN": [
    "LOGPEN"
  ], 
  "PFOCUS_EVENT_RECORD*": [
    "PointerByReference"
  ], 
  "PJOBOBJECT_NOTIFICATION_LIMIT_INFORMATION": [
    "JOBOBJECT_NOTIFICATION_LIMIT_INFORMATION", 
    "JOBOBJECT_NOTIFICATION_LIMIT_INFORMATION[]"
  ], 
  "PTpcGetSamplesArgs*": [
    "PointerByReference"
  ], 
  "PHRESULT": [
    "IntByReference", 
    "long[]"
  ], 
  "PWSTR*": [
    "WString[]", 
    "PointerByReference"
  ], 
  "BITMAPINFOHEADER*": [
    "WinGDI.BITMAPINFOHEADER"
  ], 
  "LPUSE_INFO_1*": [
    "PointerByReference"
  ], 
  "LPCMETAFILEPICT": [
    "METAFILEPICT", 
    "METAFILEPICT[]"
  ], 
  "PUSER_INFO_10*": [
    "PointerByReference"
  ], 
  "UINT_PTR**": [
    "PointerByReference"
  ], 
  "PMSG_INFO_0*": [
    "PointerByReference"
  ], 
  "PDIBSECTION*": [
    "PointerByReference"
  ], 
  "LPWKSTA_INFO_100*": [
    "PointerByReference"
  ], 
  "LPUCHAR": [
    "byte"
  ], 
  "LPRAWINPUT": [
    "RAWINPUT", 
    "RAWINPUT[]"
  ], 
  "USER_INFO_2*": [
    "USER_INFO_2", 
    "USER_INFO_2[]"
  ], 
  "PMACHINE_PROCESSOR_POWER_POLICY": [
    "MACHINE_PROCESSOR_POWER_POLICY", 
    "MACHINE_PROCESSOR_POWER_POLICY[]"
  ], 
  "PEVENTLOG_FULL_INFORMATION": [
    "EVENTLOG_FULL_INFORMATION", 
    "EVENTLOG_FULL_INFORMATION[]"
  ], 
  "CALID**": [
    "PointerByReference"
  ], 
  "UNIVERSAL_NAME_INFO**": [
    "PointerByReference"
  ], 
  "INT32": [
    "int"
  ], 
  "LPMONITORINFO": [
    "MONITORINFO", 
    "MONITORINFO[]"
  ], 
  "CONVINFO**": [
    "PointerByReference"
  ], 
  "LPSERVICE_DELAYED_AUTO_START_INFO*": [
    "PointerByReference"
  ], 
  "LPICONMETRICS": [
    "ICONMETRICS", 
    "ICONMETRICS[]"
  ], 
  "SYSTEM_POWER_POLICY*": [
    "SYSTEM_POWER_POLICY", 
    "SYSTEM_POWER_POLICY[]"
  ], 
  "SYSGEOCLASS": [
    "int"
  ], 
  "PBLENDFUNCTION": [
    "BLENDFUNCTION", 
    "BLENDFUNCTION[]"
  ], 
  "LPCCURSORINFO": [
    "CURSORINFO", 
    "CURSORINFO[]"
  ], 
  "UINT16*": [
    "ShortByReference", 
    "short[]"
  ], 
  "DFS_INFO_4*": [
    "DFS_INFO_4", 
    "DFS_INFO_4[]"
  ], 
  "MAT2*": [
    "MAT2", 
    "MAT2[]"
  ], 
  "FORMATETC**": [
    "PointerByReference"
  ], 
  "WKSTA_USER_INFO_1**": [
    "PointerByReference"
  ], 
  "LPRAWKEYBOARD*": [
    "PointerByReference"
  ], 
  "LPINT64*": [
    "PointerByReference"
  ], 
  "LONGLONG**": [
    "PointerByReference"
  ], 
  "TIME_ZONE_INFORMATION*": [
    "TIME_ZONE_INFORMATION", 
    "TIME_ZONE_INFORMATION[]"
  ], 
  "LPULARGE_INTEGER*": [
    "PointerByReference"
  ], 
  "LPCAXISINFO": [
    "AXISINFO", 
    "AXISINFO[]"
  ], 
  "PLPCGEOCLASS*": [
    "PointerByReference"
  ], 
  "LPLOGBRUSH": [
    "LOGBRUSH", 
    "LOGBRUSH[]"
  ], 
  "LPCACCEL": [
    "ACCEL", 
    "ACCEL[]"
  ], 
  "FLASHWINFO": [
    "FLASHWINFO"
  ], 
  "CALTYPE*": [
    "IntByReference", 
    "long[]"
  ], 
  "LPEVENT_TRACE*": [
    "PointerByReference"
  ], 
  "LPUSER_MODALS_INFO_1004*": [
    "PointerByReference"
  ], 
  "USER_INFO_1003*": [
    "USER_INFO_1003", 
    "USER_INFO_1003[]"
  ], 
  "LPLMSTR*": [
    "char"
  ], 
  "SHSTOCKICONINFO": [
    "SHSTOCKICONINFO"
  ], 
  "LPCWER_REPORT_TYPE": [
    "IntByReference", 
    "int[]"
  ], 
  "FILETIME**": [
    "PointerByReference"
  ], 
  "PUSER_MODALS_INFO_1001": [
    "USER_MODALS_INFO_1001", 
    "USER_MODALS_INFO_1001[]"
  ], 
  "FINDEX_SEARCH_OPS": [
    "int"
  ], 
  "PUSER_MODALS_INFO_1003": [
    "USER_MODALS_INFO_1003", 
    "USER_MODALS_INFO_1003[]"
  ], 
  "PUSER_MODALS_INFO_1002": [
    "USER_MODALS_INFO_1002", 
    "USER_MODALS_INFO_1002[]"
  ], 
  "PUSER_MODALS_INFO_1005": [
    "USER_MODALS_INFO_1005", 
    "USER_MODALS_INFO_1005[]"
  ], 
  "PUSER_MODALS_INFO_1004": [
    "USER_MODALS_INFO_1004", 
    "USER_MODALS_INFO_1004[]"
  ], 
  "CONNECTDLGSTRUCT": [
    "CONNECTDLGSTRUCT"
  ], 
  "PUSER_MODALS_INFO_1006": [
    "USER_MODALS_INFO_1006", 
    "USER_MODALS_INFO_1006[]"
  ], 
  "LPWSAPROTOCOL_INFO": [
    "WSAPROTOCOL_INFO", 
    "WSAPROTOCOL_INFO[]"
  ], 
  "PUSER_MODALS_INFO_3": [
    "USER_MODALS_INFO_3", 
    "USER_MODALS_INFO_3[]"
  ], 
  "LPCSECURITY_QUALITY_OF_SERVICE": [
    "SECURITY_QUALITY_OF_SERVICE", 
    "SECURITY_QUALITY_OF_SERVICE[]"
  ], 
  "LPCSTAT_SERVER_0": [
    "STAT_SERVER_0", 
    "STAT_SERVER_0[]"
  ], 
  "LPSYSGEOTYPE*": [
    "PointerByReference"
  ], 
  "LPJOB_INFO_3*": [
    "PointerByReference"
  ], 
  "BLENDFUNCTION*": [
    "BLENDFUNCTION", 
    "BLENDFUNCTION[]"
  ], 
  "PPOWER_POLICY*": [
    "PointerByReference"
  ], 
  "CURRENCYFMT": [
    "CURRENCYFMT"
  ], 
  "HDDEDATA": [
    "HANDLE"
  ], 
  "EVENT_TRACE_HEADER": [
    "EVENT_TRACE_HEADER"
  ], 
  "LPENUMTEXTMETRIC*": [
    "PointerByReference"
  ], 
  "LPCDATATYPES_INFO_1": [
    "DATATYPES_INFO_1", 
    "DATATYPES_INFO_1[]"
  ], 
  "LPWSTR*": [
    "WString[]", 
    "PointerByReference"
  ], 
  "PGROUP_USERS_INFO_1": [
    "GROUP_USERS_INFO_1", 
    "GROUP_USERS_INFO_1[]"
  ], 
  "PSERVICE_REQUIRED_PRIVILEGES_INFO": [
    "SERVICE_REQUIRED_PRIVILEGES_INFO", 
    "SERVICE_REQUIRED_PRIVILEGES_INFO[]"
  ], 
  "PPGEOTYPE**": [
    "PointerByReference"
  ], 
  "LPPROVIDOR_INFO_2*": [
    "PointerByReference"
  ], 
  "PLONG*": [
    "PointerByReference"
  ], 
  "SYSTEM_POWER_STATUS": [
    "SYSTEM_POWER_STATUS"
  ], 
  "LPLCID": [
    "IntByReference", 
    "long[]"
  ], 
  "PPROVIDOR_INFO_2": [
    "PROVIDOR_INFO_2", 
    "PROVIDOR_INFO_2[]"
  ], 
  "UNIVERSAL_NAME_INFO*": [
    "UNIVERSAL_NAME_INFO", 
    "UNIVERSAL_NAME_INFO[]"
  ], 
  "PLOCALGROUP_INFO_1*": [
    "PointerByReference"
  ], 
  "LPHDESK": [
    "HANDLEByReference"
  ], 
  "LPRM_PROCESS_INFO": [
    "RM_PROCESS_INFO", 
    "RM_PROCESS_INFO[]"
  ], 
  "GROUP_INFO_0*": [
    "GROUP_INFO_0", 
    "GROUP_INFO_0[]"
  ], 
  "LPUSER_INFO_1010*": [
    "PointerByReference"
  ], 
  "LPWER_REGISTER_FILE_TYPE": [
    "IntByReference", 
    "int[]"
  ], 
  "DEBUGHOOKINFO**": [
    "PointerByReference"
  ], 
  "MENUINFO**": [
    "PointerByReference"
  ], 
  "LPCCBT_CREATEWND": [
    "CBT_CREATEWND", 
    "CBT_CREATEWND[]"
  ], 
  "LPPROCESSOR_NUMBER": [
    "PROCESSOR_NUMBER", 
    "PROCESSOR_NUMBER[]"
  ], 
  "PRINTER_INFO_1**": [
    "PointerByReference"
  ], 
  "LPUSER_INFO_1*": [
    "PointerByReference"
  ], 
  "AT_INFO*": [
    "AT_INFO", 
    "AT_INFO[]"
  ], 
  "LPLOGPEN": [
    "LOGPEN", 
    "LOGPEN[]"
  ], 
  "LPWORD*": [
    "PointerByReference"
  ], 
  "PSHARE_INFO_501*": [
    "PointerByReference"
  ], 
  "PUINT32": [
    "IntByReference", 
    "int[]"
  ], 
  "JOB_INFO_4**": [
    "PointerByReference"
  ], 
  "PRINTER_DEFAULTS": [
    "PRINTER_DEFAULTS"
  ], 
  "LPDESIGNVECTOR": [
    "DESIGNVECTOR", 
    "DESIGNVECTOR[]"
  ], 
  "NETRESOURCE*": [
    "NETRESOURCE", 
    "NETRESOURCE[]"
  ], 
  "PHCONVLIST": [
    "HANDLEByReference"
  ], 
  "PROCESS_INFORMATION*": [
    "PROCESS_INFORMATION", 
    "PROCESS_INFORMATION[]"
  ], 
  "double**": [
    "PointerByReference"
  ], 
  "PROCESS_HEAP_ENTRY**": [
    "PointerByReference"
  ], 
  "PENHMETARECORD*": [
    "PointerByReference"
  ], 
  "SHQUERYRBINFO*": [
    "SHQUERYRBINFO", 
    "SHQUERYRBINFO[]"
  ], 
  "MENUITEMTEMPLATE": [
    "MENUITEMTEMPLATE"
  ], 
  "LRESULT*": [
    "IntByReference", 
    "long[]"
  ], 
  "double*": [
    "DoubleByReference", 
    "double[]"
  ], 
  "RM_UNIQUE_PROCESS*": [
    "RM_UNIQUE_PROCESS", 
    "RM_UNIQUE_PROCESS[]"
  ], 
  "LPMETAFILEPICT": [
    "METAFILEPICT", 
    "METAFILEPICT[]"
  ], 
  "LPCMSG": [
    "MSG", 
    "MSG[]"
  ], 
  "LPSERVICE_CONTROL_STATUS_REASON_PARAMS": [
    "SERVICE_CONTROL_STATUS_REASON_PARAMS", 
    "SERVICE_CONTROL_STATUS_REASON_PARAMS[]"
  ], 
  "LPUSER_INFO_1012*": [
    "PointerByReference"
  ], 
  "LPWKSTA_INFO_502": [
    "WKSTA_INFO_502", 
    "WKSTA_INFO_502[]"
  ], 
  "LPMOUSE_EVENT_RECORD*": [
    "PointerByReference"
  ], 
  "PTIME_ZONE_INFORMATION*": [
    "PointerByReference"
  ], 
  "PICONINFO": [
    "ICONINFO", 
    "ICONINFO[]"
  ], 
  "PTASKDIALOG_FLAGS*": [
    "PointerByReference"
  ], 
  "LPMEMORYSTATUS*": [
    "PointerByReference"
  ], 
  "LPGPFIDL_FLAGS*": [
    "PointerByReference"
  ], 
  "MDICREATESTRUCT*": [
    "MDICREATESTRUCT", 
    "MDICREATESTRUCT[]"
  ], 
  "RECT*": [
    "RECT", 
    "RECT[]"
  ], 
  "SERVER_TRANSPORT_INFO_0": [
    "SERVER_TRANSPORT_INFO_0"
  ], 
  "LPJOBOBJECT_CPU_RATE_CONTROL_INFORMATION": [
    "JOBOBJECT_CPU_RATE_CONTROL_INFORMATION", 
    "JOBOBJECT_CPU_RATE_CONTROL_INFORMATION[]"
  ], 
  "PAXESLIST": [
    "AXESLIST", 
    "AXESLIST[]"
  ], 
  "LPCUSER_INFO_1009": [
    "USER_INFO_1009", 
    "USER_INFO_1009[]"
  ], 
  "AT_ENUM**": [
    "PointerByReference"
  ], 
  "LPSECURITY_QUALITY_OF_SERVICE": [
    "SECURITY_QUALITY_OF_SERVICE", 
    "SECURITY_QUALITY_OF_SERVICE[]"
  ], 
  "LPACCEL*": [
    "PointerByReference"
  ], 
  "PSERVICE_DELAYED_AUTO_START_INFO*": [
    "PointerByReference"
  ], 
  "PGCP_RESULTS*": [
    "PointerByReference"
  ], 
  "LPLOCALGROUP_MEMBERS_INFO_1*": [
    "PointerByReference"
  ], 
  "PPRINTER_NOTIFY_INFO_DATA*": [
    "PointerByReference"
  ], 
  "PWINDOWPLACEMENT": [
    "WINDOWPLACEMENT", 
    "WINDOWPLACEMENT[]"
  ], 
  "TASKDIALOG_FLAGS**": [
    "PointerByReference"
  ], 
  "GROUP_USERS_INFO_1**": [
    "PointerByReference"
  ], 
  "LPCPROVIDOR_INFO_2": [
    "PROVIDOR_INFO_2", 
    "PROVIDOR_INFO_2[]"
  ], 
  "USER_INFO_1006*": [
    "USER_INFO_1006", 
    "USER_INFO_1006[]"
  ], 
  "PBYTE*": [
    "byte"
  ], 
  "VIDEOPARAMETERS": [
    "VIDEOPARAMETERS"
  ], 
  "LONG64*": [
    "LongByReference", 
    "__int64[]"
  ], 
  "MEMORYSTATUS": [
    "MEMORYSTATUS"
  ], 
  "USER_MODALS_INFO_1007*": [
    "USER_MODALS_INFO_1007", 
    "USER_MODALS_INFO_1007[]"
  ], 
  "LPCMENUITEMTEMPLATE": [
    "MENUITEMTEMPLATE", 
    "MENUITEMTEMPLATE[]"
  ], 
  "PRM_UNIQUE_PROCESS": [
    "RM_UNIQUE_PROCESS", 
    "RM_UNIQUE_PROCESS[]"
  ], 
  "PCOLORREF": [
    "IntByReference", 
    "long[]"
  ], 
  "LMSTR": [
    "char"
  ], 
  "WER_REPORT_TYPE": [
    "int"
  ], 
  "double": [
    "double"
  ], 
  "TAPE_GET_DRIVE_PARAMETERS**": [
    "PointerByReference"
  ], 
  "LPSTARTUPINFO*": [
    "PointerByReference"
  ], 
  "LPDWORD64*": [
    "PointerByReference"
  ], 
  "FOCUS_EVENT_RECORD**": [
    "PointerByReference"
  ], 
  "LPSERVICE_CONTROL_STATUS_REASON_PARAMS*": [
    "PointerByReference"
  ], 
  "LPCALID": [
    "IntByReference", 
    "long[]"
  ], 
  "SHORT": [
    "short"
  ], 
  "LPMediaLabelInfo*": [
    "PointerByReference"
  ], 
  "LPCJOBOBJECTINFOCLASS": [
    "IntByReference", 
    "int[]"
  ], 
  "LPPRINTPAGERANGE": [
    "PRINTPAGERANGE", 
    "PRINTPAGERANGE[]"
  ], 
  "ENUMLOGFONTEXDV**": [
    "PointerByReference"
  ], 
  "LPHCONVLIST": [
    "HANDLEByReference"
  ], 
  "LPGROUP_USERS_INFO_1*": [
    "PointerByReference"
  ], 
  "BATTERY_REPORTING_SCALE": [
    "BATTERY_REPORTING_SCALE"
  ], 
  "ENHMETAHEADER**": [
    "PointerByReference"
  ], 
  "PMOUSE_EVENT_RECORD*": [
    "PointerByReference"
  ], 
  "REMOTE_NAME_INFO**": [
    "PointerByReference"
  ], 
  "PAT_INFO": [
    "AT_INFO", 
    "AT_INFO[]"
  ], 
  "LPCPORT_INFO_2": [
    "PORT_INFO_2", 
    "PORT_INFO_2[]"
  ], 
  "LPCPORT_INFO_3": [
    "PORT_INFO_3", 
    "PORT_INFO_3[]"
  ], 
  "LPCPORT_INFO_1": [
    "PORT_INFO_1", 
    "PORT_INFO_1[]"
  ], 
  "LPABC*": [
    "PointerByReference"
  ], 
  "PMINIMIZEDMETRICS*": [
    "PointerByReference"
  ], 
  "LPCDCB": [
    "DCB", 
    "DCB[]"
  ], 
  "LONG32**": [
    "PointerByReference"
  ], 
  "LPTpcGetSamplesArgs": [
    "TpcGetSamplesArgs", 
    "TpcGetSamplesArgs[]"
  ], 
  "LPCLIPFORMAT": [
    "IntByReference", 
    "int[]"
  ], 
  "LPPZZWSTR*": [
    "char"
  ], 
  "PTEXTMETRIC": [
    "TEXTMETRIC", 
    "TEXTMETRIC[]"
  ], 
  "PFILE_SEGMENT_ELEMENT": [
    "FILE_SEGMENT_ELEMENT", 
    "FILE_SEGMENT_ELEMENT[]"
  ], 
  "LPCMACHINE_POWER_POLICY": [
    "MACHINE_POWER_POLICY", 
    "MACHINE_POWER_POLICY[]"
  ], 
  "PWER_REPORT_UI*": [
    "PointerByReference"
  ], 
  "PWCHAR": [
    "char"
  ], 
  "PALETTEENTRY**": [
    "PointerByReference"
  ], 
  "MSG_INFO_1**": [
    "PointerByReference"
  ], 
  "PDH_COUNTER_PATH_ELEMENTS**": [
    "PointerByReference"
  ], 
  "PRINTER_INFO_5": [
    "PRINTER_INFO_5"
  ], 
  "PRINTPAGERANGE": [
    "PRINTPAGERANGE"
  ], 
  "LPCSERVICE_STATUS": [
    "SERVICE_STATUS", 
    "SERVICE_STATUS[]"
  ], 
  "PFORM_INFO_2*": [
    "PointerByReference"
  ], 
  "PLUID": [
    "WinNT.LUID"
  ], 
  "LPRM_UNIQUE_PROCESS*": [
    "PointerByReference"
  ], 
  "PDFS_INFO_300*": [
    "PointerByReference"
  ], 
  "LPTOKEN_PRIVILEGES*": [
    "PointerByReference"
  ], 
  "LPPOWERBROADCAST_SETTING": [
    "POWERBROADCAST_SETTING", 
    "POWERBROADCAST_SETTING[]"
  ], 
  "POWER_ACTION*": [
    "IntByReference", 
    "int[]"
  ], 
  "TOKEN_DEFAULT_DACL*": [
    "TOKEN_DEFAULT_DACL", 
    "TOKEN_DEFAULT_DACL[]"
  ], 
  "LPSERVER_TRANSPORT_INFO_3*": [
    "PointerByReference"
  ], 
  "LONG*": [
    "IntByReference", 
    "long[]", 
    "LONGByReference"
  ], 
  "LPSTARTUPINFO": [
    "STARTUPINFO", 
    "STARTUPINFO[]"
  ], 
  "WER_DUMP_CUSTOM_OPTIONS*": [
    "WER_DUMP_CUSTOM_OPTIONS", 
    "WER_DUMP_CUSTOM_OPTIONS[]"
  ], 
  "LPPOWER_ACTION_POLICY*": [
    "PointerByReference"
  ], 
  "QUERY_SERVICE_LOCK_STATUS**": [
    "PointerByReference"
  ], 
  "RID_DEVICE_INFO**": [
    "PointerByReference"
  ], 
  "ITEMIDLIST**": [
    "PointerByReference"
  ], 
  "GLOBAL_MACHINE_POWER_POLICY**": [
    "PointerByReference"
  ], 
  "LPIO_COUNTERS*": [
    "PointerByReference"
  ], 
  "DFS_INFO_150**": [
    "PointerByReference"
  ], 
  "PANOSE**": [
    "PointerByReference"
  ], 
  "LPINPUT*": [
    "PointerByReference"
  ], 
  "CREATESTRUCT": [
    "CREATESTRUCT"
  ], 
  "EXECUTION_STATE*": [
    "IntByReference", 
    "long[]"
  ], 
  "LPBOOLEAN": [
    "byte"
  ], 
  "LONG64**": [
    "PointerByReference"
  ], 
  "LPCUSER_MODALS_INFO_1003": [
    "USER_MODALS_INFO_1003", 
    "USER_MODALS_INFO_1003[]"
  ], 
  "HINSTANCE*": [
    "HANDLEByReference"
  ], 
  "LPCUSER_MODALS_INFO_1001": [
    "USER_MODALS_INFO_1001", 
    "USER_MODALS_INFO_1001[]"
  ], 
  "LPCSERVICE_LAUNCH_PROTECTED_INFO": [
    "SERVICE_LAUNCH_PROTECTED_INFO", 
    "SERVICE_LAUNCH_PROTECTED_INFO[]"
  ], 
  "LPCUSER_MODALS_INFO_1007": [
    "USER_MODALS_INFO_1007", 
    "USER_MODALS_INFO_1007[]"
  ], 
  "LPCUSER_MODALS_INFO_1004": [
    "USER_MODALS_INFO_1004", 
    "USER_MODALS_INFO_1004[]"
  ], 
  "LPCUSER_MODALS_INFO_1005": [
    "USER_MODALS_INFO_1005", 
    "USER_MODALS_INFO_1005[]"
  ], 
  "LPCMENUINFO": [
    "MENUINFO", 
    "MENUINFO[]"
  ], 
  "UINT_PTR*": [
    "IntByReference", 
    "int[]"
  ], 
  "PSYSTEMTIME": [
    "SYSTEMTIME", 
    "SYSTEMTIME[]"
  ], 
  "LPCPDH_COUNTER_INFO": [
    "PDH_COUNTER_INFO", 
    "PDH_COUNTER_INFO[]"
  ], 
  "LPMACHINE_POWER_POLICY": [
    "MACHINE_POWER_POLICY", 
    "MACHINE_POWER_POLICY[]"
  ], 
  "SYSTEM_POWER_STATE": [
    "int"
  ], 
  "LPUINT": [
    "IntByReference", 
    "int[]"
  ], 
  "PPRINTPAGERANGE*": [
    "PointerByReference"
  ], 
  "LPWKSTA_INFO_101*": [
    "PointerByReference"
  ], 
  "CALID*": [
    "IntByReference", 
    "long[]"
  ], 
  "LPDFS_TARGET_PRIORITY*": [
    "PointerByReference"
  ], 
  "LPARAM**": [
    "PointerByReference"
  ], 
  "PIXELFORMATDESCRIPTOR": [
    "PIXELFORMATDESCRIPTOR"
  ], 
  "LPCWKSTA_USER_INFO_1": [
    "WKSTA_USER_INFO_1", 
    "WKSTA_USER_INFO_1[]"
  ], 
  "SERVICE_SID_INFO*": [
    "SERVICE_SID_INFO", 
    "SERVICE_SID_INFO[]"
  ], 
  "PMENUITEMTEMPLATEHEADER": [
    "MENUITEMTEMPLATEHEADER", 
    "MENUITEMTEMPLATEHEADER[]"
  ], 
  "PSERVICE_STATUS_HANDLE": [
    "HANDLEByReference"
  ], 
  "LPQUERY_USER_NOTIFICATION_STATE*": [
    "PointerByReference"
  ], 
  "LPSC_ACTION_TYPE": [
    "IntByReference", 
    "int[]"
  ], 
  "LPDATATYPES_INFO_1*": [
    "PointerByReference"
  ], 
  "LPGUID*": [
    "PointerByReference"
  ], 
  "PPROCESS_HEAP_ENTRY": [
    "PROCESS_HEAP_ENTRY", 
    "PROCESS_HEAP_ENTRY[]"
  ], 
  "RM_PROCESS_INFO**": [
    "PointerByReference"
  ], 
  "LPCQUERY_SERVICE_LOCK_STATUS": [
    "QUERY_SERVICE_LOCK_STATUS", 
    "QUERY_SERVICE_LOCK_STATUS[]"
  ], 
  "LPCSERVICE_TRIGGER_INFO": [
    "SERVICE_TRIGGER_INFO", 
    "SERVICE_TRIGGER_INFO[]"
  ], 
  "LPJOBOBJECT_SECURITY_LIMIT_INFORMATION*": [
    "PointerByReference"
  ], 
  "FILE_SEGMENT_ELEMENT": [
    "FILE_SEGMENT_ELEMENT"
  ], 
  "HINSTANCE": [
    "HANDLE"
  ], 
  "PXFORM*": [
    "PointerByReference"
  ], 
  "PRINTER_NOTIFY_INFO": [
    "PRINTER_NOTIFY_INFO"
  ], 
  "SECURITY_IMPERSONATION_LEVEL": [
    "int"
  ], 
  "LPSIZE": [
    "SIZE", 
    "SIZE[]"
  ], 
  "PLASTINPUTINFO*": [
    "PointerByReference"
  ], 
  "PSERVICE_STATUS_PROCESS*": [
    "PointerByReference"
  ], 
  "PSHFILEOPSTRUCT*": [
    "PointerByReference"
  ], 
  "LPXFORM": [
    "XFORM", 
    "XFORM[]"
  ], 
  "LPCSHELLFLAGSTATE": [
    "SHELLFLAGSTATE", 
    "SHELLFLAGSTATE[]"
  ], 
  "LPHHOOK": [
    "HANDLEByReference"
  ], 
  "LPCCREATESTRUCT": [
    "CREATESTRUCT", 
    "CREATESTRUCT[]"
  ], 
  "LPCSESSION_INFO_0": [
    "SESSION_INFO_0", 
    "SESSION_INFO_0[]"
  ], 
  "WNDCLASS*": [
    "WNDCLASS"
  ], 
  "PTOKEN_PRIVILEGES": [
    "TOKEN_PRIVILEGES", 
    "TOKEN_PRIVILEGES[]"
  ], 
  "PJOBOBJECT_EXTENDED_LIMIT_INFORMATION*": [
    "PointerByReference"
  ], 
  "PCURRENCYFMT": [
    "CURRENCYFMT", 
    "CURRENCYFMT[]"
  ], 
  "STAT_SERVER_0*": [
    "STAT_SERVER_0", 
    "STAT_SERVER_0[]"
  ], 
  "LPCRID_DEVICE_INFO_HID": [
    "RID_DEVICE_INFO_HID", 
    "RID_DEVICE_INFO_HID[]"
  ], 
  "PSHELLFLAGSTATE": [
    "SHELLFLAGSTATE", 
    "SHELLFLAGSTATE[]"
  ], 
  "PPERFORMANCE_INFORMATION*": [
    "PointerByReference"
  ], 
  "PRAWHID": [
    "RAWHID", 
    "RAWHID[]"
  ], 
  "LPLOGPALETTE*": [
    "PointerByReference"
  ], 
  "LCID*": [
    "IntByReference", 
    "long[]"
  ], 
  "USER_INFO_11*": [
    "USER_INFO_11", 
    "USER_INFO_11[]"
  ], 
  "TPMPARAMS*": [
    "TPMPARAMS", 
    "TPMPARAMS[]"
  ], 
  "PULONG32": [
    "IntByReference", 
    "int[]"
  ], 
  "LPCEVENT_INSTANCE_HEADER": [
    "EVENT_INSTANCE_HEADER", 
    "EVENT_INSTANCE_HEADER[]"
  ], 
  "PDWORD32*": [
    "PointerByReference"
  ], 
  "LPCTimeSysInfo": [
    "IntByReference", 
    "int[]"
  ], 
  "PSCROLLBARINFO*": [
    "PointerByReference"
  ], 
  "LPCWPRETSTRUCT": [
    "CWPRETSTRUCT", 
    "CWPRETSTRUCT[]"
  ], 
  "ALTTABINFO*": [
    "ALTTABINFO", 
    "ALTTABINFO[]"
  ], 
  "PDH_STATISTICS": [
    "PDH_STATISTICS"
  ], 
  "LPSHARE_INFO_2": [
    "SHARE_INFO_2", 
    "SHARE_INFO_2[]"
  ], 
  "LPSHARE_INFO_1": [
    "SHARE_INFO_1", 
    "SHARE_INFO_1[]"
  ], 
  "LPUSER_INFO_0*": [
    "PointerByReference"
  ], 
  "JOBOBJECT_RATE_CONTROL_TOLERANCE**": [
    "PointerByReference"
  ], 
  "PLPCGEOCLASS": [
    "IntByReference", 
    "long[]"
  ], 
  "EVENT_TRACE**": [
    "PointerByReference"
  ], 
  "PHWND": [
    "HANDLEByReference"
  ], 
  "WER_FILE_TYPE**": [
    "PointerByReference"
  ], 
  "PMEMORYSTATUS": [
    "MEMORYSTATUS", 
    "MEMORYSTATUS[]"
  ], 
  "LPUSER_MODALS_INFO_3*": [
    "PointerByReference"
  ], 
  "UINT*": [
    "IntByReference", 
    "int[]"
  ], 
  "LPCPRINTER_NOTIFY_INFO": [
    "PRINTER_NOTIFY_INFO", 
    "PRINTER_NOTIFY_INFO[]"
  ], 
  "PORT_INFO_2**": [
    "PointerByReference"
  ], 
  "SOUNDSENTRY": [
    "SOUNDSENTRY"
  ], 
  "HEAP_INFORMATION_CLASS**": [
    "PointerByReference"
  ], 
  "LPCSYSTEM_LOGICAL_PROCESSOR_INFORMATION": [
    "SYSTEM_LOGICAL_PROCESSOR_INFORMATION", 
    "SYSTEM_LOGICAL_PROCESSOR_INFORMATION[]"
  ], 
  "SERVICE_TRIGGER": [
    "SERVICE_TRIGGER"
  ], 
  "DFS_INFO_104": [
    "DFS_INFO_104"
  ], 
  "WKSTA_USER_INFO_0": [
    "WKSTA_USER_INFO_0"
  ], 
  "LPCOMMCONFIG": [
    "COMMCONFIG", 
    "COMMCONFIG[]"
  ], 
  "DFS_INFO_107": [
    "DFS_INFO_107"
  ], 
  "DFS_INFO_100": [
    "DFS_INFO_100"
  ], 
  "DFS_INFO_101": [
    "DFS_INFO_101"
  ], 
  "TAPE_SET_DRIVE_PARAMETERS": [
    "TAPE_SET_DRIVE_PARAMETERS"
  ], 
  "LPCONNECTDLGSTRUCT*": [
    "PointerByReference"
  ], 
  "LPKERNINGPAIR*": [
    "PointerByReference"
  ], 
  "PAXESLIST*": [
    "PointerByReference"
  ], 
  "PJOBOBJECT_SECURITY_LIMIT_INFORMATION*": [
    "PointerByReference"
  ], 
  "char*": [
    "byte", 
    "byte[]"
  ], 
  "LARGE_INTEGER**": [
    "PointerByReference"
  ], 
  "JOBOBJECT_LIMIT_VIOLATION_INFORMATION": [
    "JOBOBJECT_LIMIT_VIOLATION_INFORMATION"
  ], 
  "LPMOUSEHOOKSTRUCT*": [
    "PointerByReference"
  ], 
  "PPRINTER_DEFAULTS": [
    "PRINTER_DEFAULTS", 
    "PRINTER_DEFAULTS[]"
  ], 
  "PRGBQUAD": [
    "RGBQUAD", 
    "RGBQUAD[]"
  ], 
  "LPCTAPE_SET_DRIVE_PARAMETERS": [
    "TAPE_SET_DRIVE_PARAMETERS", 
    "TAPE_SET_DRIVE_PARAMETERS[]"
  ], 
  "PPRINTER_INFO_1*": [
    "PointerByReference"
  ], 
  "LPCPDH_RAW_COUNTER_ITEM": [
    "PDH_RAW_COUNTER_ITEM", 
    "PDH_RAW_COUNTER_ITEM[]"
  ], 
  "SCNRT_STATUS*": [
    "IntByReference", 
    "int[]"
  ], 
  "PVALENT*": [
    "PointerByReference"
  ], 
  "GROUP_INFO_1002*": [
    "GROUP_INFO_1002", 
    "GROUP_INFO_1002[]"
  ], 
  "PSID*": [
    "WinNT.PSIDByReference", 
    "PointerByReference"
  ], 
  "JOBOBJECT_EXTENDED_LIMIT_INFORMATION**": [
    "PointerByReference"
  ], 
  "PROCESSENTRY32**": [
    "PointerByReference"
  ], 
  "LPDWORD32": [
    "IntByReference", 
    "int[]"
  ], 
  "LPUCHAR*": [
    "byte"
  ], 
  "LPTSTR": [
    "String", 
    "char[]", 
    "PointerByReference", 
    "WString"
  ], 
  "LPHANLDE": [
    "HANDLEByReference"
  ], 
  "PUSER_INFO_1020": [
    "USER_INFO_1020", 
    "USER_INFO_1020[]"
  ], 
  "LPMENUEX_TEMPLATE_HEADER*": [
    "PointerByReference"
  ], 
  "LPLONG": [
    "IntByReference", 
    "long[]"
  ], 
  "BY_HANDLE_FILE_INFORMATION**": [
    "PointerByReference"
  ], 
  "LPCMACHINE_PROCESSOR_POWER_POLICY": [
    "MACHINE_PROCESSOR_POWER_POLICY", 
    "MACHINE_PROCESSOR_POWER_POLICY[]"
  ], 
  "QUERY_SERVICE_LOCK_STATUS": [
    "QUERY_SERVICE_LOCK_STATUS"
  ], 
  "LPBYTE*": [
    "byte"
  ], 
  "LPBOOL": [
    "IntByReference", 
    "int[]"
  ], 
  "PUINT_PTR": [
    "IntByReference", 
    "int[]"
  ], 
  "PPROVIDOR_INFO_1*": [
    "PointerByReference"
  ], 
  "SIGDN**": [
    "PointerByReference"
  ], 
  "PDFS_INFO_200": [
    "DFS_INFO_200", 
    "DFS_INFO_200[]"
  ], 
  "LPCANIMATIONINFO": [
    "ANIMATIONINFO", 
    "ANIMATIONINFO[]"
  ], 
  "LPEXECUTION_STATE*": [
    "PointerByReference"
  ], 
  "PORT_INFO_3*": [
    "PORT_INFO_3", 
    "PORT_INFO_3[]"
  ], 
  "LPDFS_INFO_106*": [
    "PointerByReference"
  ], 
  "ULONG64": [
    "long"
  ], 
  "LPGEOTYPE": [
    "IntByReference", 
    "long[]"
  ], 
  "GROUP_INFO_3*": [
    "GROUP_INFO_3", 
    "GROUP_INFO_3[]"
  ], 
  "LPCCONSOLE_HISTORY_INFO": [
    "CONSOLE_HISTORY_INFO", 
    "CONSOLE_HISTORY_INFO[]"
  ], 
  "HRSRC*": [
    "HANDLEByReference"
  ], 
  "HDROP*": [
    "HANDLEByReference"
  ], 
  "DFS_INFO_50**": [
    "PointerByReference"
  ], 
  "FILE_INFO_2": [
    "FILE_INFO_2"
  ], 
  "QUERY_SERVICE_CONFIG*": [
    "QUERY_SERVICE_CONFIG", 
    "QUERY_SERVICE_CONFIG[]"
  ], 
  "LPCWER_CONSENT": [
    "IntByReference", 
    "int[]"
  ], 
  "LOGPEN**": [
    "PointerByReference"
  ], 
  "PJOBOBJECT_EXTENDED_LIMIT_INFORMATION": [
    "JOBOBJECT_EXTENDED_LIMIT_INFORMATION", 
    "JOBOBJECT_EXTENDED_LIMIT_INFORMATION[]"
  ], 
  "LPUSER_INFO_21*": [
    "PointerByReference"
  ], 
  "DATATYPES_INFO_1**": [
    "PointerByReference"
  ], 
  "LPUINT8": [
    "byte"
  ], 
  "LPJOBOBJECT_LIMIT_VIOLATION_INFORMATION*": [
    "PointerByReference"
  ], 
  "COORD": [
    "COORD"
  ], 
  "LPSERVICE_TRIGGER_SPECIFIC_DATA_ITEM": [
    "SERVICE_TRIGGER_SPECIFIC_DATA_ITEM", 
    "SERVICE_TRIGGER_SPECIFIC_DATA_ITEM[]"
  ], 
  "SESSION_INFO_502**": [
    "PointerByReference"
  ], 
  "PDH_TIME_INFO": [
    "PDH_TIME_INFO"
  ], 
  "PUSER_INFO_11*": [
    "PointerByReference"
  ], 
  "UINT": [
    "int"
  ], 
  "ANIMATIONINFO**": [
    "PointerByReference"
  ], 
  "PUSER_OTHER_INFO*": [
    "PointerByReference"
  ], 
  "PUSER_MODALS_INFO_3*": [
    "PointerByReference"
  ], 
  "LPULONGLONG": [
    "DoubleByReference", 
    "double[]"
  ], 
  "LPDFS_INFO_102": [
    "DFS_INFO_102", 
    "DFS_INFO_102[]"
  ], 
  "PCONVCONTEXT*": [
    "PointerByReference"
  ], 
  "PFIXED*": [
    "PointerByReference"
  ], 
  "PROCESS_MEMORY_COUNTERS_EX*": [
    "PROCESS_MEMORY_COUNTERS_EX", 
    "PROCESS_MEMORY_COUNTERS_EX[]"
  ], 
  "LPSTREAM_INFO_LEVELS": [
    "IntByReference", 
    "int[]"
  ], 
  "PHREPORT": [
    "HANDLEByReference"
  ], 
  "SERVICE_REQUIRED_PRIVILEGES_INFO**": [
    "PointerByReference"
  ], 
  "PRM_FILTER_ACTION": [
    "IntByReference", 
    "int[]"
  ], 
  "PMediaLabelInfo": [
    "MediaLabelInfo", 
    "MediaLabelInfo[]"
  ], 
  "SERVICE_LAUNCH_PROTECTED_INFO**": [
    "PointerByReference"
  ], 
  "LASTINPUTINFO**": [
    "PointerByReference"
  ], 
  "LPCONSOLE_SCREEN_BUFFER_INFOEX*": [
    "PointerByReference"
  ], 
  "ABCFLOAT*": [
    "ABCFLOAT", 
    "ABCFLOAT[]"
  ], 
  "LPRAWINPUTDEVICE": [
    "RAWINPUTDEVICE", 
    "RAWINPUTDEVICE[]"
  ], 
  "OFSTRUCT": [
    "OFSTRUCT"
  ], 
  "LPUINT64": [
    "LongByReference", 
    "__int64[]"
  ], 
  "LPBITMAP*": [
    "PointerByReference"
  ], 
  "LPQUERY_USER_NOTIFICATION_STATE": [
    "IntByReference", 
    "int[]"
  ], 
  "LPCNETINFOSTRUCT": [
    "NETINFOSTRUCT", 
    "NETINFOSTRUCT[]"
  ], 
  "PWINDOWPLACEMENT*": [
    "PointerByReference"
  ], 
  "PSID_AND_ATTRIBUTES*": [
    "PointerByReference"
  ], 
  "PIXELFORMATDESCRIPTOR*": [
    "PIXELFORMATDESCRIPTOR", 
    "PIXELFORMATDESCRIPTOR[]"
  ], 
  "VALENT*": [
    "VALENT", 
    "VALENT[]"
  ], 
  "RGBQUAD": [
    "RGBQUAD"
  ], 
  "PPGEOTYPE": [
    "IntByReference", 
    "long[]"
  ], 
  "TASKDIALOG_FLAGS": [
    "int"
  ], 
  "__int64": [
    "long"
  ], 
  "JOBOBJECT_RATE_CONTROL_TOLERANCE*": [
    "IntByReference", 
    "int[]"
  ], 
  "PMOUSEMOVEPOINT*": [
    "PointerByReference"
  ], 
  "LPUSER_INFO_1008*": [
    "PointerByReference"
  ], 
  "WIN32_STREAM_ID": [
    "WIN32_STREAM_ID"
  ], 
  "LPCWER_REPORT_UI": [
    "IntByReference", 
    "int[]"
  ], 
  "PMENUITEMTEMPLATE*": [
    "PointerByReference"
  ], 
  "NEWTEXTMETRIC**": [
    "PointerByReference"
  ], 
  "PSERVER_TRANSPORT_INFO_0*": [
    "PointerByReference"
  ], 
  "PAPPBARDATA": [
    "APPBARDATA", 
    "APPBARDATA[]"
  ], 
  "PRINTER_INFO_6**": [
    "PointerByReference"
  ], 
  "PINT16": [
    "ShortByReference", 
    "short[]"
  ], 
  "JOBOBJECT_RATE_CONTROL_TOLERANCE_INTERVAL": [
    "int"
  ], 
  "USER_INFO_3*": [
    "USER_INFO_3", 
    "USER_INFO_3[]"
  ], 
  "__int64*": [
    "LongByReference", 
    "__int64[]"
  ], 
  "PSECURITY_QUALITY_OF_SERVICE": [
    "SECURITY_QUALITY_OF_SERVICE", 
    "SECURITY_QUALITY_OF_SERVICE[]"
  ], 
  "LPAT_INFO*": [
    "PointerByReference"
  ], 
  "PSERVICE_PREFERRED_NODE_INFO": [
    "SERVICE_PREFERRED_NODE_INFO", 
    "SERVICE_PREFERRED_NODE_INFO[]"
  ], 
  "LPMENUITEMINFO*": [
    "PointerByReference"
  ], 
  "LGRPID**": [
    "PointerByReference"
  ], 
  "LPDFS_INFO_5*": [
    "PointerByReference"
  ], 
  "SYSTEMTIME**": [
    "PointerByReference"
  ], 
  "LOCALGROUP_INFO_1002": [
    "LOCALGROUP_INFO_1002"
  ], 
  "AVRT_PRIORITY*": [
    "IntByReference", 
    "int[]"
  ], 
  "LPCRAWINPUTHEADER": [
    "RAWINPUTHEADER", 
    "RAWINPUTHEADER[]"
  ], 
  "PJOBOBJECT_BASIC_ACCOUNTING_INFORMATION*": [
    "PointerByReference"
  ], 
  "LPDFS_INFO_50": [
    "DFS_INFO_50", 
    "DFS_INFO_50[]"
  ], 
  "PHBITMAP": [
    "HANDLEByReference"
  ], 
  "PUSER_INFO_1007": [
    "USER_INFO_1007", 
    "USER_INFO_1007[]"
  ], 
  "LPPCWSTR*": [
    "char"
  ], 
  "USER_INFO_20**": [
    "PointerByReference"
  ], 
  "MENU_EVENT_RECORD*": [
    "MENU_EVENT_RECORD", 
    "MENU_EVENT_RECORD[]"
  ], 
  "PLPCSTR*": [
    "byte"
  ], 
  "PUSER_INFO_1006": [
    "USER_INFO_1006", 
    "USER_INFO_1006[]"
  ], 
  "PLARGE_INTEGER": [
    "LARGE_INTEGER", 
    "LARGE_INTEGER[]"
  ], 
  "SHELLEXECUTEINFO**": [
    "PointerByReference"
  ], 
  "PUSER_INFO_2": [
    "USER_INFO_2", 
    "USER_INFO_2[]"
  ], 
  "PUSER_INFO_1005": [
    "USER_INFO_1005", 
    "USER_INFO_1005[]"
  ], 
  "LPUINT16*": [
    "PointerByReference"
  ], 
  "PJOB_INFO_1*": [
    "PointerByReference"
  ], 
  "PRINTER_INFO_8**": [
    "PointerByReference"
  ], 
  "PUSER_INFO_4": [
    "USER_INFO_4", 
    "USER_INFO_4[]"
  ], 
  "PAUDIODESCRIPTION": [
    "AUDIODESCRIPTION", 
    "AUDIODESCRIPTION[]"
  ], 
  "ULONG32*": [
    "IntByReference", 
    "int[]"
  ], 
  "PHKEY": [
    "HANDLEByReference"
  ], 
  "DFS_INFO_6**": [
    "PointerByReference"
  ], 
  "PKERNINGPAIR": [
    "KERNINGPAIR", 
    "KERNINGPAIR[]"
  ], 
  "PUSE_INFO_1*": [
    "PointerByReference"
  ], 
  "COLOR16*": [
    "ShortByReference", 
    "short[]"
  ], 
  "LPPROCESSOR_POWER_POLICY_INFO": [
    "PROCESSOR_POWER_POLICY_INFO", 
    "PROCESSOR_POWER_POLICY_INFO[]"
  ], 
  "DVTARGETDEVICE*": [
    "DVTARGETDEVICE", 
    "DVTARGETDEVICE[]"
  ], 
  "USER_MODALS_INFO_1002**": [
    "PointerByReference"
  ], 
  "DCB*": [
    "DCB", 
    "DCB[]"
  ], 
  "ULONG**": [
    "PointerByReference"
  ], 
  "LPENUMLOGFONT": [
    "ENUMLOGFONT", 
    "ENUMLOGFONT[]"
  ], 
  "THREADENTRY32*": [
    "THREADENTRY32", 
    "THREADENTRY32[]"
  ], 
  "LPGPFIDL_FLAGS": [
    "IntByReference", 
    "int[]"
  ], 
  "LPCMONITORINFOEX": [
    "MONITORINFOEX", 
    "MONITORINFOEX[]"
  ], 
  "LPSERVER_INFO_403": [
    "SERVER_INFO_403", 
    "SERVER_INFO_403[]"
  ], 
  "VS_FIXEDFILEINFO": [
    "VS_FIXEDFILEINFO"
  ], 
  "PSIZE*": [
    "PointerByReference"
  ], 
  "SESSION_INFO_2*": [
    "SESSION_INFO_2", 
    "SESSION_INFO_2[]"
  ], 
  "LPEVENT_INSTANCE_HEADER": [
    "EVENT_INSTANCE_HEADER", 
    "EVENT_INSTANCE_HEADER[]"
  ], 
  "HCOLORSPACE": [
    "HANDLE"
  ], 
  "PPSTR*": [
    "byte"
  ], 
  "GPFIDL_FLAGS**": [
    "PointerByReference"
  ], 
  "LPUSER_INFO_1011*": [
    "PointerByReference"
  ], 
  "CONSOLE_SCREEN_BUFFER_INFO**": [
    "PointerByReference"
  ], 
  "HWND": [
    "HANDLE", 
    "Pointer"
  ], 
  "PRM_APP_TYPE": [
    "IntByReference", 
    "int[]"
  ], 
  "LPHKEY": [
    "HANDLEByReference"
  ], 
  "SHARE_INFO_1": [
    "SHARE_INFO_1"
  ], 
  "PWCT_OBJECT_TYPE*": [
    "PointerByReference"
  ], 
  "PQWORD": [
    "LongByReference", 
    "__int64[]"
  ], 
  "PPOWER_ACTION*": [
    "PointerByReference"
  ], 
  "SC_ACTION": [
    "SC_ACTION"
  ], 
  "LPBY_HANDLE_FILE_INFORMATION": [
    "BY_HANDLE_FILE_INFORMATION", 
    "BY_HANDLE_FILE_INFORMATION[]"
  ], 
  "SHARE_INFO_0": [
    "SHARE_INFO_0"
  ], 
  "RGBQUAD**": [
    "PointerByReference"
  ], 
  "PCWSTR*": [
    "WString[]", 
    "PointerByReference"
  ], 
  "PINT8": [
    "byte"
  ], 
  "DISPLAY_DEVICE": [
    "DISPLAY_DEVICE"
  ], 
  "LPLOCALGROUP_MEMBERS_INFO_2*": [
    "PointerByReference"
  ], 
  "WCHAR*": [
    "char"
  ], 
  "CCHAR": [
    "byte"
  ], 
  "SHARE_INFO_2": [
    "SHARE_INFO_2"
  ], 
  "LPNEWTEXTMETRIC": [
    "NEWTEXTMETRIC", 
    "NEWTEXTMETRIC[]"
  ], 
  "LPHINSTANCE": [
    "HANDLEByReference"
  ], 
  "PDFS_INFO_3": [
    "DFS_INFO_3", 
    "DFS_INFO_3[]"
  ], 
  "PPRINTER_ENUM_VALUES": [
    "PRINTER_ENUM_VALUES", 
    "PRINTER_ENUM_VALUES[]"
  ], 
  "LPBATTERY_REPORTING_SCALE": [
    "BATTERY_REPORTING_SCALE", 
    "BATTERY_REPORTING_SCALE[]"
  ], 
  "USE_INFO_2**": [
    "PointerByReference"
  ], 
  "PPDH_STATISTICS": [
    "PDH_STATISTICS", 
    "PDH_STATISTICS[]"
  ], 
  "PINT*": [
    "PointerByReference"
  ], 
  "PRID_DEVICE_INFO_MOUSE": [
    "RID_DEVICE_INFO_MOUSE", 
    "RID_DEVICE_INFO_MOUSE[]"
  ], 
  "WCT_OBJECT_STATUS": [
    "int"
  ], 
  "PTPMPARAMS*": [
    "PointerByReference"
  ], 
  "USER_INFO_1007**": [
    "PointerByReference"
  ], 
  "RM_APP_TYPE": [
    "int"
  ], 
  "LPMONITORINFOEX*": [
    "PointerByReference"
  ], 
  "USER_INFO_2": [
    "USER_INFO_2"
  ], 
  "POLYTEXT": [
    "POLYTEXT"
  ], 
  "LANGID*": [
    "ShortByReference", 
    "short[]"
  ], 
  "USER_MODALS_INFO_1006*": [
    "USER_MODALS_INFO_1006", 
    "USER_MODALS_INFO_1006[]"
  ], 
  "WSAPROTOCOL_INFO**": [
    "PointerByReference"
  ], 
  "GUID*": [
    "GUID", 
    "GUID[]"
  ], 
  "PWER_DUMP_TYPE*": [
    "PointerByReference"
  ], 
  "RM_APP_TYPE*": [
    "IntByReference", 
    "int[]"
  ], 
  "USER_INFO_0": [
    "USER_INFO_0"
  ], 
  "CREATESTRUCT**": [
    "PointerByReference"
  ], 
  "ULONG32**": [
    "PointerByReference"
  ], 
  "LPFILE_INFO_BY_HANDLE_CLASS": [
    "IntByReference", 
    "int[]"
  ], 
  "PDFS_INFO_5": [
    "DFS_INFO_5", 
    "DFS_INFO_5[]"
  ], 
  "LPJOBOBJECTINFOCLASS": [
    "IntByReference", 
    "int[]"
  ], 
  "UINT32": [
    "int"
  ], 
  "LPSMALL_RECT*": [
    "PointerByReference"
  ], 
  "SHARE_INFO_0**": [
    "PointerByReference"
  ], 
  "LPBITMAP": [
    "BITMAP", 
    "BITMAP[]"
  ], 
  "PMONITORINFOEX*": [
    "PointerByReference"
  ], 
  "PJOBOBJECTINFOCLASS": [
    "IntByReference", 
    "int[]"
  ], 
  "LPCPDH_FMT_COUNTERVALUE": [
    "PDH_FMT_COUNTERVALUE", 
    "PDH_FMT_COUNTERVALUE[]"
  ], 
  "PPRINTER_INFO_5*": [
    "PointerByReference"
  ], 
  "CONNECTION_INFO_1**": [
    "PointerByReference"
  ], 
  "LPCDRAWTEXTPARAMS": [
    "DRAWTEXTPARAMS", 
    "DRAWTEXTPARAMS[]"
  ], 
  "LPWKSTA_USER_INFO_0*": [
    "PointerByReference"
  ], 
  "PSERVER_TRANSPORT_INFO_3": [
    "SERVER_TRANSPORT_INFO_3", 
    "SERVER_TRANSPORT_INFO_3[]"
  ], 
  "PSERVER_TRANSPORT_INFO_2": [
    "SERVER_TRANSPORT_INFO_2", 
    "SERVER_TRANSPORT_INFO_2[]"
  ], 
  "PHCURSOR": [
    "HANDLEByReference"
  ], 
  "PKERNINGPAIR*": [
    "PointerByReference"
  ], 
  "SCROLLINFO": [
    "SCROLLINFO"
  ], 
  "CONSOLE_CURSOR_INFO**": [
    "PointerByReference"
  ], 
  "TAPE_SET_DRIVE_PARAMETERS*": [
    "TAPE_SET_DRIVE_PARAMETERS", 
    "TAPE_SET_DRIVE_PARAMETERS[]"
  ], 
  "PULONGLONG*": [
    "PointerByReference"
  ], 
  "USER_POWER_POLICY*": [
    "USER_POWER_POLICY", 
    "USER_POWER_POLICY[]"
  ], 
  "PLPSTR": [
    "byte"
  ], 
  "EFS_CERTIFICATE_BLOB**": [
    "PointerByReference"
  ], 
  "PROCESS_HEAP_ENTRY": [
    "PROCESS_HEAP_ENTRY"
  ], 
  "long*": [
    "IntByReference", 
    "long[]"
  ], 
  "PPDH_RAW_COUNTER_ITEM*": [
    "PointerByReference"
  ], 
  "LPTRACE_GUID_PROPERTIES*": [
    "PointerByReference"
  ], 
  "SERVICE_FAILURE_ACTIONS_FLAG*": [
    "SERVICE_FAILURE_ACTIONS_FLAG", 
    "SERVICE_FAILURE_ACTIONS_FLAG[]"
  ], 
  "LPLPGEOCLASS*": [
    "PointerByReference"
  ], 
  "SHARE_INFO_2**": [
    "PointerByReference"
  ], 
  "PULONG": [
    "IntByReference", 
    "long[]"
  ], 
  "JOBOBJECTINFOCLASS": [
    "int"
  ], 
  "LPTASKDIALOG_FLAGS*": [
    "PointerByReference"
  ], 
  "LPCITEMIDLIST": [
    "ITEMIDLIST", 
    "ITEMIDLIST[]"
  ], 
  "PABCFLOAT*": [
    "PointerByReference"
  ], 
  "LPCWER_DUMP_CUSTOM_OPTIONS": [
    "WER_DUMP_CUSTOM_OPTIONS", 
    "WER_DUMP_CUSTOM_OPTIONS[]"
  ], 
  "EVENT_INSTANCE_INFO*": [
    "EVENT_INSTANCE_INFO", 
    "EVENT_INSTANCE_INFO[]"
  ], 
  "LPWSADATA": [
    "WSADATA"
  ], 
  "LPNONCLIENTMETRICS": [
    "NONCLIENTMETRICS", 
    "NONCLIENTMETRICS[]"
  ], 
  "LPRECTL*": [
    "PointerByReference"
  ], 
  "PDWORD32": [
    "IntByReference", 
    "int[]"
  ], 
  "SYSTEMTIME": [
    "SYSTEMTIME"
  ], 
  "JOB_INFO_3*": [
    "JOB_INFO_3", 
    "JOB_INFO_3[]"
  ], 
  "PUSER_MODALS_INFO_1007": [
    "USER_MODALS_INFO_1007", 
    "USER_MODALS_INFO_1007[]"
  ], 
  "CBT_CREATEWND*": [
    "CBT_CREATEWND", 
    "CBT_CREATEWND[]"
  ], 
  "PCSTR": [
    "ByteByReference", 
    "char[]", 
    "String"
  ], 
  "SHITEMID*": [
    "SHITEMID", 
    "SHITEMID[]"
  ], 
  "LPLOCALGROUP_MEMBERS_INFO_0": [
    "LOCALGROUP_MEMBERS_INFO_0", 
    "LOCALGROUP_MEMBERS_INFO_0[]"
  ], 
  "PHFILE": [
    "IntByReference", 
    "int[]"
  ], 
  "PSHQUERYRBINFO": [
    "SHQUERYRBINFO", 
    "SHQUERYRBINFO[]"
  ], 
  "ACL": [
    "ACL"
  ], 
  "PDEVMODE": [
    "DEVMODE", 
    "DEVMODE[]"
  ], 
  "PFLOAT*": [
    "PointerByReference"
  ], 
  "LPSERVER_TRANSPORT_INFO_2*": [
    "PointerByReference"
  ], 
  "PIO_COUNTERS*": [
    "PointerByReference"
  ], 
  "PSERVER_TRANSPORT_INFO_1*": [
    "PointerByReference"
  ], 
  "LPSIZE*": [
    "PointerByReference"
  ], 
  "LPDWORD32*": [
    "PointerByReference"
  ], 
  "LPCMETARECORD": [
    "METARECORD", 
    "METARECORD[]"
  ], 
  "LOCALGROUP_MEMBERS_INFO_2**": [
    "PointerByReference"
  ], 
  "SERVER_INFO_101**": [
    "PointerByReference"
  ], 
  "LPBITMAPINFO": [
    "BITMAPINFO", 
    "BITMAPINFO[]"
  ], 
  "LPSERVICE_DELAYED_AUTO_START_INFO": [
    "SERVICE_DELAYED_AUTO_START_INFO", 
    "SERVICE_DELAYED_AUTO_START_INFO[]"
  ], 
  "PDH_RAW_LOG_RECORD": [
    "PDH_RAW_LOG_RECORD"
  ], 
  "LPPRINTER_NOTIFY_OPTIONS_TYPE": [
    "PRINTER_NOTIFY_OPTIONS_TYPE", 
    "PRINTER_NOTIFY_OPTIONS_TYPE[]"
  ], 
  "PSTREAM_INFO_LEVELS": [
    "IntByReference", 
    "int[]"
  ], 
  "LPCDOC_INFO_1": [
    "DOC_INFO_1", 
    "DOC_INFO_1[]"
  ], 
  "TITLEBARINFO*": [
    "TITLEBARINFO", 
    "TITLEBARINFO[]"
  ], 
  "PJOB_INFO_4": [
    "JOB_INFO_4", 
    "JOB_INFO_4[]"
  ], 
  "PJOB_INFO_2": [
    "JOB_INFO_2", 
    "JOB_INFO_2[]"
  ], 
  "PJOB_INFO_3": [
    "JOB_INFO_3", 
    "JOB_INFO_3[]"
  ], 
  "long": [
    "int"
  ], 
  "NEWTEXTMETRIC*": [
    "NEWTEXTMETRIC", 
    "NEWTEXTMETRIC[]"
  ], 
  "LPCMSG_INFO_1": [
    "MSG_INFO_1", 
    "MSG_INFO_1[]"
  ], 
  "LPCMSG_INFO_0": [
    "MSG_INFO_0", 
    "MSG_INFO_0[]"
  ], 
  "PSESSION_INFO_2*": [
    "PointerByReference"
  ], 
  "PJOB_INFO_1": [
    "JOB_INFO_1", 
    "JOB_INFO_1[]"
  ], 
  "LPSID_AND_ATTRIBUTES": [
    "SID_AND_ATTRIBUTES", 
    "SID_AND_ATTRIBUTES[]"
  ], 
  "LPRAWHID": [
    "RAWHID", 
    "RAWHID[]"
  ], 
  "LPLPWSTR": [
    "char"
  ], 
  "SYSTEMTIME*": [
    "SYSTEMTIME", 
    "SYSTEMTIME[]"
  ], 
  "RID_DEVICE_INFO": [
    "RID_DEVICE_INFO"
  ], 
  "BYTE**": [
    "byte"
  ], 
  "MINIMIZEDMETRICS*": [
    "MINIMIZEDMETRICS", 
    "MINIMIZEDMETRICS[]"
  ], 
  "JOB_INFO_3**": [
    "PointerByReference"
  ], 
  "LPWORD": [
    "ShortByReference", 
    "short[]"
  ], 
  "LPCPRINTPAGERANGE": [
    "PRINTPAGERANGE", 
    "PRINTPAGERANGE[]"
  ], 
  "LPCWSTR**": [
    "char"
  ], 
  "PROVIDOR_INFO_2*": [
    "PROVIDOR_INFO_2", 
    "PROVIDOR_INFO_2[]"
  ], 
  "LPPORT_INFO_1*": [
    "PointerByReference"
  ], 
  "PSYSTEM_POWER_STATUS": [
    "SYSTEM_POWER_STATUS", 
    "SYSTEM_POWER_STATUS[]"
  ], 
  "LPCLASTINPUTINFO": [
    "LASTINPUTINFO", 
    "LASTINPUTINFO[]"
  ], 
  "LPWMIDPREQUESTCODE*": [
    "PointerByReference"
  ], 
  "LPUSER_INFO_1017": [
    "USER_INFO_1017", 
    "USER_INFO_1017[]"
  ], 
  "PDRAWTEXTPARAMS": [
    "DRAWTEXTPARAMS", 
    "DRAWTEXTPARAMS[]"
  ], 
  "LPSHDESCRIPTIONID*": [
    "PointerByReference"
  ], 
  "PACCESS_MASK": [
    "IntByReference", 
    "long[]"
  ], 
  "HEAP_INFORMATION_CLASS": [
    "int"
  ], 
  "int*": [
    "IntByReference", 
    "int[]"
  ], 
  "USER_MODALS_INFO_0*": [
    "USER_MODALS_INFO_0", 
    "USER_MODALS_INFO_0[]"
  ], 
  "LPMENUEX_TEMPLATE_HEADER": [
    "MENUEX_TEMPLATE_HEADER", 
    "MENUEX_TEMPLATE_HEADER[]"
  ], 
  "LRESULT**": [
    "PointerByReference"
  ], 
  "LOCALGROUP_USERS_INFO_0": [
    "LOCALGROUP_USERS_INFO_0"
  ], 
  "BITMAP": [
    "BITMAP"
  ], 
  "PWSTR": [
    "CHARByReference", 
    "wchar_t[]", 
    "WString"
  ], 
  "LPJOBOBJECT_CPU_RATE_CONTROL_INFORMATION*": [
    "PointerByReference"
  ], 
  "FONTSIGNATURE": [
    "FONTSIGNATURE"
  ], 
  "SERVICE_FAILURE_ACTIONS*": [
    "SERVICE_FAILURE_ACTIONS", 
    "SERVICE_FAILURE_ACTIONS[]"
  ], 
  "STAT_SERVER_0": [
    "STAT_SERVER_0"
  ], 
  "LPCRAWINPUTDEVICE": [
    "RAWINPUTDEVICE", 
    "RAWINPUTDEVICE[]"
  ], 
  "PNET_DISPLAY_MACHINE": [
    "NET_DISPLAY_MACHINE", 
    "NET_DISPLAY_MACHINE[]"
  ], 
  "USER_INFO_2**": [
    "PointerByReference"
  ], 
  "PLRESULT": [
    "IntByReference", 
    "long[]"
  ], 
  "LPWNDCLASSEX": [
    "WNDCLASSEX", 
    "WNDCLASSEX[]"
  ], 
  "LPWPARAM*": [
    "PointerByReference"
  ], 
  "LPPROCESSOR_POWER_POLICY_INFO*": [
    "PointerByReference"
  ], 
  "PDWORDLONG*": [
    "PointerByReference"
  ], 
  "BOOL": [
    "int", 
    "boolean"
  ], 
  "PEVENTLOGRECORD*": [
    "PointerByReference"
  ], 
  "LPJOB_INFO_2*": [
    "PointerByReference"
  ], 
  "PGROUP_USERS_INFO_0": [
    "GROUP_USERS_INFO_0", 
    "GROUP_USERS_INFO_0[]"
  ], 
  "PDH_TIME_INFO*": [
    "PDH_TIME_INFO", 
    "PDH_TIME_INFO[]"
  ], 
  "LPMediaLabelInfo": [
    "MediaLabelInfo", 
    "MediaLabelInfo[]"
  ], 
  "LPNET_DISPLAY_GROUP*": [
    "PointerByReference"
  ], 
  "PUSER_INFO_1052*": [
    "PointerByReference"
  ], 
  "JOBOBJECT_SECURITY_LIMIT_INFORMATION*": [
    "JOBOBJECT_SECURITY_LIMIT_INFORMATION", 
    "JOBOBJECT_SECURITY_LIMIT_INFORMATION[]"
  ], 
  "HRESULT*": [
    "IntByReference", 
    "long[]"
  ], 
  "PGEOCLASS": [
    "IntByReference", 
    "long[]"
  ], 
  "LPCSYSTEM_INFO": [
    "SYSTEM_INFO", 
    "SYSTEM_INFO[]"
  ], 
  "VIDEOPARAMETERS*": [
    "VIDEOPARAMETERS", 
    "VIDEOPARAMETERS[]"
  ], 
  "LPSCROLLBARINFO": [
    "SCROLLBARINFO", 
    "SCROLLBARINFO[]"
  ], 
  "PRINTER_NOTIFY_OPTIONS": [
    "PRINTER_NOTIFY_OPTIONS"
  ], 
  "LPUSER_INFO_3*": [
    "PointerByReference"
  ], 
  "LPWNODE_HEADER*": [
    "PointerByReference"
  ], 
  "SHQUERYRBINFO": [
    "SHQUERYRBINFO"
  ], 
  "PDH_RAW_LOG_RECORD**": [
    "PointerByReference"
  ], 
  "LPPOWER_ACTION*": [
    "PointerByReference"
  ], 
  "LPCWINDOWPLACEMENT": [
    "WINDOWPLACEMENT", 
    "WINDOWPLACEMENT[]"
  ], 
  "LPSC_ACTION*": [
    "PointerByReference"
  ], 
  "SECURITY_ATTRIBUTES*": [
    "SECURITY_ATTRIBUTES", 
    "SECURITY_ATTRIBUTES[]"
  ], 
  "PGROUP_INFO_1": [
    "GROUP_INFO_1", 
    "GROUP_INFO_1[]"
  ], 
  "LPHENHMETAFILE": [
    "HANDLEByReference"
  ], 
  "INT16": [
    "short"
  ], 
  "LPSHFILEINFO": [
    "SHFILEINFO", 
    "SHFILEINFO[]"
  ], 
  "LPWAITCHAIN_NODE_INFO": [
    "WAITCHAIN_NODE_INFO", 
    "WAITCHAIN_NODE_INFO[]"
  ], 
  "LPPDH_DATA_ITEM_PATH_ELEMENTS": [
    "PDH_DATA_ITEM_PATH_ELEMENTS", 
    "PDH_DATA_ITEM_PATH_ELEMENTS[]"
  ], 
  "LPPCWSTR": [
    "char"
  ], 
  "LPCSERVER_INFO_102": [
    "SERVER_INFO_102", 
    "SERVER_INFO_102[]"
  ], 
  "LPCSERVER_INFO_101": [
    "SERVER_INFO_101", 
    "SERVER_INFO_101[]"
  ], 
  "LPCSERVER_INFO_100": [
    "SERVER_INFO_100", 
    "SERVER_INFO_100[]"
  ], 
  "WCHAR**": [
    "char"
  ], 
  "PSECURITY_QUALITY_OF_SERVICE*": [
    "PointerByReference"
  ], 
  "POINTL": [
    "POINTL"
  ], 
  "PSHELLEXECUTEINFO": [
    "SHELLEXECUTEINFO", 
    "SHELLEXECUTEINFO[]"
  ], 
  "DISPLAY_DEVICE**": [
    "PointerByReference"
  ], 
  "PWINSTA": [
    "HANDLEByReference"
  ], 
  "SESSION_INFO_10**": [
    "PointerByReference"
  ], 
  "LPMAT2*": [
    "PointerByReference"
  ], 
  "QWORD*": [
    "LongByReference", 
    "__int64[]"
  ], 
  "PWER_FILE_TYPE": [
    "IntByReference", 
    "int[]"
  ], 
  "DWORD32*": [
    "IntByReference", 
    "int[]"
  ], 
  "HHOOK*": [
    "HANDLEByReference"
  ], 
  "PGET_FILEEX_INFO_LEVELS*": [
    "PointerByReference"
  ], 
  "PPRINTER_INFO_6*": [
    "PointerByReference"
  ], 
  "LPWCRANGE*": [
    "PointerByReference"
  ], 
  "LPSERVICE_TRIGGER": [
    "SERVICE_TRIGGER", 
    "SERVICE_TRIGGER[]"
  ], 
  "PRAWMOUSE": [
    "RAWMOUSE", 
    "RAWMOUSE[]"
  ], 
  "PSTD_ALERT*": [
    "PointerByReference"
  ], 
  "POINT*": [
    "POINT", 
    "POINT[]"
  ], 
  "SERVICE_STATUS*": [
    "SERVICE_STATUS", 
    "SERVICE_STATUS[]"
  ], 
  "PRINTER_INFO_2*": [
    "PRINTER_INFO_2", 
    "PRINTER_INFO_2[]"
  ], 
  "LPHEAP_INFORMATION_CLASS*": [
    "PointerByReference"
  ], 
  "PWER_REGISTER_FILE_TYPE*": [
    "PointerByReference"
  ], 
  "LPMENUINFO*": [
    "PointerByReference"
  ], 
  "ATOM": [
    "short", 
    "ATOM"
  ], 
  "PKAFFINITY*": [
    "PointerByReference"
  ], 
  "MACHINE_POWER_POLICY**": [
    "PointerByReference"
  ], 
  "LPUSER_POWER_POLICY*": [
    "PointerByReference"
  ], 
  "MSG**": [
    "PointerByReference"
  ], 
  "JOBOBJECT_SECURITY_LIMIT_INFORMATION**": [
    "PointerByReference"
  ], 
  "LPCSC_ACTION": [
    "SC_ACTION", 
    "SC_ACTION[]"
  ], 
  "BLENDFUNCTION": [
    "BLENDFUNCTION"
  ], 
  "TITLEBARINFO": [
    "TITLEBARINFO"
  ], 
  "PRID_DEVICE_INFO_MOUSE*": [
    "PointerByReference"
  ], 
  "PLDT_ENTRY": [
    "LDT_ENTRY", 
    "LDT_ENTRY[]"
  ], 
  "PTAPE_GET_DRIVE_PARAMETERS*": [
    "PointerByReference"
  ], 
  "PPDH_COUNTER_INFO*": [
    "PointerByReference"
  ], 
  "LPUSER_MODALS_INFO_0*": [
    "PointerByReference"
  ], 
  "LPUINT_PTR": [
    "IntByReference", 
    "int[]"
  ], 
  "USER_MODALS_INFO_1**": [
    "PointerByReference"
  ], 
  "GROUP_INFO_2*": [
    "GROUP_INFO_2", 
    "GROUP_INFO_2[]"
  ], 
  "LPLPGEOTYPE*": [
    "IntByReference", 
    "long[]"
  ], 
  "LPDFS_INFO_107*": [
    "PointerByReference"
  ], 
  "LPSERVICE_REQUIRED_PRIVILEGES_INFO*": [
    "PointerByReference"
  ], 
  "PINT8*": [
    "byte"
  ], 
  "LPTASKDIALOG_COMMON_BUTTON_FLAGS*": [
    "PointerByReference"
  ], 
  "PSTARTUPINFO*": [
    "PointerByReference"
  ], 
  "LPLONGLONG*": [
    "PointerByReference"
  ], 
  "LPEVENT_TRACE_PROPERTIES": [
    "EVENT_TRACE_PROPERTIES", 
    "EVENT_TRACE_PROPERTIES[]"
  ], 
  "PWAITCHAIN_NODE_INFO*": [
    "PointerByReference"
  ], 
  "USER_INFO_1053**": [
    "PointerByReference"
  ], 
  "OPENASINFO": [
    "OPENASINFO"
  ], 
  "DFS_INFO_100*": [
    "DFS_INFO_100", 
    "DFS_INFO_100[]"
  ], 
  "GROUP_INFO_1": [
    "GROUP_INFO_1"
  ], 
  "GROUP_INFO_0": [
    "GROUP_INFO_0"
  ], 
  "GROUP_INFO_3": [
    "GROUP_INFO_3"
  ], 
  "SERVICE_CONTROL_STATUS_REASON_PARAMS*": [
    "SERVICE_CONTROL_STATUS_REASON_PARAMS", 
    "SERVICE_CONTROL_STATUS_REASON_PARAMS[]"
  ], 
  "LPLPGEOTYPE": [
    "IntByReference", 
    "long[]"
  ], 
  "WKSTA_INFO_502*": [
    "WKSTA_INFO_502", 
    "WKSTA_INFO_502[]"
  ], 
  "LPCRITICAL_SECTION": [
    "PRTL_CRITICAL_SECTION"
  ], 
  "PWKSTA_TRANSPORT_INFO_0*": [
    "PointerByReference"
  ], 
  "DFS_INFO_200**": [
    "PointerByReference"
  ], 
  "NDDESHAREINFO**": [
    "PointerByReference"
  ], 
  "SYSTEM_POWER_STATUS*": [
    "SYSTEM_POWER_STATUS", 
    "SYSTEM_POWER_STATUS[]"
  ], 
  "LPSESSION_INFO_2": [
    "SESSION_INFO_2", 
    "SESSION_INFO_2[]"
  ], 
  "LPICONMETRICS*": [
    "PointerByReference"
  ], 
  "LPSESSION_INFO_0": [
    "SESSION_INFO_0", 
    "SESSION_INFO_0[]"
  ], 
  "LPSESSION_INFO_1": [
    "SESSION_INFO_1", 
    "SESSION_INFO_1[]"
  ], 
  "SERVICE_DESCRIPTION*": [
    "SERVICE_DESCRIPTION", 
    "SERVICE_DESCRIPTION[]"
  ], 
  "TASKDIALOG_COMMON_BUTTON_FLAGS**": [
    "PointerByReference"
  ], 
  "WCRANGE": [
    "WCRANGE"
  ], 
  "PACCESS_MASK*": [
    "PointerByReference"
  ], 
  "LPCCURRENCYFMT": [
    "CURRENCYFMT", 
    "CURRENCYFMT[]"
  ], 
  "LPSTICKYKEYS": [
    "STICKYKEYS", 
    "STICKYKEYS[]"
  ], 
  "LPPROCESS_MEMORY_COUNTERS_EX": [
    "PROCESS_MEMORY_COUNTERS_EX", 
    "PROCESS_MEMORY_COUNTERS_EX[]"
  ], 
  "JOBOBJECT_BASIC_UI_RESTRICTIONS**": [
    "PointerByReference"
  ], 
  "LPCPOWERBROADCAST_SETTING": [
    "POWERBROADCAST_SETTING", 
    "POWERBROADCAST_SETTING[]"
  ], 
  "PDCB": [
    "DCB", 
    "DCB[]"
  ], 
  "PMENU_EVENT_RECORD": [
    "MENU_EVENT_RECORD", 
    "MENU_EVENT_RECORD[]"
  ], 
  "PDFS_INFO_300": [
    "DFS_INFO_300", 
    "DFS_INFO_300[]"
  ], 
  "PQUERY_USER_NOTIFICATION_STATE": [
    "IntByReference", 
    "int[]"
  ], 
  "USER_MODALS_INFO_1002": [
    "USER_MODALS_INFO_1002"
  ], 
  "CONNECTION_INFO_0*": [
    "CONNECTION_INFO_0", 
    "CONNECTION_INFO_0[]"
  ], 
  "TEXTMETRIC": [
    "TEXTMETRIC"
  ], 
  "DFS_INFO_6*": [
    "DFS_INFO_6", 
    "DFS_INFO_6[]"
  ], 
  "LPPWSTR*": [
    "char"
  ], 
  "ACCESSTIMEOUT**": [
    "PointerByReference"
  ], 
  "LPSERVER_INFO_102*": [
    "PointerByReference"
  ], 
  "PTOKEN_DEFAULT_DACL*": [
    "PointerByReference"
  ], 
  "PBATTERY_REPORTING_SCALE": [
    "BATTERY_REPORTING_SCALE", 
    "BATTERY_REPORTING_SCALE[]"
  ], 
  "LPCPDH_TIME_INFO": [
    "PDH_TIME_INFO", 
    "PDH_TIME_INFO[]"
  ], 
  "LPSYSTEM_INFO*": [
    "PointerByReference"
  ], 
  "GEOTYPE**": [
    "PointerByReference"
  ], 
  "long long": [
    "long"
  ], 
  "LPGUITHREADINFO*": [
    "PointerByReference"
  ], 
  "QWORD": [
    "long"
  ], 
  "LPSCROLLBARINFO*": [
    "PointerByReference"
  ], 
  "LPMENUINFO": [
    "MENUINFO", 
    "MENUINFO[]"
  ], 
  "LPEVENT_INSTANCE_HEADER*": [
    "PointerByReference"
  ], 
  "LPMSG_INFO_1": [
    "MSG_INFO_1", 
    "MSG_INFO_1[]"
  ], 
  "LPMSG_INFO_0": [
    "MSG_INFO_0", 
    "MSG_INFO_0[]"
  ], 
  "SESSION_INFO_1*": [
    "SESSION_INFO_1", 
    "SESSION_INFO_1[]"
  ], 
  "PGROUP_AFFINITY*": [
    "PointerByReference"
  ], 
  "PUSER_INFO_1010": [
    "USER_INFO_1010", 
    "USER_INFO_1010[]"
  ], 
  "LPLPCSTR*": [
    "byte"
  ], 
  "RAWINPUTDEVICE**": [
    "PointerByReference"
  ], 
  "NET_DISPLAY_MACHINE": [
    "NET_DISPLAY_MACHINE"
  ], 
  "PLOCALGROUP_MEMBERS_INFO_2": [
    "LOCALGROUP_MEMBERS_INFO_2", 
    "LOCALGROUP_MEMBERS_INFO_2[]"
  ], 
  "NETCONNECTINFOSTRUCT*": [
    "NETCONNECTINFOSTRUCT", 
    "NETCONNECTINFOSTRUCT[]"
  ], 
  "PLOCALGROUP_MEMBERS_INFO_0": [
    "LOCALGROUP_MEMBERS_INFO_0", 
    "LOCALGROUP_MEMBERS_INFO_0[]"
  ], 
  "DVTARGETDEVICE": [
    "DVTARGETDEVICE"
  ], 
  "MODULEENTRY32**": [
    "PointerByReference"
  ], 
  "PPDH_COUNTER_PATH_ELEMENTS": [
    "PDH_COUNTER_PATH_ELEMENTS", 
    "PDH_COUNTER_PATH_ELEMENTS[]"
  ], 
  "LPFILE_NOTIFY_INFORMATION*": [
    "PointerByReference"
  ], 
  "PGLOBAL_MACHINE_POWER_POLICY*": [
    "PointerByReference"
  ], 
  "SSIZE_T*": [
    "IntByReference", 
    "long[]"
  ], 
  "LPCCONSOLE_FONT_INFOEX": [
    "CONSOLE_FONT_INFOEX", 
    "CONSOLE_FONT_INFOEX[]"
  ], 
  "POWER_ACTION_POLICY*": [
    "POWER_ACTION_POLICY", 
    "POWER_ACTION_POLICY[]"
  ], 
  "SERVICE_PRESHUTDOWN_INFO*": [
    "SERVICE_PRESHUTDOWN_INFO", 
    "SERVICE_PRESHUTDOWN_INFO[]"
  ], 
  "LPDFS_INFO_4*": [
    "PointerByReference"
  ], 
  "LPUSER_MODALS_INFO_1003*": [
    "PointerByReference"
  ], 
  "SC_HANDLE": [
    "HANDLE"
  ], 
  "LPGROUP_INFO_1005*": [
    "PointerByReference"
  ], 
  "RM_UNIQUE_PROCESS": [
    "RM_UNIQUE_PROCESS"
  ], 
  "EVENT_INSTANCE_HEADER**": [
    "PointerByReference"
  ], 
  "WIN32_STREAM_ID*": [
    "WIN32_STREAM_ID", 
    "WIN32_STREAM_ID[]"
  ], 
  "LPSECURITY_CONTEXT_TRACKING_MODE": [
    "byte"
  ], 
  "LPCASSOC_FILTER": [
    "IntByReference", 
    "int[]"
  ], 
  "LPCGET_FILEEX_INFO_LEVELS": [
    "IntByReference", 
    "int[]"
  ], 
  "PSHARE_INFO_1501": [
    "SHARE_INFO_1501", 
    "SHARE_INFO_1501[]"
  ], 
  "LPSERVICE_FAILURE_ACTIONS_FLAG*": [
    "PointerByReference"
  ], 
  "LPDFS_INFO_5": [
    "DFS_INFO_5", 
    "DFS_INFO_5[]"
  ], 
  "CLIPFORMAT**": [
    "PointerByReference"
  ], 
  "LPAXISINFO*": [
    "PointerByReference"
  ], 
  "DYNAMIC_TIME_ZONE_INFORMATION**": [
    "PointerByReference"
  ], 
  "LPCSERVICE_STATUS_PROCESS": [
    "SERVICE_STATUS_PROCESS", 
    "SERVICE_STATUS_PROCESS[]"
  ], 
  "LPCEXTENDED_NAME_FORMAT": [
    "EXTENDED_NAME_FORMAT", 
    "EXTENDED_NAME_FORMAT[]"
  ], 
  "LPCLOCALGROUP_MEMBERS_INFO_1": [
    "LOCALGROUP_MEMBERS_INFO_1", 
    "LOCALGROUP_MEMBERS_INFO_1[]"
  ], 
  "LPCLOCALGROUP_MEMBERS_INFO_0": [
    "LOCALGROUP_MEMBERS_INFO_0", 
    "LOCALGROUP_MEMBERS_INFO_0[]"
  ], 
  "LPCLOCALGROUP_MEMBERS_INFO_3": [
    "LOCALGROUP_MEMBERS_INFO_3", 
    "LOCALGROUP_MEMBERS_INFO_3[]"
  ], 
  "LPCLOCALGROUP_MEMBERS_INFO_2": [
    "LOCALGROUP_MEMBERS_INFO_2", 
    "LOCALGROUP_MEMBERS_INFO_2[]"
  ], 
  "CONSOLE_SCREEN_BUFFER_INFOEX**": [
    "PointerByReference"
  ], 
  "LPDESIGNVECTOR*": [
    "PointerByReference"
  ], 
  "USER_INFO_10**": [
    "PointerByReference"
  ], 
  "EMR**": [
    "PointerByReference"
  ], 
  "LPHRESULT": [
    "IntByReference", 
    "long[]"
  ], 
  "LOGFONT*": [
    "LOGFONT", 
    "LOGFONT[]"
  ], 
  "LPSESSION_INFO_502": [
    "SESSION_INFO_502", 
    "SESSION_INFO_502[]"
  ], 
  "TAPE_GET_MEDIA_PARAMETERS*": [
    "TAPE_GET_MEDIA_PARAMETERS", 
    "TAPE_GET_MEDIA_PARAMETERS[]"
  ], 
  "LPCDRIVER_INFO_6": [
    "DRIVER_INFO_6", 
    "DRIVER_INFO_6[]"
  ], 
  "PGROUP_INFO_1*": [
    "PointerByReference"
  ], 
  "PWER_REPORT_TYPE": [
    "IntByReference", 
    "int[]"
  ], 
  "LPPRIVILEGE_SET": [
    "PRIVILEGE_SET", 
    "PRIVILEGE_SET[]"
  ], 
  "LPMETAFILEPICT*": [
    "PointerByReference"
  ], 
  "WER_CONSENT*": [
    "IntByReference", 
    "int[]"
  ], 
  "LPPOINT*": [
    "PointerByReference"
  ], 
  "DFS_TARGET_PRIORITY*": [
    "DFS_TARGET_PRIORITY", 
    "DFS_TARGET_PRIORITY[]"
  ], 
  "TpcGetSamplesArgs**": [
    "PointerByReference"
  ], 
  "LPWAITCHAIN_NODE_INFO*": [
    "PointerByReference"
  ], 
  "RAWMOUSE**": [
    "PointerByReference"
  ], 
  "PRIVILEGE_SET**": [
    "PointerByReference"
  ], 
  "LPFILE_NOTIFY_INFORMATION": [
    "FILE_NOTIFY_INFORMATION", 
    "FILE_NOTIFY_INFORMATION[]"
  ], 
  "LPAVRT_PRIORITY": [
    "IntByReference", 
    "int[]"
  ], 
  "ATOM*": [
    "ShortByReference", 
    "short[]"
  ], 
  "LPCSERVICE_DELAYED_AUTO_START_INFO": [
    "SERVICE_DELAYED_AUTO_START_INFO", 
    "SERVICE_DELAYED_AUTO_START_INFO[]"
  ], 
  "LPPRINTPROCESSOR_INFO_1": [
    "PRINTPROCESSOR_INFO_1", 
    "PRINTPROCESSOR_INFO_1[]"
  ], 
  "PDH_FMT_COUNTERVALUE**": [
    "PointerByReference"
  ], 
  "PABC": [
    "ABC", 
    "ABC[]"
  ], 
  "LPULONG32*": [
    "PointerByReference"
  ], 
  "GROUP_INFO_1002": [
    "GROUP_INFO_1002"
  ], 
  "PCPINFOEX*": [
    "PointerByReference"
  ], 
  "LPFILETIME": [
    "FILETIME", 
    "FILETIME[]"
  ], 
  "PACCEL*": [
    "PointerByReference"
  ], 
  "GROUP_INFO_1005": [
    "GROUP_INFO_1005"
  ], 
  "LPCCLIPFORMAT": [
    "IntByReference", 
    "int[]"
  ], 
  "ACCEL": [
    "ACCEL"
  ], 
  "LPPGEOTYPE": [
    "IntByReference", 
    "long[]"
  ], 
  "LPUSER_MODALS_INFO_1001": [
    "USER_MODALS_INFO_1001", 
    "USER_MODALS_INFO_1001[]"
  ], 
  "PSHFILEOPSTRUCT": [
    "SHFILEOPSTRUCT", 
    "SHFILEOPSTRUCT[]"
  ], 
  "PCONSOLE_SCREEN_BUFFER_INFO": [
    "CONSOLE_SCREEN_BUFFER_INFO", 
    "CONSOLE_SCREEN_BUFFER_INFO[]"
  ], 
  "LPPOINTL": [
    "POINTL", 
    "POINTL[]"
  ], 
  "PUSER_INFO_1003*": [
    "PointerByReference"
  ], 
  "LPCPOWER_POLICY": [
    "POWER_POLICY", 
    "POWER_POLICY[]"
  ], 
  "LPCSERVICE_TIMECHANGE_INFO": [
    "SERVICE_TIMECHANGE_INFO", 
    "SERVICE_TIMECHANGE_INFO[]"
  ], 
  "LPCMEMORYSTATUS": [
    "MEMORYSTATUS", 
    "MEMORYSTATUS[]"
  ], 
  "PLGRPID": [
    "IntByReference", 
    "long[]"
  ], 
  "METARECORD": [
    "METARECORD"
  ], 
  "LPDWORD_PTR*": [
    "PointerByReference"
  ], 
  "CONNECTDLGSTRUCT**": [
    "PointerByReference"
  ], 
  "LPJOB_INFO_4": [
    "JOB_INFO_4", 
    "JOB_INFO_4[]"
  ], 
  "LDT_ENTRY": [
    "LDT_ENTRY"
  ], 
  "HENHMETAFILE*": [
    "HANDLEByReference"
  ], 
  "PSYSTEM_POWER_POLICY": [
    "SYSTEM_POWER_POLICY", 
    "SYSTEM_POWER_POLICY[]"
  ], 
  "LPJOB_INFO_2": [
    "JOB_INFO_2", 
    "JOB_INFO_2[]"
  ], 
  "PWKSTA_USER_INFO_1101*": [
    "PointerByReference"
  ], 
  "LPLOCALGROUP_MEMBERS_INFO_3*": [
    "PointerByReference"
  ], 
  "WNODE_HEADER*": [
    "WNODE_HEADER", 
    "WNODE_HEADER[]"
  ], 
  "LPRAWKEYBOARD": [
    "RAWKEYBOARD", 
    "RAWKEYBOARD[]"
  ], 
  "PRINTER_INFO_9**": [
    "PointerByReference"
  ], 
  "LPDLGITEMTEMPLATE*": [
    "PointerByReference"
  ], 
  "LPCUMS_CREATE_THREAD_ATTRIBUTES": [
    "UMS_CREATE_THREAD_ATTRIBUTES", 
    "UMS_CREATE_THREAD_ATTRIBUTES[]"
  ], 
  "LPEMRALPHABLEND": [
    "EMRALPHABLEND", 
    "EMRALPHABLEND[]"
  ], 
  "LCTYPE**": [
    "PointerByReference"
  ], 
  "LPCFILE_INFO_BY_HANDLE_CLASS": [
    "IntByReference", 
    "int[]"
  ], 
  "USER_MODALS_INFO_1005*": [
    "USER_MODALS_INFO_1005", 
    "USER_MODALS_INFO_1005[]"
  ], 
  "PUSER_MODALS_INFO_1001*": [
    "PointerByReference"
  ], 
  "HMENU": [
    "HANDLE"
  ], 
  "HEAPLIST32*": [
    "HEAPLIST32", 
    "HEAPLIST32[]"
  ], 
  "LPCPOLYTEXT": [
    "POLYTEXT", 
    "POLYTEXT[]"
  ], 
  "TRIVERTEX**": [
    "PointerByReference"
  ], 
  "CBT_CREATEWND**": [
    "PointerByReference"
  ], 
  "WKSTA_INFO_102**": [
    "PointerByReference"
  ], 
  "USEROBJECTFLAGS**": [
    "PointerByReference"
  ], 
  "LPCWPSTRUCT*": [
    "PointerByReference"
  ], 
  "WNDCLASSEX**": [
    "PointerByReference"
  ], 
  "PWER_CONSENT": [
    "IntByReference", 
    "int[]"
  ], 
  "LPCRAWINPUT": [
    "RAWINPUT", 
    "RAWINPUT[]"
  ], 
  "IO_COUNTERS**": [
    "PointerByReference"
  ], 
  "LPCMENUBARINFO": [
    "MENUBARINFO", 
    "MENUBARINFO[]"
  ], 
  "LPCSERVICE_TRIGGER": [
    "SERVICE_TRIGGER", 
    "SERVICE_TRIGGER[]"
  ], 
  "LPUSER_INFO_22*": [
    "PointerByReference"
  ], 
  "MOUSEKEYS**": [
    "PointerByReference"
  ], 
  "PEVENT_TRACE_HEADER*": [
    "PointerByReference"
  ], 
  "TPMPARAMS": [
    "TPMPARAMS"
  ], 
  "GETPROPERTYSTOREFLAGS": [
    "int"
  ], 
  "PCPINFOEX": [
    "CPINFOEX", 
    "CPINFOEX[]"
  ], 
  "LPSERVICE_PREFERRED_NODE_INFO*": [
    "PointerByReference"
  ], 
  "LPWKSTA_USER_INFO_1*": [
    "PointerByReference"
  ], 
  "PWKSTA_INFO_502": [
    "WKSTA_INFO_502", 
    "WKSTA_INFO_502[]"
  ], 
  "PDH_FMT_COUNTERVALUE*": [
    "PDH_FMT_COUNTERVALUE", 
    "PDH_FMT_COUNTERVALUE[]"
  ], 
  "LPGCP_RESULTS": [
    "GCP_RESULTS", 
    "GCP_RESULTS[]"
  ], 
  "LPCOLOR16": [
    "ShortByReference", 
    "short[]"
  ], 
  "PMENUEX_TEMPLATE_ITEM*": [
    "PointerByReference"
  ], 
  "PSERVICE_FAILURE_ACTIONS": [
    "SERVICE_FAILURE_ACTIONS", 
    "SERVICE_FAILURE_ACTIONS[]"
  ], 
  "SECURITY_CONTEXT_TRACKING_MODE**": [
    "byte"
  ], 
  "PSAPI_WS_WATCH_INFORMATION*": [
    "PSAPI_WS_WATCH_INFORMATION", 
    "PSAPI_WS_WATCH_INFORMATION[]"
  ], 
  "PUSER_INFO_11": [
    "USER_INFO_11", 
    "USER_INFO_11[]"
  ], 
  "PPRINTER_NOTIFY_INFO": [
    "PRINTER_NOTIFY_INFO", 
    "PRINTER_NOTIFY_INFO[]"
  ], 
  "PUSER_INFO_10": [
    "USER_INFO_10", 
    "USER_INFO_10[]"
  ], 
  "HCOLORSPACE*": [
    "HANDLEByReference"
  ], 
  "JOBOBJECT_RATE_CONTROL_TOLERANCE_INTERVAL**": [
    "PointerByReference"
  ], 
  "COMSTAT**": [
    "PointerByReference"
  ], 
  "LPULONG_PTR*": [
    "PointerByReference"
  ], 
  "PCONSOLE_FONT_INFO*": [
    "PointerByReference"
  ], 
  "LPHANDLETABLE*": [
    "PointerByReference"
  ], 
  "LPULONGLONG*": [
    "PointerByReference"
  ], 
  "FINDEX_INFO_LEVELS": [
    "int"
  ], 
  "KEY_EVENT_RECORD": [
    "KEY_EVENT_RECORD"
  ], 
  "AXISINFO*": [
    "AXISINFO", 
    "AXISINFO[]"
  ], 
  "LPNETCONNECTINFOSTRUCT": [
    "NETCONNECTINFOSTRUCT", 
    "NETCONNECTINFOSTRUCT[]"
  ], 
  "LPUSER_OTHER_INFO": [
    "USER_OTHER_INFO", 
    "USER_OTHER_INFO[]"
  ], 
  "EVENT_TRACE": [
    "EVENT_TRACE"
  ], 
  "LPCSHARE_INFO_501": [
    "SHARE_INFO_501", 
    "SHARE_INFO_501[]"
  ], 
  "PRAWKEYBOARD*": [
    "PointerByReference"
  ], 
  "LPDCB*": [
    "PointerByReference"
  ], 
  "PDIBSECTION": [
    "DIBSECTION", 
    "DIBSECTION[]"
  ], 
  "POSVERSIONINFOEX*": [
    "PointerByReference"
  ], 
  "OFSTRUCT*": [
    "OFSTRUCT", 
    "OFSTRUCT[]"
  ], 
  "LPMOUSEHOOKSTRUCT": [
    "MOUSEHOOKSTRUCT", 
    "MOUSEHOOKSTRUCT[]"
  ], 
  "WORD*": [
    "ShortByReference", 
    "short[]"
  ], 
  "PSC_ACTION_TYPE*": [
    "PointerByReference"
  ], 
  "PNONCLIENTMETRICS": [
    "NONCLIENTMETRICS", 
    "NONCLIENTMETRICS[]"
  ], 
  "LPCUSER_MODALS_INFO_1006": [
    "USER_MODALS_INFO_1006", 
    "USER_MODALS_INFO_1006[]"
  ], 
  "LPSERVER_TRANSPORT_INFO_1*": [
    "PointerByReference"
  ], 
  "LPULONG32": [
    "IntByReference", 
    "int[]"
  ], 
  "LPVALENT": [
    "VALENT", 
    "VALENT[]"
  ], 
  "PMINIMIZEDMETRICS": [
    "MINIMIZEDMETRICS", 
    "MINIMIZEDMETRICS[]"
  ], 
  "LPCOUTLINETEXTMETRIC": [
    "OUTLINETEXTMETRIC", 
    "OUTLINETEXTMETRIC[]"
  ], 
  "PHEAP_INFORMATION_CLASS": [
    "IntByReference", 
    "int[]"
  ], 
  "NONCLIENTMETRICS**": [
    "PointerByReference"
  ], 
  "LPPOINTL*": [
    "PointerByReference"
  ], 
  "LPCNET_DISPLAY_MACHINE": [
    "NET_DISPLAY_MACHINE", 
    "NET_DISPLAY_MACHINE[]"
  ], 
  "LPCWSAPROTOCOLCHAIN": [
    "WSAPROTOCOLCHAIN", 
    "WSAPROTOCOLCHAIN[]"
  ], 
  "LPCCHAR_INFO": [
    "CHAR_INFO", 
    "CHAR_INFO[]"
  ], 
  "PZZWSTR**": [
    "char"
  ], 
  "PMODULEENTRY32*": [
    "PointerByReference"
  ], 
  "LPCGROUP_USERS_INFO_0": [
    "GROUP_USERS_INFO_0", 
    "GROUP_USERS_INFO_0[]"
  ], 
  "PCONSOLE_FONT_INFOEX": [
    "CONSOLE_FONT_INFOEX", 
    "CONSOLE_FONT_INFOEX[]"
  ], 
  "LPDLGTEMPLATE*": [
    "PointerByReference"
  ], 
  "PGLOBAL_POWER_POLICY": [
    "GLOBAL_POWER_POLICY", 
    "GLOBAL_POWER_POLICY[]"
  ], 
  "LPADDJOB_INFO_1*": [
    "PointerByReference"
  ], 
  "SERVER_TRANSPORT_INFO_3": [
    "SERVER_TRANSPORT_INFO_3"
  ], 
  "LPUSER_MODALS_INFO_1007": [
    "USER_MODALS_INFO_1007", 
    "USER_MODALS_INFO_1007[]"
  ], 
  "CALID": [
    "int"
  ], 
  "PGEOID*": [
    "PointerByReference"
  ], 
  "LPWIN32_FIND_STREAM_DATA*": [
    "PointerByReference"
  ], 
  "LPUSER_MODALS_INFO_1003": [
    "USER_MODALS_INFO_1003", 
    "USER_MODALS_INFO_1003[]"
  ], 
  "PROCESSOR_POWER_POLICY*": [
    "PROCESSOR_POWER_POLICY", 
    "PROCESSOR_POWER_POLICY[]"
  ], 
  "SC_ACTION_TYPE**": [
    "PointerByReference"
  ], 
  "LPLPCGEOTYPE": [
    "IntByReference", 
    "long[]"
  ], 
  "HW_PROFILE_INFO**": [
    "PointerByReference"
  ], 
  "PRINTER_NOTIFY_OPTIONS_TYPE*": [
    "PRINTER_NOTIFY_OPTIONS_TYPE", 
    "PRINTER_NOTIFY_OPTIONS_TYPE[]"
  ], 
  "LPCONVCONTEXT*": [
    "PointerByReference"
  ], 
  "LPPTSTR": [
    "char"
  ], 
  "LPCGROUP_USERS_INFO_1": [
    "GROUP_USERS_INFO_1", 
    "GROUP_USERS_INFO_1[]"
  ], 
  "LPCFILTERKEYS": [
    "FILTERKEYS", 
    "FILTERKEYS[]"
  ], 
  "PEVENTMSG*": [
    "PointerByReference"
  ], 
  "UCHAR*": [
    "byte"
  ], 
  "LPUSER_MODALS_INFO_1004": [
    "USER_MODALS_INFO_1004", 
    "USER_MODALS_INFO_1004[]"
  ], 
  "LPLONG_PTR": [
    "IntByReference", 
    "long[]"
  ], 
  "DFS_INFO_5**": [
    "PointerByReference"
  ], 
  "RM_PROCESS_INFO": [
    "RM_PROCESS_INFO"
  ], 
  "USER_MODALS_INFO_1*": [
    "USER_MODALS_INFO_1", 
    "USER_MODALS_INFO_1[]"
  ], 
  "PPROPERTYKEY*": [
    "PointerByReference"
  ], 
  "PGRADIENT_RECT*": [
    "PointerByReference"
  ], 
  "LPSESSION_INFO_10*": [
    "PointerByReference"
  ], 
  "PROVIDOR_INFO_2": [
    "PROVIDOR_INFO_2"
  ], 
  "LPCJOBOBJECT_BASIC_AND_IO_ACCOUNTING_INFORMATION": [
    "JOBOBJECT_BASIC_AND_IO_ACCOUNTING_INFORMATION", 
    "JOBOBJECT_BASIC_AND_IO_ACCOUNTING_INFORMATION[]"
  ], 
  "STD_ALERT": [
    "STD_ALERT"
  ], 
  "TpcGetSamplesArgs": [
    "TpcGetSamplesArgs"
  ], 
  "LPCPROCESS_MEMORY_COUNTERS": [
    "PROCESS_MEMORY_COUNTERS", 
    "PROCESS_MEMORY_COUNTERS[]"
  ], 
  "PDYNAMIC_TIME_ZONE_INFORMATION*": [
    "PointerByReference"
  ], 
  "LPUSER_MODALS_INFO_0": [
    "USER_MODALS_INFO_0", 
    "USER_MODALS_INFO_0[]"
  ], 
  "LPCEVENTLOGRECORD": [
    "EVENTLOGRECORD", 
    "EVENTLOGRECORD[]"
  ], 
  "CBT_CREATEWND": [
    "CBT_CREATEWND"
  ], 
  "LPUSER_MODALS_INFO_1": [
    "USER_MODALS_INFO_1", 
    "USER_MODALS_INFO_1[]"
  ], 
  "LPCPERFORMANCE_INFORMATION": [
    "PERFORMANCE_INFORMATION", 
    "PERFORMANCE_INFORMATION[]"
  ], 
  "TCHAR*": [
    "char"
  ], 
  "LPUSER_MODALS_INFO_2": [
    "USER_MODALS_INFO_2", 
    "USER_MODALS_INFO_2[]"
  ], 
  "RAWINPUTHEADER*": [
    "RAWINPUTHEADER", 
    "RAWINPUTHEADER[]"
  ], 
  "SERVER_TRANSPORT_INFO_1": [
    "SERVER_TRANSPORT_INFO_1"
  ], 
  "LPSERVER_TRANSPORT_INFO_2": [
    "SERVER_TRANSPORT_INFO_2", 
    "SERVER_TRANSPORT_INFO_2[]"
  ], 
  "LPSERVER_TRANSPORT_INFO_3": [
    "SERVER_TRANSPORT_INFO_3", 
    "SERVER_TRANSPORT_INFO_3[]"
  ], 
  "PSESSION_INFO_2": [
    "SESSION_INFO_2", 
    "SESSION_INFO_2[]"
  ], 
  "PROVIDOR_INFO_1": [
    "PROVIDOR_INFO_1"
  ], 
  "PLATENCY_TIME*": [
    "PointerByReference"
  ], 
  "SERVER_TRANSPORT_INFO_2": [
    "SERVER_TRANSPORT_INFO_2"
  ], 
  "JOBOBJECT_BASIC_PROCESS_ID_LIST**": [
    "PointerByReference"
  ], 
  "LPCALTYPE*": [
    "PointerByReference"
  ], 
  "BOOL**": [
    "PointerByReference"
  ], 
  "LPTCHAR": [
    "char"
  ], 
  "float**": [
    "PointerByReference"
  ], 
  "LPLOCALGROUP_INFO_0*": [
    "PointerByReference"
  ], 
  "DFS_INFO_100**": [
    "PointerByReference"
  ], 
  "LPPRINTER_INFO_5": [
    "PRINTER_INFO_5", 
    "PRINTER_INFO_5[]"
  ], 
  "PLOCALGROUP_INFO_0*": [
    "PointerByReference"
  ], 
  "LPNDDESHAREINFO*": [
    "PointerByReference"
  ], 
  "LPLOCALGROUP_INFO_1002*": [
    "PointerByReference"
  ], 
  "PLPGEOCLASS**": [
    "PointerByReference"
  ], 
  "LPUSER_INFO_22": [
    "USER_INFO_22", 
    "USER_INFO_22[]"
  ], 
  "LPUSER_INFO_21": [
    "USER_INFO_21", 
    "USER_INFO_21[]"
  ], 
  "LPUSER_INFO_20": [
    "USER_INFO_20", 
    "USER_INFO_20[]"
  ], 
  "PSHORT": [
    "ShortByReference", 
    "short[]"
  ], 
  "LPCMENUEX_TEMPLATE_HEADER": [
    "MENUEX_TEMPLATE_HEADER", 
    "MENUEX_TEMPLATE_HEADER[]"
  ], 
  "USE_INFO_1**": [
    "PointerByReference"
  ], 
  "PINT64*": [
    "PointerByReference"
  ], 
  "PSYSTEM_POWER_POLICY*": [
    "PointerByReference"
  ], 
  "PUSER_INFO_1017": [
    "USER_INFO_1017", 
    "USER_INFO_1017[]"
  ], 
  "PPOWERBROADCAST_SETTING*": [
    "PointerByReference"
  ], 
  "LPENUMTEXTMETRIC": [
    "ENUMTEXTMETRIC", 
    "ENUMTEXTMETRIC[]"
  ], 
  "PSCROLLINFO*": [
    "PointerByReference"
  ], 
  "WKSTA_USER_INFO_1101**": [
    "PointerByReference"
  ], 
  "LPWIN32_STREAM_ID*": [
    "PointerByReference"
  ], 
  "DFS_INFO_3**": [
    "PointerByReference"
  ], 
  "PDFS_INFO_105*": [
    "PointerByReference"
  ], 
  "TAPE_GET_DRIVE_PARAMETERS": [
    "TAPE_GET_DRIVE_PARAMETERS"
  ], 
  "LPDOCINFO*": [
    "PointerByReference"
  ], 
  "LPPDH_TIME_INFO": [
    "PDH_TIME_INFO", 
    "PDH_TIME_INFO[]"
  ], 
  "PUINT16*": [
    "PointerByReference"
  ], 
  "PSERVER_TRANSPORT_INFO_0": [
    "SERVER_TRANSPORT_INFO_0", 
    "SERVER_TRANSPORT_INFO_0[]"
  ], 
  "RASTERIZER_STATUS**": [
    "PointerByReference"
  ], 
  "LPATOM*": [
    "PointerByReference"
  ], 
  "USER_MODALS_INFO_1003": [
    "USER_MODALS_INFO_1003"
  ], 
  "LPSC_ACTION_TYPE*": [
    "PointerByReference"
  ], 
  "PEXECUTION_STATE*": [
    "PointerByReference"
  ], 
  "FONTSIGNATURE**": [
    "PointerByReference"
  ], 
  "LPSOUNDSENTRY*": [
    "PointerByReference"
  ], 
  "LPULONG64*": [
    "PointerByReference"
  ], 
  "LPBY_HANDLE_FILE_INFORMATION*": [
    "PointerByReference"
  ], 
  "LPEMRALPHABLEND*": [
    "PointerByReference"
  ], 
  "ICONINFO": [
    "ICONINFO"
  ], 
  "LPCJOBOBJECT_RATE_CONTROL_TOLERANCE": [
    "IntByReference", 
    "int[]"
  ], 
  "PMEMORY_BASIC_INFORMATION": [
    "MEMORY_BASIC_INFORMATION", 
    "MEMORY_BASIC_INFORMATION[]"
  ], 
  "LPPOLYTEXT": [
    "POLYTEXT", 
    "POLYTEXT[]"
  ], 
  "PDFS_STORAGE_INFO*": [
    "PointerByReference"
  ], 
  "COMBOBOXINFO**": [
    "PointerByReference"
  ], 
  "JOBOBJECT_CPU_RATE_CONTROL_INFORMATION**": [
    "PointerByReference"
  ], 
  "LPCFILE_NOTIFY_INFORMATION": [
    "FILE_NOTIFY_INFORMATION", 
    "FILE_NOTIFY_INFORMATION[]"
  ], 
  "SIZE_T*": [
    "IntByReference", 
    "long[]", 
    "ULONG_PTRByReference"
  ], 
  "FLOAT**": [
    "PointerByReference"
  ], 
  "PRINTER_INFO_3*": [
    "PRINTER_INFO_3", 
    "PRINTER_INFO_3[]"
  ], 
  "_EXCEPTION_POINTERS*": [
    "Pointer"
  ], 
  "LPCTRACE_GUID_PROPERTIES": [
    "TRACE_GUID_PROPERTIES", 
    "TRACE_GUID_PROPERTIES[]"
  ], 
  "LPDFS_INFO_107": [
    "DFS_INFO_107", 
    "DFS_INFO_107[]"
  ], 
  "LPHPALETTE": [
    "HANDLEByReference"
  ], 
  "PFOCUS_EVENT_RECORD": [
    "FOCUS_EVENT_RECORD", 
    "FOCUS_EVENT_RECORD[]"
  ], 
  "OFSTRUCT**": [
    "PointerByReference"
  ], 
  "PPZZWSTR": [
    "char"
  ], 
  "PTEXTMETRIC*": [
    "PointerByReference"
  ], 
  "PRID_DEVICE_INFO": [
    "RID_DEVICE_INFO", 
    "RID_DEVICE_INFO[]"
  ], 
  "GEOTYPE": [
    "int"
  ], 
  "NETRESOURCE": [
    "NETRESOURCE"
  ], 
  "SHELLEXECUTEINFO*": [
    "SHELLEXECUTEINFO", 
    "SHELLEXECUTEINFO[]"
  ], 
  "PSAPI_WS_WATCH_INFORMATION": [
    "PSAPI_WS_WATCH_INFORMATION"
  ], 
  "LPCDIBSECTION": [
    "DIBSECTION", 
    "DIBSECTION[]"
  ], 
  "CONSOLE_HISTORY_INFO*": [
    "CONSOLE_HISTORY_INFO", 
    "CONSOLE_HISTORY_INFO[]"
  ], 
  "LPAT_INFO": [
    "AT_INFO", 
    "AT_INFO[]"
  ], 
  "ACCESSTIMEOUT": [
    "ACCESSTIMEOUT"
  ], 
  "PCCHAR*": [
    "byte"
  ], 
  "LPGROUP_USERS_INFO_0": [
    "GROUP_USERS_INFO_0", 
    "GROUP_USERS_INFO_0[]"
  ], 
  "HCRYPTHASH": [
    "ULONG_PTR"
  ], 
  "WIN32_FIND_STREAM_DATA": [
    "WIN32_FIND_STREAM_DATA"
  ], 
  "LPSYSTEM_POWER_CAPABILITIES*": [
    "PointerByReference"
  ], 
  "LPDFS_INFO_100*": [
    "PointerByReference"
  ], 
  "MENUEX_TEMPLATE_ITEM": [
    "MENUEX_TEMPLATE_ITEM"
  ], 
  "COMPUTER_NAME_FORMAT": [
    "int"
  ], 
  "LPLONG32*": [
    "PointerByReference"
  ], 
  "PGEOCLASS*": [
    "IntByReference", 
    "long[]"
  ], 
  "PUSE_INFO_2": [
    "USE_INFO_2", 
    "USE_INFO_2[]"
  ], 
  "GEOTYPE***": [
    "IntByReference", 
    "long[]"
  ], 
  "PSYSTEM_POWER_CAPABILITIES": [
    "SYSTEM_POWER_CAPABILITIES", 
    "SYSTEM_POWER_CAPABILITIES[]"
  ], 
  "WER_DUMP_TYPE**": [
    "PointerByReference"
  ], 
  "DFS_INFO_101*": [
    "DFS_INFO_101", 
    "DFS_INFO_101[]"
  ], 
  "SSIZE_T": [
    "int"
  ], 
  "MEMORYSTATUSEX": [
    "MEMORYSTATUSEX"
  ], 
  "EXTENDED_NAME_FORMAT": [
    "EXTENDED_NAME_FORMAT"
  ], 
  "GROUP_INFO_3**": [
    "PointerByReference"
  ], 
  "LPCSTARTUPINFO": [
    "STARTUPINFO", 
    "STARTUPINFO[]"
  ], 
  "LPGEOCLASS": [
    "IntByReference", 
    "long[]"
  ], 
  "LPLPARAM": [
    "IntByReference", 
    "long[]"
  ], 
  "LPSERVER_INFO_101*": [
    "PointerByReference"
  ], 
  "ENHMETARECORD": [
    "ENHMETARECORD"
  ], 
  "VS_FIXEDFILEINFO*": [
    "VS_FIXEDFILEINFO", 
    "VS_FIXEDFILEINFO[]"
  ], 
  "LPSHFILEOPSTRUCT*": [
    "PointerByReference"
  ], 
  "PSHELLEXECUTEINFO*": [
    "PointerByReference"
  ], 
  "PPROVIDOR_INFO_2*": [
    "PointerByReference"
  ], 
  "PDH_STATISTICS*": [
    "PDH_STATISTICS", 
    "PDH_STATISTICS[]"
  ], 
  "POPEN_AS_INFO_FLAGS": [
    "IntByReference", 
    "int[]"
  ], 
  "LPDYNAMIC_TIME_ZONE_INFORMATION*": [
    "PointerByReference"
  ], 
  "USER_MODALS_INFO_1004**": [
    "PointerByReference"
  ], 
  "PWSTR**": [
    "char"
  ], 
  "PWSAPROTOCOL_INFO": [
    "WSAPROTOCOL_INFO", 
    "WSAPROTOCOL_INFO[]"
  ], 
  "DYNAMIC_TIME_ZONE_INFORMATION": [
    "DYNAMIC_TIME_ZONE_INFORMATION"
  ], 
  "LPSERVER_TRANSPORT_INFO_0": [
    "SERVER_TRANSPORT_INFO_0", 
    "SERVER_TRANSPORT_INFO_0[]"
  ], 
  "SHARE_INFO_1005": [
    "SHARE_INFO_1005"
  ], 
  "SHARE_INFO_1004": [
    "SHARE_INFO_1004"
  ], 
  "LPITEMIDLIST": [
    "ITEMIDLIST", 
    "ITEMIDLIST[]"
  ], 
  "LPSERVER_TRANSPORT_INFO_1": [
    "SERVER_TRANSPORT_INFO_1", 
    "SERVER_TRANSPORT_INFO_1[]"
  ], 
  "SC_STATUS_TYPE": [
    "int"
  ], 
  "CONSOLE_SELECTION_INFO": [
    "CONSOLE_SELECTION_INFO"
  ], 
  "LPCJOBOBJECT_NOTIFICATION_LIMIT_INFORMATION": [
    "JOBOBJECT_NOTIFICATION_LIMIT_INFORMATION", 
    "JOBOBJECT_NOTIFICATION_LIMIT_INFORMATION[]"
  ], 
  "LPDFS_TARGET_PRIORITY_CLASS": [
    "IntByReference", 
    "int[]"
  ], 
  "LPTHREADENTRY32": [
    "THREADENTRY32", 
    "THREADENTRY32[]"
  ], 
  "LPCPANOSE": [
    "PANOSE", 
    "PANOSE[]"
  ], 
  "SHDESCRIPTIONID**": [
    "PointerByReference"
  ], 
  "PNTMS_NOTIFICATIONINFORMATION*": [
    "PointerByReference"
  ], 
  "PSERVICE_DESCRIPTION": [
    "SERVICE_DESCRIPTION", 
    "SERVICE_DESCRIPTION[]"
  ], 
  "HACCEL*": [
    "HANDLEByReference"
  ], 
  "LPCSYSGEOTYPE": [
    "IntByReference", 
    "int[]"
  ], 
  "LPCGLOBAL_MACHINE_POWER_POLICY": [
    "GLOBAL_MACHINE_POWER_POLICY", 
    "GLOBAL_MACHINE_POWER_POLICY[]"
  ], 
  "JOBOBJECT_LIMIT_VIOLATION_INFORMATION**": [
    "PointerByReference"
  ], 
  "CONNECTION_INFO_1*": [
    "CONNECTION_INFO_1", 
    "CONNECTION_INFO_1[]"
  ], 
  "LPRID_DEVICE_INFO_MOUSE": [
    "RID_DEVICE_INFO_MOUSE", 
    "RID_DEVICE_INFO_MOUSE[]"
  ], 
  "TRACE_GUID_PROPERTIES**": [
    "PointerByReference"
  ], 
  "LPMONITORINFOEX": [
    "MONITORINFOEX", 
    "MONITORINFOEX[]"
  ], 
  "LPCSERVICE_REQUIRED_PRIVILEGES_INFO": [
    "SERVICE_REQUIRED_PRIVILEGES_INFO", 
    "SERVICE_REQUIRED_PRIVILEGES_INFO[]"
  ], 
  "LPCHW_PROFILE_INFO": [
    "HW_PROFILE_INFO", 
    "HW_PROFILE_INFO[]"
  ], 
  "PHDESK": [
    "HANDLEByReference"
  ], 
  "NEWTEXTMETRICEX*": [
    "NEWTEXTMETRICEX", 
    "NEWTEXTMETRICEX[]"
  ], 
  "FILE_INFO_2*": [
    "FILE_INFO_2", 
    "FILE_INFO_2[]"
  ], 
  "TRACE_GUID_REGISTRATION": [
    "TRACE_GUID_REGISTRATION"
  ], 
  "ULONG*": [
    "IntByReference", 
    "long[]"
  ], 
  "WSAPROTOCOLCHAIN": [
    "WSAPROTOCOLCHAIN"
  ], 
  "PRAWINPUT": [
    "RAWINPUT", 
    "RAWINPUT[]"
  ], 
  "PUNIVERSAL_NAME_INFO": [
    "UNIVERSAL_NAME_INFO", 
    "UNIVERSAL_NAME_INFO[]"
  ], 
  "FILETIME": [
    "FILETIME"
  ], 
  "PNETCONNECTINFOSTRUCT*": [
    "PointerByReference"
  ], 
  "LPCFILETIME": [
    "FILETIME", 
    "FILETIME[]"
  ], 
  "TOKEN_GROUPS*": [
    "TOKEN_GROUPS", 
    "TOKEN_GROUPS[]"
  ], 
  "GPFIDL_FLAGS": [
    "int"
  ], 
  "ABC": [
    "ABC"
  ], 
  "LPUSN*": [
    "PointerByReference"
  ], 
  "COLORREF**": [
    "PointerByReference"
  ], 
  "PIO_COUNTERS": [
    "IO_COUNTERS", 
    "IO_COUNTERS[]"
  ], 
  "LPGET_FILEEX_INFO_LEVELS*": [
    "PointerByReference"
  ], 
  "PRAWINPUTDEVICELIST*": [
    "PointerByReference"
  ], 
  "EVENTMSG*": [
    "EVENTMSG", 
    "EVENTMSG[]"
  ], 
  "ENUM_SERVICE_STATUS_PROCESS*": [
    "ENUM_SERVICE_STATUS_PROCESS", 
    "ENUM_SERVICE_STATUS_PROCESS[]"
  ], 
  "PGROUP_INFO_1002": [
    "GROUP_INFO_1002", 
    "GROUP_INFO_1002[]"
  ], 
  "STD_ALERT*": [
    "STD_ALERT", 
    "STD_ALERT[]"
  ], 
  "PJOBOBJECT_BASIC_LIMIT_INFORMATION": [
    "JOBOBJECT_BASIC_LIMIT_INFORMATION", 
    "JOBOBJECT_BASIC_LIMIT_INFORMATION[]"
  ], 
  "LPOVERLAPPED_COMPLETION_ROUTINE": [
    "WinNT.OVERLAPPED_COMPLETION_ROUTINE"
  ], 
  "PSYSTEM_BATTERY_STATE*": [
    "PointerByReference"
  ], 
  "PPRINTER_DEFAULTS*": [
    "PointerByReference"
  ], 
  "PUSER_MODALS_INFO_1003*": [
    "PointerByReference"
  ], 
  "LPSHQUERYRBINFO": [
    "SHQUERYRBINFO", 
    "SHQUERYRBINFO[]"
  ], 
  "PRINTER_INFO_6": [
    "PRINTER_INFO_6"
  ], 
  "NETINFOSTRUCT*": [
    "NETINFOSTRUCT", 
    "NETINFOSTRUCT[]"
  ], 
  "NET_DISPLAY_GROUP": [
    "NET_DISPLAY_GROUP"
  ], 
  "LPMODULEENTRY32*": [
    "PointerByReference"
  ], 
  "QUERY_SERVICE_CONFIG**": [
    "PointerByReference"
  ], 
  "MACHINE_POWER_POLICY*": [
    "MACHINE_POWER_POLICY", 
    "MACHINE_POWER_POLICY[]"
  ], 
  "GEOID*": [
    "IntByReference", 
    "long[]"
  ], 
  "LPUSER_MODALS_INFO_1006": [
    "USER_MODALS_INFO_1006", 
    "USER_MODALS_INFO_1006[]"
  ], 
  "LPCWINDOW_BUFFER_SIZE_RECORD": [
    "WINDOW_BUFFER_SIZE_RECORD", 
    "WINDOW_BUFFER_SIZE_RECORD[]"
  ], 
  "LPSYSTEM_LOGICAL_PROCESSOR_INFORMATION*": [
    "PointerByReference"
  ], 
  "LPCSMALL_RECT": [
    "SMALL_RECT", 
    "SMALL_RECT[]"
  ], 
  "LPGLOBAL_MACHINE_POWER_POLICY": [
    "GLOBAL_MACHINE_POWER_POLICY", 
    "GLOBAL_MACHINE_POWER_POLICY[]"
  ], 
  "PSTICKYKEYS*": [
    "PointerByReference"
  ], 
  "MediaLabelInfo": [
    "MediaLabelInfo"
  ], 
  "LPPRINTER_INFO_4": [
    "PRINTER_INFO_4", 
    "PRINTER_INFO_4[]"
  ], 
  "LPUSER_INFO_24": [
    "USER_INFO_24", 
    "USER_INFO_24[]"
  ], 
  "WKSTA_USER_INFO_1*": [
    "WKSTA_USER_INFO_1", 
    "WKSTA_USER_INFO_1[]"
  ], 
  "LPSHORT": [
    "ShortByReference", 
    "short[]"
  ], 
  "LPCENHMETARECORD": [
    "ENHMETARECORD", 
    "ENHMETARECORD[]"
  ], 
  "PUSER_INFO_24*": [
    "PointerByReference"
  ], 
  "LPUSER_INFO_23": [
    "USER_INFO_23", 
    "USER_INFO_23[]"
  ], 
  "WKSTA_INFO_100": [
    "WKSTA_INFO_100"
  ], 
  "NET_DISPLAY_USER": [
    "NET_DISPLAY_USER"
  ], 
  "PUSER_MODALS_INFO_0*": [
    "PointerByReference"
  ], 
  "LPCWCRANGE": [
    "WCRANGE", 
    "WCRANGE[]"
  ], 
  "LPCNUMBERFMT": [
    "NUMBERFMT", 
    "NUMBERFMT[]"
  ], 
  "COMMPROP**": [
    "PointerByReference"
  ], 
  "PDFS_INFO_6*": [
    "PointerByReference"
  ], 
  "LPCSERVICE_CONTROL_STATUS_REASON_PARAMS": [
    "SERVICE_CONTROL_STATUS_REASON_PARAMS", 
    "SERVICE_CONTROL_STATUS_REASON_PARAMS[]"
  ], 
  "WER_REPORT_UI": [
    "int"
  ], 
  "PJOBOBJECT_RATE_CONTROL_TOLERANCE_INTERVAL": [
    "IntByReference", 
    "int[]"
  ], 
  "LPFLASHWINFO*": [
    "PointerByReference"
  ], 
  "LPTpcGetSamplesArgs*": [
    "PointerByReference"
  ], 
  "LPCACL": [
    "ACL", 
    "ACL[]"
  ], 
  "LPLOGFONT": [
    "LOGFONT", 
    "LOGFONT[]"
  ], 
  "PSERVICE_REQUIRED_PRIVILEGES_INFO*": [
    "PointerByReference"
  ], 
  "ULONG_PTR**": [
    "PointerByReference"
  ], 
  "INT32*": [
    "IntByReference", 
    "int[]"
  ], 
  "TOKEN_INFORMATION_CLASS": [
    "int"
  ], 
  "LPCPIXELFORMATDESCRIPTOR": [
    "PIXELFORMATDESCRIPTOR", 
    "PIXELFORMATDESCRIPTOR[]"
  ], 
  "PEVENT_INSTANCE_INFO*": [
    "PointerByReference"
  ], 
  "SERVICE_REQUIRED_PRIVILEGES_INFO": [
    "SERVICE_REQUIRED_PRIVILEGES_INFO"
  ], 
  "ULONG": [
    "int"
  ], 
  "RAWINPUT**": [
    "PointerByReference"
  ], 
  "WCT_OBJECT_TYPE**": [
    "PointerByReference"
  ], 
  "LPHDDEDATA": [
    "HANDLEByReference"
  ], 
  "PPDH_DATA_ITEM_PATH_ELEMENTS*": [
    "PointerByReference"
  ], 
  "LPCAT_INFO": [
    "AT_INFO", 
    "AT_INFO[]"
  ], 
  "DWORD64**": [
    "PointerByReference"
  ], 
  "PHPALETTE": [
    "HANDLEByReference"
  ], 
  "SHSTOCKICONID": [
    "int"
  ], 
  "USER_INFO_1017*": [
    "USER_INFO_1017", 
    "USER_INFO_1017[]"
  ], 
  "LPUSER_INFO_2*": [
    "PointerByReference"
  ], 
  "HMONITOR": [
    "WinUser.HMONITOR"
  ], 
  "LPCSHELLEXECUTEINFO": [
    "SHELLEXECUTEINFO", 
    "SHELLEXECUTEINFO[]"
  ], 
  "PWER_REGISTER_FILE_TYPE": [
    "IntByReference", 
    "int[]"
  ], 
  "TIME_ZONE_INFORMATION": [
    "TIME_ZONE_INFORMATION"
  ], 
  "size_t*": [
    "IntByReference", 
    "long[]"
  ], 
  "LPSERVER_INFO_101": [
    "SERVER_INFO_101", 
    "SERVER_INFO_101[]"
  ], 
  "LPSERVER_INFO_100": [
    "SERVER_INFO_100", 
    "SERVER_INFO_100[]"
  ], 
  "LPSERVER_INFO_102": [
    "SERVER_INFO_102", 
    "SERVER_INFO_102[]"
  ], 
  "HEAPLIST32**": [
    "PointerByReference"
  ], 
  "LPCJOBOBJECT_SECURITY_LIMIT_INFORMATION": [
    "JOBOBJECT_SECURITY_LIMIT_INFORMATION", 
    "JOBOBJECT_SECURITY_LIMIT_INFORMATION[]"
  ], 
  "PSYSTEM_POWER_STATE*": [
    "PointerByReference"
  ], 
  "CHAR": [
    "byte"
  ], 
  "ETW_BUFFER_CONTEXT*": [
    "ETW_BUFFER_CONTEXT", 
    "ETW_BUFFER_CONTEXT[]"
  ], 
  "PRECT*": [
    "PointerByReference"
  ], 
  "SHFILEINFO**": [
    "PointerByReference"
  ], 
  "APPBARDATA**": [
    "PointerByReference"
  ], 
  "ACCESS_MASK*": [
    "IntByReference", 
    "long[]"
  ], 
  "RAWINPUT": [
    "RAWINPUT"
  ], 
  "LPUSEROBJECTFLAGS*": [
    "PointerByReference"
  ], 
  "LPWKSTA_USER_INFO_1101": [
    "WKSTA_USER_INFO_1101", 
    "WKSTA_USER_INFO_1101[]"
  ], 
  "SECURITY_QUALITY_OF_SERVICE*": [
    "SECURITY_QUALITY_OF_SERVICE", 
    "SECURITY_QUALITY_OF_SERVICE[]"
  ], 
  "GET_FILEEX_INFO_LEVELS": [
    "int"
  ], 
  "EVENTLOG_FULL_INFORMATION**": [
    "PointerByReference"
  ], 
  "short*": [
    "ShortByReference", 
    "short[]"
  ], 
  "PPZZWSTR*": [
    "char"
  ], 
  "SERVICE_TRIGGER_SPECIFIC_DATA_ITEM**": [
    "PointerByReference"
  ], 
  "PDWORD": [
    "IntByReference", 
    "long[]"
  ], 
  "WORD": [
    "short", 
    "int"
  ], 
  "KAFFINITY*": [
    "IntByReference", 
    "long[]"
  ], 
  "LPCGEOTYPE**": [
    "PointerByReference"
  ], 
  "LPCTSTR": [
    "String", 
    "WString", 
    "char[]"
  ], 
  "HKEY*": [
    "HANDLEByReference"
  ], 
  "USER_INFO_1008*": [
    "USER_INFO_1008", 
    "USER_INFO_1008[]"
  ], 
  "LPNONCLIENTMETRICS*": [
    "PointerByReference"
  ], 
  "PJOBOBJECT_CPU_RATE_CONTROL_INFORMATION*": [
    "PointerByReference"
  ], 
  "GLOBAL_USER_POWER_POLICY**": [
    "PointerByReference"
  ], 
  "LPCJOBOBJECT_BASIC_PROCESS_ID_LIST": [
    "JOBOBJECT_BASIC_PROCESS_ID_LIST", 
    "JOBOBJECT_BASIC_PROCESS_ID_LIST[]"
  ], 
  "LPCTRACE_GUID_REGISTRATION": [
    "TRACE_GUID_REGISTRATION", 
    "TRACE_GUID_REGISTRATION[]"
  ], 
  "PUINT64": [
    "LongByReference", 
    "__int64[]"
  ], 
  "DLGITEMTEMPLATE*": [
    "DLGITEMTEMPLATE", 
    "DLGITEMTEMPLATE[]"
  ], 
  "PWNODE_HEADER*": [
    "PointerByReference"
  ], 
  "PPROCESSENTRY32*": [
    "PointerByReference"
  ], 
  "OPENASINFO*": [
    "OPENASINFO", 
    "OPENASINFO[]"
  ], 
  "LPWCT_OBJECT_TYPE": [
    "IntByReference", 
    "int[]"
  ], 
  "FILE_INFO_BY_HANDLE_CLASS*": [
    "IntByReference", 
    "int[]"
  ], 
  "LPCNEWTEXTMETRICEX": [
    "NEWTEXTMETRICEX", 
    "NEWTEXTMETRICEX[]"
  ], 
  "MENUEX_TEMPLATE_ITEM*": [
    "MENUEX_TEMPLATE_ITEM", 
    "MENUEX_TEMPLATE_ITEM[]"
  ], 
  "PGLOBAL_USER_POWER_POLICY": [
    "GLOBAL_USER_POWER_POLICY", 
    "GLOBAL_USER_POWER_POLICY[]"
  ], 
  "JOBOBJECT_NOTIFICATION_LIMIT_INFORMATION**": [
    "PointerByReference"
  ], 
  "LPCLIENTCREATESTRUCT": [
    "CLIENTCREATESTRUCT", 
    "CLIENTCREATESTRUCT[]"
  ], 
  "COMMCONFIG**": [
    "PointerByReference"
  ], 
  "HACCEL": [
    "HANDLE"
  ], 
  "EVENT_TRACE_PROPERTIES": [
    "EVENT_TRACE_PROPERTIES"
  ], 
  "LPOPENASINFO": [
    "OPENASINFO", 
    "OPENASINFO[]"
  ], 
  "NET_DISPLAY_GROUP*": [
    "NET_DISPLAY_GROUP", 
    "NET_DISPLAY_GROUP[]"
  ], 
  "DOCINFO*": [
    "DOCINFO", 
    "DOCINFO[]"
  ], 
  "PJOBOBJECT_RATE_CONTROL_TOLERANCE": [
    "IntByReference", 
    "int[]"
  ], 
  "USER_INFO_1020**": [
    "PointerByReference"
  ], 
  "PENUMTEXTMETRIC": [
    "ENUMTEXTMETRIC", 
    "ENUMTEXTMETRIC[]"
  ], 
  "LONG**": [
    "PointerByReference"
  ], 
  "USER_INFO_21": [
    "USER_INFO_21"
  ], 
  "LPCDESIGNVECTOR": [
    "DESIGNVECTOR", 
    "DESIGNVECTOR[]"
  ], 
  "LPCEVENT_TRACE_PROPERTIES": [
    "EVENT_TRACE_PROPERTIES", 
    "EVENT_TRACE_PROPERTIES[]"
  ], 
  "USER_INFO_1009**": [
    "PointerByReference"
  ], 
  "LPABC": [
    "ABC", 
    "ABC[]"
  ], 
  "PPCSTR*": [
    "byte"
  ], 
  "PJOBOBJECT_NOTIFICATION_LIMIT_INFORMATION*": [
    "PointerByReference"
  ], 
  "DIBSECTION": [
    "DIBSECTION"
  ], 
  "LPCEVENT_INSTANCE_INFO": [
    "EVENT_INSTANCE_INFO", 
    "EVENT_INSTANCE_INFO[]"
  ], 
  "USER_INFO_1**": [
    "PointerByReference"
  ], 
  "POINT": [
    "POINT"
  ], 
  "GENERIC_MAPPING*": [
    "GENERIC_MAPPING", 
    "GENERIC_MAPPING[]"
  ], 
  "USER_INFO_22": [
    "USER_INFO_22"
  ], 
  "GLYPHSET**": [
    "PointerByReference"
  ], 
  "PWER_REPORT_UI": [
    "IntByReference", 
    "int[]"
  ], 
  "FILE_INFO_3*": [
    "FILE_INFO_3", 
    "FILE_INFO_3[]"
  ], 
  "LPWER_DUMP_TYPE*": [
    "PointerByReference"
  ], 
  "JOB_INFO_3": [
    "JOB_INFO_3"
  ], 
  "PFORMATETC": [
    "FORMATETC", 
    "FORMATETC[]"
  ], 
  "KAFFINITY**": [
    "PointerByReference"
  ], 
  "LPsize_t*": [
    "PointerByReference"
  ], 
  "PRECTL": [
    "RECTL", 
    "RECTL[]"
  ], 
  "LPLONGLONG": [
    "DoubleByReference", 
    "double[]"
  ], 
  "LPCDEVMODE": [
    "DEVMODE", 
    "DEVMODE[]"
  ], 
  "LPWIN32_FIND_DATA*": [
    "PointerByReference"
  ], 
  "PSOUNDSENTRY": [
    "SOUNDSENTRY", 
    "SOUNDSENTRY[]"
  ], 
  "PDWORD_PTR": [
    "IntByReference", 
    "long[]"
  ], 
  "HBRUSH": [
    "HANDLE"
  ], 
  "LPLOGPALETTE": [
    "LOGPALETTE", 
    "LOGPALETTE[]"
  ], 
  "PEVENT_TRACE_HEADER": [
    "EVENT_TRACE_HEADER", 
    "EVENT_TRACE_HEADER[]"
  ], 
  "LPWPARAM": [
    "IntByReference", 
    "int[]"
  ], 
  "LPCPRINTPROCESSOR_INFO_1": [
    "PRINTPROCESSOR_INFO_1", 
    "PRINTPROCESSOR_INFO_1[]"
  ], 
  "LPSHARE_INFO_1005": [
    "SHARE_INFO_1005", 
    "SHARE_INFO_1005[]"
  ], 
  "LPSHARE_INFO_1004": [
    "SHARE_INFO_1004", 
    "SHARE_INFO_1004[]"
  ], 
  "LPSHARE_INFO_1006": [
    "SHARE_INFO_1006", 
    "SHARE_INFO_1006[]"
  ], 
  "PRM_UNIQUE_PROCESS*": [
    "PointerByReference"
  ], 
  "LPCTEXTMETRIC": [
    "TEXTMETRIC", 
    "TEXTMETRIC[]"
  ], 
  "WCRANGE*": [
    "WCRANGE", 
    "WCRANGE[]"
  ], 
  "LPFINDEX_INFO_LEVELS": [
    "IntByReference", 
    "int[]"
  ], 
  "PHFONT": [
    "HANDLEByReference"
  ], 
  "CPINFO": [
    "CPINFO"
  ], 
  "PMACHINE_POWER_POLICY": [
    "MACHINE_POWER_POLICY", 
    "MACHINE_POWER_POLICY[]"
  ], 
  "PSTAT_SERVER_0*": [
    "PointerByReference"
  ], 
  "OPEN_AS_INFO_FLAGS**": [
    "PointerByReference"
  ], 
  "TAPE_GET_DRIVE_PARAMETERS*": [
    "TAPE_GET_DRIVE_PARAMETERS", 
    "TAPE_GET_DRIVE_PARAMETERS[]"
  ], 
  "LPSERVER_TRANSPORT_INFO_0*": [
    "PointerByReference"
  ], 
  "FORM_INFO_1": [
    "FORM_INFO_1"
  ], 
  "WKSTA_INFO_100**": [
    "PointerByReference"
  ], 
  "TOKEN_GROUPS**": [
    "PointerByReference"
  ], 
  "LPCOLORREF": [
    "IntByReference", 
    "long[]"
  ], 
  "PSHARE_INFO_2": [
    "SHARE_INFO_2", 
    "SHARE_INFO_2[]"
  ], 
  "LPWSTR**": [
    "char"
  ], 
  "PHKL": [
    "HANDLEByReference"
  ], 
  "PPANOSE": [
    "PANOSE", 
    "PANOSE[]"
  ], 
  "PUMS_CREATE_THREAD_ATTRIBUTES*": [
    "PointerByReference"
  ], 
  "LPDFS_INFO_2": [
    "DFS_INFO_2", 
    "DFS_INFO_2[]"
  ], 
  "SESSION_INFO_2**": [
    "PointerByReference"
  ], 
  "LPUSER_INFO_1052*": [
    "PointerByReference"
  ], 
  "GET_FILEEX_INFO_LEVELS**": [
    "PointerByReference"
  ], 
  "LPCPDH_COUNTER_PATH_ELEMENTS": [
    "PDH_COUNTER_PATH_ELEMENTS", 
    "PDH_COUNTER_PATH_ELEMENTS[]"
  ], 
  "PHFILE*": [
    "PointerByReference"
  ], 
  "PSESSION_INFO_0*": [
    "PointerByReference"
  ], 
  "LPTRACKMOUSEEVENT": [
    "TRACKMOUSEEVENT", 
    "TRACKMOUSEEVENT[]"
  ], 
  "LPJOB_INFO_3": [
    "JOB_INFO_3", 
    "JOB_INFO_3[]"
  ], 
  "CONSOLE_CURSOR_INFO*": [
    "CONSOLE_CURSOR_INFO", 
    "CONSOLE_CURSOR_INFO[]"
  ], 
  "MSG*": [
    "MSG", 
    "MSG[]"
  ], 
  "LPPORT_INFO_3*": [
    "PointerByReference"
  ], 
  "LPCUSE_INFO_1": [
    "USE_INFO_1", 
    "USE_INFO_1[]"
  ], 
  "GLOBAL_POWER_POLICY": [
    "GLOBAL_POWER_POLICY"
  ], 
  "LPEVENTLOGRECORD*": [
    "PointerByReference"
  ], 
  "LPCPOINTL": [
    "POINTL", 
    "POINTL[]"
  ], 
  "PRGBQUAD*": [
    "PointerByReference"
  ], 
  "LPCSYSTEM_BATTERY_STATE": [
    "SYSTEM_BATTERY_STATE", 
    "SYSTEM_BATTERY_STATE[]"
  ], 
  "COMSTAT*": [
    "COMSTAT", 
    "COMSTAT[]"
  ], 
  "FORM_INFO_2": [
    "FORM_INFO_2"
  ], 
  "SHITEMID": [
    "SHITEMID"
  ], 
  "SHFILEOPSTRUCT": [
    "SHFILEOPSTRUCT"
  ], 
  "PMONITOR_INFO_2": [
    "MONITOR_INFO_2", 
    "MONITOR_INFO_2[]"
  ], 
  "PUSHORT*": [
    "PointerByReference"
  ], 
  "PTASKDIALOG_COMMON_BUTTON_FLAGS*": [
    "PointerByReference"
  ], 
  "MONITORINFOEX*": [
    "MONITORINFOEX", 
    "MONITORINFOEX[]"
  ], 
  "PPCWSTR": [
    "char"
  ], 
  "DWORDLONG**": [
    "PointerByReference"
  ], 
  "PRM_FILTER_ACTION*": [
    "PointerByReference"
  ], 
  "PWMIDPREQUESTCODE*": [
    "PointerByReference"
  ], 
  "LPCONVCONTEXT": [
    "CONVCONTEXT", 
    "CONVCONTEXT[]"
  ], 
  "LPCPSAPI_WS_WATCH_INFORMATION": [
    "PSAPI_WS_WATCH_INFORMATION", 
    "PSAPI_WS_WATCH_INFORMATION[]"
  ], 
  "PENUM_SERVICE_STATUS_PROCESS": [
    "ENUM_SERVICE_STATUS_PROCESS", 
    "ENUM_SERVICE_STATUS_PROCESS[]"
  ], 
  "LPLPGEOCLASS": [
    "IntByReference", 
    "long[]"
  ], 
  "PUSER_INFO_1012*": [
    "PointerByReference"
  ], 
  "LPDLGITEMTEMPLATE": [
    "DLGITEMTEMPLATE", 
    "DLGITEMTEMPLATE[]"
  ], 
  "PHRESULT*": [
    "PointerByReference"
  ], 
  "LPJOBOBJECT_SECURITY_LIMIT_INFORMATION": [
    "JOBOBJECT_SECURITY_LIMIT_INFORMATION", 
    "JOBOBJECT_SECURITY_LIMIT_INFORMATION[]"
  ], 
  "PDFS_INFO_8*": [
    "PointerByReference"
  ], 
  "PHRSRC": [
    "HANDLEByReference"
  ], 
  "FLOAT": [
    "float"
  ], 
  "LPCJOBOBJECT_END_OF_JOB_TIME_INFORMATION": [
    "JOBOBJECT_END_OF_JOB_TIME_INFORMATION", 
    "JOBOBJECT_END_OF_JOB_TIME_INFORMATION[]"
  ], 
  "PSERVER_INFO_403": [
    "SERVER_INFO_403", 
    "SERVER_INFO_403[]"
  ], 
  "MONITORINFOEX**": [
    "PointerByReference"
  ], 
  "PPRINTER_INFO_7*": [
    "PointerByReference"
  ], 
  "TOKEN_TYPE": [
    "int"
  ], 
  "PSYSTEM_POWER_STATE": [
    "IntByReference", 
    "int[]"
  ], 
  "LPKEY_EVENT_RECORD": [
    "KEY_EVENT_RECORD", 
    "KEY_EVENT_RECORD[]"
  ], 
  "PDEVMODE*": [
    "PointerByReference"
  ], 
  "USER_INFO_24*": [
    "USER_INFO_24", 
    "USER_INFO_24[]"
  ], 
  "LPCPROCESSOR_POWER_POLICY": [
    "PROCESSOR_POWER_POLICY", 
    "PROCESSOR_POWER_POLICY[]"
  ], 
  "LDT_ENTRY*": [
    "LDT_ENTRY", 
    "LDT_ENTRY[]"
  ], 
  "LPCSHARE_INFO_1006": [
    "SHARE_INFO_1006", 
    "SHARE_INFO_1006[]"
  ], 
  "ANIMATIONINFO*": [
    "ANIMATIONINFO", 
    "ANIMATIONINFO[]"
  ], 
  "LPCSHARE_INFO_1004": [
    "SHARE_INFO_1004", 
    "SHARE_INFO_1004[]"
  ], 
  "PRINTER_INFO_2**": [
    "PointerByReference"
  ], 
  "HICON*": [
    "HANDLEByReference"
  ], 
  "PUSN*": [
    "PointerByReference"
  ], 
  "LPCSCROLLBARINFO": [
    "SCROLLBARINFO", 
    "SCROLLBARINFO[]"
  ], 
  "LPLOCALGROUP_INFO_1*": [
    "PointerByReference"
  ], 
  "PNUMBERFMT": [
    "NUMBERFMT", 
    "NUMBERFMT[]"
  ], 
  "EVENTLOGRECORD": [
    "EVENTLOGRECORD"
  ], 
  "LPENUMLOGFONTEX": [
    "ENUMLOGFONTEX", 
    "ENUMLOGFONTEX[]"
  ], 
  "LPRID_DEVICE_INFO_KEYBOARD": [
    "RID_DEVICE_INFO_KEYBOARD", 
    "RID_DEVICE_INFO_KEYBOARD[]"
  ], 
  "PHIGHCONTRAST*": [
    "PointerByReference"
  ], 
  "LPOSVERSIONINFOEX*": [
    "PointerByReference"
  ], 
  "LPSERVICE_REQUIRED_PRIVILEGES_INFO": [
    "SERVICE_REQUIRED_PRIVILEGES_INFO", 
    "SERVICE_REQUIRED_PRIVILEGES_INFO[]"
  ], 
  "SERVICE_TRIGGER*": [
    "SERVICE_TRIGGER", 
    "SERVICE_TRIGGER[]"
  ], 
  "PNEWTEXTMETRICEX*": [
    "PointerByReference"
  ], 
  "ASSOC_FILTER*": [
    "IntByReference", 
    "int[]"
  ], 
  "PANOSE": [
    "PANOSE"
  ], 
  "WCT_OBJECT_TYPE*": [
    "IntByReference", 
    "int[]"
  ], 
  "PDEBUGHOOKINFO*": [
    "PointerByReference"
  ], 
  "NTMS_NOTIFICATIONINFORMATION": [
    "NTMS_NOTIFICATIONINFORMATION"
  ], 
  "LPDFS_TARGET_PRIORITY_CLASS*": [
    "PointerByReference"
  ], 
  "LPJOBOBJECT_BASIC_PROCESS_ID_LIST*": [
    "PointerByReference"
  ], 
  "PPOINT*": [
    "PointerByReference"
  ], 
  "LPLARGE_INTEGER*": [
    "PointerByReference"
  ], 
  "LPGLYPHSET": [
    "GLYPHSET", 
    "GLYPHSET[]"
  ], 
  "PPRINTER_INFO_5": [
    "PRINTER_INFO_5", 
    "PRINTER_INFO_5[]"
  ], 
  "PPRINTER_INFO_4": [
    "PRINTER_INFO_4", 
    "PRINTER_INFO_4[]"
  ], 
  "CHAR_INFO": [
    "CHAR_INFO"
  ], 
  "KBDLLHOOKSTRUCT*": [
    "KBDLLHOOKSTRUCT", 
    "KBDLLHOOKSTRUCT[]"
  ], 
  "PPRINTER_INFO_1": [
    "PRINTER_INFO_1", 
    "PRINTER_INFO_1[]"
  ], 
  "PPRINTER_INFO_3": [
    "PRINTER_INFO_3", 
    "PRINTER_INFO_3[]"
  ], 
  "WER_FILE_TYPE": [
    "int"
  ], 
  "LOCALGROUP_MEMBERS_INFO_3**": [
    "PointerByReference"
  ], 
  "PUSER_POWER_POLICY": [
    "USER_POWER_POLICY", 
    "USER_POWER_POLICY[]"
  ], 
  "PSMALL_RECT": [
    "SMALL_RECT", 
    "SMALL_RECT[]"
  ], 
  "PPRINTER_INFO_9": [
    "PRINTER_INFO_9", 
    "PRINTER_INFO_9[]"
  ], 
  "PPRINTER_INFO_8": [
    "PRINTER_INFO_8", 
    "PRINTER_INFO_8[]"
  ], 
  "PEFS_CERTIFICATE_BLOB*": [
    "PointerByReference"
  ], 
  "INT8**": [
    "byte"
  ], 
  "LPLASTINPUTINFO": [
    "LASTINPUTINFO", 
    "LASTINPUTINFO[]"
  ], 
  "DFS_STORAGE_INFO*": [
    "DFS_STORAGE_INFO", 
    "DFS_STORAGE_INFO[]"
  ], 
  "USN**": [
    "PointerByReference"
  ], 
  "LPSERVICE_STATUS_HANDLE": [
    "HANDLEByReference"
  ], 
  "BSMINFO": [
    "BSMINFO"
  ], 
  "LPCPROCESSENTRY32": [
    "PROCESSENTRY32", 
    "PROCESSENTRY32[]"
  ], 
  "PUCHAR": [
    "byte"
  ], 
  "LPSCNRT_STATUS*": [
    "PointerByReference"
  ], 
  "RID_DEVICE_INFO_MOUSE**": [
    "PointerByReference"
  ], 
  "HPEN*": [
    "HANDLEByReference"
  ], 
  "PSTR*": [
    "String[]", 
    "PointerByReference"
  ], 
  "LPUSER_INFO_1009": [
    "USER_INFO_1009", 
    "USER_INFO_1009[]"
  ], 
  "LPRM_FILTER_ACTION*": [
    "PointerByReference"
  ], 
  "LPUSER_INFO_1003": [
    "USER_INFO_1003", 
    "USER_INFO_1003[]"
  ], 
  "LPUSER_INFO_1005": [
    "USER_INFO_1005", 
    "USER_INFO_1005[]"
  ], 
  "LPUSER_INFO_1006": [
    "USER_INFO_1006", 
    "USER_INFO_1006[]"
  ], 
  "DLGTEMPLATE*": [
    "DLGTEMPLATE", 
    "DLGTEMPLATE[]"
  ], 
  "LCTYPE*": [
    "IntByReference", 
    "long[]"
  ], 
  "DFS_STORAGE_INFO_1*": [
    "DFS_STORAGE_INFO_1", 
    "DFS_STORAGE_INFO_1[]"
  ], 
  "LPWCT_OBJECT_STATUS": [
    "IntByReference", 
    "int[]"
  ], 
  "DLGITEMTEMPLATE": [
    "DLGITEMTEMPLATE"
  ], 
  "DFS_STORAGE_INFO**": [
    "PointerByReference"
  ], 
  "USER_POWER_POLICY": [
    "USER_POWER_POLICY"
  ], 
  "EFS_CERTIFICATE_BLOB*": [
    "EFS_CERTIFICATE_BLOB", 
    "EFS_CERTIFICATE_BLOB[]"
  ], 
  "REMOTE_NAME_INFO": [
    "REMOTE_NAME_INFO"
  ], 
  "SYSTEM_POWER_POLICY**": [
    "PointerByReference"
  ], 
  "DISCDLGSTRUCT*": [
    "DISCDLGSTRUCT", 
    "DISCDLGSTRUCT[]"
  ], 
  "PDOC_INFO_1": [
    "DOC_INFO_1", 
    "DOC_INFO_1[]"
  ], 
  "PRASTERIZER_STATUS*": [
    "PointerByReference"
  ], 
  "PLCTYPE": [
    "IntByReference", 
    "long[]"
  ], 
  "PCWPSTRUCT*": [
    "PointerByReference"
  ], 
  "HMETAFILE*": [
    "HANDLEByReference"
  ], 
  "PACCEL": [
    "ACCEL", 
    "ACCEL[]"
  ], 
  "LPCCPINFOEX": [
    "CPINFOEX", 
    "CPINFOEX[]"
  ], 
  "WER_DUMP_CUSTOM_OPTIONS": [
    "WER_DUMP_CUSTOM_OPTIONS"
  ], 
  "POINTL**": [
    "PointerByReference"
  ], 
  "PSHARE_INFO_1": [
    "SHARE_INFO_1", 
    "SHARE_INFO_1[]"
  ], 
  "LPDFS_INFO_101*": [
    "PointerByReference"
  ], 
  "PNETCONNECTINFOSTRUCT": [
    "NETCONNECTINFOSTRUCT", 
    "NETCONNECTINFOSTRUCT[]"
  ], 
  "PEVENT_TRACE*": [
    "PointerByReference"
  ], 
  "LPLOGPEN*": [
    "PointerByReference"
  ], 
  "DRAWTEXTPARAMS": [
    "DRAWTEXTPARAMS"
  ], 
  "LASTINPUTINFO*": [
    "LASTINPUTINFO", 
    "LASTINPUTINFO[]"
  ], 
  "LPLPSTR*": [
    "byte"
  ], 
  "JOBOBJECT_BASIC_UI_RESTRICTIONS*": [
    "JOBOBJECT_BASIC_UI_RESTRICTIONS", 
    "JOBOBJECT_BASIC_UI_RESTRICTIONS[]"
  ], 
  "LPCSHARE_INFO_1501": [
    "SHARE_INFO_1501", 
    "SHARE_INFO_1501[]"
  ], 
  "DFS_INFO_102*": [
    "DFS_INFO_102", 
    "DFS_INFO_102[]"
  ], 
  "PLOCALGROUP_INFO_1002*": [
    "PointerByReference"
  ], 
  "LPSHELLEXECUTEINFO": [
    "SHELLEXECUTEINFO", 
    "SHELLEXECUTEINFO[]"
  ], 
  "ENUMLOGFONTEXDV*": [
    "ENUMLOGFONTEXDV", 
    "ENUMLOGFONTEXDV[]"
  ], 
  "LPEVENT_TRACE_HEADER*": [
    "PointerByReference"
  ], 
  "va_list*": [
    "Pointer", 
    "String[]"
  ], 
  "LPSYSTEM_POWER_INFORMATION*": [
    "PointerByReference"
  ], 
  "USN": [
    "double"
  ], 
  "size_t": [
    "int"
  ], 
  "PUINT32*": [
    "PointerByReference"
  ], 
  "LPNTMS_ALLOCATION_INFORMATION": [
    "NTMS_ALLOCATION_INFORMATION", 
    "NTMS_ALLOCATION_INFORMATION[]"
  ], 
  "LPSERVICE_LAUNCH_PROTECTED_INFO": [
    "SERVICE_LAUNCH_PROTECTED_INFO", 
    "SERVICE_LAUNCH_PROTECTED_INFO[]"
  ], 
  "LPLOCALGROUP_INFO_1002": [
    "LOCALGROUP_INFO_1002", 
    "LOCALGROUP_INFO_1002[]"
  ], 
  "LPEXTENDED_NAME_FORMAT": [
    "EXTENDED_NAME_FORMAT", 
    "EXTENDED_NAME_FORMAT[]"
  ], 
  "LPSYSTEM_POWER_CAPABILITIES": [
    "SYSTEM_POWER_CAPABILITIES", 
    "SYSTEM_POWER_CAPABILITIES[]"
  ], 
  "PSOUNDSENTRY*": [
    "PointerByReference"
  ], 
  "PPROPERTYKEY": [
    "PROPERTYKEY", 
    "PROPERTYKEY[]"
  ], 
  "DEVMODE**": [
    "PointerByReference"
  ], 
  "RM_PROCESS_INFO*": [
    "RM_PROCESS_INFO", 
    "RM_PROCESS_INFO[]"
  ], 
  "LPCMAT2": [
    "MAT2", 
    "MAT2[]"
  ], 
  "LPSYSTEM_POWER_INFORMATION": [
    "SYSTEM_POWER_INFORMATION", 
    "SYSTEM_POWER_INFORMATION[]"
  ], 
  "PTAPE_SET_MEDIA_PARAMETERS*": [
    "PointerByReference"
  ], 
  "PUINT": [
    "IntByReference", 
    "int[]"
  ], 
  "LPPDH_RAW_LOG_RECORD": [
    "PDH_RAW_LOG_RECORD", 
    "PDH_RAW_LOG_RECORD[]"
  ], 
  "LPCIO_COUNTERS": [
    "IO_COUNTERS", 
    "IO_COUNTERS[]"
  ], 
  "PROCESS_INFORMATION": [
    "PROCESS_INFORMATION"
  ], 
  "WNDCLASSEX": [
    "WNDCLASSEX"
  ], 
  "PGCP_RESULTS": [
    "GCP_RESULTS", 
    "GCP_RESULTS[]"
  ], 
  "LPDIBSECTION": [
    "DIBSECTION", 
    "DIBSECTION[]"
  ], 
  "COMMPROP*": [
    "COMMPROP", 
    "COMMPROP[]"
  ], 
  "LPTASKDIALOG_FLAGS": [
    "IntByReference", 
    "int[]"
  ], 
  "DFS_INFO_8*": [
    "DFS_INFO_8", 
    "DFS_INFO_8[]"
  ], 
  "SHARE_INFO_502*": [
    "SHARE_INFO_502", 
    "SHARE_INFO_502[]"
  ], 
  "LPAXESLIST*": [
    "PointerByReference"
  ], 
  "PStringTable": [
    "StringTable", 
    "StringTable[]"
  ], 
  "COMMCONFIG": [
    "COMMCONFIG"
  ], 
  "LPCSYSTEM_POWER_CAPABILITIES": [
    "SYSTEM_POWER_CAPABILITIES", 
    "SYSTEM_POWER_CAPABILITIES[]"
  ], 
  "PCWSTR**": [
    "char"
  ], 
  "LPCPOWER_ACTION": [
    "IntByReference", 
    "int[]"
  ], 
  "LPCQUERY_USER_NOTIFICATION_STATE": [
    "IntByReference", 
    "int[]"
  ], 
  "LPCDFS_INFO_104": [
    "DFS_INFO_104", 
    "DFS_INFO_104[]"
  ], 
  "LPCDFS_INFO_105": [
    "DFS_INFO_105", 
    "DFS_INFO_105[]"
  ], 
  "LPCDFS_INFO_106": [
    "DFS_INFO_106", 
    "DFS_INFO_106[]"
  ], 
  "LPCDFS_INFO_107": [
    "DFS_INFO_107", 
    "DFS_INFO_107[]"
  ], 
  "LPCDFS_INFO_100": [
    "DFS_INFO_100", 
    "DFS_INFO_100[]"
  ], 
  "PFORM_INFO_1": [
    "FORM_INFO_1", 
    "FORM_INFO_1[]"
  ], 
  "PFORM_INFO_2": [
    "FORM_INFO_2", 
    "FORM_INFO_2[]"
  ], 
  "LPCDFS_INFO_103": [
    "DFS_INFO_103", 
    "DFS_INFO_103[]"
  ], 
  "SERVICE_PRESHUTDOWN_INFO": [
    "SERVICE_PRESHUTDOWN_INFO"
  ], 
  "LPWSTR": [
    "CHARByReference", 
    "wchar_t[]", 
    "WString", 
    "char[]"
  ], 
  "PPDH_RAW_LOG_RECORD": [
    "PDH_RAW_LOG_RECORD", 
    "PDH_RAW_LOG_RECORD[]"
  ], 
  "ATOM**": [
    "PointerByReference"
  ], 
  "LPCONSOLE_FONT_INFO*": [
    "PointerByReference"
  ], 
  "WKSTA_USER_INFO_0*": [
    "WKSTA_USER_INFO_0", 
    "WKSTA_USER_INFO_0[]"
  ], 
  "LPCWIN32_FIND_STREAM_DATA": [
    "WIN32_FIND_STREAM_DATA", 
    "WIN32_FIND_STREAM_DATA[]"
  ], 
  "PCONNECTION_INFO_0*": [
    "PointerByReference"
  ], 
  "LPCWKSTA_INFO_101": [
    "WKSTA_INFO_101", 
    "WKSTA_INFO_101[]"
  ], 
  "RID_DEVICE_INFO*": [
    "RID_DEVICE_INFO", 
    "RID_DEVICE_INFO[]"
  ], 
  "HCONVLIST": [
    "HANDLE"
  ], 
  "LPCWKSTA_INFO_102": [
    "WKSTA_INFO_102", 
    "WKSTA_INFO_102[]"
  ], 
  "LPPRINTER_INFO_8*": [
    "PointerByReference"
  ], 
  "PUSER_MODALS_INFO_2*": [
    "PointerByReference"
  ], 
  "PTASKDIALOG_COMMON_BUTTON_FLAGS": [
    "IntByReference", 
    "int[]"
  ], 
  "LPLOCALGROUP_USERS_INFO_0*": [
    "PointerByReference"
  ], 
  "SHFILEINFO": [
    "SHFILEINFO"
  ], 
  "PENUMLOGFONTEXDV*": [
    "PointerByReference"
  ], 
  "PEVENTLOG_FULL_INFORMATION*": [
    "PointerByReference"
  ], 
  "LPTRACE_LOGFILE_HEADER": [
    "TRACE_LOGFILE_HEADER", 
    "TRACE_LOGFILE_HEADER[]"
  ], 
  "PPRINTER_NOTIFY_OPTIONS_TYPE": [
    "PRINTER_NOTIFY_OPTIONS_TYPE", 
    "PRINTER_NOTIFY_OPTIONS_TYPE[]"
  ], 
  "MDICREATESTRUCT**": [
    "PointerByReference"
  ], 
  "LPCHAR*": [
    "byte"
  ], 
  "EFS_HASH_BLOB**": [
    "PointerByReference"
  ], 
  "INPUT_RECORD**": [
    "PointerByReference"
  ], 
  "long long**": [
    "PointerByReference"
  ], 
  "PUSER_INFO_1051": [
    "USER_INFO_1051", 
    "USER_INFO_1051[]"
  ], 
  "PUSER_INFO_1052": [
    "USER_INFO_1052", 
    "USER_INFO_1052[]"
  ], 
  "PUSER_INFO_1053": [
    "USER_INFO_1053", 
    "USER_INFO_1053[]"
  ], 
  "LPMDICREATESTRUCT*": [
    "PointerByReference"
  ], 
  "LPULONG64": [
    "LongByReference", 
    "__int64[]"
  ], 
  "LPCUNIVERSAL_NAME_INFO": [
    "UNIVERSAL_NAME_INFO", 
    "UNIVERSAL_NAME_INFO[]"
  ], 
  "RGBQUAD*": [
    "RGBQUAD", 
    "RGBQUAD[]"
  ], 
  "LPSC_ACTION": [
    "SC_ACTION", 
    "SC_ACTION[]"
  ], 
  "PSHARE_INFO_0": [
    "SHARE_INFO_0", 
    "SHARE_INFO_0[]"
  ], 
  "LPUSER_INFO_1017*": [
    "PointerByReference"
  ], 
  "LPPDH_RAW_LOG_RECORD*": [
    "PointerByReference"
  ], 
  "LPCONVINFO": [
    "CONVINFO", 
    "CONVINFO[]"
  ], 
  "TOKEN_PRIVILEGES*": [
    "TOKEN_PRIVILEGES", 
    "TOKEN_PRIVILEGES[]"
  ], 
  "POLYTEXT*": [
    "POLYTEXT", 
    "POLYTEXT[]"
  ], 
  "SERVICE_SID_INFO**": [
    "PointerByReference"
  ], 
  "LPUSER_MODALS_INFO_1002": [
    "USER_MODALS_INFO_1002", 
    "USER_MODALS_INFO_1002[]"
  ], 
  "LPCUSER_INFO_1051": [
    "USER_INFO_1051", 
    "USER_INFO_1051[]"
  ], 
  "PJOB_INFO_2*": [
    "PointerByReference"
  ], 
  "LPSERVICE_STATUS_PROCESS*": [
    "PointerByReference"
  ], 
  "LPREMOTE_NAME_INFO": [
    "REMOTE_NAME_INFO", 
    "REMOTE_NAME_INFO[]"
  ], 
  "PKEY_EVENT_RECORD*": [
    "PointerByReference"
  ], 
  "PPDH_FMT_COUNTERVALUE_ITEM": [
    "PDH_FMT_COUNTERVALUE_ITEM", 
    "PDH_FMT_COUNTERVALUE_ITEM[]"
  ], 
  "PLONG": [
    "IntByReference", 
    "long[]", 
    "LONGByReference"
  ], 
  "LPUSN": [
    "DoubleByReference", 
    "double[]"
  ], 
  "PBITMAP": [
    "BITMAP", 
    "BITMAP[]"
  ], 
  "SERVICE_SID_INFO": [
    "SERVICE_SID_INFO"
  ], 
  "PWPARAM*": [
    "PointerByReference"
  ], 
  "LPCPINFOEX": [
    "CPINFOEX", 
    "CPINFOEX[]"
  ], 
  "EVENT_TRACE_PROPERTIES**": [
    "PointerByReference"
  ], 
  "LPSHITEMID*": [
    "PointerByReference"
  ], 
  "PLDT_ENTRY*": [
    "PointerByReference"
  ], 
  "PDWORD*": [
    "PointerByReference"
  ], 
  "PSERVICE_TRIGGER_INFO*": [
    "PointerByReference"
  ], 
  "LPINT8": [
    "byte"
  ], 
  "DISCDLGSTRUCT": [
    "DISCDLGSTRUCT"
  ], 
  "LPNETINFOSTRUCT*": [
    "PointerByReference"
  ], 
  "LPCADDJOB_INFO_1": [
    "ADDJOB_INFO_1", 
    "ADDJOB_INFO_1[]"
  ], 
  "PROCESSOR_POWER_POLICY**": [
    "PointerByReference"
  ], 
  "DIBSECTION**": [
    "PointerByReference"
  ], 
  "LPLASTINPUTINFO*": [
    "PointerByReference"
  ], 
  "DVTARGETDEVICE**": [
    "PointerByReference"
  ], 
  "PDFS_INFO_105": [
    "DFS_INFO_105", 
    "DFS_INFO_105[]"
  ], 
  "PDFS_INFO_104": [
    "DFS_INFO_104", 
    "DFS_INFO_104[]"
  ], 
  "PDFS_INFO_107": [
    "DFS_INFO_107", 
    "DFS_INFO_107[]"
  ], 
  "LPWER_REPORT_UI": [
    "IntByReference", 
    "int[]"
  ], 
  "PMSG_INFO_0": [
    "MSG_INFO_0", 
    "MSG_INFO_0[]"
  ], 
  "PMSG_INFO_1": [
    "MSG_INFO_1", 
    "MSG_INFO_1[]"
  ], 
  "PDFS_INFO_103": [
    "DFS_INFO_103", 
    "DFS_INFO_103[]"
  ], 
  "SYSTEM_LOGICAL_PROCESSOR_INFORMATION**": [
    "PointerByReference"
  ], 
  "DWORD32": [
    "int"
  ], 
  "BLENDFUNCTION**": [
    "PointerByReference"
  ], 
  "PJOBOBJECT_RATE_CONTROL_TOLERANCE*": [
    "PointerByReference"
  ], 
  "LPCONSOLE_SELECTION_INFO*": [
    "PointerByReference"
  ], 
  "NDDESHAREINFO": [
    "NDDESHAREINFO"
  ], 
  "PNET_DISPLAY_MACHINE*": [
    "PointerByReference"
  ], 
  "GETPROPERTYSTOREFLAGS**": [
    "PointerByReference"
  ], 
  "LPSIZE_T": [
    "IntByReference", 
    "long[]"
  ], 
  "PTimeSysInfo*": [
    "PointerByReference"
  ], 
  "ULONG64**": [
    "PointerByReference"
  ], 
  "WINDOWPLACEMENT*": [
    "WINDOWPLACEMENT", 
    "WINDOWPLACEMENT[]"
  ], 
  "LPTRACE_GUID_PROPERTIES": [
    "TRACE_GUID_PROPERTIES", 
    "TRACE_GUID_PROPERTIES[]"
  ], 
  "PCTSTR*": [
    "WString[]", 
    "PointerByReference"
  ], 
  "PNDDESHAREINFO*": [
    "PointerByReference"
  ], 
  "LPCLOCALGROUP_INFO_0": [
    "LOCALGROUP_INFO_0", 
    "LOCALGROUP_INFO_0[]"
  ], 
  "USER_INFO_1014**": [
    "PointerByReference"
  ], 
  "PGRADIENT_TRIANGLE": [
    "GRADIENT_TRIANGLE", 
    "GRADIENT_TRIANGLE[]"
  ], 
  "LPCKERNINGPAIR": [
    "KERNINGPAIR", 
    "KERNINGPAIR[]"
  ], 
  "ITEMIDLIST*": [
    "ITEMIDLIST", 
    "ITEMIDLIST[]"
  ], 
  "UINT64": [
    "long"
  ], 
  "LPMSG_INFO_1*": [
    "PointerByReference"
  ], 
  "WKSTA_USER_INFO_1": [
    "WKSTA_USER_INFO_1"
  ], 
  "USER_MODALS_INFO_1001": [
    "USER_MODALS_INFO_1001"
  ], 
  "USER_MODALS_INFO_1006": [
    "USER_MODALS_INFO_1006"
  ], 
  "USER_MODALS_INFO_1007": [
    "USER_MODALS_INFO_1007"
  ], 
  "USER_MODALS_INFO_1004": [
    "USER_MODALS_INFO_1004"
  ], 
  "USER_MODALS_INFO_1005": [
    "USER_MODALS_INFO_1005"
  ], 
  "LPOSVERSIONINFOEX": [
    "OSVERSIONINFOEX", 
    "OSVERSIONINFOEX[]"
  ], 
  "PSHSTOCKICONINFO": [
    "SHSTOCKICONINFO", 
    "SHSTOCKICONINFO[]"
  ], 
  "LPPRINTER_NOTIFY_INFO": [
    "PRINTER_NOTIFY_INFO", 
    "PRINTER_NOTIFY_INFO[]"
  ], 
  "LPDWORD": [
    "IntByReference", 
    "long[]", 
    "int", 
    "Pointer"
  ], 
  "PSMALL_RECT*": [
    "PointerByReference"
  ], 
  "LPCUSER_MODALS_INFO_1002": [
    "USER_MODALS_INFO_1002", 
    "USER_MODALS_INFO_1002[]"
  ], 
  "LPCUSER_INFO_1053": [
    "USER_INFO_1053", 
    "USER_INFO_1053[]"
  ], 
  "LPCUSER_INFO_1052": [
    "USER_INFO_1052", 
    "USER_INFO_1052[]"
  ], 
  "PUSE_INFO_0": [
    "USE_INFO_0", 
    "USE_INFO_0[]"
  ], 
  "DFS_INFO_8": [
    "DFS_INFO_8"
  ], 
  "PCWSTR": [
    "CHARByReference", 
    "wchar_t[]", 
    "WString"
  ], 
  "PMONITOR_INFO_1*": [
    "PointerByReference"
  ], 
  "PUSE_INFO_1": [
    "USE_INFO_1", 
    "USE_INFO_1[]"
  ], 
  "PEMRALPHABLEND*": [
    "PointerByReference"
  ], 
  "TimeSysInfo": [
    "int"
  ], 
  "PPDH_FMT_COUNTERVALUE*": [
    "PointerByReference"
  ], 
  "PHRGN": [
    "HANDLEByReference"
  ], 
  "LPsize_t": [
    "IntByReference", 
    "long[]"
  ], 
  "LPUSE_INFO_0": [
    "USE_INFO_0", 
    "USE_INFO_0[]"
  ], 
  "PNETINFOSTRUCT": [
    "NETINFOSTRUCT", 
    "NETINFOSTRUCT[]"
  ], 
  "LPCLOGPALETTE": [
    "LOGPALETTE", 
    "LOGPALETTE[]"
  ], 
  "ABC*": [
    "ABC", 
    "ABC[]"
  ], 
  "PAXISINFO": [
    "AXISINFO", 
    "AXISINFO[]"
  ], 
  "JOB_INFO_2**": [
    "PointerByReference"
  ], 
  "PPOWER_ACTION": [
    "IntByReference", 
    "int[]"
  ], 
  "PCONSOLE_SCREEN_BUFFER_INFO*": [
    "PointerByReference"
  ], 
  "SERVER_INFO_100*": [
    "SERVER_INFO_100", 
    "SERVER_INFO_100[]"
  ], 
  "LPCDFS_INFO_4": [
    "DFS_INFO_4", 
    "DFS_INFO_4[]"
  ], 
  "PLPSTR*": [
    "byte"
  ], 
  "PDWORD_PTR*": [
    "PointerByReference"
  ], 
  "LPSCROLLINFO": [
    "SCROLLINFO", 
    "SCROLLINFO[]"
  ], 
  "PDISCDLGSTRUCT": [
    "DISCDLGSTRUCT", 
    "DISCDLGSTRUCT[]"
  ], 
  "PPDH_DATA_ITEM_PATH_ELEMENTS": [
    "PDH_DATA_ITEM_PATH_ELEMENTS", 
    "PDH_DATA_ITEM_PATH_ELEMENTS[]"
  ], 
  "NTMS_GUID": [
    "GUID"
  ], 
  "SOUNDSENTRY**": [
    "PointerByReference"
  ], 
  "LPCSESSION_INFO_1": [
    "SESSION_INFO_1", 
    "SESSION_INFO_1[]"
  ], 
  "SERVER_TRANSPORT_INFO_2**": [
    "PointerByReference"
  ], 
  "SID_AND_ATTRIBUTES*": [
    "SID_AND_ATTRIBUTES", 
    "SID_AND_ATTRIBUTES[]"
  ], 
  "PDFS_TARGET_PRIORITY_CLASS*": [
    "PointerByReference"
  ], 
  "PCALTYPE*": [
    "PointerByReference"
  ], 
  "NETCONNECTINFOSTRUCT": [
    "NETCONNECTINFOSTRUCT"
  ], 
  "PLPARAM*": [
    "PointerByReference"
  ], 
  "SERVICE_REQUIRED_PRIVILEGES_INFO*": [
    "SERVICE_REQUIRED_PRIVILEGES_INFO", 
    "SERVICE_REQUIRED_PRIVILEGES_INFO[]"
  ], 
  "PROCESS_MEMORY_COUNTERS_EX**": [
    "PointerByReference"
  ], 
  "LPSERVICE_PRESHUTDOWN_INFO*": [
    "PointerByReference"
  ], 
  "LPCOLORREF*": [
    "PointerByReference"
  ], 
  "LPPALETTEENTRY*": [
    "PointerByReference"
  ], 
  "MODULEENTRY32": [
    "MODULEENTRY32"
  ], 
  "LPCFLASHWINFO": [
    "FLASHWINFO", 
    "FLASHWINFO[]"
  ], 
  "SC_LOCK": [
    "Pointer", 
    "LPVOID"
  ], 
  "LPFILTERKEYS*": [
    "PointerByReference"
  ], 
  "LPTAPE_GET_MEDIA_PARAMETERS*": [
    "PointerByReference"
  ], 
  "INPUT*": [
    "INPUT", 
    "INPUT[]"
  ], 
  "TCHAR**": [
    "char"
  ], 
  "PFE_IMPORT_FUNC": [
    "WinBase.FE_IMPORT_FUNC"
  ], 
  "PGET_FILEEX_INFO_LEVELS": [
    "IntByReference", 
    "int[]"
  ], 
  "LPFIXED": [
    "FIXED", 
    "FIXED[]"
  ], 
  "WINSTA*": [
    "HANDLEByReference"
  ], 
  "LPJOBOBJECTINFOCLASS*": [
    "PointerByReference"
  ], 
  "LPCSERVICE_TRIGGER_SPECIFIC_DATA_ITEM": [
    "SERVICE_TRIGGER_SPECIFIC_DATA_ITEM", 
    "SERVICE_TRIGGER_SPECIFIC_DATA_ITEM[]"
  ], 
  "HRESULT": [
    "int"
  ], 
  "LPTSTR*": [
    "PointerByReference"
  ], 
  "PUINT*": [
    "PointerByReference"
  ], 
  "LPWER_DUMP_CUSTOM_OPTIONS": [
    "WER_DUMP_CUSTOM_OPTIONS", 
    "WER_DUMP_CUSTOM_OPTIONS[]"
  ], 
  "LPCUSER_MODALS_INFO_2": [
    "USER_MODALS_INFO_2", 
    "USER_MODALS_INFO_2[]"
  ], 
  "LPCUSER_MODALS_INFO_3": [
    "USER_MODALS_INFO_3", 
    "USER_MODALS_INFO_3[]"
  ], 
  "LPCUSER_MODALS_INFO_0": [
    "USER_MODALS_INFO_0", 
    "USER_MODALS_INFO_0[]"
  ], 
  "COLOR16": [
    "short"
  ], 
  "LPSHFILEINFO*": [
    "PointerByReference"
  ], 
  "RAWINPUT*": [
    "RAWINPUT", 
    "RAWINPUT[]"
  ], 
  "LPCMOUSEKEYS": [
    "MOUSEKEYS", 
    "MOUSEKEYS[]"
  ], 
  "PUCHAR*": [
    "byte"
  ], 
  "LPFINDEX_SEARCH_OPS": [
    "IntByReference", 
    "int[]"
  ], 
  "DLGITEMTEMPLATE**": [
    "PointerByReference"
  ], 
  "PUINT8": [
    "byte"
  ], 
  "LPRAWHID*": [
    "PointerByReference"
  ], 
  "PSESSION_INFO_502*": [
    "PointerByReference"
  ], 
  "PAVRT_PRIORITY*": [
    "PointerByReference"
  ], 
  "PGLYPHMETRICS": [
    "GLYPHMETRICS", 
    "GLYPHMETRICS[]"
  ], 
  "LPSYSTEM_POWER_POLICY*": [
    "PointerByReference"
  ], 
  "LPOPEN_AS_INFO_FLAGS*": [
    "PointerByReference"
  ], 
  "LPCCOMMCONFIG": [
    "COMMCONFIG", 
    "COMMCONFIG[]"
  ], 
  "PSESSION_INFO_1*": [
    "PointerByReference"
  ], 
  "WCRANGE**": [
    "PointerByReference"
  ], 
  "LPMENUBARINFO": [
    "MENUBARINFO", 
    "MENUBARINFO[]"
  ], 
  "LPQWORD": [
    "LongByReference", 
    "__int64[]"
  ], 
  "PSERVER_INFO_100*": [
    "PointerByReference"
  ], 
  "PSESSION_INFO_10": [
    "SESSION_INFO_10", 
    "SESSION_INFO_10[]"
  ], 
  "RM_FILTER_ACTION**": [
    "PointerByReference"
  ], 
  "PLMSTR*": [
    "char"
  ], 
  "LPPOWER_POLICY": [
    "POWER_POLICY", 
    "POWER_POLICY[]"
  ], 
  "LPWCT_OBJECT_STATUS*": [
    "PointerByReference"
  ], 
  "QWORD**": [
    "PointerByReference"
  ], 
  "LPGLOBAL_USER_POWER_POLICY*": [
    "PointerByReference"
  ], 
  "LPFILE_INFO_2*": [
    "PointerByReference"
  ], 
  "POWERBROADCAST_SETTING": [
    "POWERBROADCAST_SETTING"
  ], 
  "PPDH_COUNTER_PATH_ELEMENTS*": [
    "PointerByReference"
  ], 
  "CURSORINFO": [
    "CURSORINFO"
  ], 
  "LPWNDCLASSEX*": [
    "PointerByReference"
  ], 
  "LPANIMATIONINFO*": [
    "PointerByReference"
  ], 
  "TASKDIALOG_FLAGS*": [
    "IntByReference", 
    "int[]"
  ], 
  "USER_MODALS_INFO_3*": [
    "USER_MODALS_INFO_3", 
    "USER_MODALS_INFO_3[]"
  ], 
  "LPPORT_INFO_2*": [
    "PointerByReference"
  ], 
  "IO_COUNTERS": [
    "IO_COUNTERS"
  ], 
  "LPARAM*": [
    "IntByReference", 
    "long[]"
  ], 
  "LPFILE_INFO_3": [
    "FILE_INFO_3", 
    "FILE_INFO_3[]"
  ], 
  "CHAR*": [
    "byte"
  ], 
  "LPFINDEX_INFO_LEVELS*": [
    "PointerByReference"
  ], 
  "LPMSG_INFO_0*": [
    "PointerByReference"
  ], 
  "PULARGE_INTEGER": [
    "ULARGE_INTEGER", 
    "ULARGE_INTEGER[]"
  ], 
  "USER_INFO_0**": [
    "PointerByReference"
  ], 
  "PBITMAPINFO*": [
    "PointerByReference"
  ], 
  "LPSTICKYKEYS*": [
    "PointerByReference"
  ], 
  "LPCCONSOLE_READCONSOLE_CONTROL": [
    "CONSOLE_READCONSOLE_CONTROL", 
    "CONSOLE_READCONSOLE_CONTROL[]"
  ], 
  "HDWP": [
    "HANDLE"
  ], 
  "LPCWKSTA_USER_INFO_0": [
    "WKSTA_USER_INFO_0", 
    "WKSTA_USER_INFO_0[]"
  ], 
  "PDRIVER_INFO_6": [
    "DRIVER_INFO_6", 
    "DRIVER_INFO_6[]"
  ], 
  "LPCBT_CREATEWND": [
    "CBT_CREATEWND", 
    "CBT_CREATEWND[]"
  ], 
  "SERVER_TRANSPORT_INFO_1**": [
    "PointerByReference"
  ], 
  "PCALTYPE": [
    "IntByReference", 
    "long[]"
  ], 
  "HGDIOBJ": [
    "HANDLE"
  ], 
  "DFS_TARGET_PRIORITY**": [
    "PointerByReference"
  ], 
  "LPFINDEX_SEARCH_OPS*": [
    "PointerByReference"
  ], 
  "PHACCEL": [
    "HANDLEByReference"
  ], 
  "JOBOBJECTINFOCLASS**": [
    "PointerByReference"
  ], 
  "SHARE_INFO_1006": [
    "SHARE_INFO_1006"
  ], 
  "LPFILE_INFO_2": [
    "FILE_INFO_2", 
    "FILE_INFO_2[]"
  ], 
  "LGRPID*": [
    "IntByReference", 
    "long[]"
  ], 
  "POVERLAPPED": [
    "OVERLAPPED", 
    "OVERLAPPED[]"
  ], 
  "LPAPPBARDATA": [
    "APPBARDATA", 
    "APPBARDATA[]"
  ], 
  "SESSION_INFO_10": [
    "SESSION_INFO_10"
  ], 
  "HKL": [
    "HANDLE"
  ], 
  "LPTOKEN_GROUPS*": [
    "PointerByReference"
  ], 
  "DWORDLONG": [
    "long"
  ], 
  "ALG_ID": [
    "UINT"
  ], 
  "JOBOBJECT_NOTIFICATION_LIMIT_INFORMATION": [
    "JOBOBJECT_NOTIFICATION_LIMIT_INFORMATION"
  ], 
  "WMIDPREQUESTCODE**": [
    "PointerByReference"
  ], 
  "PMACHINE_POWER_POLICY*": [
    "PointerByReference"
  ], 
  "PGROUP_INFO_1005*": [
    "PointerByReference"
  ], 
  "LPCSIGDN": [
    "IntByReference", 
    "int[]"
  ], 
  "GRADIENT_TRIANGLE*": [
    "GRADIENT_TRIANGLE", 
    "GRADIENT_TRIANGLE[]"
  ], 
  "PSYSTEM_POWER_STATUS*": [
    "PointerByReference"
  ], 
  "PGROUP_INFO_0": [
    "GROUP_INFO_0", 
    "GROUP_INFO_0[]"
  ], 
  "LPUSER_INFO_1053*": [
    "PointerByReference"
  ], 
  "LPHEAP_INFORMATION_CLASS": [
    "IntByReference", 
    "int[]"
  ], 
  "PDOCINFO": [
    "DOCINFO", 
    "DOCINFO[]"
  ], 
  "PLONG_PTR": [
    "IntByReference", 
    "long[]"
  ], 
  "LPCOMMTIMEOUTS*": [
    "PointerByReference"
  ], 
  "LPUSER_INFO_4*": [
    "PointerByReference"
  ], 
  "TRACE_GUID_REGISTRATION*": [
    "TRACE_GUID_REGISTRATION", 
    "TRACE_GUID_REGISTRATION[]"
  ], 
  "LPABCFLOAT": [
    "ABCFLOAT", 
    "ABCFLOAT[]"
  ], 
  "LPWER_REPORT_INFORMATION*": [
    "PointerByReference"
  ], 
  "PGLOBAL_MACHINE_POWER_POLICY": [
    "GLOBAL_MACHINE_POWER_POLICY", 
    "GLOBAL_MACHINE_POWER_POLICY[]"
  ], 
  "LPCMODULEINFO": [
    "MODULEINFO", 
    "MODULEINFO[]"
  ], 
  "StringTable**": [
    "PointerByReference"
  ], 
  "PHANDLETABLE*": [
    "PointerByReference"
  ], 
  "float*": [
    "FloatByReference", 
    "float[]"
  ], 
  "LPCMSLLHOOKSTRUCT": [
    "MSLLHOOKSTRUCT", 
    "MSLLHOOKSTRUCT[]"
  ], 
  "PDLGITEMTEMPLATE*": [
    "PointerByReference"
  ], 
  "LPCSYSTEM_POWER_POLICY": [
    "SYSTEM_POWER_POLICY", 
    "SYSTEM_POWER_POLICY[]"
  ], 
  "PETW_BUFFER_CONTEXT": [
    "ETW_BUFFER_CONTEXT", 
    "ETW_BUFFER_CONTEXT[]"
  ], 
  "LPHEAPENTRY32*": [
    "PointerByReference"
  ], 
  "LPPAINTSTRUCT*": [
    "PointerByReference"
  ], 
  "LPCVIDEOPARAMETERS": [
    "VIDEOPARAMETERS", 
    "VIDEOPARAMETERS[]"
  ], 
  "LPCDFS_STORAGE_INFO_1": [
    "DFS_STORAGE_INFO_1", 
    "DFS_STORAGE_INFO_1[]"
  ], 
  "POUTLINETEXTMETRIC*": [
    "PointerByReference"
  ], 
  "LPCOMSTAT*": [
    "PointerByReference"
  ], 
  "WER_DUMP_CUSTOM_OPTIONS**": [
    "PointerByReference"
  ], 
  "LPLONG*": [
    "PointerByReference"
  ], 
  "SCROLLBARINFO**": [
    "PointerByReference"
  ], 
  "PHMODULE": [
    "HANDLEByReference"
  ], 
  "LPDEVMODE": [
    "DEVMODE", 
    "DEVMODE[]"
  ], 
  "LPPDH_STATISTICS": [
    "PDH_STATISTICS", 
    "PDH_STATISTICS[]"
  ], 
  "RAWINPUTHEADER": [
    "RAWINPUTHEADER"
  ], 
  "LPCOORD*": [
    "PointerByReference"
  ], 
  "DFS_INFO_7": [
    "DFS_INFO_7"
  ], 
  "ULONGLONG*": [
    "DoubleByReference", 
    "double[]"
  ], 
  "PSERVER_INFO_101": [
    "SERVER_INFO_101", 
    "SERVER_INFO_101[]"
  ], 
  "LPUSER_INFO_1012": [
    "USER_INFO_1012", 
    "USER_INFO_1012[]"
  ], 
  "LPUSER_INFO_1011": [
    "USER_INFO_1011", 
    "USER_INFO_1011[]"
  ], 
  "PULONGLONG": [
    "DoubleByReference", 
    "double[]"
  ], 
  "GEOTYPE****": [
    "PointerByReference"
  ], 
  "TOGGLEKEYS**": [
    "PointerByReference"
  ], 
  "LPUSER_INFO_1014": [
    "USER_INFO_1014", 
    "USER_INFO_1014[]"
  ], 
  "LPDCB": [
    "DCB", 
    "DCB[]"
  ], 
  "SHDESCRIPTIONID": [
    "SHDESCRIPTIONID"
  ], 
  "SERVICE_TRIGGER_SPECIFIC_DATA_ITEM": [
    "SERVICE_TRIGGER_SPECIFIC_DATA_ITEM"
  ], 
  "DWORD64*": [
    "LongByReference", 
    "__int64[]"
  ], 
  "LPPSTR*": [
    "byte"
  ], 
  "FINDEX_INFO_LEVELS*": [
    "IntByReference", 
    "int[]"
  ], 
  "GROUP_USERS_INFO_1*": [
    "GROUP_USERS_INFO_1", 
    "GROUP_USERS_INFO_1[]"
  ], 
  "LPINT*": [
    "PointerByReference"
  ], 
  "LPTITLEBARINFO*": [
    "PointerByReference"
  ], 
  "LPCDLGITEMTEMPLATE": [
    "DLGITEMTEMPLATE", 
    "DLGITEMTEMPLATE[]"
  ], 
  "MONITOR_INFO_2": [
    "MONITOR_INFO_2"
  ], 
  "MONITOR_INFO_1": [
    "MONITOR_INFO_1"
  ], 
  "LPCFILE_INFO_3": [
    "FILE_INFO_3", 
    "FILE_INFO_3[]"
  ], 
  "LPPROCESS_INFORMATION": [
    "PROCESS_INFORMATION", 
    "PROCESS_INFORMATION[]"
  ], 
  "LPSTD_ALERT*": [
    "PointerByReference"
  ], 
  "PCLIPFORMAT*": [
    "PointerByReference"
  ], 
  "SECURITY_QUALITY_OF_SERVICE**": [
    "PointerByReference"
  ], 
  "PWKSTA_INFO_102*": [
    "PointerByReference"
  ], 
  "LPCDISPLAY_DEVICE": [
    "DISPLAY_DEVICE", 
    "DISPLAY_DEVICE[]"
  ], 
  "PRASTERIZER_STATUS": [
    "RASTERIZER_STATUS", 
    "RASTERIZER_STATUS[]"
  ], 
  "LPCWMIDPREQUESTCODE": [
    "IntByReference", 
    "int[]"
  ], 
  "LPHKL": [
    "HANDLEByReference"
  ], 
  "LPUSE_INFO_2": [
    "USE_INFO_2", 
    "USE_INFO_2[]"
  ], 
  "LPCCOLORADJUSTMENT": [
    "COLORADJUSTMENT", 
    "COLORADJUSTMENT[]"
  ], 
  "PSAPI_WS_WATCH_INFORMATION**": [
    "PointerByReference"
  ], 
  "GUID**": [
    "PointerByReference"
  ], 
  "PROCESS_MEMORY_COUNTERS*": [
    "PROCESS_MEMORY_COUNTERS", 
    "PROCESS_MEMORY_COUNTERS[]"
  ], 
  "PWNDCLASSEX*": [
    "PointerByReference"
  ], 
  "PBSMINFO": [
    "BSMINFO", 
    "BSMINFO[]"
  ], 
  "LPRASTERIZER_STATUS*": [
    "PointerByReference"
  ], 
  "WIN32_FIND_DATA*": [
    "WIN32_FIND_DATA", 
    "WIN32_FIND_DATA[]"
  ], 
  "LPDFS_INFO_102*": [
    "PointerByReference"
  ], 
  "DFS_INFO_5": [
    "DFS_INFO_5"
  ], 
  "STREAM_INFO_LEVELS**": [
    "PointerByReference"
  ], 
  "SHARE_INFO_503*": [
    "SHARE_INFO_503", 
    "SHARE_INFO_503[]"
  ], 
  "PBOOL": [
    "IntByReference", 
    "int[]"
  ], 
  "SYSGEOTYPE": [
    "int"
  ], 
  "PTOKEN_GROUPS*": [
    "PointerByReference"
  ], 
  "GLYPHMETRICS*": [
    "GLYPHMETRICS", 
    "GLYPHMETRICS[]"
  ], 
  "PPROCESSOR_NUMBER": [
    "PROCESSOR_NUMBER", 
    "PROCESSOR_NUMBER[]"
  ], 
  "DFS_INFO_103*": [
    "DFS_INFO_103", 
    "DFS_INFO_103[]"
  ], 
  "LPCOLOR16*": [
    "PointerByReference"
  ], 
  "PUSER_MODALS_INFO_1002*": [
    "PointerByReference"
  ], 
  "PPROCESSENTRY32": [
    "PROCESSENTRY32", 
    "PROCESSENTRY32[]"
  ], 
  "INT64": [
    "long"
  ], 
  "RID_DEVICE_INFO_KEYBOARD**": [
    "PointerByReference"
  ], 
  "DFS_INFO_9*": [
    "DFS_INFO_9", 
    "DFS_INFO_9[]"
  ], 
  "LPCUSER_INFO_10": [
    "USER_INFO_10", 
    "USER_INFO_10[]"
  ], 
  "LPCUSER_INFO_11": [
    "USER_INFO_11", 
    "USER_INFO_11[]"
  ], 
  "PRINTER_INFO_6*": [
    "PRINTER_INFO_6", 
    "PRINTER_INFO_6[]"
  ], 
  "STD_ALERT**": [
    "PointerByReference"
  ], 
  "PPOLYTEXT*": [
    "PointerByReference"
  ], 
  "PHMETAFILE": [
    "HANDLEByReference"
  ], 
  "LPCABCFLOAT": [
    "ABCFLOAT", 
    "ABCFLOAT[]"
  ], 
  "LOCALGROUP_USERS_INFO_0**": [
    "PointerByReference"
  ], 
  "LPCGEOCLASS**": [
    "PointerByReference"
  ], 
  "LPCKBDLLHOOKSTRUCT": [
    "KBDLLHOOKSTRUCT", 
    "KBDLLHOOKSTRUCT[]"
  ], 
  "LPCSTREAM_INFO_LEVELS": [
    "IntByReference", 
    "int[]"
  ], 
  "PENHMETAHEADER": [
    "ENHMETAHEADER", 
    "ENHMETAHEADER[]"
  ], 
  "LPNEWTEXTMETRIC*": [
    "PointerByReference"
  ], 
  "LPULONG_PTR": [
    "IntByReference", 
    "long[]"
  ], 
  "USER_MODALS_INFO_1007**": [
    "PointerByReference"
  ], 
  "LPMONITOR_INFO_1*": [
    "PointerByReference"
  ], 
  "PTCHAR": [
    "char"
  ], 
  "JOBOBJECT_BASIC_UI_RESTRICTIONS": [
    "JOBOBJECT_BASIC_UI_RESTRICTIONS"
  ], 
  "LPGROUP_INFO_2*": [
    "PointerByReference"
  ], 
  "LPETW_BUFFER_CONTEXT": [
    "ETW_BUFFER_CONTEXT", 
    "ETW_BUFFER_CONTEXT[]"
  ], 
  "GRADIENT_TRIANGLE**": [
    "PointerByReference"
  ], 
  "LPTHREAD_START_ROUTINE": [
    "WinBase.FOREIGN_THREAD_START_ROUTINE"
  ], 
  "PSYSGEOCLASS": [
    "IntByReference", 
    "int[]"
  ], 
  "NET_DISPLAY_MACHINE**": [
    "PointerByReference"
  ], 
  "PSHORT*": [
    "PointerByReference"
  ], 
  "LPSHQUERYRBINFO*": [
    "PointerByReference"
  ], 
  "LPCSCROLLINFO": [
    "SCROLLINFO", 
    "SCROLLINFO[]"
  ], 
  "LOCALGROUP_INFO_1**": [
    "PointerByReference"
  ], 
  "PQUERY_SERVICE_LOCK_STATUS": [
    "QUERY_SERVICE_LOCK_STATUS", 
    "QUERY_SERVICE_LOCK_STATUS[]"
  ], 
  "LPUNIVERSAL_NAME_INFO*": [
    "PointerByReference"
  ], 
  "LPHACCEL": [
    "HANDLEByReference"
  ], 
  "HFONT": [
    "HANDLE"
  ], 
  "LPPORT_INFO_1": [
    "PORT_INFO_1", 
    "PORT_INFO_1[]"
  ], 
  "SC_ACTION_TYPE": [
    "int"
  ], 
  "LPFORMATETC*": [
    "PointerByReference"
  ], 
  "PORT_INFO_1*": [
    "PORT_INFO_1", 
    "PORT_INFO_1[]"
  ], 
  "SID_AND_ATTRIBUTES**": [
    "PointerByReference"
  ], 
  "PDFS_INFO_1*": [
    "PointerByReference"
  ], 
  "LPCSERVICE_PRESHUTDOWN_INFO": [
    "SERVICE_PRESHUTDOWN_INFO", 
    "SERVICE_PRESHUTDOWN_INFO[]"
  ], 
  "SCROLLINFO*": [
    "SCROLLINFO", 
    "SCROLLINFO[]"
  ], 
  "SHARE_INFO_1006**": [
    "PointerByReference"
  ], 
  "LPCMediaLabelInfo": [
    "MediaLabelInfo", 
    "MediaLabelInfo[]"
  ], 
  "USER_INFO_1051**": [
    "PointerByReference"
  ], 
  "DFS_INFO_106": [
    "DFS_INFO_106"
  ], 
  "LOCALGROUP_MEMBERS_INFO_3*": [
    "LOCALGROUP_MEMBERS_INFO_3", 
    "LOCALGROUP_MEMBERS_INFO_3[]"
  ], 
  "WNODE_HEADER**": [
    "PointerByReference"
  ], 
  "SERVER_INFO_402**": [
    "PointerByReference"
  ], 
  "PINT32": [
    "IntByReference", 
    "int[]"
  ], 
  "LPCEVENT_TRACE_HEADER": [
    "EVENT_TRACE_HEADER", 
    "EVENT_TRACE_HEADER[]"
  ], 
  "LPCONSOLE_FONT_INFOEX*": [
    "PointerByReference"
  ], 
  "LPLATENCY_TIME": [
    "IntByReference", 
    "int[]"
  ], 
  "PMENUITEMINFO": [
    "MENUITEMINFO", 
    "MENUITEMINFO[]"
  ], 
  "LPBLENDFUNCTION": [
    "BLENDFUNCTION", 
    "BLENDFUNCTION[]"
  ], 
  "OPEN_AS_INFO_FLAGS*": [
    "IntByReference", 
    "int[]"
  ], 
  "ADDJOB_INFO_1**": [
    "PointerByReference"
  ], 
  "PULONG_PTR": [
    "IntByReference", 
    "long[]"
  ], 
  "STARTUPINFO*": [
    "STARTUPINFO", 
    "STARTUPINFO[]"
  ], 
  "LPRECTL": [
    "RECTL", 
    "RECTL[]"
  ], 
  "JOBOBJECT_BASIC_AND_IO_ACCOUNTING_INFORMATION": [
    "JOBOBJECT_BASIC_AND_IO_ACCOUNTING_INFORMATION"
  ], 
  "LPSYSTEM_POWER_STATUS": [
    "SYSTEM_POWER_STATUS", 
    "SYSTEM_POWER_STATUS[]"
  ], 
  "USER_OTHER_INFO**": [
    "PointerByReference"
  ], 
  "LPCRECT": [
    "RECT", 
    "RECT[]"
  ], 
  "TRACE_LOGFILE_HEADER**": [
    "PointerByReference"
  ], 
  "LPCFORM_INFO_1": [
    "FORM_INFO_1", 
    "FORM_INFO_1[]"
  ], 
  "JOB_INFO_4": [
    "JOB_INFO_4"
  ], 
  "PHCONV": [
    "HANDLEByReference"
  ], 
  "LPUSER_INFO_1024*": [
    "PointerByReference"
  ], 
  "JOB_INFO_1": [
    "JOB_INFO_1"
  ], 
  "LPGEOTYPE**": [
    "IntByReference", 
    "long[]"
  ], 
  "HRESULT**": [
    "PointerByReference"
  ], 
  "LPCRM_APP_TYPE": [
    "IntByReference", 
    "int[]"
  ], 
  "LPAVRT_PRIORITY*": [
    "PointerByReference"
  ], 
  "LPWER_DUMP_TYPE": [
    "IntByReference", 
    "int[]"
  ], 
  "PUSER_MODALS_INFO_1004*": [
    "PointerByReference"
  ], 
  "PUSER_INFO_24": [
    "USER_INFO_24", 
    "USER_INFO_24[]"
  ], 
  "USER_INFO_1051": [
    "USER_INFO_1051"
  ], 
  "HCRYPTHASH*": [
    "ULONG_PTRByReference"
  ], 
  "USER_INFO_1053": [
    "USER_INFO_1053"
  ], 
  "USER_INFO_1052": [
    "USER_INFO_1052"
  ], 
  "POWER_POLICY": [
    "POWER_POLICY"
  ], 
  "SERVICE_TRIGGER_INFO**": [
    "PointerByReference"
  ], 
  "BYTE*": [
    "byte", 
    "char[]", 
    "ByteByReference"
  ], 
  "PVOID*": [
    "PointerByReference"
  ], 
  "LPRECT*": [
    "PointerByReference"
  ], 
  "PHENHMETAFILE": [
    "HANDLEByReference"
  ], 
  "PWCRANGE*": [
    "PointerByReference"
  ], 
  "PDFS_TARGET_PRIORITY_CLASS": [
    "IntByReference", 
    "int[]"
  ], 
  "LPCPRINTER_NOTIFY_INFO_DATA": [
    "PRINTER_NOTIFY_INFO_DATA", 
    "PRINTER_NOTIFY_INFO_DATA[]"
  ], 
  "PALETTEENTRY": [
    "PALETTEENTRY"
  ], 
  "SHDESCRIPTIONID*": [
    "SHDESCRIPTIONID", 
    "SHDESCRIPTIONID[]"
  ], 
  "LPCPINFO*": [
    "PointerByReference"
  ], 
  "PWIN32_FIND_STREAM_DATA*": [
    "PointerByReference"
  ], 
  "LPCFOCUS_EVENT_RECORD": [
    "FOCUS_EVENT_RECORD", 
    "FOCUS_EVENT_RECORD[]"
  ], 
  "LPCPOINT": [
    "POINT", 
    "POINT[]"
  ], 
  "JOBOBJECT_RATE_CONTROL_TOLERANCE_INTERVAL*": [
    "IntByReference", 
    "int[]"
  ], 
  "FILE_INFO_3": [
    "FILE_INFO_3"
  ], 
  "PAXISINFO*": [
    "PointerByReference"
  ], 
  "USER_OTHER_INFO": [
    "USER_OTHER_INFO"
  ], 
  "LPCCOMMPROP": [
    "COMMPROP", 
    "COMMPROP[]"
  ], 
  "LPCSERVICE_SID_INFO": [
    "SERVICE_SID_INFO", 
    "SERVICE_SID_INFO[]"
  ], 
  "LPCDFS_TARGET_PRIORITY": [
    "DFS_TARGET_PRIORITY", 
    "DFS_TARGET_PRIORITY[]"
  ], 
  "LPUSER_MODALS_INFO_1005": [
    "USER_MODALS_INFO_1005", 
    "USER_MODALS_INFO_1005[]"
  ], 
  "PLOCALGROUP_USERS_INFO_0*": [
    "PointerByReference"
  ], 
  "PSYSTEM_POWER_LEVEL*": [
    "PointerByReference"
  ], 
  "LPLPCWSTR*": [
    "char"
  ], 
  "HDDEDATA*": [
    "HANDLEByReference"
  ], 
  "WKSTA_USER_INFO_0**": [
    "PointerByReference"
  ], 
  "PLOGFONT*": [
    "PointerByReference"
  ], 
  "LPCWSTR*": [
    "WString[]", 
    "PointerByReference"
  ], 
  "LPSHARE_INFO_1004*": [
    "PointerByReference"
  ], 
  "SHORT**": [
    "PointerByReference"
  ], 
  "LPUSER_INFO_20*": [
    "PointerByReference"
  ], 
  "PCSTR**": [
    "byte"
  ], 
  "int": [
    "int"
  ], 
  "LPCACCESSTIMEOUT": [
    "ACCESSTIMEOUT", 
    "ACCESSTIMEOUT[]"
  ], 
  "PSERVER_INFO_100": [
    "SERVER_INFO_100", 
    "SERVER_INFO_100[]"
  ], 
  "HGLOBAL*": [
    "HANDLEByReference"
  ], 
  "PSERVER_INFO_102": [
    "SERVER_INFO_102", 
    "SERVER_INFO_102[]"
  ], 
  "PSHSTOCKICONID": [
    "IntByReference", 
    "int[]"
  ], 
  "LPLPGEOCLASS**": [
    "PointerByReference"
  ], 
  "LPUINT32": [
    "IntByReference", 
    "int[]"
  ], 
  "PROCESS_MEMORY_COUNTERS_EX": [
    "PROCESS_MEMORY_COUNTERS_EX"
  ], 
  "LPCHEAP_INFORMATION_CLASS": [
    "IntByReference", 
    "int[]"
  ], 
  "PGROUP_INFO_1005": [
    "GROUP_INFO_1005", 
    "GROUP_INFO_1005[]"
  ], 
  "MSG": [
    "MSG"
  ], 
  "LPSERVICE_FAILURE_ACTIONS_FLAG": [
    "SERVICE_FAILURE_ACTIONS_FLAG", 
    "SERVICE_FAILURE_ACTIONS_FLAG[]"
  ], 
  "LPGEOCLASS**": [
    "IntByReference", 
    "long[]"
  ], 
  "ITEMIDLIST": [
    "ITEMIDLIST"
  ], 
  "DFS_INFO_9**": [
    "PointerByReference"
  ], 
  "MAT2**": [
    "PointerByReference"
  ], 
  "LONG_PTR": [
    "int", 
    "Pointer"
  ], 
  "PUINT8*": [
    "byte"
  ], 
  "INPUT_RECORD*": [
    "INPUT_RECORD", 
    "INPUT_RECORD[]"
  ], 
  "HWINSTA": [
    "HANDLE"
  ], 
  "LPPSAPI_WS_WATCH_INFORMATION": [
    "PSAPI_WS_WATCH_INFORMATION", 
    "PSAPI_WS_WATCH_INFORMATION[]"
  ], 
  "LOCALGROUP_INFO_1*": [
    "LOCALGROUP_INFO_1", 
    "LOCALGROUP_INFO_1[]"
  ], 
  "GEOID": [
    "int"
  ], 
  "NET_DISPLAY_USER*": [
    "NET_DISPLAY_USER", 
    "NET_DISPLAY_USER[]"
  ], 
  "PPSTR": [
    "byte"
  ], 
  "WINDOWINFO*": [
    "WINDOWINFO", 
    "WINDOWINFO[]"
  ], 
  "LPQUERY_SERVICE_LOCK_STATUS*": [
    "PointerByReference"
  ], 
  "GLOBAL_MACHINE_POWER_POLICY*": [
    "GLOBAL_MACHINE_POWER_POLICY", 
    "GLOBAL_MACHINE_POWER_POLICY[]"
  ], 
  "PTAPE_SET_MEDIA_PARAMETERS": [
    "TAPE_SET_MEDIA_PARAMETERS", 
    "TAPE_SET_MEDIA_PARAMETERS[]"
  ], 
  "LPSERVICE_FAILURE_ACTIONS*": [
    "PointerByReference"
  ], 
  "LPEXTENDED_NAME_FORMAT*": [
    "PointerByReference"
  ], 
  "PRAWINPUTDEVICELIST": [
    "RAWINPUTDEVICELIST", 
    "RAWINPUTDEVICELIST[]"
  ], 
  "PMONITOR_INFO_2*": [
    "PointerByReference"
  ], 
  "PPWSTR*": [
    "char"
  ], 
  "LPGRADIENT_RECT": [
    "GRADIENT_RECT", 
    "GRADIENT_RECT[]"
  ], 
  "DWORD_PTR*": [
    "IntByReference", 
    "long[]"
  ], 
  "BOOLEAN**": [
    "byte"
  ], 
  "PSHARE_INFO_503*": [
    "PointerByReference"
  ], 
  "LPSERVICE_STATUS_PROCESS": [
    "SERVICE_STATUS_PROCESS", 
    "SERVICE_STATUS_PROCESS[]"
  ], 
  "LPCTITLEBARINFO": [
    "TITLEBARINFO", 
    "TITLEBARINFO[]"
  ], 
  "PRIVILEGE_SET": [
    "PRIVILEGE_SET"
  ], 
  "LOGPALETTE*": [
    "LOGPALETTE", 
    "LOGPALETTE[]"
  ], 
  "LPCSYSTEMTIME": [
    "SYSTEMTIME", 
    "SYSTEMTIME[]"
  ], 
  "UINT8": [
    "byte"
  ], 
  "PUSER_INFO_1017*": [
    "PointerByReference"
  ], 
  "LPVALENT*": [
    "PointerByReference"
  ], 
  "PCONSOLE_READCONSOLE_CONTROL*": [
    "PointerByReference"
  ], 
  "PCONSOLE_CURSOR_INFO*": [
    "PointerByReference"
  ], 
  "LPPRINTER_INFO_9": [
    "PRINTER_INFO_9", 
    "PRINTER_INFO_9[]"
  ], 
  "LPPRINTER_INFO_6": [
    "PRINTER_INFO_6", 
    "PRINTER_INFO_6[]"
  ], 
  "LPPRINTER_INFO_7": [
    "PRINTER_INFO_7", 
    "PRINTER_INFO_7[]"
  ], 
  "PWSAPROTOCOL_INFO*": [
    "PointerByReference"
  ], 
  "LPSHARE_INFO_0": [
    "SHARE_INFO_0", 
    "SHARE_INFO_0[]"
  ], 
  "LPPRINTER_INFO_2": [
    "PRINTER_INFO_2", 
    "PRINTER_INFO_2[]"
  ], 
  "PUSE_INFO_2*": [
    "PointerByReference"
  ], 
  "LPPRINTER_INFO_1": [
    "PRINTER_INFO_1", 
    "PRINTER_INFO_1[]"
  ], 
  "WPARAM": [
    "int", 
    "long"
  ], 
  "PTAPE_GET_MEDIA_PARAMETERS*": [
    "PointerByReference"
  ], 
  "PSECURITY_CONTEXT_TRACKING_MODE*": [
    "byte"
  ], 
  "EXECUTION_STATE": [
    "int"
  ], 
  "HIGHCONTRAST": [
    "HIGHCONTRAST"
  ], 
  "MediaLabelInfo*": [
    "MediaLabelInfo", 
    "MediaLabelInfo[]"
  ], 
  "PHEAPLIST32": [
    "HEAPLIST32", 
    "HEAPLIST32[]"
  ], 
  "PUSER_INFO_20*": [
    "PointerByReference"
  ], 
  "SHARE_INFO_1501**": [
    "PointerByReference"
  ], 
  "LPCCHAR": [
    "byte"
  ], 
  "LPCDFS_INFO_101": [
    "DFS_INFO_101", 
    "DFS_INFO_101[]"
  ], 
  "LPCAXESLIST": [
    "AXESLIST", 
    "AXESLIST[]"
  ], 
  "PSESSION_INFO_10*": [
    "PointerByReference"
  ], 
  "PTRACE_LOGFILE_HEADER*": [
    "PointerByReference"
  ], 
  "SERVICE_STATUS_PROCESS*": [
    "SERVICE_STATUS_PROCESS", 
    "SERVICE_STATUS_PROCESS[]"
  ], 
  "LPDFS_INFO_7*": [
    "PointerByReference"
  ], 
  "PFLOAT": [
    "FloatByReference", 
    "float[]"
  ], 
  "SESSION_INFO_502*": [
    "SESSION_INFO_502", 
    "SESSION_INFO_502[]"
  ], 
  "RAWINPUTDEVICE": [
    "RAWINPUTDEVICE"
  ], 
  "USER_INFO_1052*": [
    "USER_INFO_1052", 
    "USER_INFO_1052[]"
  ], 
  "LPCBT_CREATEWND*": [
    "PointerByReference"
  ], 
  "LPALTTABINFO": [
    "ALTTABINFO", 
    "ALTTABINFO[]"
  ], 
  "PMONITORINFOEX": [
    "MONITORINFOEX", 
    "MONITORINFOEX[]"
  ]
}