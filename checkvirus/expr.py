from os import path
from PySystem import SystemUtl

def FindUnsAPIinLog():
    fpath   = path.split(path.dirname(__file__))[0] + '\\DBSource\\BEPUM-YenAPIStub\\'
    yen_folders = SystemUtl.GetSubFolder(fpath)
    yen_apistubs= []
    for folder in yen_folders:
        files = SystemUtl.GetFiles(fpath+folder+'\\')
        for item in files:
            yen_apistubs.append(item.replace('.java',''))
    #print yen_apistubs
    
    fpath   = path.split(path.dirname(__file__))[0] + '\\checkvirus\\logs\\'
    log_files = SystemUtl.GetFiles(fpath)
    ret     = {} # pair of Virus name - New supported APIs
    for f in log_files:
        fin = open(fpath+f,'r')
        ret[f] = []
        for line in fin:
            if line.startswith('Call API:'): 
                api = line.replace('\n','').split(':')[1]
                if api.endswith('A') or api.endswith('W'): api = api[:-1]
                if not (api in yen_apistubs):
                    ret[f].append(api)
        ret[f] = set(ret[f])
    
    for log in ret:
        print log
        for item in ret[log]: 
            print item,
        print
        
    #print log_files
FindUnsAPIinLog()