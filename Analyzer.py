# -*- coding: utf-8 -*-
'''
@Author : LE VINH
@Project: BE-PUM, API Stub generation
@Time   : JAIST (2015-2016)
@Summary: Review API specification (add buffer - buffer length parameter specification)
'''

from os import path
from PySystem import SystemUtl


import collections, json

# Find APIs has < 3 parameters, but has buffer pointer (e.g., FindFirstVolume, GetConsoleTitle, GetCurrentDirectory
def FindAPIsForPresentation():
    # Load data & create classifer mode
    fin     = open("../DBSource/DataLv2/ms_api_review.json")
    ms_apis = collections.OrderedDict()
    for line in fin:
        api = json.JSONDecoder(object_pairs_hook=collections.OrderedDict).decode(line)
        ms_apis[api["fname"]] = api
    fin.close()
    
    count = 0;
    for fname in ms_apis:
        api = ms_apis[fname]
        if len(api['fargs']) <= 3:
            if len(api['relation']) > 0:
                print fname
                count += 1
    print count
#FindAPIsForPresentation()

# Tim cac API stubs trong cai cu ma cai moi khong sinh ra duoc
def analysis():
    fpath   = path.split(path.dirname(__file__))[0] + '\\pywin32api\\DBSource\\'
    
    yen_folders = SystemUtl.GetSubFolder(fpath+'BEPUM-YenAPIStub\\')    
    yen_apistubs= []
    for folder in yen_folders:
        files = SystemUtl.GetFiles(fpath + 'BEPUM-YenAPIStub\\' + folder + '\\')
        for item in files:
            yen_apistubs.append(item)    
    print 'Yen API Stub: ', len(yen_apistubs)
    
    vinh_folders = SystemUtl.GetSubFolder(fpath+'BEPUM-VinhAPIStub\\')    
    vinh_apistubs= []
    for folder in vinh_folders:
        files = SystemUtl.GetFiles(fpath + 'BEPUM-VinhAPIStub\\' + folder + '\\functions\\')
        for item in files:
            vinh_apistubs.append(item)    
    print 'Vinh API Stub: ', len(vinh_apistubs)
    
    noHTML_apis = "GetAclInformation, RegisterWowExec, RtlUnwind, RtlZeroMemory, SetHandleCount, wsprintf, _lclose, _lcreat, _llseek, _lopen, atexit, calloc, fclose, fopen, fprintf, fread, free, fwrite, isalpha, is_wctype, malloc, memcpy, memmove, memset, puts, remove, srand, strncat, strncmp, strrchr, strstr, time, _assert, _cexit, _chkesp, _controlfp, _except_handler3, _fpreset, _initterm, _itoa, _itow, _ltoa, _mbsrchr, _setmode, _strcmpi, __getmainargs, __p__commode, __p__environ, __p__fmode, __p___initenv, __set_app_type, NtCreateSection, NtProtectVirtualMemory, NtQueryInformationProcess, NtWriteVirtualMemory, CoCreateGuid, DialogBoxIndirectParam, DialogBoxParam, IsDialogMessage, HttpSendRequest";
    noHTML_apis = noHTML_apis.split(', ')
    noHTML_apis = [item+'.java' for item in noHTML_apis]
    print 'No HTML APIs: ', len(noHTML_apis)
    
    count = 0 # fail in automatic but success in manual
    for item in yen_apistubs:
        if not (item in vinh_apistubs):
            if not (item in noHTML_apis):
                print item.replace('.java','') + ',',
                count += 1
    print '\n', count
analysis()

# Find APIs has < 3 parameters, but has buffer pointer (e.g., FindFirstVolume, GetConsoleTitle, GetCurrentDirectory
def FindAPIsForPresentation2():
    # Load data & create classifer mode
    fin     = open("DBSource/DataLv2/ms_api_review.json")
    ms_apis = collections.OrderedDict()
    for line in fin:
        api = json.JSONDecoder(object_pairs_hook=collections.OrderedDict).decode(line)
        ms_apis[api["fname"]] = api
    fin.close()
    
    count = 0;
    for fname in ms_apis:
        api = ms_apis[fname]
        if len(api['fargs']) == 0:
                print fname
                count += 1
    print count
#FindAPIsForPresentation2()