# -*- coding: utf-8 -*-
'''
@Author : LE VINH
@Project: BE-PUM, API Stub generation
@Time   : JAIST (2015-2016)
@Summary: API stub generation
@deprecated: Move to the project 'WinAPI GenStub' (Java)
'''

from os import path
import json, collections

# Initial Code template from file
def LoadTemplate():
    fin = open('template/APIstub.tmpt','r')
    tmp = ''
    template = []
    for line in fin:
        if not line.startswith('/*'):
            if len(line)>1: tmp += line                
        else:
            if len(tmp)>0:  template.append(tmp)
            tmp = ''
    template.append(tmp)

    # Code generation: String format with placeholder in Python
    # http://stackoverflow.com/questions/30646697/replace-placeholder-tags-with-dictionary-fields-in-python 
    apistub = template[0].expandtabs(4)   # Main API stub program 
    part1   = template[1].expandtabs(4)   # Step 1: get original parameter values from stack - CODE
    part2   = template[2].expandtabs(4)   # Step 2: type conversion from C++ to Java - CODE
    part3   = template[3].expandtabs(4)   # Step 3: call API function - CODE
    part4   = template[4].expandtabs(4)   # Step 4: update environment (memory & eax register) - CODE
    return apistub, part1, part2, part3, part4
    
# Generate API Stub
def GenerateAPIStub(fname):
    # Load API features
    fpath = path.split(path.dirname(__file__))[0] + '\\DBSource\\'
    fin1  = open(fpath+'DataLv1\\ms_api.json', 'r')
    ms_apis = collections.OrderedDict()
    for line in fin1:
        api = json.JSONDecoder(object_pairs_hook=collections.OrderedDict).decode(line)
        ms_apis[api["fname"]] = api
    flib = ms_apis[fname]['flib'].title()
    fargs_num = len(ms_apis[fname]['fargs'])
    args_list_name = ', '.join([p['name'] for p in ms_apis[fname]['fargs']])
    
    # Load type mapping table
    fin2  = open(fpath+'DataLv2\\type_table.ls', 'r')
    type_table = json.load(fin2) 
    
    # Load code template
    apistub, tpart1, tpart2, tpart3, tpart4 = LoadTemplate()    
    part1 = ''
    for idx, item in enumerate(ms_apis[fname]['fargs']): part1 += tpart1.format(index=idx)
    
    part2 = ''
    for idx, item in enumerate(ms_apis[fname]['fargs']):
        part2 += tpart2.format(type=type_table[item['type']][0], name=item['name'], index=idx)
    
    part3 = ''
    part3 = tpart3.format(flib=flib, fname=fname, para_list=args_list_name)
    
    part4 = tpart4
    
    placeholders = {'fname':fname, 'flib':flib, 'fargs_num':fargs_num,
                    'part1':part1, 'part2':part2, 'part3':part3, 'part4':part4}
    print apistub.format(**placeholders)
    
GenerateAPIStub('GetDateFormat')     
Java_ = '''
public class GetDateFormat extends Kernel32API {
    public GetDateFormat() { super(); NUM_OF_PARMS = 6; }
    @Override
    public void execute() {
        long t1 = this.params.get(0); long t2 = this.params.get(1);
        long t3 = this.params.get(2); long t4 = this.params.get(3);
        long t5 = this.params.get(4); long t6 = this.params.get(5);
        
        LCID Locale = new LCID(t1); DWORD dwFlags  = new DWORD(t2);
        SYSTEMTIME lpDate = null; WString lpFormat = null;
        char[] lpDateStr  = null; int cchDate      = (int) t6;
        
        if (t3 != 0L) {
            lpDate = new SYSTEMTIME();
            lpDate.wYear  = (short)((LongValue)memory.getWordMemoryValue(t3)).getValue();
            lpDate.wMonth = (short)((LongValue)memory.getWordMemoryValue(t3 += 2)).getValue();
            lpDate.wDayOfWeek = (short)((LongValue)memory.getWordMemoryValue(t3 += 2)).getValue();
            lpDate.wDay   = (short)((LongValue)memory.getWordMemoryValue(t3 += 2)).getValue();
            lpDate.wHour  = (short)((LongValue)memory.getWordMemoryValue(t3 += 2)).getValue();
            lpDate.wMinute= (short)((LongValue)memory.getWordMemoryValue(t3 += 2)).getValue();
            lpDate.wSecond= (short)((LongValue)memory.getWordMemoryValue(t3 += 2)).getValue();
            lpDate.wMilliseconds = (short)((LongValue)memory.getWordMemoryValue(t3 += 2)).getValue();
        } else { lpDate = null; }        
        if (t4==0L) {lpFormat = null;} else {lpFormat = new WString(memory.getText(this,t4));}
        if (t5==0L) {lpDateStr = null;} else {lpDateStr = new char[(int) t6 + 1];}

        int ret = Kernel32DLL.INSTANCE.
                  GetDateFormatW(Locale, dwFlags, lpDate, lpFormat, lpDateStr, cchDate);
        if (t5!=0L && cchDate!=0) {memory.setText(this, t5, new String(lpDateStr), ret);}
        register.mov("eax", new LongValue(ret));        
    }
}
'''