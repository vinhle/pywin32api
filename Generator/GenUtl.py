# -*- coding: utf-8 -*-
'''
@Author : LE VINH
@Project: BE-PUM, API Stub generation
@Time   : JAIST (2015-2016)
@Summary: Utilities for generation purpose
'''

from os import path
from PySystem import SystemUtl
from Extractor import StructExtractor

# Find the nested definition of Structures
def FindNestedDefinition():
    fpath   = path.split(path.dirname(__file__))[0] + '\\DBSource\\MS-Struct\\'
    structs = SystemUtl.GetFiles(fpath)
    defins  = [StructExtractor.ExtractStruct(item) for item in structs]    
    count = 0
    for item in defins:
        num_struct = item.count('struct')
        num_enum   = item.count('enum')
        num_union  = item.count('union')
        if num_struct + num_enum + num_union >= 2:
            count += 1
            print item
    print 'Nested Structure: ', count
#FindNestedDefinition()