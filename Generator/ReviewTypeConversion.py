# -*- coding: utf-8 -*-
'''
@Author : LE VINH
@Project: BE-PUM, API Stub generation
@Time   : JAIST (2015-2016)
@Summary: Review type mapping rules -> a suitable json format for the project in Java
'''

import json, collections
from os import path
from Crawler import CrawlerUtl
from PySystem import SystemUtl


# Create Map from simple Type's name -> full Type's name (classes supported by JNA)
def JNAClassMap():
    classes = collections.OrderedDict()
    fpath   = path.split(path.dirname(__file__))[0] + '\\DBSource\\JNA-HTML\\UsefulPage\\All Classes (JNA API).html'
    html    = CrawlerUtl.GetHTMLTree(fpath, 'offline')
    lists   = html.find_all('li')
    for item in lists:
        a = item.find('a')
        href = a['href'].strip().replace('.html','').replace('.','/').split('/')
        className = href[-1]
        classPath = '.'.join(href)
        classes[className] = classPath
    #for item in classes: print item, classes[item]
    print len(classes)
    
    fpath   = path.split(path.dirname(__file__))[0] + '\\DBSource\\DataLv2\\'
    with open(fpath+'jna_classes.json', 'w') as fout:
        json.dump(classes, fout, indent=2)
#JNAClassMap()


# Change format DataLv2/type_table.json -> DataLv2/type_table2.json format in WinAPI GenStub project (Java)
def ReviewTypeMap():
    type_table_review = []
    remain_types = []       # may be ambiguity cases
    
    fpath = path.split(path.dirname(__file__))[0] + '\\DBSource\\'    
    fin1  = open(fpath+'DataLv2\\type_table.ls', 'r')
    fin2  = open(fpath+'DataLv2\\jna_classes.json', 'r') 
    type_table  = json.load(fin1)
    jna_classes = json.load(fin2)
    
    structs = SystemUtl.GetFiles(fpath+'MS-Struct\\')
    structs = [s.replace('.html','') for s in structs]
    
    for item in type_table:
        type_table_item   = {}
        # Case 1: One mapping C -> Java (about 2200 types)
        if len(type_table[item]) == 1:
            type_table_item['source'] = item
            type_table_item['target'] = type_table[item][0]
            if type_table_item['target'] in jna_classes:
                type_table_item['classPath'] = jna_classes[type_table_item['target']]
            elif type_table_item['target'] in structs:
                type_table_item['classPath'] = 'v2.org.analysis.apihandle.structures.' + type_table_item['target']           
            type_table_review.append(type_table_item)
            continue
        
        # Case 2: Pointer mapping C -> Java (LPABC -> ABC | ABC[] (about 1900 types)
        if (len(type_table[item])==2) and (type_table[item][0]+'[]'==type_table[item][1]):
            type_table_item['source'] = item
            type_table_item['target'] = type_table[item][0]
            type_table_item['other']  = type_table[item][1]
            if type_table_item['target'] in jna_classes:
                type_table_item['classPath'] = jna_classes[type_table_item['target']]
            elif type_table_item['target'] in structs:
                type_table_item['classPath'] = 'v2.org.analysis.apihandle.structures.' + type_table_item['target']           
            type_table_review.append(type_table_item)
            continue
        
        # Case 3: Primitives pointer C -> Java (xxxByReference, xxx[]: IntByReference, int[]); about 188 types
        if (len(type_table[item])>=2) and (type_table[item][0].endswith('ByReference')):
            type_table_item['source'] = item
            type_table_item['target'] = type_table[item][0]
            type_table_item['other']  = ', '.join(type_table[item][1:])
            if type_table_item['target'] in jna_classes:
                type_table_item['classPath'] = jna_classes[type_table_item['target']]
            type_table_review.append(type_table_item)
            continue
         
        # Case 4: String (LPCSTR, LPCTSTR, LPTSTR)
        if item.endswith("STR"):
            type_table_item['source'] = item
            if item.endswith("WSTR"):
                type_table_item['target'] = "WString"
                type_table_item['classPath'] = "com.sun.jna.WString"
            else:
                type_table_item['target'] = "String"
                type_table_item['classPath'] = "java.lang.String"
            type_table_review.append(type_table_item)
            continue
        
        # Case 5: void pointer, equivalent type mapping (int=DWORD) 42 types; correct by the following method (mannual check)     
        remain_types.append(item)    
    for item in remain_types:
        print item, type_table[item]
        type_table_item = {}
        type_table_item['source'] = item
        type_table_item['target'] = type_table[item][0]
        type_table_item['other']  = ', '.join(type_table[item][1:])
        if type_table_item['target'] in jna_classes:
                type_table_item['classPath'] = jna_classes[type_table_item['target']]
        type_table_review.append(type_table_item)
    
    # Make table Map become more optimization, old(DWORD->int) -> now(DWORD->DWORD): 61 cases
    for item in type_table_review:
        if item['source'] in jna_classes:
            if item['target'] != item['source']:
                item['target'] = item['source']
                item['classPath'] = jna_classes[item['source']]
    
    # Remove parameter is callback function, void pointer
    for item in type_table_review:
        if item['target'] == 'PRTL_CRITICAL_SECTION':
            type_table_review.remove(item)
        if item['target'] == 'Callback':
            type_table_review.remove(item)
    
        if item['source'] == 'void*':
            type_table_review.remove(item)
        if item['source'] == 'VOID*':
            type_table_review.remove(item)
        if item['source'] == 'PVOID':
            type_table_review.remove(item)
        if item['source'] == 'LPVOID':
            type_table_review.remove(item)
    
    #for item in type_table_review: print item
    #for item in remain_types: print item, type_table[item]
    print len(type_table_review)
    print len(remain_types)
    
    with open(fpath+'\\DataLv2\\type_table_review.json', 'w') as fout:
        json.dump(type_table_review, fout, indent=2) #indent: add new line
    
    return type_table_review
    
ReviewTypeMap()