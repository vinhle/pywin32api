# -*- coding: utf-8 -*-
'''
@Author : LE VINH
@Project: BE-PUM, API Stub generation
@Time   : JAIST (2015-2016)
@Summary: Build a table of rules used to infer Java type from C type
'''

import networkx as nx
import re, json, collections
from os import path
from PySystem import SystemUtl

# Build Total Type Conversion table: Type C -> Type Java
def ConstructTypeGraph():
    G = nx.DiGraph()
    fpath = path.split(path.split(path.dirname(__file__))[0])[0] + '\\DBSource\\'
    
    ''' Structure and their variants '''    
    structs = SystemUtl.GetFiles(fpath+'MS-Struct\\')
    structs = [s.replace('.html','') for s in structs]
    for item in structs:
        # level 0 - itself, e.g: SYSTEMTIME
        G.add_node(item, map=[item])
        # level 1 - pointer, e.g: SYSTEMTIME*, PSYSTEMTIME, LPSYSTEMTIME
        G.add_nodes_from([item+'*','P'+item,'LP'+item,'LPC'+item], map=[item, item+'[]'])
        G.add_edges_from([(item,item+'*'), (item,'P'+item), (item,'LP'+item), (item,'LPC'+item)], weight=1)
        # level 2 - pointer of pointer, e.g: SYSTEMTIME**, PSYSTEMTIME*, LPSYSTEMTIME*
        G.add_nodes_from([item+'**','P'+item+'*','LP'+item+'*'], map=['PointerByReference'])
        G.add_edges_from([(item,item+'**'), (item,'P'+item+'*'), (item,'LP'+item+'*')], weight=2)
    
    ''' List all enum types '''
    fin3 = open(fpath+'DataLv1\\ms_enum.ls', 'r')
    for item in fin3:
        item = item.strip()
        G.add_node(item, map=['int'])
        G.add_edge(item, 'int', weight=0)
        G.add_nodes_from([item+'*','P'+item,'LP'+item,'LPC'+item], map=['IntByReference','int[]']);
        G.add_edges_from([(item,item+'*'), (item,'P'+item), (item,'LP'+item), (item,'LPC'+item)], weight=1)
        G.add_nodes_from([item+'**','P'+item+'*','LP'+item+'*'], map=['PointerByReference'])
        G.add_edges_from([(item,item+'**'), (item,'P'+item+'*'), (item,'LP'+item+'*')], weight=2)
        
    ''' Primitive types and their variants '''
    pri_C_types      = ['char', 'short', 'wchar_t', 'int', 'long', 'long long', '__int64', 'float', 'double']
    # long -> int or NativeLong
    pri_java_types   = ['byte', 'short', 'char', 'int', 'int', 'long', 'long', 'float', 'double']
    pri_JNA_pointers = ['ByteByReference', 'ShortByReference', 'CHARByReference', 'IntByReference','IntByReference',
                        'LongByReference', 'LongByReference', 'FloatByReference', 'DoubleByReference']
    for i in range(len(pri_C_types)):
        # level 0 - itself
        G.add_node(pri_C_types[i], map=[pri_java_types[i]])
        # level 1 - pointer, e.g: int*
        G.add_node(pri_C_types[i]+'*', map=[pri_JNA_pointers[i], pri_C_types[i]+'[]'])
        G.add_edges_from([(pri_C_types[i],pri_C_types[i]+'*')], weight=1)
        # level 2 - pointer of pointer, e.g:int**
        G.add_node(pri_C_types[i]+'**', map=['PointerByReference'])
        G.add_edges_from([(pri_C_types[i],pri_C_types[i]+'**')], weight=2)
    G.node['char*']['map'].append('String')
    G.node['wchar_t*']['map'].append('WString')
    
    ''' Special cases '''
    special_types = ['HANDLE', 'LPVOID']
    G.add_node('HANDLE', map=['HANDLE'])
    G.add_nodes_from(['HANDLE*','PHANDLE','LPHANLDE'], map=['HANDLEByReference'])
    G.add_edges_from([('HANDLE','HANDLE*'), ('HANDLE','PHANDLE'), ('HANDLE','LPHANDLE')], weight=1)
    
    G.add_nodes_from(['void*','VOID*','PVOID','LPVOID','PVOID64'], map=['Pointer', 'LPVOID'])
    G.add_nodes_from(['LPCSTR','LPSTR','PCSTR','PSTR'], map=G.node['char*']['map'])
    G.add_edges_from([('char','LPCSTR'),('char','LPSTR'),('char','PCSTR'),('char','PSTR')], weight=1)
    G.add_nodes_from(['LPCWSTR','LPWSTR','PCWSTR','PWSTR', 'PCTSTR','PTSTR','PZZWSTR'], map=G.node['wchar_t*']['map'])
    G.add_edges_from([('wchar_t','LPCWSTR'),('wchar_t','LPWSTR'),('wchar_t','PCWSTR'),('wchar_t','PWSTR'), 
                      ('wchar_t','PCTSTR'), ('wchar_t','PTSTR'), ('wchar_t','PZZWSTR')], weight=1)

    ''' Renaming type from Window Data Type Reference '''
    fin1      = open(fpath+'DataLv1\\ms_def_wintype.ls', 'r')
    win_types = [line.strip('\n') for line in fin1 if line.startswith('typedef')]
    win_types = [re.sub('(typedef|far|unsigned|signed|const|CONST|__nullterminated|;)', '', line) for line in win_types]
    win_types = [item.strip().split() for item in win_types] # win_type0 win_type1
    #print win_types
    for item in win_types:
        if not item[1].startswith('*'): #e.g. int DWORD, but not 'DWORD *PDWORD'
            G.add_edge(item[0], item[1], weight=0)
            G.add_edge(item[1], item[0], weight=0)
    #print G.edge['char']

    ''' Graph-tuning: all equavalent types should be connected directly with thier original type '''
    for item in pri_C_types + special_types:
        reachable_nodes = nx.single_source_shortest_path(G, item).keys()
        if item == 'HANDLE':
            rename_types = set(reachable_nodes) - set(['HANDLE','HANDLE*','PHANDLE','LPHANLDE'])
        elif item == 'char':
            rename_types = set(reachable_nodes) - set([item, 'LPCSTR','LPSTR','PCSTR','PSTR'])
        elif item == 'wchar_t':
            rename_types = set(reachable_nodes) - set([item, 'LPCWSTR','LPWSTR','PCWSTR','PWSTR','PCTSTR','PTSTR','PZZWSTR'])
        else:
            rename_types = set(reachable_nodes) - set([item,item+'*',item+'**'])
        for tmp in rename_types:
            # update map for renaming types
            if tmp in G.edge[item]:
                G.node[tmp]['map'] = G.node[item]['map']
            # if tmp is not directed child of item -> remove it, and add it again as directed child of item
            else:
                G.remove_node(tmp)
                G.add_node(tmp, map=G.node[item]['map'])
                G.add_edge(item, tmp, weight=0)
                G.add_edge(tmp, item, weight=0)                
    #print nx.single_source_shortest_path(G, 'char')

    ''' Generate pointer level-1, level-2 for renaming types '''
    for item in pri_C_types + ['HANDLE']:
        if item == 'HANDLE':
            rename_types = set(G.successors(item)) - set(['HANDLE*','PHANDLE','LPHANLDE'])
            for tmp in rename_types:
                G.add_nodes_from([tmp+'*', 'P'+tmp, 'LP'+tmp], map=G.node[item+'*']['map'])
                G.add_edges_from([(tmp,tmp+'*'), (tmp,'P'+tmp), (tmp,'LP'+tmp)], weight=1)
        else:
            rename_types = set(G.successors(item)) - set([item+'*',item+'**'])
            for tmp in rename_types:
                G.add_nodes_from([tmp+'*', 'P'+tmp, 'LP'+tmp], map=G.node[item+'*']['map'])
                G.add_edges_from([(tmp,tmp+'*'), (tmp,'P'+tmp), (tmp,'LP'+tmp)], weight=1)
                G.add_nodes_from([tmp+'**', 'P'+tmp+'*', 'LP'+tmp+'*'], map=G.node[item+'**']['map'])
                G.add_edges_from([(tmp,tmp+'**'), (tmp,'P'+tmp+'*'), (tmp,'LP'+tmp+'*')], weight=2)
    G.add_nodes_from([ 'LPCSTR*','LPSTR*','PCSTR*','PSTR*'], map=['String[]','PointerByReference'])
    G.add_nodes_from([ 'LPCWSTR*','LPWSTR*','PCWSTR*','PWSTR*','PCTSTR*','PTSTR*','PZZWSTR*'], map=['WString[]','PointerByReference'])
    #print nx.single_source_shortest_path(G, 'char')
    #for item in G.nodes(): if not 'map' in G.node[item]: print item

    ''' Learn from observed data types '''
    fin2  = open(fpath+'DataLv1\\msjna_type_map.ls', 'r')
    nodes = G.nodes() # list of type names
    msjna_type_map = json.load(fin2)    
    #print 'Types do not appear by rule, but can learn from database, such as PHKEY, unknown struct'
    for item in msjna_type_map:
        if item in nodes:
            #G.node[item]['map'] += msjna_type_map[item]   -> Khong hieu sao cho nay lai sai  
            '''
            print G.node["PLCID"]['map']
            print G.node["PDWORD"]['map']
            G.node["PDWORD"]['map'] = G.node["PDWORD"]['map'] + ['vinh'] -> update only node "PDWORD" 
            G.node["PDWORD"]['map'] += ['vinh'] -> update both node "PDWORD" and "PLCID"
            G.node["PLCID"]['map'] += ['abac']
            print G.node["PLCID"]['map']
            print G.node["PDWORD"]['map']
            return'''
            for tmp in msjna_type_map[item]: # remove duplicate from observered data, e.g. WinNT.ABC 
                if (not '.' in tmp) and (not tmp in G.node[item]['map']):
                    G.node[item]['map'] = G.node[item]['map'] + [tmp]
        else:
            if item == "PLCID": print 'v', item 
            G.add_node(item, map=msjna_type_map[item])
    
    ''' Exception types '''
    G.add_node('SIZEL', map=G.node['SIZE']['map'])
    G.add_node('NTMS_GUID', map=G.node['GUID']['map'])          

    ''' Write to type_table.ls and Print to screen '''
    print 'Total supported types:', len(G.nodes())
    type_table = {}
    for n in G.nodes():
        #print n, G.node[n]['map']
        try:
            type_table[n] = G.node[n]['map']
        except:
            print n, G.node[n]            
            
    with open(fpath+'\\DataLv2\\type_table.ls', 'w') as fout:
        json.dump(type_table, fout, indent=2) #indent: add new line
    
    return G
#G = ConstructTypeGraph()

''' Update the value mapping types in one node -> equivalent nodes are updated -> easy to keep consistence '''
def UpdateTypeGraph(G, node, map_value):
    #https://msdn.microsoft.com/en-us/library/dexter.functioncatall.pd(v=vs.90).aspx
    ''' Update type mapping:
    type mapping in current node <-> type mapping in nodes 
                                        which their distance from them to current-node's parent =
                                        the distance from current node to its parent
    '''
    G.node[node]['map'] = map_value # update type mapping in current node
    parents = G.predecessors(node)  # return a list parents of item
    if len(parents) != 0:            
        for pa in parents:
            distance = G.edge[pa][node]['weight']   # distance from current node to its parent
            reachable_path = nx.single_source_dijkstra(G,pa)
            reachable_len  = reachable_path[0]      # distance from other nodes to current's parent node
            for n in reachable_len:
                if reachable_len[n] == distance:
                    # update type mapping in level-equal nodes
                    G.node[n]['map'] = G.node[node]['map']

#UpdateTypeGraph(G, 'PLCID', 'vinh')
#print G.node['PDWORD'], G.node['long*']

# Get unsupported type
def GetUnsupportType():
    fpath            = path.split(path.split(path.dirname(__file__))[0])[0] + '\\DBSource\\'
    fin1             = open(fpath+'DataLv1\\msjna_unsup_type.ls', 'r')
    msjna_unsup_type = [line.strip('\n') for line in fin1]
    
    fin2       = open(fpath+'DataLv2\\type_table.ls', 'r')
    type_table = json.load(fin2)
    
    chuoi = ''
    for item in msjna_unsup_type:
        if not item in type_table:
            #print item
            chuoi += item + '\n'
    print 'There are %d unsupported types' % chuoi.count('\n')
    open(fpath+'DataLv2\unsup_type.ls', 'w').write(chuoi)
#GetUnsupportType()

def GetAPIContainUnsupportStruct():
    fpath = path.split(path.split(path.dirname(__file__))[0])[0] + '\\DBSource\\'
    
    # Load a list of unsupported Struct
    fin1 = open(fpath + 'DataLv2\\unsup_type.ls')
    un_structs = [line.strip('\n') for line in fin1]
    # Load a list of API
    fin2 = open(fpath + 'DataLv1\\ms_api.json', 'r')
    ms_apis = collections.OrderedDict()
    for line in fin2:
        api = json.JSONDecoder(object_pairs_hook=collections.OrderedDict).decode(line)
        ms_apis[api["fname"]] = api
        
    # list which apis contain unsupported Struct
    un_apis = []
    for api in ms_apis:
        for para in ms_apis[api]['fargs']:
            if para['type'] in un_structs: un_apis.append(api)
    print len(un_apis)
    return un_apis
#GetAPIContainUnsupportStruct()

# There is no WinAPI that have callback function as argument was implemented in JNA or from Yen-san
# There is unsupported types which don't appear in implemented API
def ProveNoCallbackInJNA():
    fpath = path.split(path.split(path.dirname(__file__))[0])[0] + '\\DBSource\\' 
    fin1  = open(fpath+'DataLv1\\msjna_type_map.ls', 'r')
    msjna_type_map = json.load(fin1)
    
    fin2  = open(fpath+'DataLv2\\unsup_type.ls', 'r')
    unsup_type = [line.strip('\n') for line in fin2]
    
    for item in unsup_type:
        if item in msjna_type_map: print item
#ProveNoCallbackInJNA()