# -*- coding: utf-8 -*-
'''
@Author : LE VINH
@Project: BE-PUM, API Stub generation
@Time   : JAIST (2015-2016)
@Summary: Type transformation
'''

import json
from os import path
from PySystem import SystemUtl
from Extractor import StructExtractor
from Generator.TypeMap import StructureTransformer, UnionTransformer, EnumTransformer

CType1 = '''
typedef struct tagINPUT {
  DWORD type;
  union {
    MOUSEINPUT    mi;
    KEYBDINPUT    ki;
    HARDWAREINPUT hi;
  };
} INPUT, *PINPUT;
'''

CType2 = '''
typedef struct _EVENT_INSTANCE_HEADER {
  USHORT        Size;
  union {
    USHORT FieldTypeFlags;
    struct {
      UCHAR HeaderType;
      UCHAR MarkerFlags;
    };
  };
  union {
    ULONG  Version;
    struct {
      UCHAR  Type;
      UCHAR  Level;
      USHORT Version;
    } Class;
  };
  ULONG         ThreadId;
  ULONG         ProcessId;
  LARGE_INTEGER TimeStamp;
  ULONGLONG     RegHandle;
  ULONG         InstanceId;
  ULONG         ParentInstanceId;
  union {
    struct {
      ULONG ClientContext;
      ULONG Flags;
    };
    struct {
      ULONG KernelTime;
      ULONG UserTime;
    };
    ULONG64 ProcessorTime;
  };
  ULONGLONG     ParentRegHandle;
} EVENT_INSTANCE_HEADER;
'''
CType3 = '''
typedef struct _CHAR_INFO {
  union {
    WCHAR UnicodeChar;
    CHAR  AsciiChar;
  } Char;
  WORD  Attributes;
} CHAR_INFO, *PCHAR_INFO;
'''

''' Check wether type definition including nested type difinition 
@return: definition without nested definition, and separated nested difinition '''
def SeparateTypeDefinition(CType):
    CType = CType.strip().split('\n')
    try:
        tname = CType[-1].split()[1][:-1] # struct name
    except IndexError:
        print 'It is not typedef, only declare a type'
        tname = CType[0].split()[1]
    
    original_type = [CType[0]]
    nested_types  = []  # list nested type difinitions
    isNested = 0        # flag is show wether definition including nested-type
    stack    = []       # check blance brackets -> detect nested difinition
    nfield   = 0        # the number of fields 
    tmp = ''
    
    for line in CType[1:-1]:
        if ('union ' in line) or ('struct ' in line) or ('enum ' in line): isNested = 1
        if isNested == 1:
            tmp += line+'\n'
            if '{' in line: stack.append(1)
            if '}' in line: stack.pop()
            if len(stack) == 0:
                nfield += 1
                fletter = tmp.strip()[0].upper() # the first letter of nested type definition
                nestedTypeName = tname+'_'+fletter+str(nfield) # nested type_name = current type_name + _[S|U|E] + position of field
                original_type.append(nestedTypeName + ' tmp'+str(nfield)+';')
                nested_types.append('\n'.join(tmp.strip().split('\n')[:-1]) + '\n} '+nestedTypeName+';')               
                #nested_types.append(tmp.strip()[:-1] + ' '+nestedTypeName+';')
                isNested = 0; tmp = '' # reset tmp string and flag is Nested
        else:
            nfield += 1
            original_type.append(line)
    original_type.append(CType[-1])
    original_type = '\n'.join(original_type)
    #print original_type
    #for item in nested_types: print item
    return original_type, nested_types
#SeparateTypeDefinition(CType2)

# Transform Type definition in C -> Class definition in Java-JNA '''
def TransformType(in_code):
    in_code = in_code.strip()
    first_line = in_code.split('\n')[0]
    
    # Seperated nested definition
    original_type, nested_types = SeparateTypeDefinition(in_code)
    for item in nested_types: TransformType(item)
    
    # Identify kind of type Struct, Enum, Union & Transform    
    if 'struct' in first_line:
        type_name, out_code = StructureTransformer.Transform(original_type)
    if 'union' in first_line:
        type_name, out_code = UnionTransformer.Transform(original_type)
    if 'enum' in first_line:
        #type_name, out_code = EnumTransformer.Transform(original_type)
        #print type_name
        return
    
    # Save to file
    if (out_code != None):
        fout = open(type_name+'.java','w')
        fout.write(out_code)
        #print out_code
    return out_code
#TransformType(CType3)

# Transform all possible structure definition
def BatchTransformType():
    fpath = path.split(path.split(path.dirname(__file__))[0])[0] + '\\DBSource\\'
    fin   = open(fpath+'DataLv2\\jna_classes.json', 'r')
    jna_supported_struct = json.load(fin)
    jna_supported_struct = jna_supported_struct.keys()
    
    # Load jna supported struct
#     fpath = path.split(path.split(path.dirname(__file__))[0])[0] + '\\DBSource\\'
#     fin   = open(fpath+'DataLv1\\jna_supported_struct2.ls', 'r')
#     jna_supported_struct = [line.strip('\n') for line in fin]
#     jna_supported_struct = set(jna_supported_struct)
     
    # Load all possible type description
    structs = SystemUtl.GetFiles(fpath+'MS-Struct\\')
    structs = [s.replace('.html','') for s in structs]
     
    # Generation
    for item in structs:
        if not item in jna_supported_struct:
            print '0', item
            in_code = StructExtractor.ExtractStruct(item+'.html')
            print '1', item
            TransformType(in_code)
            print '2', item
BatchTransformType()