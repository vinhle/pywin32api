# -*- coding: utf-8 -*-
'''
@Author : LE VINH
@Project: BE-PUM, API Stub generation
@Time   : JAIST (2015-2016)
@Summary: Union type transformation
'''

import json, re
from os import path

# Load code template from file
def LoadTemplate():
    fin = open('template/Union.tmpt','r')
    tmp = ''
    template = []
    for line in fin:
        if not line.startswith('/*-'): 
            if len(line)>1: tmp += line             
        else:
            if len(tmp)>0:  template.append(tmp)
            tmp = ''
    template.append(tmp)

    # Code generation: String format with placeholder in Python
    # http://stackoverflow.com/questions/30646697/replace-placeholder-tags-with-dictionary-fields-in-python 
    struct = template[0].expandtabs(4)   # Main class
    part1  = template[1].expandtabs(4)   # Step 1: Define possible constant value, this part can be empty
    part2  = template[2].expandtabs(4)   # Step 2: Define inner type class - nested type, this part can be empty
    part3  = template[3].expandtabs(4)   # Step 3: Define fields of class
    #print struct, part1, part2, part3
    return struct, part1, part2, part3
#LoadTemplate()

# Transform Union definition in C -> Class definition in Java-JNA
def Transform(in_code):
    # Token analysis
    in_code = in_code.strip().split('\n')
    static  = ''
    uname   = in_code[-1].split()[1][:-1] # union name
    paras   = [para.replace(';','').split() for para in in_code[1:-1]] # parameter list
    
    # Load type mapping table
    fpath = path.split(path.split(path.dirname(__file__))[0])[0] + '\\DBSource\\DataLv2\\'
    fin   = open(fpath+'type_table_review.json', 'r')
    arr_type_table = json.load(fin) # a list
    type_table = {}
    for item in arr_type_table: type_table[item['source']] = item['target']
    
    # Load code template
    union, tpart1, tpart2, tpart3 = LoadTemplate()
    
    # Code generation
    part1 = ''
    
    part2 = ''
    
    part3 = ''
    constants = {}
    for para in paras:
        if para[0] in type_table: # type has already been in type_table
            if not para[1].endswith(']'):
                part3 += tpart3.format(ftype=type_table[para[0]], fname=para[1]);
            else: # array
                arr_size = re.search(r'\[(.+)\]', para[1]).group(1)
                if arr_size in constants: arr_size = str(constants[arr_size])
                fname = re.sub(r'\[.+\]', '', para[1])
                part3 += '\tpublic '+type_table[para[0]]+'[] '+ fname + ' = new '+type_table[para[0]]+'['+arr_size+'];\n'
        else:   # type isn't in type_table (nested-type, new type)
            part3 += tpart3.format(ftype=para[0], fname=para[1]);
    
    placeholders = {'static':static, 'uname':uname,
                    'part1':part1, 'part2':part2, 'part3':part3}
    out_code = union.format(**placeholders)
    return uname, out_code

CUnion = '''
typedef union Example19Union {
    int intnumber;
    float floatnumber;
    char* stringval;
} Example19Union;
'''
JavaUnion = '''
public static class Example19Union extends Union {
    public static class ByValue extends Example19Union implements Union.ByValue {};
    public int intnumber;
    public float floatnumber;
    public String stringval;
}
'''
#print Transform(CUnion)[1]