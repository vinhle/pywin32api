# -*- coding: utf-8 -*-
'''
@Author : LE VINH
@Project: BE-PUM, API Stub generation
@Time   : JAIST (2015-2016)
@Summary: Structure type transformation
'''

import json, re
from os import path

# Load code template from file
def LoadTemplate():
    fin = open('template/Structure.tmpt','r')
    tmp = ''
    template = []
    for line in fin:
        if not line.startswith('/*-'): 
            if len(line)>1: tmp += line             
        else:
            if len(tmp)>0:  template.append(tmp)
            tmp = ''
    template.append(tmp)

    # Code generation: String format with placeholder in Python
    # http://stackoverflow.com/questions/30646697/replace-placeholder-tags-with-dictionary-fields-in-python 
    struct = template[0].expandtabs(4)   # Main class
    part1  = template[1].expandtabs(4)   # Step 1: Define possible constant value, this part can be empty
    part2  = template[2].expandtabs(4)   # Step 2: Define inner type class - nested type, this part can be empty
    part3  = template[3].expandtabs(4)   # Step 3: Define fields of class
    part4  = template[4].expandtabs(4)   # Step 4: List of field names
    #print struct, part1, part2, part3, part4
    return struct, part1, part2, part3, part4
#LoadTemplate()

# Transform Structure definition in C -> Class definition in Java-JNA
def Transform(in_code):
    # Token analysis
    in_code = in_code.strip().split('\n')
    static  = ''
    try:
        sname = in_code[-1].split()[1][:-1] # struct name
    except IndexError:
        print 'It is not typedef, only declare a type'
        sname = in_code[0].split()[1]
    paras   = [para.replace(';','').split() for para in in_code[1:-1] if not para.startswith('#')] # parameter list
    
    # Load type mapping table
    fpath = path.split(path.split(path.dirname(__file__))[0])[0] + '\\DBSource\\DataLv2\\'
    fin   = open(fpath+'type_table_review.json', 'r')
    arr_type_table = json.load(fin) # a list
    type_table = {}
    for item in arr_type_table: type_table[item['source']] = item['target']
    
    # Load code template
    struct, tpart1, tpart2, tpart3, tpart4 = LoadTemplate()
    
    # Code generation
    part1 = ''
    
    part2 = ''
    
    part3 = ''
    constants = {"LF_FACESIZE":32, "MAX_DEFAULTCHAR":2, "MAX_LEADBYTES":12, "MAX_PATH":260, "CCHFORMNAME":32,
                 "LF_FULLFACESIZE": 64, "HW_PROFILE_GUIDLEN":39, "MAX_PROFILE_LEN":80, "MAX_MODULE_NAME32+1":256,
                 "MAX_PATH":260, "OFS_MAXPATHNAME":128, "CCH_RM_MAX_APP_NAME+1":256, "CCH_RM_MAX_SVC_NAME+1":64,
                 "CCHILDREN_SCROLLBAR+1":6, "CCHILDREN_TITLEBAR+1":6, "ENCRYPTED_PWLEN":16, "WCT_OBJNAME_LENGTH":128,
                 "WER_MAX_PREFERRED_MODULES_BUFFER":256, "ANYSIZE_ARRAY":1, "MAX_PATH+36":296, "WSAPROTOCOL_LEN+1":256,
                 "CCHDEVICENAME":32, "MM_MAX_NUMAXES":16, "MAX_PROTOCOL_CHAIN":7, "EVLEN+1":17, "SNLEN+1":81,
                 "MM_MAX_AXES_NAMELEN":16, "NUM_DISCHARGE_POLICIES":14}
    for para in paras:
        if para[1].startswith('*'):
            para[0] += '*'
            para[1] = para[1].replace('*','') 
        if para[0] in type_table: # type has already been in type_table
            if not para[1].endswith(']'):
                part3 += tpart3.format(ftype=type_table[para[0]], fname=para[1]);
            else: # array
                arr_size = re.search(r'\[(.+)\]', para[1]).group(1)
                if arr_size in constants: arr_size = str(constants[arr_size])
                fname = re.sub(r'\[.+\]', '', para[1])
                part3 += '\tpublic '+type_table[para[0]]+'[] '+ fname + ' = new '+type_table[para[0]]+'['+arr_size+'];\n'
        else:   # type isn't in type_table (nested-type, new type)
            part3 += tpart3.format(ftype=para[0], fname=para[1]);
    part3 = part3.expandtabs(4)
    
    part4 = ''
    for para in paras:
        if para[1].startswith('*'):
            para[1] = para[1].replace('*','')
        if para[1].endswith(']'):
            para[1] = re.sub(r'\[.+\]', '', para[1])
        part4 += tpart4.format(fname=para[1]) + ', '
    part4 = part4[:-2]
    
    placeholders = {'static':static, 'sname':sname,
                    'part1':part1, 'part2':part2, 'part3':part3, 'part4':part4}
    out_code = struct.format(**placeholders)
    
    return sname, out_code

CStruct1 = '''
typedef struct _SYSTEMTIME {
  WORD wYear;
  WORD wMonth;
  WORD wDayOfWeek;
  WORD wDay;
  WORD wHour;
  WORD wMinute;
  WORD wSecond;
  WORD wMilliseconds;
} SYSTEMTIME, *PSYSTEMTIME;
'''
CStruct2 = '''
typedef struct tagNONCLIENTMETRICS {
  UINT    cbSize;
  int     iBorderWidth;
  int     iScrollWidth;
  int     iScrollHeight;
  int     iCaptionWidth;
  int     iCaptionHeight;
  LOGFONT lfCaptionFont;
  int     iSmCaptionWidth;
  int     iSmCaptionHeight;
  LOGFONT lfSmCaptionFont;
  int     iMenuWidth;
  int     iMenuHeight;
  LOGFONT lfMenuFont;
  LOGFONT lfStatusFont;
  LOGFONT lfMessageFont;
#if (WINVER >= 0x0600)
  int     iPaddedBorderWidth;
#endif 
} NONCLIENTMETRICS, *PNONCLIENTMETRICS, *LPNONCLIENTMETRICS;
'''
JavaStruct = '''
public static class SYSTEMTIME extends Structure {
    public short wYear;
    public short wMonth;
    public short wDayOfWeek;
    public short wDay;
    public short wHour;
    public short wMinute;
    public short wSecond;
    public short wMilliseconds;
    protected List<String> getFieldOrder() {
        return Arrays.asList(new String[] {
                "wYear", "wMonth", "wDayOfWeek", 
                "wDay", "wHour", "wMinute", 
                "wSecond", "wMilliseconds"});
    }
'''
CType3 = '''
typedef struct _COMM_CONFIG {
  DWORD dwSize;
  WORD  wVersion;
  WORD  wReserved;
  DCB   dcb;
  DWORD dwProviderSubType;
  DWORD dwProviderOffset;
  DWORD dwProviderSize;
  WCHAR wcProviderData[abc@];
} COMMCONFIG, *LPCOMMCONFIG;
'''
#print Transform(CStruct2)[1]
#print Transform(CType3)[1]