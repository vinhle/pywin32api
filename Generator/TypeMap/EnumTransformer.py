# -*- coding: utf-8 -*-
'''
@Author : LE VINH
@Project: BE-PUM, API Stub generation
@Time   : JAIST (2015-2016)
@Summary: Enum type transformation
'''

# Transform Enum definition in C -> Class definition in Java-JNA
def Transform(in_code):
    # Token analysis
    in_code = in_code.strip().split('\n')
    try:
        enum = in_code[-1].split()[1][:-1] # enum name
    except IndexError:
        print 'It is not typedef, only declare a type'
        enum = in_code[0].split()[1]
    fields  = [field.replace(',','').split() for field in in_code[1:-1]] # enum fields
    
    # Code generation
    out_code  = 'package v2.org.analysis.apihandle.structures;\n\n'
    out_code += 'import com.sun.jna.Structure;\n'
    out_code += 'public static interface ' + enum + ' {\n'
    # https://msdn.microsoft.com/en-us/library/whbyts4t.aspx: How to get implicit value of enum in C
    value = 0
    for field in fields:
        if len(field) > 1:
            field[2] = field[2].replace('(int)','')
            if field[2].startswith('0x'): field[2] = str(int(field[2],16)) # Convert hex string to int
            #print field
            out_code += '\tpublic static final int ' + field[0] +' = '+ field[2] + ';\n'
            value = int(field[2]) + 1
        else:
            out_code += '\tpublic static final int ' + field[0] +' = '+ str(value) + ';\n'
            value = value + 1
    out_code += '}\n'
    out_code = out_code.expandtabs(4)
    return enum, out_code

CEnum1 = '''
typedef enum  { 
  QUNS_NOT_PRESENT              = 1,
  QUNS_BUSY                     = 2,
  QUNS_RUNNING_D3D_FULL_SCREEN  = 3,
  QUNS_PRESENTATION_MODE        = 4,
  QUNS_ACCEPTS_NOTIFICATIONS    = 5,
  QUNS_QUIET_TIME               = 6,
  QUNS_APP                      = 7
} QUERY_USER_NOTIFICATION_STATE;
'''
CEnum2 = '''
typedef enum DAY {
    saturday,
    sunday, 
    monday,
    tuesday = 1,
    wednesday,
    thursday = 8,
    friday
} workday;
'''
CEnum3 = '''
typedef enum _SIGDN { 
  SIGDN_NORMALDISPLAY                = 0x00000000,
  SIGDN_PARENTRELATIVEPARSING        = (int)0x80018001,
  SIGDN_DESKTOPABSOLUTEPARSING       = (int)0x80028000,
  SIGDN_PARENTRELATIVEEDITING        = (int)0x80031001,
  SIGDN_DESKTOPABSOLUTEEDITING       = (int)0x8004c000,
  SIGDN_FILESYSPATH                  = (int)0x80058000,
  SIGDN_URL                          = (int)0x80068000,
  SIGDN_PARENTRELATIVEFORADDRESSBAR  = (int)0x8007c001,
  SIGDN_PARENTRELATIVE               = (int)0x80080001,
  SIGDN_PARENTRELATIVEFORUI          = (int)0x80094001
} SIGDN;
'''
CEnum4 = '''
enum SYSGEOCLASS {
  GEOCLASS_NATION  = 16, 
  GEOCLASS_REGION  = 14, 
  GEOCLASS_ALL     = 0
};
'''
JavaEnum = '''
public interface QUERY_USER_NOTIFICATION_STATE {
    public static final int QUNS_NOT_PRESENT = 1;
    public static final int QUNS_BUSY = 2;
    public static final int QUNS_RUNNING_D3D_FULL_SCREEN = 3;
    public static final int QUNS_PRESENTATION_MODE  = 4;
    public static final int QUNS_ACCEPTS_NOTIFICATIONS  = 4;
    public static final int QUNS_QUIET_TIME  = 5;
    public static final int QUNS_APP  = 6;
}
'''
#print Transform(CEnum4)[1]