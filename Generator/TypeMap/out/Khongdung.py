import re, json
from os import path
from PySystem import SystemUtl

def BuildTypeTable():
    type_table = {}
    fpath      = path.split(path.split(path.dirname(__file__))[0])[0] + '\\DBSource\\'
    
    # Load msjna_type_map.ls -> type_table
    fin1           = open(fpath+'DataLv1\\msjna_type_map.ls', 'r')
    msjna_type_map = json.load(fin1)
    type_table.update(msjna_type_map)
    
    # Load type in msjna_unsup_type.ls and jna_supported_struct2.ls -> type_table 
    fin2                 = open(fpath+'DataLv1\\msjna_unsup_type.ls', 'r')
    msjna_unsup_type     = [line.strip('\n') for line in fin2]
    #fin3 = open(fpath + 'jna_supported_struct.ls', 'r'); jna_supported_struct = pickle.load(fin3)
    #supported struct from http://java-native-access.github.io/jna/4.2.1/, part all classes
    fin3                 = open(fpath+'DataLv1\\jna_supported_struct2.ls', 'r')
    jna_supported_struct = [line.strip('\n') for line in fin3]
    for item in msjna_unsup_type:
        if item in jna_supported_struct:            
            type_table[item] = [item]
        else:            
            if item.startswith('P'):
                if item[1:] in jna_supported_struct:
                    type_table[item] = [item[1:]]
            elif item.startswith('LP'):
                if item[2:] in jna_supported_struct:
                    type_table[item] = [item[2:]]
    
    # Load type in MS-Struct folder -> type_table
    structs = SystemUtl.GetFiles(fpath+'MS-Struct')
    structs = [f.replace('.html','') for f in structs]
    #for item in structs: print item
    for item in msjna_unsup_type:
        if item in structs:
            if not item in type_table: type_table[item] = ['VStruct.'+item]
        else:
            if item.startswith('P'):
                if item[1:] in structs:
                    if not item in type_table: type_table[item] = ['VStruct.'+item[1:]]
            elif item.startswith('LP'):
                if item[2:] in structs:
                    if not item in type_table: type_table[item] = ['VStruct.'+item[2:]]
    
    # Load type in MS-CallFunct folder -> type_table
    # Impossible to execute callback function from BE-PUM
    
    # Load type in ms_win_type.ls
    fin4 = open(fpath+'DataLv1\\ms_def_wintype.ls', 'r')
    win_types = [line.strip('\n') for line in fin4 if line.startswith('typedef')]
    win_types = [re.sub('(typedef|unsigned|signed|const|far|;|\*)', '', line) for line in win_types]
    win_types = [item.strip() for item in win_types] # win_types0 win_types1
    win_types0 = [item.split()[0] for item in win_types]
    win_types1 = [item.split()[1] for item in win_types]
    for item0, item1 in zip(win_types0,win_types1):
        if item1 in msjna_unsup_type:
            if item0 in type_table:
                print item1, item0, type_table[item0]
                if not item1 in type_table:
                    type_table[item1] = type_table[item0]           
    
    print len(type_table)
    # Write to type_table.ls
    with open(fpath+'\\DataLv2\\type_table.ls', 'w') as fout:
        json.dump(type_table, fout, indent=2) #indent: add new line
    return type_table
#BuildTypeTable()