# -*- coding: utf-8 -*-
'''
@Author : LE VINH
@Project: BE-PUM, API Stub generation
@Time   : JAIST (2015-2016)
@Summary: API interface generation
@deprecated: Move to the project 'WinAPI GenStub' (Java)
'''

import json, collections
from os import path

#  Transform from API signature in C -> API interface  in Java
def GenerateAPIInterface(fname):
    # Load API features
    fpath = path.split(path.dirname(__file__))[0] + '\\DBSource\\'
    fin1  = open(fpath+'DataLv1\\ms_api.json', 'r')
    ms_apis = collections.OrderedDict()
    for line in fin1:
        api = json.JSONDecoder(object_pairs_hook=collections.OrderedDict).decode(line)
        ms_apis[api["fname"]] = api
    
    # Load type mapping table
    fin2  = open(fpath+'DataLv2\\type_table.ls', 'r')
    type_table = json.load(fin2)
    
    # Code generation
    flib = ms_apis[fname]['flib'].title()
    out_code = 'import com.sun.jna.Native;\nimport com.sun.jna.win32.StdCallLibrary;\n\n'
    out_code += 'public interface ' + flib + ' extends StdCallLibrary {\n'
    out_code += '\t'+ flib + ' INSTANCE = ('+ flib +') Native.loadLibrary("'+ flib +'", '+ flib +'.class);\n'
    out_code += '\t'+ ms_apis[fname]['ftype'] +' '+ fname +'('
    for item in ms_apis[fname]['fargs']:
        out_code += type_table[item['type']][0] +' '+ item['name'] +', '
    out_code  = out_code[:-2] + ');\n}'
    print out_code
    
    # Save to Java file

GenerateAPIInterface('GetSystemTime')
java_ = '''
public interface Kernel32 extends StdCallLibrary {
    Kernel32 INSTANCE = (Kernel32) Native.loadLibrary("kernel32", Kernel32.class);
    void GetSystemTime(SYSTEMTIME lpSystemTime);
}
'''