# -*- coding: utf-8 -*-
'''
@Author : LE VINH
@Project: BE-PUM, API Stub generation
@Time   : JAIST (2015-2016)
@Summary: Review API specification (add buffer - buffer length parameter specification)
'''

import collections, json
from operator import itemgetter
from Extractor import PBufferClassifier, PLengthClassifier

# Update the specification of an API by its name
def UpdateSingleAPI(fname):
    # Load data & create classifer mode
    fin     = open("../DBSource/DataLv1/ms_api.json")
    ms_apis = collections.OrderedDict()
    for line in fin:
        api = json.JSONDecoder(object_pairs_hook=collections.OrderedDict).decode(line)
        ms_apis[api["fname"]] = api
    fin.close()
    classifer_mode = PLengthClassifier.BuildClassifierModel('Maxent')
    api            = ms_apis[fname]
    
    # Identify buffer poitner parameter
    buffer_idxs = []
    for idx, arg in enumerate(api['fargs']):
        isPointer, simi_degree = PBufferClassifier.isBufferPointer(arg)
        if isPointer == 2:
            buffer_idxs.append(idx)
            print api['fname'], arg['name'], idx
    
    # Get features of other parameters, then predict buffer relation
    relation = []
    tmp_rel  = []   # in case: > 1 para predicted as buffer length parameter
    for buffer_idx in buffer_idxs:
        for para_idx, arg in enumerate(api['fargs']):
            if para_idx != buffer_idx:
                featureset = PLengthClassifier.GetParaFeatures(api, buffer_idx, para_idx)
                output     = classifer_mode.classify(featureset)
                dist       = classifer_mode.prob_classify(featureset)
                if (output == 'true'):
                    tmp_rel.append({'pbuffer':buffer_idx,'plength':para_idx, 'accur': dist.prob('true')})
        tmp_rel = sorted(tmp_rel, key=itemgetter('accur'), reverse=True)[0]
        relation.append(tmp_rel)
        tmp_rel = []
    print relation
    
    # Update API specification
    ms_apis[fname]['relation'] = relation
    data_set = ''
    for api in ms_apis: data_set += json.dumps(ms_apis[api]) + '\n'
    fout = open('../DBSource/DataLv2/ms_api_review.json', 'w')
    fout.write(data_set)
#UpdateSingleAPI('GetPrinterData')

# Update the specification of all APIs
def UpdateEntireAPIs():
    # Load data & create classifer mode
    fin     = open("../DBSource/DataLv1/ms_api.json")
    ms_apis = collections.OrderedDict()
    for line in fin:
        api = json.JSONDecoder(object_pairs_hook=collections.OrderedDict).decode(line)
        ms_apis[api["fname"]] = api
    fin.close()
    classifer_mode = PLengthClassifier.BuildClassifierModel('Maxent')
    
    for fname in ms_apis:
        api            = ms_apis[fname]
        print fname        
        # Identify buffer poitner parameter
        buffer_idxs = []
        for idx, arg in enumerate(api['fargs']):
            isPointer, simi_degree = PBufferClassifier.isBufferPointer(arg)
            if isPointer == 2:
                buffer_idxs.append(idx)
                print api['fname'], arg['name'], idx
        
        # Get features of other parameters, then predict buffer relation
        relation = []
        tmp_rel  = []   # in case: > 1 para predicted as buffer length parameter
        for buffer_idx in buffer_idxs:
            for para_idx, arg in enumerate(api['fargs']):
                if para_idx != buffer_idx:
                    featureset = PLengthClassifier.GetParaFeatures(api, buffer_idx, para_idx)
                    output     = classifer_mode.classify(featureset)
                    dist       = classifer_mode.prob_classify(featureset)
                    if (output == 'true'):
                        tmp_rel.append({'pbuffer':buffer_idx,'plength':para_idx, 'accur': dist.prob('true')})
            if len(tmp_rel) > 0: # prevent case: existing buffer pointer - no buffer length parameter
                tmp_rel = sorted(tmp_rel, key=itemgetter('accur'), reverse=True)[0]
                relation.append(tmp_rel)
            tmp_rel = []
        #print relation
        
        # Update API specification
        ms_apis[fname]['relation'] = relation
        
    data_set = ''
    for api in ms_apis: data_set += json.dumps(ms_apis[api]) + '\n'
    fout = open('../DBSource/DataLv2/ms_api_review.json', 'w')
    fout.write(data_set)
UpdateEntireAPIs()
