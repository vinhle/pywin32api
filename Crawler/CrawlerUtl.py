# -*- coding: utf-8 -*-
'''
@Author : LE VINH
@Project: BE-PUM, API Stub generation
@Time   : JAIST (2015-2016)
@Summary: Utilites for crawling
'''

import urllib, urllib2, json
from bs4 import BeautifulSoup
from googleapiclient.discovery import build
 
# Parse HTML file (online/offline) to a HTML object by BeautifulSoup
def GetHTMLTree(url, flag):
    if flag == 'online':
        html_txt = urllib2.urlopen(url).read()
    elif flag == 'offline':
        html_txt = open(url, 'r').read()
    html_obj = BeautifulSoup(html_txt, 'html.parser')
    return html_obj


# Save a webpage to the local database
def SavePage(url, fname, fpath):
    '''
    @param url: URL link (online)
    @param fname: file name
    @param fpath: file path
    @note html_txt is in raw text, and we don't know the type of encoding.
    - See tag charset in HTML source to know which is the type of site encoding: anscii, ansi, iso-8859-1, utf-8, Shift_JIS, ...
    - UTF-8 is often used to decode text
    - Use ascii to encode, and error_handle = xmlcharrefreplace to find char_code of strange characters
    - Filter out strange characters
    '''
    html_txt = urllib2.urlopen(url).read() # <type 'str'>
    html_txt = html_txt.decode('utf-8').encode("ascii", "xmlcharrefreplace")
    html_txt = html_txt.replace('&#160;', ' ')
    fin = open(fpath+'\\'+fname, 'w')
    fin.write(html_txt)
    fin.close()


# Find the URL of API description via Google Search API (SOAP Service)
# Unfortunately, Google no longer supports the SOAP API for search - out of date now
def GoogleSearch(keyword):
    # Google Search XML/SOAP Web service in a Python App
    query = urllib.urlencode({'q': keyword})
    url   = 'http://ajax.googleapis.com/ajax/services/search/web?v=1.0&%s' % query
    search_response = urllib.urlopen(url)
    search_results  = search_response.read()
    results = json.loads(search_results)
    data    = results['responseData']
    #print 'Total results: %s' % data['cursor']['estimatedResultCount']
    hits = data['results']
    #print 'Top %d hits:' % len(hits)
    #for h in hits: print ' ', h['url']
    #print 'For more results, see %s' % data['cursor']['moreResultsUrl']
    apiurl = hits[0]['url']
    if apiurl.startswith('https://msdn.microsoft.com/'):
        return apiurl
    else:
        print "Not found the suitable result"
        return None
#GoogleSearch('CharNext')


# Find the URL of API description via Google Custom Search API - quoto <= 100 query/day
''' Tutorial
https://developers.google.com/api-client-library/python/apis/customsearch/v1#sample
http://stackoverflow.com/questions/37083058/programmatically-searching-google-in-python-using-custom-search
https://github.com/google/google-api-python-client/blob/master/samples/customsearch/main.py
http://stackoverflow.com/questions/4504717/calling-googles-custom-search-api-via-python
http://it-ovid.blogspot.jp/2012/11/using-python-with-google-api-custom.html
'''
def GoogleCustomSearch(keyword):
    # Build a service object for interacting with the API. Visit
    # the Google APIs Console <http://code.google.com/apis/console>
    # to get an API key for your own application.
    service = build("customsearch", "v1", developerKey="AIzaSyCsY2HGnU2g1f8D2CuNZbZ1CU7m4SAax-o")
    
    # CSE reference
    # https://developers.google.com/resources/api-libraries/documentation/customsearch/v1/python/latest/customsearch_v1.cse.html
    # Custom Search Engine need to be configured in https://cse.google.com (account levinh.hedspi)
    res = service.cse().list(
        q  = keyword,                             # string, query (required)
        cx ='007805733380142889653:d_gaaqeu32c',  # the custom search engine ID to scope this search query
        #lr = 'lang_en'                           # string, the language restriction for the search results
        ).execute()
    
#     print len(res)
#     print type(res[u'items'])
    if len(res[u'items']) > 0:
        print res[u'items'][0][u'title']
        print res[u'items'][0][u'link']
        return res[u'items'][0][u'link']
    else:
        return None
#GoogleCustomSearch('CryptCreateHash')