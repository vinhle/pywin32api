# -*- coding: utf-8 -*-
'''
@Author : LE VINH
@Project: BE-PUM, API Stub generation
@Time   : JAIST (2015-2016)
@Summary: Collect call back functions description from MSDN
'''

import re, os
from os import path
from Crawler import CrawlerUtl
from PySystem import SystemUtl

# Get the pair of Struct and URL in an API description
def GetCallbackFunctURL(fname):
    dict_struct_url = {}     
    fpath    = path.split(path.dirname(__file__))[0] + '\\DBSource\\MS-HTML\\' + fname + '.html'
    html     = CrawlerUtl.GetHTMLTree(fpath, 'offline')
        
    # parameter description (the first <dl> tag in the webpage)
    descripts = html.find("div", {"id": "mainSection"}).find_all("dl")[0]
    descripts = descripts.find_all("dd") # Each <dd> tag contains the description of a parameter
    for dd in descripts:
        dd_txt     = dd.get_text()
        hyperlinks = dd.find_all("a")
        for a in hyperlinks:
            cbfunct = a.get_text()
            if (len(cbfunct.split())==1) and ('callback function' in dd_txt):
                if not cbfunct in dict_struct_url:
                    dict_struct_url[cbfunct] = a['href']
                    print fname, cbfunct, a['href']
    return dict_struct_url
#GetCallbackFunctURL('SetAbortProc')

# Store all possible Struct descriptions (HTML) into the local database
def SaveCallbackFunctPage():
    dict_struct_url = {}
    fpath = path.split(path.dirname(__file__))[0] + '\\DBSource\\MS-HTML'
    files = SystemUtl.GetFiles(fpath)
    files = [f.replace('.html','') for f in files]
    for api in files:
        dict_struct_url.update(GetCallbackFunctURL(api))

    for item in dict_struct_url:
        fpath = path.split(path.dirname(__file__))[0] + '\\DBSource\\MS-CallFunct'
        CrawlerUtl.SavePage(dict_struct_url[item], item+'.html', fpath)
        print item, dict_struct_url[item]
    print len(dict_struct_url)
#SaveCallbackFunctPage()

# The name of type in API != the name of callback function
def GetNameCBFunct(fname):
    fpath     = path.split(path.dirname(__file__))[0] + '\\DBSource\\MS-CallFunct\\' + fname + '.html'
    html      = CrawlerUtl.GetHTMLTree(fpath, 'offline')
    descripts = html.find("div", {"id": "mainSection"}).find_all("p")[0]
    descripts = descripts.get_text()
    struct = re.search(r'\b[A-Z|_]+\b', descripts).group(0)
    print fname, struct
    return struct
#GetNameCBFunct('EnumWindowsProc')

# Change in style <Callback_function Struct_name>
def StanderFileName():
    fpath   = path.split(path.dirname(__file__))[0] + '\\DBSource\\MS-CallFunct\\'
    cbfunct = SystemUtl.GetFiles(fpath)
    cbfunct = [f.replace('.html','') for f in cbfunct]
    print cbfunct
    for item in cbfunct:
        struct = GetNameCBFunct(item)
        if struct != None:
            os.rename(fpath + item+'.html', fpath + item+' '+struct+'.html')
#StanderFileName()
    