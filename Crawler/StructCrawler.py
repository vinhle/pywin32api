# -*- coding: utf-8 -*-
'''
@Author : LE VINH
@Project: BE-PUM, API Stub generation
@Time   : JAIST (2015-2016)
@Summary: Collect Structs description from API parameter description
'''

from os import path
from Crawler import CrawlerUtl
from PySystem import SystemUtl

# Get a dictionary of Struct and its URL in an API
def GetStructURL(fname):
    dict_struct_url = {}
    fpath    = path.split(path.dirname(__file__))[0] + '\\DBSource\\MS-HTML\\' + fname + '.html'
    html     = CrawlerUtl.GetHTMLTree(fpath, 'offline')
        
    # Parameter description (the first <dl> tag in webpage)
    descripts = html.find("div", {"id": "mainSection"}).find_all("dl")[0]
    descripts = descripts.find_all("dd") # Each <dd> tag is description of a parameter
    for dd in descripts:
        dd_txt     = dd.get_text()
        hyperlinks = dd.find_all("a")
        for a in hyperlinks:
            struct = a.get_text()
            if (len(struct.split())==1) and (struct+' structure' in dd_txt):
                if not struct in dict_struct_url:
                    dict_struct_url[struct] = a['href']
                    print fname, struct, a['href']
    return dict_struct_url
#GetStructURL('MultinetGetConnectionPerformance')

# Save all Struct description to the local database
def SaveStructPage():
    dict_struct_url = {}
    fpath = path.split(path.dirname(__file__))[0] + '\\DBSource\\MS-HTML'
    files = SystemUtl.GetFiles(fpath)
    files = [f.replace('.html','') for f in files]
    for api in files:
        dict_struct_url.update(GetStructURL(api))

    for item in dict_struct_url:
        fpath = path.split(path.dirname(__file__))[0] + '\\DBSource\\MS-Struct'
        CrawlerUtl.SavePage(dict_struct_url[item], item+'.html', fpath)
        print item, dict_struct_url[item]
    print len(dict_struct_url)
#SaveStructPage()