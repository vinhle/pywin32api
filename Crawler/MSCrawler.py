# -*- coding: utf-8 -*-
'''
@Author : LE VINH
@Project: BE-PUM, API Stub generation
@Time   : JAIST (2015-2016)
@Summary: Collect APIs description from MSDN
'''

import time, CrawlerUtl

# Get the URL list of API categories
def GetApiCat():
    html = CrawlerUtl.GetHTMLTree('https://msdn.microsoft.com/en-us/library/aa383688%28VS.85%29.aspx', 'online')
    mainSection = html.find("div", {"id": "mainSection"})
    mainSection = mainSection.ul.find_all('a')
    links = []
    for li in mainSection:
        #print li.get_text(), li['href']
        links.append(li['href'])
    return links


# Get the list of pairs (API name, API url address)
def GetApiUrl(url):
    '''
    @param url: the URL list of API categories
    '''
    html = CrawlerUtl.GetHTMLTree(url, 'online')
    mainSection = html.find("div", {"id": "mainSection"})
    mainSection = mainSection.dl.find_all('a')
    links = []
    for dd in mainSection:
        try:
            print dd.strong.get_text(), dd['href']
            fname = dd.strong.get_text()
            links.append((fname, dd['href']))
        except:
            print "error: ", dd['href']
            links.append(('error', dd['href']))
    return links


# Save all APIs description (HTML format) to the local database
def SaveAPIPages():
    fout     = open('../DBSource/DataLv1/ms_html.ls', 'w')
    api_list = ''
    count    = 0
    api_cats = GetApiCat()
    for api_cats in api_cats:
        api_urls  = GetApiUrl(api_cats)
        for item in api_urls:
            url   = item[1]
            fname = item[0]
            fpath = '../DBSource/MS-HTML/' + fname + '.html'
            CrawlerUtl.SavePage(url, fname, fpath)
            api_list += fname + '\n'
            count += 1
    api_list += 'Total APIs: ', count
    fout.write(api_list)

start_time = time.time()
SaveAPIPages()
print("--- %s seconds ---" % (time.time() - start_time))