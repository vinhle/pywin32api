# -*- coding: utf-8 -*-
'''
@Author : LE VINH
@Project: BE-PUM, API Stub generation
@Time   : JAIST (2015-2016)
@Summary: Collecting extra APIs description from MSDN
'''

import json, collections
from os import path
from google import google
from Crawler import CrawlerUtl
from PySystem import SystemUtl

# Crawl APIs declared in JNA but aren't in the local database
def CrawlAPIinJNA():
    ''' (40 APIs) AccessCheck,  AdjustTokenPrivileges,  CloseEncryptedFileRaw,  ConvertSidToStringSid,  ConvertStringSidToSid,  CreateWellKnownSid,  DuplicateToken,  DuplicateTokenEx,  GetFileSecurity,  GetLengthSid,  GetNamedSecurityInfo,  GetSecurityDescriptorLength,  GetTokenInformation,  GetUserNameW,  ImpersonateLoggedOnUser,  ImpersonateSelf,  IsValidAcl,  IsValidSecurityDescriptor,  IsValidSid,  IsWellKnownSid,  LogonUser,  LookupAccountName,  LookupAccountSid,  LookupPrivilegeName,  LookupPrivilegeValue,  MapGenericMask,  OpenEncryptedFileRaw,  OpenProcessToken,  OpenThreadToken,  ReadEncryptedFileRaw,  RevertToSelf,  SetNamedSecurityInfo,  SetThreadToken,  WriteEncryptedFileRaw,  ChoosePixelFormat,  SetPixelFormat,  CreateProcessW,  DuplicateHandle,  SetHandleInformation,  ZwQueryKey '''
    fpath   = path.split(path.dirname(__file__))[0] + '\\DBSource'
    ms_html = SystemUtl.GetFiles(fpath+'\\MS-HTML')
    ms_html = [item.replace('.html','') for item in ms_html]
    
    fin = open(fpath + '\\DataLv1\\jna_supported_api.json')
    supported_apis = collections.OrderedDict()   
    for line in fin:
        api = json.JSONDecoder(object_pairs_hook=collections.OrderedDict).decode(line)
        supported_apis[api["fname"]] = api
    
    not_indb = []
    for api in supported_apis:
        if not (api in ms_html):
            not_indb.append(api)
            print api
    print len(not_indb)
    
    for api in not_indb:
        print api
        url   = CrawlerUtl.GoogleSearch(api)
        fname = api + '.html'
        #print path.dirname(__file__)
        CrawlerUtl.SavePage(url, fname, path.dirname(__file__))
#CrawlAPIinJNA()


# Crawl APIs implemented in BE-PUM but aren't in the local database
# Not run this functions, Google Search API via AJAX close now
def CrawlAPIinBEPUM():
    ''' from function GetUnsportedAPIDescrip() in YenExtractor.py
    InitCommonControls DecodePointer EncodePointer FormatMessage GetAclInformation IsDBCSLeadByte MulDiv MultiByteToWideChar RegisterWowExec RtlUnwind RtlZeroMemory SetHandleCount WideCharToMultiByte wsprintf _lclose _lcreat _llseek _lopen PathFindFileName PathUndecorate NtCreateSection NtProtectVirtualMemory NtQueryInformationProcess NtWriteVirtualMemory gethostname WSACleanup WSAStartup DialogBoxIndirectParam DialogBoxParam IsDialogMessage StrChrI CoCreateGuid CoFileTimeNow CoInitialize CoUninitialize OleInitialize OleUninitialize CryptAcquireContext CryptCreateHash CryptDecrypt CryptDeriveKey CryptDestroyHash CryptDestroyKey CryptHashData CryptReleaseContext atexit calloc fclose fopen fprintf fread free fwrite isalpha is_wctype malloc memcpy memmove memset puts remove srand strncat strncmp strrchr strstr time _assert _cexit _chkesp _controlfp _except_handler3 _fpreset _initterm _itoa _itow _ltoa _mbsrchr _setmode _strcmpi __getmainargs __p__commode __p__environ __p__fmode __p___initenv __set_app_type FtpGetFile HttpAddRequestHeaders HttpOpenRequest HttpSendRequest InternetCloseHandle InternetConnect InternetOpen InternetOpenUrl InternetReadFile InternetSetOption '''
    not_indb = 'atexit calloc fclose fopen fprintf fread free fwrite isalpha is_wctype malloc memcpy memmove memset puts remove srand strncat strncmp strrchr strstr time _assert _cexit _chkesp _controlfp _except_handler3 _fpreset _initterm _itoa _itow _ltoa _mbsrchr _setmode _strcmpi __getmainargs __p__commode __p__environ __p__fmode __p___initenv __set_app_type'
    # 41 C-runtime library.
    not_indb = not_indb.split()
    for api in not_indb:
        print api
        url   = CrawlerUtl.GoogleCustomSearch(api)
        fname = api + '.html'
        #print path.dirname(__file__)
        CrawlerUtl.SavePage(url, fname, path.dirname(__file__))
#CrawlAPIinBEPUM()