# -*- coding: utf-8 -*-
'''
@Author : LE VINH
@Project: BE-PUM, API Stub generation
@Time   : JAIST (2015-2016)
@Summary: Collect DLLs escription in JNA
'''

import CrawlerUtl
from os import path

# Save all DLLs description (HTML format) in JNA to the local database
def SaveJNAPages():
    win32_interface = '''
        http://java-native-access.github.io/jna/4.2.1/com/sun/jna/platform/win32/Kernel32.html
        http://java-native-access.github.io/jna/4.2.1/com/sun/jna/platform/win32/User32.html
        http://java-native-access.github.io/jna/4.2.1/com/sun/jna/platform/win32/Advapi32.html
        http://java-native-access.github.io/jna/4.2.1/com/sun/jna/platform/win32/GDI32.html
        http://java-native-access.github.io/jna/4.2.1/com/sun/jna/platform/win32/Shell32.html
        http://java-native-access.github.io/jna/4.2.1/com/sun/jna/platform/win32/NtDll.html
    '''
    
    win32_interface_ls = win32_interface.strip().split('\n')
    for url in win32_interface_ls:
        fname = url.split("/")[-1]
        fpath = path.split(path.dirname(__file__))[0] + '\\DBSource\\JNA-HTML'
        CrawlerUtl.SavePage(url, fname, fpath)
#SaveJNAPages()