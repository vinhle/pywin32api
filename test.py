import collections, json

fin = open('DBSource/DataLv1/ms_api.json');
ms_apis = collections.OrderedDict()
for line in fin:
    api = json.JSONDecoder(object_pairs_hook=collections.OrderedDict).decode(line)
    ms_apis[api["fname"]] = api

ret = []
for item in ms_apis:
    ret.append(ms_apis[item]['ftype'])
    if (ms_apis[item]['ftype'] == 'PVOID') or (ms_apis[item]['ftype'] == 'void*'):
        print ms_apis[item]['fname']
    if (ms_apis[item]['ftype'] == 'LPWSTR*') or (ms_apis[item]['ftype'] == 'LPSTR'):
        print 'Str ' + ms_apis[item]['fname']
     
    if ms_apis[item]['ftype'] == 'HMENU':
        print 'Handle ' + ms_apis[item]['fname']
# for item in set(ret):
#     print item,
    
#FARPROC HHOOK HDWP HFILE HGLOBAL LONG HKL LPEXCEPTION_POINTERS LPTOP_LEVEL_EXCEPTION_FILTER NET_API_STATUS void HRESULT PTP_WORK GEOID HMODULE HWND LPVOID SC_HANDLE EFaultRepRetVal LONG_PTR LONGLONG HFONT SHORT SC_LOCK HDC PTP_WAIT HPEN ULONG HDESK ULONG_PTR HDEVNOTIFY UCHAR PTP_CLEANUP_GROUP HRSRC LPTCH LPARAM HPOWERNOTIFY HDDEDATA HLOCAL INT HMENU SIZE_T HRGN HMONITOR HGDIOBJ NTSTATUS HINSTANCE WORD HPALETTE HENHMETAFILE long PTP_POOL HACCEL HANDLE HMETAFILE HSZ UINT_PTR HBITMAP PIDLIST_ABSOLUTE HWCT COLORREF USHORT SERVICE_STATUS_HANDLE BOOLEAN BOOL HCONVLIST LRESULT LPSTR HBRUSH PDH_STATUS int LPTSTR PTP_IO HICON DWORD HWINSTA HCONV LCID PTP_TIMER ULONGLONG COORD UINT LPBYTE ATOM EXECUTION_STATE TRACEHANDLE LPWSTR* HCURSOR LANGID PVOID