# -*- coding: utf-8 -*-
'''
@author: Le Vinh
'''

import json, collections, pickle
from os import path

'''
@return: a list types mapping to char[] and byte[] (Buffer Pointer)
'''
def GetPBufferType():
    fpath = path.split(path.split(path.dirname(__file__))[0])[0] + '\\DBSource\\DataLv1\\msjna_type_map.ls'
    fin   = open(fpath, 'r')
    tdict = json.load(fin)
    plist = []
    for item in tdict:
        for typ in tdict[item]:
            buffer_cond = (typ == 'byte[]') or (typ == 'char[]')
            if buffer_cond: plist.append(item)
    print plist
    return plist
#GetPBufferType()

'''
@return: a list types mapping to xxString (String Pointer)
'''
def GetPStringType():
    fpath = path.split(path.split(path.dirname(__file__))[0])[0] + '\\DBSource\\DataLv1\\msjna_type_map.ls'
    fin   = open(fpath, 'r')
    tdict = json.load(fin)
    plist = []
    for item in tdict:
        for typ in tdict[item]:
            string_cond  = typ.endswith('String')
            if string_cond : plist.append(item)
    print plist
    return plist
#GetPStringType()

'''
@return: a list types mapping to xxByReference (Cell Pointer)
'''
def GetPCellType():
    fpath = path.split(path.split(path.dirname(__file__))[0])[0] + '\\DBSource\\DataLv1\\msjna_type_map.ls'
    fin   = open(fpath, 'r')
    tdict = json.load(fin)
    plist = []
    for item in tdict:
        for typ in tdict[item]:
            cell_cond  = typ.endswith('ByReference')
            if cell_cond : plist.append(item)
    print len(plist)
    for i in plist: print i + ',',
    return plist
#GetPCellType()

'''
@return: a list types mapping to sub-class of Structure (Structure Pointer)
'''
def GetPStructureType():
    fpath = path.split(path.split(path.dirname(__file__))[0])[0] + '\\DBSource\\DataLv1\\'
    fin   = open(fpath+'msjna_type_map.ls', 'r')
    tdict = json.load(fin)
    plist = []
    with open(fpath+'jna_supported_struct.ls', 'rb') as f:
        jna_struct = pickle.load(f)
    for item in tdict:
        for typ in tdict[item]:
            struct_cond  = typ in jna_struct
            if struct_cond : plist.append(item)
    print len(plist), plist
    return plist
#GetPStructureType()



'''
@return: a pair of list <Function Names and Pointer Types>
'''
def GetTotalPointerMS():
    fpath = path.split(path.split(path.dirname(__file__))[0])[0] + '\\DBSource\\DataLv1\\ms_html.json'
    fin = open(fpath)
    flist = [] # API list
    plist = [] # Pointer type list
    for line in fin:
        api = json.loads(line)
        for arg in api["paramList"]:
            if arg["type"].startswith("LP"): # or arg["type"].startswith("P"):
                flist.append(api["funcName"])
                plist.append(arg["type"])
                print api["funcName"], arg["type"]
    print "Corpus has %i pointer parameters in %i API with %i types of pointers" % (len(plist), len(set(flist)), len(set(plist)))
    return (set(flist), set(plist))
#GetTotalPointerMS()

def CheckSALandTypemap():
    fpath = path.split(path.split(path.dirname(__file__))[0])[0] + '\\DBSource\\DataLv1\\'
    
    # Load file jna_supported.json to dictionary
    fin1  = open(fpath + 'jna_supported_api.json')
    jna_apis = collections.OrderedDict()
    for line in fin1:
        api = json.JSONDecoder(object_pairs_hook=collections.OrderedDict).decode(line)
        jna_apis[api["funcName"]] = api
    # Load file yen_supported_api to dictionary
    fin2  = open(fpath + 'yen_supported_api.json')
    yen_apis = collections.OrderedDict()
    for line in fin2:
        api = json.JSONDecoder(object_pairs_hook=collections.OrderedDict).decode(line)
        yen_apis[api["funcName"]] = api
    # Load file ms_html.json to dictionary
    fin3  = open(fpath + 'ms_html.json')    
    ms_apis  = collections.OrderedDict()
    for line in fin3:
        api = json.JSONDecoder(object_pairs_hook=collections.OrderedDict).decode(line)
        ms_apis[api["funcName"]] = api
    
    # Build type mapping from jna_apis
    '''for api in jna_apis:
        if api in ms_apis:
            for ms_arg, jna_arg in zip(ms_apis[api]['paramList'], jna_apis[api]['paramList']):
                if jna_arg['type'] == 'char[]' or jna_arg['type'] == 'byte[]':
                    print api, ms_arg['pass']
    for api in yen_apis:
        if api in ms_apis:
            for ms_arg, yen_arg in zip(ms_apis[api]['paramList'], yen_apis[api]['paramList']):
                if yen_arg['type'] == 'char[]' or yen_arg['type'] == 'byte[]':
                    print api, ms_arg['pass']'''
    for api in jna_apis:
        if api in ms_apis:
            for ms_arg, jna_arg in zip(ms_apis[api]['paramList'], jna_apis[api]['paramList']):
                if ms_arg['type'] == 'LPCTSTR' and jna_arg['type'] != 'byte[]':
                    print api, ms_arg['pass']
#CheckSALandTypemap()
                