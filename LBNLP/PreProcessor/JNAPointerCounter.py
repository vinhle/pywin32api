# -*- coding: utf-8 -*-
'''
@author: Le Vinh
'''

import json, pickle
from os import path

'''
@return: list of pair (API name, position of buffer pointer parameter
'''
def CountBufferPointerJNA():
    fpath = path.split(path.split(path.dirname(__file__))[0])[0] + '\\DBSource\\DataLv1\\jna_supported.json'
    fin = open(fpath)
    plist = [] # API list
    for line in fin:
        api = json.loads(line)
        for idx,arg in enumerate(api["paramList"]):
            buffer_cond = (arg["type"] == 'byte[]') or (arg["type"] == 'char[]')
            if buffer_cond:
                plist.append((api["funcName"], idx))
    for item in plist:
        print item[0], item[1]
    print 'Buffer pointer: ', len(plist)
    return plist
#CountBufferPointerJNA()

'''
@return: list of pair (API name, position of String pointer parameter
'''
def CountStringPointerJNA():
    fpath = path.split(path.split(path.dirname(__file__))[0])[0] + '\\DBSource\\DataLv1\\jna_supported.json'
    fin = open(fpath)
    plist = [] # API list
    for line in fin:
        api = json.loads(line)
        for idx,arg in enumerate(api["paramList"]):
            string_cond = arg["type"].endswith('String')
            if string_cond:
                plist.append((api["funcName"], idx))
    for item in plist:
        print item[0], item[1]
    print 'String pointer: ', len(plist)
    return plist
#CountStringPointerJNA()

'''
@return: list of pair (API name, position of cell pointer parameter
'''
def CountCellPointerJNA():
    fpath = path.split(path.split(path.dirname(__file__))[0])[0] + '\\DBSource\\DataLv1\\jna_supported.json'
    fin = open(fpath)
    plist = [] # API list
    for line in fin:
        api = json.loads(line)
        for idx,arg in enumerate(api["paramList"]):
            cell_cond = arg["type"].endswith('ByReference')
            if cell_cond:
                plist.append((api["funcName"], idx))
    for item in plist:
        print item[0], item[1]
    print 'Cell pointer: ', len(plist)
    return plist
#CountCellPointerJNA()

'''
@return: list of pair (API name, position of structure pointer parameter
structure pointer has type is sub-class of Structure in JNA
http://java-native-access.github.io/jna/4.2.1/com/sun/jna/Structure.html
'''
def CountStructerPointerJNA():
    fpath = path.split(path.split(path.dirname(__file__))[0])[0] + '\\DBSource\\DataLv1\\'
    fin = open(fpath+'jna_supported.json')
    plist = [] # API list
    with open(fpath+'jna_supported_struct.ls', 'rb') as f:
        jna_struct = pickle.load(f)
    for line in fin:
        api = json.loads(line)
        for idx,arg in enumerate(api["paramList"]):
            struct_cond = arg["type"] in jna_struct
            if struct_cond:
                plist.append((api["funcName"], idx))
    for item in plist:
        print item[0], item[1]
    print 'Structure pointer: ', len(plist)
    return plist
#CountStructerPointerJNA()

'''
@note: print the total pointer of supported mapping API in JNA
'''
def CountTotalPointerJNA():
    fpath    = path.split(path.split(path.dirname(__file__))[0])[0] + '\\DBSource\\DataLv1\\jna_supported.json'
    num_apis = sum(1 for line in open(fpath))
    pbuffer = CountBufferPointerJNA()
    pstring = CountStringPointerJNA()
    pcell   = CountCellPointerJNA()
    pstruct = CountStructerPointerJNA()
    ptotal  = pbuffer + pstring + pcell + pstruct
    print 'Buffer: %i pointers in %i APIs' % (len(pbuffer), len(set([api for (api,idx) in pbuffer])))
    print 'String: %i pointers in %i APIs' % (len(pstring), len(set([api for (api,idx) in pstring])))
    print 'Cell: %i pointers in %i APIs' % (len(pcell), len(set([api for (api,idx) in pcell])))
    print 'Structure pointer: %i pointers in %i APIs' % (len(pstruct), len(set([api for (api,idx) in pstruct])))
    print 'Total: %i pointers in %i APIs' % (len(ptotal), num_apis)
CountTotalPointerJNA()