# -*- coding: utf-8 -*-
'''
@Author : LE VINH
@Project: BE-PUM, API Stub generation
@Time   : JAIST (2015-2016)
@Summary: Classify memory length parameter - Apply Bayes learning
'''

#from __future__ import division
import nltk, random
import json, collections
from os import path
from PyNLP import SentSimilarity

# Build a training dataset (Json format) from its simple format (list)
def BuildTrainingSet():
    fin1    = open("../DBSource/DataLv1/ms_api.json")
    ms_apis = collections.OrderedDict()
    for line in fin1:
        api = json.JSONDecoder(object_pairs_hook=collections.OrderedDict).decode(line)
        ms_apis[api["fname"]] = api
        
    fin2    = open("rel_data/train_set.ls")
    apis    = {}
    for line in fin2:
        item  = line.split()
        fapi  = item[0]
        idx_pointer = item[1]
        idx_length  = item[2]
        if not fapi in api:
            apis[fapi]   = ms_apis[fapi]
        apis[fapi]['relation'].append({'source':idx_pointer, 'target':idx_length, 'category':'BufferMemory'})
    
    train_set = ''
    for api in apis:
        train_set += json.dumps(apis[api]) + '\n'
    print train_set    
    fout = open('rel_data/train_set.json', 'w')
    fout.write(train_set)
#BuildTrainingSet()


# Extract features of an API in the correct format for ML algorithm (new instance process)
def GetParaFeatures(api, buffer_idx, para_idx):
    ''' 
    @param buffer_idx: index of buffer parameter
    @param para_idx: index of current parameter
    '''
    features = {}
    
    para = api['fargs'][para_idx]
    if para_idx != buffer_idx:
        if '_In_' in para['SAL']:       # SAL annotations
            featr1 = '_In_'
        else: featr1 = '_Out_'
        
        featr2 = para['type']           # type of parameter
        featr3 = para_idx - buffer_idx  # distance between position of buffer pointer & length parameter

        base_sent  = 'the parameter describes the size of buffer' 
        first_sent = para['descrip'].split('.')[0] # the first sentence of description
        featr4 = SentSimilarity.GetCosine2(SentSimilarity.Text2Vector2(base_sent),SentSimilarity.Text2Vector2(first_sent))
        if featr4 > 0.8: featr4 = True
        else: featr4 = False
        
        featr5 = para['name'].endswith('Size') or para['name'].endswith('Length')
        
        features = {'sal':featr1, 'type':featr2, 'dis':featr3, 'simi':featr4, 'name':featr5}
    #print '>Feature: ', features    
    return features


# Extract features of an API in the correct format for ML algorithm (training process)
def ExtractFeatures(api):
    '''
    @param api: API information in plain string
    '''
    features = []    # list features of the api
    info_api = json.JSONDecoder(object_pairs_hook=collections.OrderedDict).decode(api)
    
    # iterate over parameters relation
    bpointer_idx = [] # list of buffer-pointer parameter's index
    blength_idx  = [] # list of buffer-length parameter's index
    for relation in info_api['relation']:
        if relation['category'] == 'BufferMemory':
            bpointer_idx.append(int(relation['source']))    # buffer-pointer parameter's index
            blength_idx.append(int(relation['target']))     # buffer-length parameter's index

    # iterate over parameters to extract features
    for idx, para in enumerate(info_api['fargs']):
        if '_In_' in para['SAL']:       # SAL annotations
            featr1 = '_In_'
        else: featr1 = '_Out_'
        featr2 = para['type']           # type of parameter
        featr3 = idx - bpointer_idx[0]  # distance between position of buffer pointer & length parameter

        base_sent  = 'the parameter describes the size of buffer' 
        first_sent = para['descrip'].split('.')[0] # the first sentence of description
        featr4 = SentSimilarity.GetCosine2(SentSimilarity.Text2Vector2(base_sent),SentSimilarity.Text2Vector2(first_sent))
        if featr4 > 0.8: featr4 = True
        else: featr4 = False
        
        featr5 = para['name'].endswith('Size') or para['name'].endswith('Length')
        
        if not (idx in blength_idx):
            features.append(({'sal':featr1, 'type':featr2, 'dis':featr3, 'name':featr5, 'simi':featr4}, 'false'))
        else:
            features.append(({'sal':featr1, 'type':featr2, 'dis':featr3, 'name':featr5, 'simi':featr4}, 'true'))
            
    #print '>Feature: ', features
    return features
#ExtractFeatures('{"fname": "GlobalGetAtomName", "flib": "kernel32", "ftype": "UINT", "fargs": [{"SAL": "_In_", "type": "ATOM", "name": "nAtom", "descrip": "The global atom associated with the character string to be retrieved."}, {"SAL": "_Out_", "type": "LPTSTR", "name": "lpBuffer", "descrip": "The buffer for the character string."}, {"SAL": "_In_", "type": "int", "name": "nSize", "descrip": "The size, in characters, of the buffer."}], "relation": [{"category": "BufferMemory", "source": "1", "target": "2"}]}')


# Build a classifier model (three options)
def BuildClassifierModel(algorithm):
    fpath = path.split(path.dirname(__file__))[0] + '\\Extractor\\rel_data\\train_set.json'
    fin1  = open(fpath)
    data_set  = [line for line in fin1]    
    random.shuffle(data_set)    
    data_feas = [ExtractFeatures(api) for api in data_set]
    data_feas = [item for sublist in data_feas for item in sublist] # flatten list of list
    #print 'The number of training elements:', len(data_feas)
    #for item in train_feas: print item
    
    classifier = None
    if (algorithm   == 'NaiveBayes'):
        classifier  = nltk.NaiveBayesClassifier.train(data_feas)
    elif (algorithm == 'DecisionTree'):
        classifier  = nltk.DecisionTreeClassifier.train(data_feas)
    elif (algorithm == 'Maxent'):
        classifier  = nltk.MaxentClassifier.train(data_feas, trace=0)
    #print classifier
    return classifier


# Evaluate classifier model (1 times)
def EvaluateModel1():
    fin1 = open("rel_data/train_set.json")
    data_set = [line for line in fin1]
    print len(data_set)
    
    random.shuffle(data_set)
    test_api  = data_set[ : len(data_set)/3]
    train_api = data_set[len(data_set)/3 : ]
    
    # Train set
    train_feas = [ExtractFeatures(api) for api in train_api]
    print sum([len(item) for item in train_feas])
    train_feas = [item for sublist in train_feas for item in sublist] # flatten list of list
    print 'The number of training elements:', len(train_feas)    
    #for item in train_feas: print item
    
    # Test set
    test_feas = [ExtractFeatures(api) for api in test_api]
    test_feas = [item for sublist in test_feas for item in sublist] # flatten list of list
    print 'The number of testing elements:', len(test_feas)
    #for item in test_feas: print item
    
    classifier1 = nltk.NaiveBayesClassifier.train(train_feas)
    print 'The accuracy of Naive Bayes method:', (nltk.classify.accuracy(classifier1, test_feas))
    classifier2 = nltk.DecisionTreeClassifier.train(train_feas)
    print 'The accuracy of Decision Tree method:', (nltk.classify.accuracy(classifier2, test_feas))
    classifier3 = nltk.MaxentClassifier.train(train_feas, trace=0)
    print 'The accuracy of Maxent Classifier method:', (nltk.classify.accuracy(classifier3, test_feas))
    
    # Find error cases in Bayes Model. E.g., LoadString(para: uID), GetVolumeInformation(lpMaximumComponentLength)
    print len(test_feas)
    for (featr, tag) in test_feas:
        guess = classifier1.classify(featr)
        if guess != tag:
            print tag, guess, featr
            dist = classifier1.prob_classify(featr)
            for label in dist.samples():
                print("%s: %f" % (label, dist.prob(label)))
    # lỗi do trường hợp lạ đặc biệt, length được lưu bởi 1 biến con trỏ
#EvaluateModel1()


# Evaluate classifier model (3-fold cross validation)
def EvaluateModel2():
    fin1 = open("rel_data/train_set.json")
    data_set = [line for line in fin1]
    data_set = [ExtractFeatures(api) for api in data_set]
    data_set = [item for sublist in data_set for item in sublist] # flatten list of list
    print len(data_set)
    random.shuffle(data_set)
    
    # 3-fold cross validation
    num_folds = 3
    subset_size = len(data_set)/num_folds
    Naive_Bayes_Acc     = []
    Decision_Tree_Acc   = []
    Maxent_Acc          = []
    for i in range(num_folds):
        test_feas  = data_set[i*subset_size : (i+1)*subset_size]
        train_feas = data_set[:i*subset_size] + data_set[(i+1)*subset_size:]
        print 'The number of training elements:', len(train_feas)
        print 'The number of testing elements:', len(test_feas)
        
        classifier1 = nltk.NaiveBayesClassifier.train(train_feas)
        Naive_Bayes_Acc.append(nltk.classify.accuracy(classifier1, test_feas))
        print 'The accuracy of Naive Bayes method:', Naive_Bayes_Acc[i]
#         print classifier1.classify({'sal':'_Int_', 'type':'int', 'dis':1, 'simi':1, 'name':False})
#         dist = classifier1.prob_classify({'sal':'_Int_', 'type':'int', 'dis':1, 'simi':1, 'name':False})
#         for label in dist.samples():
#             print("%s: %f" % (label, dist.prob(label)))
        
        classifier2 = nltk.DecisionTreeClassifier.train(train_feas)
        Decision_Tree_Acc.append(nltk.classify.accuracy(classifier2, test_feas))
        print 'The accuracy of Decision Tree method:', Decision_Tree_Acc[i]
        
        classifier3 = nltk.MaxentClassifier.train(train_feas, trace=0)
        Maxent_Acc.append(nltk.classify.accuracy(classifier3, test_feas))
        print 'The accuracy of Maxent Classifier method:', Maxent_Acc[i]
    
    print 'Average the accuracy of Naive Bayes method:', float(sum(Naive_Bayes_Acc))/len(Naive_Bayes_Acc)
    print 'Average the accuracy of Decision Tree method:', float(sum(Decision_Tree_Acc))/len(Decision_Tree_Acc)
    print 'Average the accuracy of Maxent Classifier method:', float(sum(Maxent_Acc))/len(Maxent_Acc)        
#EvaluateModel2()


# Print description of pointer-length parameter (analysis purpose)
def PrintTextToAnalysis():
    fin     = open('rel_data/train_set.json')
    api_ls  = collections.OrderedDict()
    for line in fin:
        api = json.JSONDecoder(object_pairs_hook=collections.OrderedDict).decode(line)
        api_ls[api["fname"]] = api
    
    fin2 = open("rel_data/train_set.ls")
    for line in fin2:
        items       = line.split()
        fapi_name   = items[0]
        idx_pointer = int(items[1])
        idx_size    = int(items[2])
        print api_ls[fapi_name]["fname"], idx_pointer, idx_size
        
        para_pointer= api_ls[fapi_name]['fargs'][idx_pointer]        
        print para_pointer['SAL'], para_pointer['type'], para_pointer['name']
        print para_pointer['descrip']
        
        para_size = api_ls[fapi_name]['fargs'][idx_size]
        print para_size['SAL'], para_size['type'], para_size['name']
        print para_size['descrip']
#PrintTextToAnalysis()

# Explicit show an example of computation process
def ShowExample():
    fin1 = open("rel_data/train_set.json")
    data_set = [line for line in fin1]
    data_set = [ExtractFeatures(api) for api in data_set]
    data_set = [item for sublist in data_set for item in sublist] # flatten list of list
    num_data = len(data_set)
    print len(data_set)
    
    for item in data_set: print item
    
    count = 0
    for item in data_set:
        if ('_In_' in item[0]['sal']) and (item[1] == 'true'): count += 1
    print 'SAL|yes', count, count/num_data
     
    count = 0
    for item in data_set:
        if ('_In_' in item[0]['sal']) and (item[1] == 'false'): count += 1
    print 'SAL|no', count, count/num_data
     
    count = 0
    for item in data_set:
        if item[0]['type'] == 'int' and item[1] == 'true': count += 1
    print 'type|yes', count, count/num_data
     
    count = 0
    for item in data_set:
        if item[0]['type'] == 'int' and item[1] == 'false': count += 1
    print 'type|no', count, count/num_data
     
    count = 0
    for item in data_set:
        if item[0]['dis'] == 1 and item[1] == 'true': count += 1
    print 'dis|yes', count, count/num_data
     
    count = 0
    for item in data_set:
        if item[0]['dis'] == 1 and item[1] == 'false': count += 1
    print 'dis|no', count, count/num_data
     
    count = 0
    for item in data_set:
        if item[0]['simi'] == True and item[1] == 'true': count += 1
    print 'simi|yes', count, count/num_data
     
    count = 0
    for item in data_set:
        if item[0]['simi'] == True and item[1] == 'false': count += 1
    print 'simi|no', count, count/num_data
     
    count = 0
    for item in data_set:
        if item[0]['name'] == False and item[1] == 'true': count += 1
    print 'name|yes', count, count/num_data
     
    count = 0
    for item in data_set:
        if item[0]['name'] == False and item[1] == 'false': count += 1
    print 'name|no', count, count/num_data
    
    count = 0
    for item in data_set:
        if item[1] == 'true': count += 1
    print 'true', count, count/num_data
    
    count = 0
    for item in data_set:
        if item[1] == 'false': count += 1
    print 'false', count, count/num_data
#ShowExample()