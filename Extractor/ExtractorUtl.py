# -*- coding: utf-8 -*-
'''
@Author : LE VINH
@Project: BE-PUM, API Stub generation
@Time   : JAIST (2015-2016)
@Summary: Utilites for extracting
'''

import re, json, collections
from os import path
from Crawler import CrawlerUtl
from PySystem import SystemUtl

# Get the list of APIs in the local database
def GetListAPI_MS():
    fpath = path.split(path.dirname(__file__))[0] + '\\DBSource'
    files = SystemUtl.GetFiles(fpath+'\\MS-HTML')
    return files
#print len(GetListAPI_MS())


# Get the description of a parameter at index idx
def GetParaDescrip_MS(fname, idx):
    '''
    @param fname: name of API
    @param idx: index (position) of parameter  
    '''
    url  = path.split(path.dirname(__file__))[0] + '\\DBSource\\MS-HTML\\' + fname + '.html'    
    html = CrawlerUtl.GetHTMLTree(url, 'offline')
    main = html.find("div", {"id": "mainSection"})
    dl   = main.find_all('dl')[0]
    dds  = dl.find_all('dd')
    para = dds[idx]
    desc_para = para.get_text().strip().replace('\n',' ')
    desc_para = re.sub("\s\s+" , " ", desc_para)
    print desc_para
    return desc_para
#GetParaDescrip_MS("WriteProcessMemory", 4)


# Get the list of APIs in a DLL (declared in JNA)
def GetListAPI_JNA(fdll):
    '''
    @param fdll: library name
    '''
    api_ls = []
    fpath  = path.split(path.dirname(__file__))[0] + '\\DBSource\\DataLv1\\jna_supported_api.ls'
    fin = open(fpath)
    for line in fin:
        item = line.split()
        if re.sub(r'<|.html>', '',item[0]) == fdll: api_ls.append(item[2])
    print 'JNA has already declared %i API of library %s' % (len(api_ls), fdll)
    return (fdll, api_ls)
#GetListAPI_JNA('User32')


# Get the API method signature in JNA
def GetAPISignature_JNA(fname):
    '''
    @param fname: API name
    '''
    fpath = path.split(path.dirname(__file__))[0] + '\\DBSource\\DataLv1\\jna_supported_api.json'
    fin   = open(fpath)
    signature_json = collections.OrderedDict()
    
    # Load file json into a dictionary
    for line in fin:
        #api = json.loads(line)
        # get JSON to load into an OrderedDict in Python
        api = json.JSONDecoder(object_pairs_hook=collections.OrderedDict).decode(line)
        signature_json[api["fname"]] = api
    if fname in signature_json:
        print signature_json[fname]
#GetAPISignature_JNA('OpenSCManager')