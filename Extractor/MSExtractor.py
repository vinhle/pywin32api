# -*- coding: utf-8 -*-
'''
@Author : LE VINH
@Project: BE-PUM, API Stub generation
@Time   : JAIST (2015-2016)
@Summary: Extract API information from HTML files and save to json dictionary
'''

import re, json, collections
import InfoAPI, ExtractorUtl
from os import path
from Crawler import CrawlerUtl


# Extract information from HTML description (without parameter description)
def ExtractAPIBasicFeatures(fname):
    '''
    @param fname: API name  
    @return: InfoAPI object
    '''
    fpath = path.split(path.dirname(__file__))[0] + '\\DBSource\\MS-HTML\\'+ fname
    html  = CrawlerUtl.GetHTMLTree(fpath, 'offline')
        
    # Library location
    flib  = ""
    ("table", {"class":"memberSummary", "summary":"Method Summary table, listing methods, and an explanation"})
    table = html.find_all('table', {"class":""})[-1] # the last tag <table> in page            
    rows  = table.find_all('tr')                     # all tag <tr> in this table
    for row in rows:
        if row.th.p.get_text().strip() == "Library":
            flib = row.td.dl.dt.get_text()
            flib = re.match(r"(\w+).lib", flib).group(0)
            flib = flib[:-4].lower()        # library name
    
    # Function structure 
    funct    = html.find("div", class_="codeSnippetContainerCode")
    fcontent = funct.div.pre.get_text().strip()
    #fcontent = re.match(ur"(([.+\n]|[.+])+\);)", fcontent).group()
    #print type(fcontent), fcontent
    lines = fcontent.split('\n')
    fline = lines[0].split()
    ftype = fline[0]                                # type of return value    
    fname = re.match(r"(\w+)", fline[-1]).group(0)  # function name
    paras = lines[1:-1]                             # input parameters list
    fargs = []
    for para in paras:
        words = para.split()[::-1]
        words = [w.replace(',', '') for w in words]        
        if len(words) == 2:
            fargs.append((words[0], words[1]))
        else:
            fargs.append((words[0], words[1], words[2]))
    
    # Create API object
    api_obj = InfoAPI.API(ftype, fname, fargs, flib, 'descrip')
    return api_obj.MakeFullJson()
#ExtractAPIBasicFeatures("AccessCheck.html")


# Extract information from HTML description (with parameter description)
def ExtractAPIBasicFeatures2(fname):
    '''
    @param fname: API name  
    @return: InfoAPI object
    '''
    fpath = path.split(path.dirname(__file__))[0] + '\\DBSource\\MS-HTML\\'+ fname
    html  = CrawlerUtl.GetHTMLTree(fpath, 'offline')
        
    # Library location
    flib  = ""
    ("table", {"class":"memberSummary", "summary":"Method Summary table, listing methods, and an explanation"})
    table = html.find_all('table', {"class":""})[-1] # the last tag <table> in page            
    rows  = table.find_all('tr')                     # all tag <tr> in this table
    for row in rows:
        if row.th.p.get_text().strip() == "Library":
            flib = row.td.dl.dt.get_text()
            flib = re.match(r"(\w+).lib", flib).group(0)
            flib = flib[:-4].lower()        # library name
    
    # Function structure 
    funct    = html.find("div", class_="codeSnippetContainerCode")
    fcontent = funct.div.pre.get_text().strip()
    #fcontent = re.match(ur"(([.+\n]|[.+])+\);)", fcontent).group()
    #print type(fcontent), fcontent
    lines = fcontent.split('\n')
    fline = lines[0].split()
    ftype = fline[0]                                # type of return value    
    fname = re.match(r"(\w+)", fline[-1]).group(0)  # function name
    paras = lines[1:-1]                             # input parameters list
    fargs = []  # list of parameter without parameter discription
    for para in paras:
        words = para.split()[::-1]
        words = [w.replace(',', '') for w in words]        
        opt = ''
        if 'volatile' in words: opt = 'volatile'    # volatile/constant keyword
        words = [w for w in words if w != 'const' and w != 'volatile']
        
        if len(words) == 2:
            if words[0].startswith('*'):
                words[1] += '*'             # E.g: DWORD *a; -> considering type 'DWORD*', para name 'a'
                words[0]  = words[0][1:]
            fargs.append((words[0], words[1], opt))
        else:
            if words[0].startswith('*'):
                words[1] += '*'             # E.g: DWORD *a; -> considering type 'DWORD*', para name 'a'
                words[0]  = words[0][1:]
            fargs.append((words[0], words[1], opt, words[2]))
        print fargs[-1]
        
    # Parameter description
    descripts = html.find("div", {"id": "mainSection"}).find_all("dl")[0]
    descripts = descripts.find_all("dd")
    para_des  = []
    for item in descripts:
        tmp = item.find_all("p")
        mota = ""
        for t in tmp:
            chuoi = t.get_text().strip().replace("\n", " ")
            chuoi = re.sub(' +|\t+',' ', chuoi)
            chuoi = re.sub(' +|\t+',' ', chuoi)
            if not chuoi.startswith("Type"):
                mota += chuoi
        para_des.append(mota)
    fparas = []
    for arg in fargs:
        tmp = arg + (para_des[fargs.index(arg)],)
        fparas.append(tmp)
    
    # Create API object
    api_obj = InfoAPI.API(ftype, fname, fparas, flib, 'hello')
    return api_obj.MakeFullJson()
#ExtractAPIBasicFeatures2("ChangeDisplaySettingsEx.html")
#ExtractAPIBasicFeatures2("InterlockedCompareExchange.html")
#ExtractAPIBasicFeatures2("InternetCloseHandle.html")


# Write all APIs specification into a json file 'ms_api.json'
def WriteJsonList():
    files = ExtractorUtl.GetListAPI_MS()
    fpath = path.split(path.dirname(__file__))[0] + '\\DBSource'    
    fout = open(fpath+'\\DataLv1\\ms_api.json', 'w')
    list_api = ""
    for api in files:
        print api
        list_api += ExtractAPIBasicFeatures2(api.strip()) + '\n'
    fout.write(list_api)
#WriteJsonList()


# Get a dictionary contains a list of APIs grouped by DLLs
def APIsGroupByDLL():
    fpath = path.split(path.dirname(__file__))[0] + '\\DBSource\\'
    fin   = open(fpath + 'DataLv1\\ms_api.json')    
    ms_apis = collections.OrderedDict()
    for line in fin:
        api = json.JSONDecoder(object_pairs_hook=collections.OrderedDict).decode(line)
        ms_apis[api["fname"]] = api
    
    apisGroup = {}
    for api in ms_apis:
        dll = ms_apis[api]['flib']
        if not dll in apisGroup: apisGroup[dll] = []
        apisGroup[dll].append(ms_apis[api]['fname'])
    
    str_out = ''
    for dll in apisGroup:
        str_out += dll + '\n'
        for api in apisGroup[dll]:
            str_out += api.strip() + ' '
        str_out += '\n'
    fout = open(fpath + 'DataLv2\\api_groupby_dll.ls', 'w')
    fout.write(str_out)
    #print str_out
#APIsGroupByDLL()