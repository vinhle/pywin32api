# -*- coding: utf-8 -*-
'''
@Author : LE VINH
@Project: BE-PUM, API Stub generation
@Time   : JAIST (2015-2016)
@Summary: Analyze APIs stub in BE-PUM
'''

import re, json, collections
import InfoAPI
from os import path
from PySystem import SystemUtl


# Get the dictionary of all manually implemented API Stub grouped by dll in BE-PUM
def GetYenAPIStub():
    fpath   = path.split(path.dirname(__file__))[0] + '\\DBSource\\BEPUM-YenAPIStub\\'
    folders = SystemUtl.GetSubFolder(fpath)
    
    apistubs= {}
    for folder in folders:
        apistubs[folder] = []
        files = SystemUtl.GetFiles(fpath + '\\' + folder)
        for item in files:
            if not item in apistubs[folder]:
                apistubs[folder].append(item)
    
    fpath    = path.split(path.dirname(__file__))[0] + '\\DBSource\\DataLv2\\'
    with open(fpath+'yen_apistub.ls', 'w') as fout:
        json.dump(apistubs, fout, indent=2) #indent: add new line
#GetYenAPIStub()

# Get the list of API which were implemented in BE-PUM, but the local database doesn't contain thier description
def GetUnsportedAPIDescrip():
    fpath = path.split(path.dirname(__file__))[0] + '\\DBSource\\'
    fin   = open(fpath + 'DataLv2\\yen_apistub.ls')
    stubs = json.load(fin)    
    apistubs = []
    for dll in stubs:
        for item in stubs[dll]: apistubs.append(item.replace('.java',''))
        
    apiHTMLs = SystemUtl.GetFiles(fpath + 'MS-HTML\\')
    for idx in range(len(apiHTMLs)):
        apiHTMLs[idx] = apiHTMLs[idx].replace('.html','')
    
    unsupportAPIs = [] 
    for item in apistubs:
        if not (item in apiHTMLs): unsupportAPIs.append(item)
    print len(unsupportAPIs)
    for item in unsupportAPIs: print item,
#GetUnsportedAPIDescrip()

# Extract the basic information in implemented API interface in BE-PUM (by Yen-san)
def ExtractAPIBasicFeatures(fsig):
    '''
    @param fsig: API signature
    @return: InfoAPI object
    '''
    flib  = re.match(r'.+; (.+)\.java', fsig).group(1)
    ftype = re.match(r'(\w+) ', fsig).group(1)
    fname = re.match(r'.+ (.+)\(', fsig).group(1)
    paras = re.match(r'.+\((.+)\)', fsig)
    fargs = []
    if paras != None:
        paras = paras.group(1).replace(', ', ',')
        paras = paras.split(",")
        for para in paras:
            para = para.split()
            fargs.append((para[1], para[0]))
    
    api_obj = InfoAPI.API(ftype, fname, fargs, flib, 'hello')
    #print  api_obj
    api_json = api_obj.MakeBasicJson()
    return api_json
#ExtractAPIBasicFeatures('DWORD SetBkColor(HDC hdc, DWORD crColor); Gdi32DLL.java')


# Write all API information into the json file 'yen_supported_api.json'
def WriteJsonList():
    fpath = path.split(path.dirname(__file__))[0] + '\\DBSource\\DataLv1'
    fin   = open(fpath+'\\yen_supported_api.ls')
    list_api = ''
    for line in fin:
        list_api += ExtractAPIBasicFeatures(line) + '\n'
    fout  = open(fpath+'\\yen_supported_api.json', 'w')
    fout.write(list_api)
#WriteJsonList()


# Find the overlap APIs interface between pre-defined in JNA and implemented in BE-PUM
def FindOverlapAPI():
    fpath = path.split(path.split(path.dirname(__file__))[0])[0] + '\\DBSource\\DataLv1\\'  
    
    fin1 = open(fpath + 'jna_supported.json')
    jna_list = collections.OrderedDict()
    for line in fin1:
        api = json.loads(line)
        jna_list[api["funcName"]] = api
    
    fin2 = open(fpath + 'yen_supported_api.json')
    yen_list = collections.OrderedDict()
    for line in fin2:
        api = json.loads(line)
        yen_list[api["funcName"]] = api
    
    overlap = []
    for item in yen_list:
        if item in jna_list: overlap.append(yen_list[item]["funcName"])
    print 'The number of overlap API interface:', len(overlap)
    for item in overlap: print item
#FindOverlapAPI()

# def GetYenAPIStub2():
#     fpath   = path.split(path.dirname(__file__))[0] + '\\DBSource\\BEPUM-YenAPIStub\\kernel32\\'    
#     files = SystemUtl.GetFiles(fpath)
#     for item in files: print item
# GetYenAPIStub2()