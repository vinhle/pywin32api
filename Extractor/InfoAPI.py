# -*- coding: utf-8 -*-
'''
@Author : LE VINH
@Project: BE-PUM, API Stub generation
@Time   : JAIST (2015-2016)
@Summary: Class for API specification
'''

import json, collections

class API:
    count = 0
    # Define an InfoAPI class
    def __init__(self, ftype, fname, fargs, flib, fdes):
        API.count += 1
        self.id    = API.count      # int, function id
        self.ftype = ftype          # string, type of return value 
        self.fname = fname          # string, function name
        self.fargs = fargs          # list of tuple (type_arg, name_arg, pass_arg, option, descrip)
        self.flib  = flib           # string, library (dll)
        self.fdes  = fdes           # string, function description        
    
    # Compare two object, used in sorted list
    def __eq__(self, other):
        return self.ftype == other.ftype and self.fname == other.fname and self.fargs == other.fargs 
    
    #  Return a printable representation of an object
    def __repr__(self):
        count = 0
        txt   = self.ftype + ' ' + self.fname + '('
        for arg in self.fargs:
            count += 1
            arg_rev = arg[::-1]
            if len(arg_rev) == 2:
                txt += arg_rev[0] + ' ' + arg_rev[1]
            else:
                txt += arg_rev[0] + ' ' + arg_rev[1] + ' ' + arg_rev[2]
            if count < len(self.fargs): txt += ', '
        txt += ') in ' + self.flib + '.lib\n'
        txt += self.fdes
        return txt
    
    # Clone a new object
    def __copy__(self):
        #sprint '__copy__()'
        return API(self.ftype, self.fname, self.fargs, self.flib, self.fdes)
    
    # Create the json presentation of an object with relation information
    def MakeFullJson(self):
        apis = collections.OrderedDict()     # Order of dictionary for original input
        args = {}                            # Dictionary for parameters
        rels = {}                            # Dictionary for relations
        apis["fname"]     = self.fname
        apis["flib"]      = self.flib
        apis["ftype"]     = self.ftype
        
        apis["fargs"]    = []
        for arg in self.fargs:
            args["name"] = arg[0]
            args["type"] = arg[1]
            if len(arg) == 4:
                args["opt"] = arg[2]
                args["SAL"] = ""                
                args["descrip"] = arg[3]
            else:
                args["opt"] = arg[2]
                args["SAL"] = arg[3]
                args["descrip"] = arg[4]
            apis["fargs"].append(args.copy())
        
        apis["relation"] = []
        """for tmp in self.frels:
            rels["pbuffer"]  = tmp[0]
            rels["plength"]  = tmp[1]
            rels["accur"]    = tmp[2]
            api["relation"].append(rels)"""
        
        json_str = json.dumps(apis)
        print type(json_str), json_str
        return json_str
    
    # Create the basic json presentation of an object
    def MakeBasicJson(self):
        apis = collections.OrderedDict()     # Order of dictionary for original input
        args = {}                            # Dictionary for parameters
        apis["fname"]    = self.fname
        apis["flib"]     = self.flib
        apis["ftype"]    = self.ftype
        
        apis["fargs"]    = []
        for arg in self.fargs:
            args["name"] = arg[0]
            #print args["name"], arg[0]
            args["type"] = arg[1]
            apis["fargs"].append(args.copy())
        
        json_str = json.dumps(apis)
        print type(json_str), json_str
        return json_str