# -*- coding: utf-8 -*-
'''
@Author : LE VINH
@Project: BE-PUM, API Stub generation
@Time   : JAIST (2015-2016)
@Summary: Classify a pointer is Cell or Buffer pointer
'''

from __future__ import division
import json, random, collections
from os import path
from PyNLP import SentSimilarity

# Predict an pointer parameter is whether cell pointer or buffer pointer
def isBufferPointer(arg):
    # Load type mapping table
    fpath = path.split(path.dirname(__file__))[0] + '\\DBSource\\DataLv2\\'
    fin   = open(fpath+'type_table.ls', 'r')
    type_table = json.load(fin)
    
    isPointer = 0 # 0: not pointer, 1: cell pointer, 2: buffer pointer
    if arg['type'].endswith('*'):   isPointer = 1
    if arg['type'].endswith('STR'): isPointer = 1
    if arg['type'].startswith('P'):
        if arg['type'][1:] in type_table: isPointer = 1 # check whether type is a structure started by P itself (POINT)
    if arg['type'].startswith('LP'):
        if arg['type'][2:] in type_table: isPointer = 1
    
    if isPointer == 0: return 0, 0 # not pointer
    base_sent  = 'the parameter points to a buffer'
    first_sent = arg['descrip'].split('.')[0] # the first sentence of description
    simi_degree= SentSimilarity.GetCosine1(SentSimilarity.Text2Vector1(base_sent),SentSimilarity.Text2Vector1(first_sent))
    if simi_degree > 0.8: isPointer = 2
    return isPointer, simi_degree
#arg = {"opt": "", "SAL": "_In_", "type": "POINT*", "name": "lppt", "descrip": "A pointer to an array of POINT structures that define the vertices of the polygons in logical units. The polygons are specified consecutively. Each polygon is presumed closed and each vertex is specified only once."}
#arg = {"opt": "", "SAL": "_Out_opt_", "type": "LPSTR", "name": "lpOemName", "descrip": "A pointer to a buffer that receives the OEM string that corresponds to Name. This parameter can be NULL."}
#arg = {"opt": "", "SAL": "_Inout_", "type": "PULONG_PTR", "name": "NumberOfPages", "descrip": "The size of the physical memory to allocate, in pages.To determine the page size of the computer, use the GetSystemInfo function. On output, this parameter receives the number of pages that are actually allocated, which might be less than the number requested."}
# print isBufferPointer(arg)

# Evaluate buffer pointer prediction by cosin similarity method
def EvaluateBufferPointerMethod():
    fpath = path.split(path.dirname(__file__))[0] + '\\Extractor\\rel_data\\'
    fin1 = open(fpath + 'train_set.json')
    data_set = [json.JSONDecoder(object_pairs_hook=collections.OrderedDict).decode(line) for line in fin1]
    random.shuffle(data_set)
    
    instance_count = 0
    correct_count  = 0
    simi_degree_ls = []
    for api in data_set:
        bufferpointes = [int(item['source']) for item in api['relation']] # a list of buffer pointer index
        #print api['fname'], bufferpointes
        for idx, arg in enumerate(api['fargs']):
            instance_count += 1
            isPointer, simi_degree = isBufferPointer(arg)
            if isPointer == 2: # evaluate method
                if idx in bufferpointes: correct_count += 1
                else: print 'Incorrect prediction (should not buffer_pointer but yes): ', api['fname'], idx
            else:
                if not (idx in bufferpointes): correct_count += 1
                else: print 'Wrong prediction: (should buffer_pointer but no)', api['fname'], idx
            
            if idx in bufferpointes: # evaluate cosin similarity threshold
                simi_degree_ls.append(simi_degree)
    
    print 'Total parameter instances: ', instance_count
    print 'Total correct prediction: ', correct_count
    print 'Accuracy: ' + str(100*correct_count/instance_count) + '%'
    print 'Average cosin similarity of buffer pointer parameter: ', sum(simi_degree_ls)/len(simi_degree_ls)
    ''' e.g., Failure:
    A pointer to the first member in an array (AnimatePalette, ppe)
    '''
#EvaluateBufferPointerMethod()


# Find API and array paramter types (in Java) from implemented APIs in JNA and BE-PUM
def FindArrayArguments():
    '''
    @note: the purpose of this function is to analyze
    @return: a dictionary of pairs (API -> array types)
    '''
    fpath = path.split(path.dirname(__file__))[0] + '\\DBSource\\DataLv1\\'
    
    # Load jna_supported_api to dictionary
    fin1  = open(fpath + 'jna_supported_api.json')
    jna_apis = collections.OrderedDict()
    for line in fin1:
        api = json.JSONDecoder(object_pairs_hook=collections.OrderedDict).decode(line)
        jna_apis[api["fname"]] = api
        
    # Load yen_supported_api to dictionary
    fin2  = open(fpath + 'yen_supported_api.json')
    yen_apis = collections.OrderedDict()
    for line in fin2:
        api = json.JSONDecoder(object_pairs_hook=collections.OrderedDict).decode(line)
        yen_apis[api["fname"]] = api
        
    # Load ms_html.json to dictionary
    fin3  = open(fpath + 'ms_api.json')    
    ms_apis  = collections.OrderedDict()
    for line in fin3:
        api = json.JSONDecoder(object_pairs_hook=collections.OrderedDict).decode(line)
        ms_apis[api["fname"]] = api
    
    api_args = {}   # list of API - thier array arguments
    for api in jna_apis:
        for arg in jna_apis[api]['fargs']:
            if arg['type'].endswith('[]'):
                api_args[jna_apis[api]['fname']] = [arg['type'], arg['name']]
    for api in yen_apis:
        for arg in yen_apis[api]['fargs']:
            if arg['type'].endswith('[]'):
                api_args[yen_apis[api]['fname']] = [arg['type'], arg['name']]
                
    for item in api_args:
        print item, api_args[item]
#FindArrayArguments()