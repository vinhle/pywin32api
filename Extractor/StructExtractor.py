# -*- coding: utf-8 -*-
'''
@Author : LE VINH
@Project: BE-PUM, API Stub generation
@Time   : JAIST (2015-2016)
@Summary: Extract Struct information from HTML files
'''

import re
from os import path
from Crawler import CrawlerUtl
from PySystem import SystemUtl

# Get Struct definition from HTML description
def ExtractStruct(sname):
    fpath = path.split(path.dirname(__file__))[0] + '\\DBSource\\MS-Struct\\'+ sname
    html  = CrawlerUtl.GetHTMLTree(fpath, 'offline')
    struct   = html.find("div", class_="codeSnippetContainerCode")
    scontent = struct.div.pre.get_text().strip()
    #print scontent
    return scontent
#ExtractStruct('BITMAPINFO.html')


# Get the name of header file (xxx.h), which it belongs to
def ExtractHeader(sname):
    fpath = path.split(path.dirname(__file__))[0] + '\\DBSource\\MS-Struct\\'+ sname
    html  = CrawlerUtl.GetHTMLTree(fpath, 'offline')
    
    # Header location
    sheader  = ""
    table = html.find_all('table', {"class":""})[-1] # the last tag <table> in page            
    rows  = table.find_all('tr')            # all tag <tr> in this table
    for row in rows:
        if row.th.p.get_text().strip() == "Header":
            sheader = row.td.dl.dt.get_text()
            sheader = re.match(r"(\w+).h", sheader).group(0)
            sheader = sheader.lower()   # header name
    #print sheader
    return sheader
#print ExtractHeader('BITMAPINFO.html')

# Get all names of header files containing Struct definition (only among local database)
def FindAllHeader():
    fpath = path.split(path.dirname(__file__))[0] + '\\DBSource\\MS-Struct\\'
    structs = SystemUtl.GetFiles(fpath)
    headers = []
    for item in structs:
        headers.append(ExtractHeader(item))
    headers = set(headers)
    print len(headers), headers
#FindAllHeader()