# -*- coding: utf-8 -*-
'''
@Author : LE VINH
@Project: BE-PUM, API Stub generation
@Time   : JAIST (2015-2016)
@Summary: Build a table of rules used to infer Java type from C type
'''

import json, collections
from os import path

# Type mapping table between Microsoft and (jna_supported_api and yen_supported_api)
def BuildTypeMapping():
    fpath = path.split(path.dirname(__file__))[0] + '\\DBSource\\DataLv1\\'
    msjna_type_map = {}
    
    # Load file jna_supported_api to dictionary
    fin1  = open(fpath + 'jna_supported_api.json')
    jna_apis = collections.OrderedDict()
    for line in fin1:
        api = json.JSONDecoder(object_pairs_hook=collections.OrderedDict).decode(line)
        jna_apis[api["fname"]] = api
    # Load file yen_supported_api to dictionary
    fin2  = open(fpath + 'yen_supported_api.json')
    yen_apis = collections.OrderedDict()
    for line in fin2:
        api = json.JSONDecoder(object_pairs_hook=collections.OrderedDict).decode(line)
        yen_apis[api["fname"]] = api
    # Load file ms_html.json to dictionary
    fin3  = open(fpath + 'ms_api.json')    
    ms_apis  = collections.OrderedDict()
    for line in fin3:
        api = json.JSONDecoder(object_pairs_hook=collections.OrderedDict).decode(line)
        ms_apis[api["fname"]] = api
    
    # Build type mapping from jna_apis
    for api in jna_apis:
        if api in ms_apis:
            for ms_arg, jna_arg in zip(ms_apis[api]['fargs'], jna_apis[api]['fargs']):
                if not ms_arg['type'] in msjna_type_map:
                    msjna_type_map[ms_arg['type']] = [jna_arg['type']]
                else:
                    if not jna_arg['type'] in msjna_type_map[ms_arg['type']]:
                        msjna_type_map[ms_arg['type']].append(jna_arg['type'])
                #print ms_arg['type'], jna_arg['type']
    # Build type mapping from yen_apis
    for api in yen_apis:
        if api in ms_apis:
            for ms_arg, yen_arg in zip(ms_apis[api]['fargs'], yen_apis[api]['fargs']):
                if not ms_arg['type'] in msjna_type_map:
                    msjna_type_map[ms_arg['type']] = [yen_arg['type']]
                else:
                    if not yen_arg['type'] in msjna_type_map[ms_arg['type']]:
                        flag = 0
                        for item in msjna_type_map[ms_arg['type']]: # remove case WinDef.DWORDByReference and DWORDByReference
                            if yen_arg['type'] in item: flag = 1; break
                        if flag == 0:
                            msjna_type_map[ms_arg['type']].append(yen_arg['type'])
                        
    with open(fpath+'msjna_type_map.ls', 'w') as fout:
        json.dump(msjna_type_map, fout, indent=2) #indent: add new line
    
    print 'the number of possible type mapping:', len(msjna_type_map)
    count = 0
    for api in msjna_type_map:
        if len(msjna_type_map[api]) > 1:
            count += 1
            print api, msjna_type_map[api]
    print 'the number of types which has more than one mapping: ', count
#BuildTypeMapping()

def ReviewTypeMapping():
    fpath = path.split(path.dirname(__file__))[0] + '\\DBSource\\DataLv1\\'    
    fin  = open(fpath+'msjna_type_map.ls', 'r')
    msjna_type_map = json.load(fin)
    
    review_typemapping = {'PDWORD': [u'WinDef.DWORDByReference'], 
                          'HGLOBAL': [u'HANDLE'],
                          'SIZE_T*': [u'ULONG_PTRByReference'],
                          'HWND': [u'WinDef.HWND'],
                          'LPVOID': [u'String', u'Structure', u'Pointer', u'byte[]', u'WinNT.FILE_NOTIFY_INFORMATION', u'WinDef.LPVOID', u'ByteBuffer'],
                          'POINT*': [u'WinDef.POINT[]', u'WinDef.POINT'],
                          'PACL': [u'ACL'],
                          'LARGE_INTEGER*': [u'LARGE_INTEGER'],
                          'ULONG': [u'WinDef.ULONG'],
                          'PSID': [u'WinNT.PSID'],
                          'LPARAM': [u'WinDef.LPARAM'],
                          'HLOCAL': [u'HANDLE'],
                          'PULONG': [u'WinDef.ULONGByReference'],
                          'SIZE_T': [u'BaseTSD.SIZE_T'],
                          'REGSAM': [u'int'],
                          'LPBYTE': [u'byte[]', u'Pointer', u'Buffer'],
                          'LONG': [u'LONG'],
                          'LPCTSTR': [u'String', u'WString', u'char[]'],
                          'HANDLE': [u'WinNT.HANDLE'],
                          'COLORREF': [u'DWORD'],
                          'LPDWORD': [u'WinDef.DWORDByReference'],
                          'BOOL': [u'WinDef.BOOL'],
                          'PSID*': [u'WinNT.PSIDByReference'],
                          'VOID*': [u'Pointer', u'PointerByReference', u'LPVOID'],
                          'LPTSTR': [u'String', u'char[]', u'PointerByReference', u'WString'],
                          'DWORD': [u'WinDef.DWORD'],
                          'LPOVERLAPPED': [u'WinBase.OVERLAPPED'],
                          'BYTE*': [u'char[]', u'ByteByReference'],
                          #'LPCVOID': [u'Pointer', u'byte[]', u'LPVOID'],
                          'UINT': [u'WinDef.UINT'],
                          'WPARAM': [u'WinDef.WPARAM'],
                          #'PVOID': [u'Pointer', u'byte[]', u'Structure', u'PVOID', u'Buffer']
                        }
    for item in review_typemapping:
        msjna_type_map[item] = review_typemapping[item]
    with open(fpath+'msjna_type_map_review.ls', 'w') as fout:
        json.dump(msjna_type_map, fout, indent=2) #indent: add new line
    
    count = 0
    for api in msjna_type_map:
        if len(msjna_type_map[api]) > 1:
            count += 1
            print api, msjna_type_map[api]
    print 'the number of types which has more than one mapping: ', count
#ReviewTypeMapping()
    
    
# Get unsupported type from MS-JNA implemented API
def GetUnsupportType():
    fpath = path.split(path.dirname(__file__))[0] + '\\DBSource\\DataLv1\\'
    unsup_types    = []
    
    #fin1  = open(fpath+'msjna_type_map.ls', 'r')
    fin1  = open(fpath+'msjna_type_map_review.ls', 'r')
    msjna_type_map = json.load(fin1)

    ms_apis  = collections.OrderedDict()
    fin2  = open(fpath + 'ms_api.json')
    for line in fin2:
        api = json.JSONDecoder(object_pairs_hook=collections.OrderedDict).decode(line)
        ms_apis[api["fname"]] = api
    
    for api in ms_apis:
        for arg in ms_apis[api]['fargs']:
            if not arg['type'] in msjna_type_map:
                unsup_types.append(arg['type'])
    
    unsup_types = sorted(set(unsup_types))
    print 'the number of types which do not appear in type mapping table', len(unsup_types)
    chuoi = ''
    for item in unsup_types: chuoi += item+'\n'
    fout = open(fpath+'msjna_unsup_type.ls', 'w')
    fout.write(chuoi)
#GetUnsupportType()