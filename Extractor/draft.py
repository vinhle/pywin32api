import json, collections
import PBufferClassifier

# Find APIs and buffer pointer for the training data (require mannually verification)
def FindAPIforTrainingData():
    fin1    = open("../DBSource/DataLv1/ms_api.json")
    ms_apis = collections.OrderedDict()
    for line in fin1:
        api = json.JSONDecoder(object_pairs_hook=collections.OrderedDict).decode(line)
        ms_apis[api["fname"]] = api
    
    for api in ms_apis:
        if len(ms_apis[api]['flib']) > 0:
            for arg in  ms_apis[api]['fargs']:
                isPointer, simi_degree = PBufferClassifier.isBufferPointer(arg)
                if isPointer == 2:
                    print ms_apis[api]['fname'], arg['name']
FindAPIforTrainingData()
            