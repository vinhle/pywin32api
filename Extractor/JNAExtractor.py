# -*- coding: utf-8 -*-
'''
@Author : LE VINH
@Project: BE-PUM, API Stub generation
@Time   : JAIST (2015-2016)
@Summary: Class for API specification
'''

from os import path
import re
import InfoAPI
from Crawler import CrawlerUtl
from PySystem import SystemUtl


# Get a list of APIs interface signature which declared in JNA
def ExtractAPISignature(fdll):
    '''
    @param fdll: library filename
    @note: fpath = '../../DBSource/JNA-HTML/'+ fdll
    This code can work well for the relative path from inside this file.
    However, it can not work well if this function is used from other files,
    because the relative path is considered from the called file.
    '''
    signatures = ''
    fpath = path.split(path.dirname(__file__))[0] + '\\DBSource\\JNA-HTML\\'+ fdll
    html  = CrawlerUtl.GetHTMLTree(fpath, 'offline')
    table = html.find("table", {"class":"memberSummary", "summary":"Method Summary table, listing methods, and an explanation"})
    rows  = table.find_all('tr')[1:]
    for row in rows:
        cells = row.find_all("td")
        rtype = cells[0].get_text()      # return type
        fcont = cells[1].code.get_text() # function signature
        fcont = fcont.replace('\n',' ')
        fcont = re.sub(r' +', ' ', fcont)
        fsign = '<'+fdll+'> ' + rtype + ' ' + fcont
        fsign = fsign.encode('ascii', 'replace').replace('?', ' ').replace('(', ' (')
        signatures += fsign + '\n'
    return signatures
#print ExtractAPISignature('User32.html')


# Write all APIs signature declared in JNA to the file 'jna_supported_api.ls'
def WriteAPISignature():
    jna_supported_api = ''
    fpath = path.split(path.dirname(__file__))[0] + '\\DBSource\\DataLv1\\jna_supported_api.ls'
    fin   = open(fpath, 'w')
    fdlls = SystemUtl.GetFiles(path.split(path.dirname(__file__))[0] + '\\DBSource\\JNA-HTML')
    for fdll in fdlls:
        print fdll
        jna_supported_api += ExtractAPISignature(fdll)
    fin.write(jna_supported_api)
#WriteAPISignature()


# Extract basic features of an API from API interface (only for API declared in JNA)
def ExtractAPIBasicFeatures(fsig):
    '''
    @param fsig: API signature
    @return: InfoAPI object
    '''
    flib  = re.match(r'<(\w+)\.html>', fsig).group(1)
    ftype = re.match(r'.+> ([\w|.]+) ', fsig).group(1)
    fname = re.match(r'.+ (.+) \(', fsig).group(1)
    paras = re.match(r'.+\((.+)\)', fsig)
    fargs = []
    if paras != None:
        paras = paras.group(1).replace(', ', ',')
        paras = paras.split(",")
        for para in paras:
            para = para.split()
            fargs.append((para[1], para[0]))
    
    api_obj = InfoAPI.API(ftype, fname, fargs, flib, 'hello')
    #print  api_obj
    api_json = api_obj.MakeBasicJson()
    return api_json
#ExtractAPIBasicFeatures('<Advapi32.html> Winsvc.SC_HANDLE OpenSCManager (String lpMachineName, String lpDatabaseName, int dwDesiredAccess)')


# Write the basic features of all APIs into a json file 'jna_supported_api.json'
def WriteJsonList():
    fpath = path.split(path.dirname(__file__))[0] + '\\DBSource\\DataLv1'
    fin   = open(fpath+'\\jna_supported_api.ls')
    list_api = ''
    for line in fin:
        list_api += ExtractAPIBasicFeatures(line) + '\n'
    fout  = open(fpath+'\\jna_supported_api.json', 'w')
    fout.write(list_api)
#WriteJsonList()